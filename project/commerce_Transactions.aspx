﻿<%@ Page Title="" Language="C#" MasterPageFile="~/commerceMaster.Master" AutoEventWireup="true" CodeBehind="commerce_Transactions.aspx.cs" Inherits="project.commerce_Transactions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="form1">
    <section>
        <div class="col-3">
            <asp:Label ID="Lbl_userID" runat="server" Text="User Name" class="h3"></asp:Label>
            <h3>Transaction History</h3>
        </div>
        <hr />
        <div class="d-flex justify-content-center">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" class="table table-bordered">
                <Columns>
                    <asp:BoundField DataField="date" HeaderText="Date" DataFormatString="{0:M/d/yyyy}"/>
                    <asp:BoundField DataField="transID" HeaderText="Transaction ID" />
                    <asp:BoundField DataField="reference" HeaderText="Reference" />
                    <asp:BoundField DataField="price" HeaderText="Price" />
                    <asp:BoundField DataField="qty" HeaderText="Quantity" />
                    <asp:BoundField DataField="total" HeaderText="Total" />
                    <asp:BoundField DataField="debit" HeaderText="Debit (Withdrawal)" />
                    <asp:BoundField DataField="credit" HeaderText="Credit (Deposit)" />
                    <asp:BoundField DataField="status" HeaderText="Status" />
                </Columns>
            </asp:GridView> 
        </div>
    </section>
</div>
</asp:Content>
