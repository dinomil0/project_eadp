﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class financial_AnalysisDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

        public Dictionary<string, financial_Analysis> read_Financial_Analysis(string userID)
        {
            try
            {
                Dictionary<string, financial_Analysis> financial_Analysis_List = new Dictionary<string, financial_Analysis>();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                SqlDataAdapter da;
                StringBuilder sqlString = new StringBuilder();
                sqlString.AppendLine("SELECT * FROM FINANCIAL_ANALYSIS");
                sqlString.AppendLine("WHERE USERID = @PARAUSERID");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlString.ToString(), sqlConnection);
                da.SelectCommand.Parameters.AddWithValue("@PARAUSERID", userID);
                da.Fill(ds, "financial_AnalysisTable");
                int rec_cnt = ds.Tables["financial_AnalysisTable"].Rows.Count;
                if (rec_cnt == 0)
                {
                    financial_Analysis_List = null;
                }
                else
                {
                    foreach (DataRow row in ds.Tables["financial_AnalysisTable"].Rows)
                    {
                        financial_Analysis each_Financial_Analysis = new financial_Analysis();
                        each_Financial_Analysis.userID = Convert.ToString(row["userID"]);
                        each_Financial_Analysis.name = Convert.ToString(row["name"]);
                        each_Financial_Analysis.update_Detail = Convert.ToDateTime(row["update_Detail"]);
                        each_Financial_Analysis.amount = Convert.ToDecimal(row["amount"]);
                        financial_Analysis_List[Convert.ToString(row["name"])] = each_Financial_Analysis;
                    }
                }
                return financial_Analysis_List;
            }
            catch
            {
                return null;
            }
        }

        public void update_Financial_Analysis(decimal working_Capital, decimal working_Capital_Ratio, string userID)
        {
            try
            {
                StringBuilder sqlString = new StringBuilder();
                SqlCommand sqlCommand = new SqlCommand();
                sqlString.AppendLine("UPDATE FINANCIAL_ANALYSIS");
                sqlString.AppendLine("SET UPDATE_DETAIL = GETDATE(), AMOUNT = @PARAAMOUNT");
                sqlString.AppendLine("WHERE USERID = @PARAUSERID AND NAME = 'Working Capital'");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                sqlCommand = new SqlCommand(sqlString.ToString(), sqlConnection);
                sqlCommand.Parameters.AddWithValue("@PARAAMOUNT", working_Capital);
                sqlCommand.Parameters.AddWithValue("@PARAUSERID", userID);
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();

                StringBuilder sqlString1 = new StringBuilder();
                SqlCommand sqlCommand1 = new SqlCommand();
                sqlString1.AppendLine("UPDATE FINANCIAL_ANALYSIS");
                sqlString1.AppendLine("SET UPDATE_DETAIL = GETDATE(), AMOUNT = @PARAAMOUNT");
                sqlString1.AppendLine("WHERE USERID = @PARAUSERID AND NAME = 'Working Capital Ratio'");
                SqlConnection sqlConnection1 = new SqlConnection(DBConnect);
                sqlCommand1 = new SqlCommand(sqlString1.ToString(), sqlConnection1);
                sqlCommand1.Parameters.AddWithValue("@PARAAMOUNT", working_Capital_Ratio);
                sqlCommand1.Parameters.AddWithValue("@PARAUSERID", userID);
                sqlConnection1.Open();
                sqlCommand1.ExecuteNonQuery();
                sqlConnection1.Close();
            }
            catch
            {
                //Exception
            }
        }
    }
}