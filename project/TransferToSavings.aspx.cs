﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class TransferToSavings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userID = Session["custNRIC"].ToString();
            if (Session["custNRIC"] == null)
            {
                Response.Redirect("landing.aspx");
            }
            if (!IsPostBack)
            {
                AccountsDAO accDAO = new AccountsDAO();
                List<Accounts> bankaccnumList = new List<Accounts>();
                bankaccnumList = accDAO.getbackaccnumbyuserID(userID);

                DdlTransferfrom.Items.Clear();
                DdlTransferfrom.Items.Insert(0, new ListItem("--Select--", "0"));
                DdlTransferfrom.AppendDataBoundItems = true;
                DdlTransferfrom.DataTextField = "bankaccname";
                DdlTransferfrom.DataValueField = "bankaccnum";
                DdlTransferfrom.DataSource = bankaccnumList;
                DdlTransferfrom.DataBind();

                Savings savObj = new Savings();
                SavingsDAO savDAO = new SavingsDAO();
                savObj = savDAO.getTDbyUserSavingID(Convert.ToInt32(Session["SSsavingID"].ToString()));
                if (Session["SSsavingID"] != null)
                {
                    LblTransferSavingsID.Text = Session["SSsavingID"].ToString();
                    LblTransferSavingsGoal.Text = savObj.savingGoal.ToString();
                    LblTransferAmountSaved.Text = savObj.amountsaved.ToString();
                }
            }
        }

        protected void DdlTransferfrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            Accounts accObj = new Accounts();
            AccountsDAO accDAO = new AccountsDAO();

            //Session["userID"] = "S1234567A";
            string userID = Session["custNRIC"].ToString();

            accObj = accDAO.getbalanceBybankaccnum(Convert.ToInt32(DdlTransferfrom.SelectedValue));

            if (accObj.bankaccnum == Convert.ToInt32(DdlTransferfrom.SelectedValue))
            {
                LblTransferBalance.Text = "$" + accObj.balance.ToString();
            }
            else
            {
                Lbl_err.Text += "Account not found in user";
            }
        }

        protected void BtnTransferSavings_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.Now;
                double amount = Convert.ToDouble(TxtAmountinput.Text);
                string userID = Session["custNRIC"].ToString();
                TransactionHistoryDAO newdata = new TransactionHistoryDAO();
                int frombankaccnum = Convert.ToInt32(DdlTransferfrom.SelectedValue);
                int insCnt = newdata.InsertTransactionsNormal(date, "Savings", "Withdrawal", amount, "Successful", userID, frombankaccnum, frombankaccnum);
                if (insCnt == 1)
                {
                    Accounts accsObj = new Accounts();
                    AccountsDAO accsDAO = new AccountsDAO();

                    accsObj = accsDAO.getbankaccbalance(frombankaccnum);
                    accsObj.balance = accsObj.balance - amount;

                    AccountsDAO updateDAOwithdrawal = new AccountsDAO();
                    updateDAOwithdrawal.updatebankaccbalance(frombankaccnum, accsObj.balance);

                    Savings savObj = new Savings();
                    SavingsDAO savDAO = new SavingsDAO();

                    savObj = savDAO.getTDbyUserIDobj(userID, Convert.ToInt32(LblTransferSavingsID.Text));
                    savObj.amountsaved += amount;

                    SavingsDAO updateDAOsavingsBal = new SavingsDAO();
                    updateDAOsavingsBal.UpdateTDforsavingbalance(Convert.ToInt32(LblTransferSavingsID.Text), savObj.amountsaved);
                }
                else
                {
                    Lbl_error.Text += "Customer class not found";
                }
                Response.Redirect("TransferToSavings.aspx");
            }
            catch (Exception f)
            {
                Lbl_error.Text = f.ToString();
            }
        }
    }
}