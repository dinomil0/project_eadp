﻿using System;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Text;
using System.Collections.Generic;

namespace project.DAL
{
    public class customerDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        public int read_Customer_Age(string userID)
        {
            int customer_Age = 0;

            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                SqlDataAdapter da;
                StringBuilder sqlString = new StringBuilder();
                sqlString.AppendLine("SELECT * FROM CUSTOMER");
                sqlString.AppendLine("WHERE USERID = @PARAUSERID");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlString.ToString(), sqlConnection);
                da.SelectCommand.Parameters.AddWithValue("PARAUSERID", userID);
                da.Fill(ds, "customerTable");
                int rec_cnt = ds.Tables["customerTable"].Rows.Count;
                if (rec_cnt == 0)
                {
                    return customer_Age;
                }
                else
                {
                    foreach (DataRow row in ds.Tables["customerTable"].Rows)
                    {
                        int current_Year = Convert.ToInt32(DateTime.Today.Year);
                        customer_Age = current_Year - Convert.ToInt32(((DateTime)row["DOB"]).ToString("yyyy"));
                    }
                }
                return customer_Age;
            }
            catch
            {
                return customer_Age;
            }
        }
        //public customer getCustomerById(string custId)
        //{
        //    //Get connection string from web.config
        //    string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

        //    SqlDataAdapter da;
        //    DataSet ds = new DataSet();

        //    //Create Adapter
        //    //WRITE SQL Statement to retrieve all columns from Customer by customer Id using query parameter
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.AppendLine("Select * from Customer where");
        //    sqlCommand.AppendLine("custId = @paraCustId");
        //    //***TO TUTOR : Simulate system error  *****
        //    // change custId in where clause to custId1 or 
        //    // change connection string in web config to a wrong file name  

        //    customer obj = new customer();   // create a customer instance

        //    SqlConnection myConn = new SqlConnection(DBConnect);
        //    da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
        //    da.SelectCommand.Parameters.AddWithValue("paraCustId", custId);
        //    // fill dataset
        //    da.Fill(ds, "custTable");
        //    int rec_cnt = ds.Tables["custTable"].Rows.Count;
        //    if (rec_cnt > 0)
        //    {
        //        DataRow row = ds.Tables["custTable"].Rows[0];  // Sql command returns only one record
        //        obj.customerNRIC = row["custId"].ToString();
        //        obj.customerName = row["custName"].ToString();
        //        obj.customerPhone = Convert.ToInt32(row["custMobile"]);
        //        obj.customerGender = row["custGender"].ToString();
        //        obj.customerDOB = Convert.ToDateTime(row["custDOB"].ToString());
        //        obj.customerPassword = row["custPassword"].ToString();
        //        obj.customerAddr = row["custAddress"].ToString() + "Singapore " + row["custPostal"].ToString();
        //        obj.customerEmail = row["custEmail"].ToString();
        //        obj.customerCS = row["creditScore"].ToString();
        //    }
        //    else
        //    {
        //        obj = null;
        //    }

        //    return obj;
        //}

        public customer GetCustomerLoginCredential(string username)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("Select * from Customer where");
            sqlCommand.AppendLine("userID = @custNRIC OR phone = @custPhone");

            customer obj = new customer();

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("custNRIC", username);
            if (!int.TryParse(username, out int intUsername))
            {
                da.SelectCommand.Parameters.AddWithValue("custPhone", intUsername);
            }
                

            da.Fill(ds, "Customer");

            int rec_cnt = ds.Tables["Customer"].Rows.Count;
            if (rec_cnt > 0)
            {
                DataRow row = ds.Tables["Customer"].Rows[0];  // Sql command returns only one record
                obj.userID = row["userID"].ToString();
                obj.name = row["name"].ToString();
                obj.phonenum = Convert.ToInt32(row["phone"]);
                obj.customerGender = row["gender"].ToString();
                obj.customerDOB = Convert.ToDateTime(row["DOB"]);
                obj.customerPassword = row["password"].ToString();
                obj.customerAddr = row["mailingAdd"].ToString();
                obj.customerEmail = row["emailAdd"].ToString();
                obj.customerCS = row["creditScore"].ToString();
                obj.customerCC = row["cc"].ToString();
            }
            else
            {
                obj = null;
            }
            return obj;
        }

        public customer getUserByID()
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Customer");
            //sqlCommand.AppendLine("userID = @paraUserId");

            customer obj = new customer();

            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            //da.SelectCommand.Parameters.AddWithValue("paraID", "10001");

            da.Fill(ds, "CustomerTable");
            int rec_cnt = ds.Tables["CustomerTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["CustomerTable"].Rows)
                {
                    obj.userID = row["userID"].ToString();
                    obj.name = row["name"].ToString();
                    obj.cc = row["cc"].ToString();
                    obj.phonenum = Convert.ToInt32(row["phone"]);
                }

            }
            else
            {
                obj = null;
            }

            return obj;
        }
        public customer getNamebyphonenum(int phone)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Customer");
            sqlCommand.AppendLine("WHERE phone = @paraphone");

            customer obj = new customer();

            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("paraphone", phone);

            da.Fill(ds, "CustomerTable");
            int rec_cnt = ds.Tables["CustomerTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                DataRow row = ds.Tables["CustomerTable"].Rows[0];
                obj.userID = row["userID"].ToString();
                obj.name = row["name"].ToString();
                obj.cc = row["cc"].ToString();
                obj.phonenum = Convert.ToInt32(row["phone"]);
            }
            else
            {
                obj = null;
            }

            return obj;
        }
        public customer getUserIDbyphonenum(int phone)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Customer");
            sqlCommand.AppendLine("WHERE phone = @paraphone");

            customer obj = new customer();

            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("paraphone", phone);

            da.Fill(ds, "CustomerTable");
            int rec_cnt = ds.Tables["CustomerTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                DataRow row = ds.Tables["CustomerTable"].Rows[0];
                obj.userID = row["userID"].ToString();
                obj.name = row["name"].ToString();
                obj.cc = row["cc"].ToString();
                obj.phonenum = Convert.ToInt32(row["phone"]);


            }
            else
            {
                obj = null;
            }

            return obj;
        }
        public customer getNamebynric(string userID)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Customer");
            sqlCommand.AppendLine("WHERE userID = @parauserID");

            customer obj = new customer();

            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            // fill dataset

            da.Fill(ds, "CustomerTable");
            int rec_cnt = ds.Tables["CustomerTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                DataRow row = ds.Tables["CustomerTable"].Rows[0];
                obj.userID = row["userID"].ToString();
                obj.name = row["name"].ToString();
                obj.cc = row["cc"].ToString();
                obj.phonenum = Convert.ToInt32(row["phone"]);


            }
            else
            {
                obj = null;
            }

            return obj;
        }
        public customer getphonenumbybankaccnum(int bankaccnum)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Customer");
            sqlCommand.AppendLine("WHERE bankaccnum = @parabankaccnum");

            //sqlCommand.AppendLine("SELECT * FROM customer");
            //sqlCommand.AppendLine("INNER JOIN Accounts ON Accounts.userID = customer.userID");
            //sqlCommand.AppendLine("WHERE Accounts.bankaccnum = @parabankaccnum");

            customer obj = new customer();

            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parabankaccnum", bankaccnum);
            // fill dataset

            da.Fill(ds, "CustomerTable");
            int rec_cnt = ds.Tables["CustomerTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                DataRow row = ds.Tables["CustomerTable"].Rows[0];
                obj.userID = row["userID"].ToString();
                obj.name = row["name"].ToString();
                obj.cc = row["cc"].ToString();
                obj.phonenum = Convert.ToInt32(row["phone"]);


            }
            else
            {
                obj = null;
            }

            return obj;
        }
        public customer getphonenumbyuserID(string userID)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Customer");
            sqlCommand.AppendLine("WHERE userID = @parauserID");

            customer obj = new customer();

            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            // fill dataset

            da.Fill(ds, "CustomerTable");
            int rec_cnt = ds.Tables["CustomerTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                DataRow row = ds.Tables["CustomerTable"].Rows[0];
                obj.userID = row["userID"].ToString();
                obj.name = row["name"].ToString();
                obj.cc = row["cc"].ToString();
                obj.phonenum = Convert.ToInt32(row["phone"]);


            }
            else
            {
                obj = null;
            }
            return obj;
        }
        


    }
}