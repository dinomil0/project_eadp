﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Text;

namespace project.DAL
{
    public class AccountsDAO
    {
        //New Version
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        public List<int> read_Account(string userID)
        {
            List<int> accountList = new List<int>();

            SqlDataAdapter da;
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM ACCOUNTS WHERE");
            sqlCommand.AppendLine("USERID = @PARAUSERID");
            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("PARAUSERID", userID);
            da.Fill(ds, "accountTable");
            int rec_cnt = ds.Tables["accountTable"].Rows.Count;
            if (rec_cnt == 0)
            {
                //Exception
            }
            else
            {
                foreach (DataRow row in ds.Tables["accountTable"].Rows)
                {
                    accountList.Add(Convert.ToInt32(row["bankaccnum"]));
                }
            }
            return accountList;
        }
        public List<Accounts> getAccdetailsuserID(string userID)
        {
            SqlDataAdapter da;
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();

            List<Accounts> accountList = new List<Accounts>();

            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * From Accounts where");
            sqlCommand.AppendLine("userID = @parauserID");
 
            SqlConnection myConn = new SqlConnection(DBConnect);

            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

            da.Fill(ds, "accountdetailsTable");
            int rec_cnt = ds.Tables["accountdetailsTable"].Rows.Count;
            if (rec_cnt == 0)
            {
                //accountList = null;
            }
            else
            {
                // Step 7 : Iterate DataRow to extract table * columns
                //          Create budget instance and add the instance to a List collection  
                foreach (DataRow row in ds.Tables["accountdetailsTable"].Rows)
                {
                    Accounts objdetails = new Accounts();
                    objdetails.cardNum = Convert.ToInt64(row["cardNum"]);
                    objdetails.bankaccname = row["bankaccname"].ToString();
                    objdetails.balance = Convert.ToDouble(row["balance"]);
                    objdetails.userID = Convert.ToString(row["userID"]);
                    accountList.Add(objdetails);
                }
            }
            return accountList;
        }
        public List<Accounts> getbackaccnumbyuserID(string userID)
        {
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            List<Accounts> bankaccnumList = new List<Accounts>();

            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * From Accounts ");
            sqlCommand.AppendLine("where userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);

            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            da.Fill(ds, "bankaccnumTable");

            int rec_cnt = ds.Tables["bankaccnumTable"].Rows.Count;
            if (rec_cnt == 0)
            {
                bankaccnumList = null;
            }
            else
            {
                foreach (DataRow row in ds.Tables["bankaccnumTable"].Rows)
                {
                    Accounts obj = new Accounts();
                    obj.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
                    obj.bankaccname = row["bankaccname"].ToString();
                    obj.balance = Convert.ToDouble(row["balance"]);
                    obj.amountowed = Convert.ToDouble(row["amountowed"]);
                    obj.bankname = row["bankname"].ToString();
                    bankaccnumList.Add(obj);
                }
            }
            return bankaccnumList;
        }

        public List<int> getbackaccnumbyuserIDint(string userID)
        {
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            List<int> bankaccnumList = new List<int>();

            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * From Accounts ");
            sqlCommand.AppendLine("where userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);

            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            da.Fill(ds, "bankaccnumTable");

            int rec_cnt = ds.Tables["bankaccnumTable"].Rows.Count;
            if (rec_cnt == 0)
            {
                bankaccnumList = null;
            }
            else
            {
                foreach (DataRow row in ds.Tables["bankaccnumTable"].Rows)
                {
                    Accounts obj = new Accounts();
                    obj.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
                    bankaccnumList.Add(obj.bankaccnum);
                }
            }
            return bankaccnumList;
        }

        public List<string> getbackaccnamebyuserIDstring(string userID)
        {
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            List<string> bankaccnumList = new List<string>();

            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * From Accounts ");
            sqlCommand.AppendLine("where userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);

            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            da.Fill(ds, "bankaccnameTable");

            int rec_cnt = ds.Tables["bankaccnameTable"].Rows.Count;
            if (rec_cnt == 0)
            {
                bankaccnumList = null;
            }
            else
            {
                foreach (DataRow row in ds.Tables["bankaccnameTable"].Rows)
                {
                    Accounts obj = new Accounts();
                    obj.bankaccname = row["bankaccname"].ToString();
                    bankaccnumList.Add(obj.bankaccname);
                }
            }
            return bankaccnumList;
        }

        public List<double> getTotalbalancefromALLbankacc(string userID)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();

            List<double> bankaccTotal = new List<double>();

            double totalbalance = 0;
            double totalowed = 0;
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT balance, amountowed FROM Accounts");
            sqlCommand.AppendLine("where userID = @parauserID");

            Accounts obj = new Accounts();
            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            // fill dataset
            da.Fill(ds, "TotalTable");
            int rec_cnt = ds.Tables["TotalTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["TotalTable"].Rows)
                {
                    obj.balance = Convert.ToDouble(row["balance"]);
                    obj.amountowed = Convert.ToDouble(row["amountowed"]);
                    totalbalance += obj.balance;
                    totalowed += obj.amountowed;

                }
                bankaccTotal.Add(totalbalance);
                bankaccTotal.Add(totalowed);
            }
            else
            {
                bankaccTotal = null;
            }

            return bankaccTotal;
        }

        public Accounts getbalanceBybankaccnum(int bankaccnum)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Accounts");
            sqlCommand.AppendLine("where bankaccnum = @parabankaccnum");

            Accounts obj = new Accounts();

            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parabankaccnum", bankaccnum);
            // fill dataset
            da.Fill(ds, "BankAccountBalanceTable");
            int rec_cnt = ds.Tables["BankAccountBalanceTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["BankAccountBalanceTable"].Rows)
                {
                    obj.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
                    obj.userID = row["userID"].ToString();
                    obj.balance = Convert.ToDouble(row["balance"]);
                    obj.bankaccname = row["bankaccname"].ToString();

                    //bankname, merchant, balance
                }

            }
            else
            {
                obj = null;
            }

            return obj;
        }

        public Accounts getbankaccs(string userID)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Accounts");
            sqlCommand.AppendLine("where userID = @parauserID");

            Accounts obj = new Accounts();

            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            // fill dataset
            da.Fill(ds, "BankAccountTable");
            int rec_cnt = ds.Tables["BankAccountTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["BankAccountTable"].Rows)
                {
                    obj.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
                    //bankname, merchant, balance
                }

            }
            else
            {
                obj = null;
            }

            return obj;
        }

        public Accounts getbankaccbalance(int bankaccnum)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Accounts");
            sqlCommand.AppendLine("where bankaccnum = @parabankaccnum");

            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("@parabankaccnum", bankaccnum);

            Accounts obj = new Accounts();

            //da.SelectCommand.Parameters.AddWithValue("@parabalance", balance);
            // fill dataset
            da.Fill(ds, "BalanceRemainingTable");
            int rec_cnt = ds.Tables["BalanceRemainingTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["BalanceRemainingTable"].Rows)
                {
                    obj.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
                    obj.userID = row["userID"].ToString();
                    obj.balance = Convert.ToDouble(row["balance"]);
                    obj.bankaccname = row["bankaccname"].ToString();

                    //bankname, merchant, balance
                }

            }
            else
            {
                obj = null;
            }

            return obj;
        }

        public void updatebankaccbalance(int bankaccnum, double balance)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("UPDATE Accounts SET balance = @parabalance");
            sqlCommand.AppendLine("where bankaccnum = @parabankaccnum");

            Accounts obj = new Accounts();   // create a Accounts instance

            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("@parabankaccnum", bankaccnum);
            da.SelectCommand.Parameters.AddWithValue("@parabalance", balance);
            //fill dataset
            da.Fill(ds, "Accounts");

        }

        public void createBankACC(string custNRIC, string bankName, double balance)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            StringBuilder sqlStr = new StringBuilder();
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("INSERT INTO Accounts (bankaccnum, userID, bankname, balance, bankaccname)");
            sqlStr.AppendLine("VALUES (@bankaccnum,@userID,@bankname,@balance,@bankaccname)");

            Accounts obj = new Accounts();

            Random rand = new Random();
            var bankaccnum = 0;
            var bankaccname ="";
            if(bankName == "DBS")
            {
                bankaccnum = rand.Next(100000000, 999999999);
                bankaccname = "DBS Loan Account";
            }
            else if (bankName == "OCBC")
            {
                bankaccnum = rand.Next(1000000, 9999999);
                bankaccname = "OCBC Loan Account";
            }
            else
            {
                bankaccnum = rand.Next(100000000, 999999999);
                bankaccname = "UOB Loan Account";
            }

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);
            sqlCmd.Parameters.AddWithValue("@bankaccnum", bankaccnum);
            sqlCmd.Parameters.AddWithValue("@userID", custNRIC);
            sqlCmd.Parameters.AddWithValue("@bankname", bankName);
            sqlCmd.Parameters.AddWithValue("@balance", balance);
            sqlCmd.Parameters.AddWithValue("@bankaccname", bankaccname);

            myConn.Open();
            sqlCmd.ExecuteNonQuery();
            myConn.Close();
        }
    }
}
