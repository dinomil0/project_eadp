﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class commerceLanding : System.Web.UI.Page
    {
        string userID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            userID = Session["custNRIC"].ToString();
            Class_ProductDAO prodDAO = new Class_ProductDAO();
            List<Class_Product> products = prodDAO.get_products();
            int count = products.Count();
            DataTable table = new DataTable();
            table.Columns.Add("ProdImage");
            table.Columns.Add("ProdName");
            table.Columns.Add("ProdId");
            table.Columns.Add("ProdPrice");
            for (int i = 0; i < count; i++)
            {
                Class_Product product = new Class_Product();
                product = products[i];
                if (prodDAO.getSellerID(product.ProdId) != userID)
                {
                    if (product.ProdQty > 0)
                    {
                        //Get area code from session
                        //Get currency details from area code
                        //Convert Product Price to local currency
                        string strBase64 = Convert.ToBase64String(product.ProdImage);
                        string url = "data:img/png;base64," + strBase64;
                        table.Rows.Add(url, product.ProdName, product.ProdId, product.ProdPrice);
                    }
                }
            }
            ProdList.DataSource = table;
            ProdList.DataBind();
        }
        protected void Buy_Click(object sender, CommandEventArgs e)
        {
            string[] arg = new string[3];
            arg = e.CommandArgument.ToString().Split(';');
            Response.Redirect("commerce_BuyPage.aspx?ProdName=" + arg[0] + "&ProdId=" + arg[1] + "&price" + arg[2]);
        }

        protected void Search_TextChanged(object sender, EventArgs e)
        {

        }
    }
}