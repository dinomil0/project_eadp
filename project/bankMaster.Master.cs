﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class bankMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }
                if (!string.IsNullOrEmpty(Session["custNRIC"].ToString())) { this.FindControl("pnlLogout").Visible = true; }
                if (Session["custNRIC"].ToString().Substring(0, 1) == "A") { this.FindControl("pnlAdmin").Visible = true; } else { this.FindControl("pnlCust").Visible = true; }
                string userID = Session["custNRIC"].ToString();
                NotificationsDAO notiDAO = new NotificationsDAO();
                List<string> notiList = new List<string>();
                notiList = notiDAO.getAllNotificationsbyuserID(userID);
                DataTable table = new DataTable();
                table.Columns.Add("details");

                Savings savObj = new Savings();
                SavingsDAO savDAO = new SavingsDAO();

                savObj = savDAO.getTDbyUserID(userID);
                foreach (string i in notiList)
                {
                    table.Rows.Add(i);
                }
                (this.FindControl("Notifications") as DataList).DataSource = table;
                (this.FindControl("Notifications") as DataList).DataBind();
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("landing.aspx");
        }
    }
}