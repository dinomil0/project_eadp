﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="PaymentQRCodeLanding.aspx.cs" Inherits="project.PaymentQRCodeLanding" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="accountsnmoney" class="border border-secondary bg-light shadow p-3 mb-5 rounded">       
        
        <asp:Button runat="server" style=" border-right:solid 1px" Text="Generate QR Code" />
        <i class="fas fa-qrcode"></i>

        <asp:Button runat="server" Text="Scan QR Code" />
        <i class="material-icons"></i>
     
    </section>
</asp:Content>
