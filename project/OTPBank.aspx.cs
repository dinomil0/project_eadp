﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TextmagicRest;
using project.DAL;

namespace project
{
    public partial class WebForm6 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }
            if (!Page.IsPostBack)
            {
                var userID = Session["custNRIC"].ToString();

                customer customer = new customer();
                customerDAO customerDAO = new customerDAO();
                customer = customerDAO.GetCustomerLoginCredential(userID);

                var number = customer.customerCC + customer.phonenum.ToString();

                Random rand = new Random();
                var otp = rand.Next(100000, 999999);

                OTPBank otpObj = new OTPBank();
                OTPBankDAO otpDAO = new OTPBankDAO();

                DateTime now = DateTime.Now;
                DateTime nowadd15 = now.AddMinutes(15);

                otpDAO.InsertOTP(otp, now, nowadd15, number, userID);
                sendMessage("Your OTP is " + otp + ". It expires in 15 mins.", number);

                otpObj = otpDAO.getOTP(userID);

                if (now >= otpObj.dateadd15)
                {
                    otpDAO.DeleteOTP(userID);
                }
            }
           
        }
        protected void OTPgenerator_Click(object sender, EventArgs e)
        {
            var userID = Session["custNRIC"].ToString();
            OTPBank otpObj = new OTPBank();
            OTPBankDAO otpDAO = new OTPBankDAO();

            otpObj = otpDAO.getOTP(userID);
            if (otpObj != null)
            {
                customer customer = new customer();
                customerDAO customerDAO = new customerDAO();
                customer = customerDAO.GetCustomerLoginCredential(userID);

                var number = customer.cc + customer.phonenum;
                DateTime now = DateTime.Now;
                DateTime nowadd15 = now.AddMinutes(15);

                Random rand = new Random();
                var otp = rand.Next(100000, 999999);

                otpDAO.DeleteOTP(userID);
                otpDAO.InsertOTP(otp, now, nowadd15, number, userID);
                sendMessage("Your OTP is " + otp + ". It expires in 15 mins.", number);
            }
        }

        public static void sendMessage(string msg, string number)
        {
            var client = new Client("limzhengjie", "l0KkWkR1KECotdNGZNGFzgrJums4Q6");
            var link = client.SendMessage(msg, number);
        }

        protected void OTPsubmit_Click(object sender, EventArgs e)
        {
            var userID = Session["custNRIC"].ToString();
            OTPBank otpObj = new OTPBank();
            OTPBankDAO otpDAO = new OTPBankDAO();

            otpObj = otpDAO.getOTP(userID);
            if (Convert.ToInt32(tbotp.Text) == otpObj.otp)
            {
                Response.Redirect("bankHome.aspx");
            }
        }
    }
}