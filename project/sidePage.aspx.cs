﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class sidePage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userID = Session["custNRIC"].ToString();
            if (Session["custNRIC"] == null)
            {
                Response.Redirect("landing.aspx");
            }
            if (Session["custNRIC"].ToString().Substring(0, 1) == "A") { Response.Redirect("adminLoan.aspx"); }
            if (Convert.ToBoolean(Session["loginStatus"]))
            {
                Panel pnlLogout = this.Master.FindControl("pnlLogout") as Panel;
                pnlLogout.Visible = true;
            }
        }
    }
}