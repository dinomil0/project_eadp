﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class commerce_Listing : System.Web.UI.Page
    {
        string userID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            userID = Session["custNRIC"].ToString();
            //Total up the price of all products
            Lbl_userID.Text = userID;
            Class_ProductDAO prodDAO = new Class_ProductDAO();
            List<Class_Product> products = prodDAO.get_listing(userID);
            int count = products.Count();
            DataTable table = new DataTable();
            table.Columns.Add("ProdImage");
            table.Columns.Add("ProdName");
            table.Columns.Add("ProdId");
            table.Columns.Add("ProdPrice");
            table.Columns.Add("ProdQty");
            for (int i = 0; i < count; i++)
            {
                Class_Product product = new Class_Product();
                product = products[i];
                string strBase64 = Convert.ToBase64String(product.ProdImage);
                string url = "data:img/png;base64," + strBase64;
                table.Rows.Add(url, product.ProdName, product.ProdId, product.ProdPrice, product.ProdQty);
            }
            ProdList.DataSource = table;
            ProdList.DataBind();
        }
        protected void btn_Cancel_Click(object sender, CommandEventArgs e)
        {
            string ProdId = e.CommandArgument.ToString();
            Class_ProductDAO prodDAO = new Class_ProductDAO();
            prodDAO.Less_Product(ProdId);
            Response.Redirect("commerce_Listing.aspx");
        }
    }
}