﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class OTPBank
    {
        public OTPBank()
        {

        }
        public int otpID { get; set; }
        public int otp { get; set; }
        public DateTime date { get; set; }
        public DateTime dateadd15 { get; set; }
        public string phonenum { get; set; }
        public string userID { get; set; }
    }
}