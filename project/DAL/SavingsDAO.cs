﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class SavingsDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        public List<Savings> getTDbyuserID(string userID)
        {
            List<Savings> SavingsList = new List<Savings>();
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();
            //
            // Step 3 :Create SQLcommand to select all columns from TDMaster by parameterised customer id
            //          where TD is not matured yet

            StringBuilder sqlStr = new StringBuilder();
            sqlStr.AppendLine("SELECT * From Savings where userID = @parauserID");
            //sqlStr.AppendLine("where userID = @paraCustId");

            SqlConnection myConn = new SqlConnection(DBConnect);
            SqlDataAdapter da = new SqlDataAdapter(sqlStr.ToString(), myConn);

            // Step 5 :add value to parameter 

            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

            // Step 6: fill dataset
            da.Fill(ds, "Savings");

            // Step 7: Iterate the rows from TableTD above to create a collection of TD
            //         for this particular customer 

            int rec_cnt = ds.Tables["Savings"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["Savings"].Rows)
                {
                    Savings obj = new Savings();   // create a Transaction History instance
                    obj.savingsID = Convert.ToInt32(row["savingsID"]);
                    obj.savingGoal = Convert.ToDouble(row["savingGoal"]);
                    obj.amountsaved = Convert.ToDouble(row["amountsaved"]);
                    obj.description = row["description"].ToString();
                    obj.startDate = Convert.ToDateTime(row["startDate"]);
                    obj.endDate = Convert.ToDateTime(row["endDate"]);
                    obj.userID = row["userID"].ToString();
                    obj.bankaccnum = Convert.ToInt32(row["bankaccnum"].ToString());
                    obj.status = Convert.ToInt32(row["status"]);

                    SavingsList.Add(obj);
                }
            }
            else
            {
                SavingsList = null;
            }

            return SavingsList;

        }
        public Dictionary<string, double> getGaugechartdata(string userID)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();

            Dictionary<string, double> savingsdict = new Dictionary<string, double>();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Savings");
            sqlCommand.AppendLine("where userID = @parauserID");

            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            // fill dataset
            da.Fill(ds, "GaugeGraphTable");
            int rec_cnt = ds.Tables["GaugeGraphTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["GaugeGraphTable"].Rows)
                {
                    Savings obj = new Savings();
                    obj.savingsID = Convert.ToInt32(row["savingsID"]);
                    obj.savingGoal = Convert.ToDouble(row["savingGoal"]);
                    obj.amountsaved = (Convert.ToDouble(row["amountsaved"]) / Convert.ToDouble(row["savingGoal"]) * 100);
                    obj.description = row["description"].ToString();
                    obj.startDate = Convert.ToDateTime(row["startDate"]);
                    obj.endDate = Convert.ToDateTime(row["endDate"]);
                    obj.userID = row["userID"].ToString();
                    obj.bankaccnum = Convert.ToInt32(row["bankaccnum"].ToString());

                    savingsdict[obj.savingsID.ToString()] = obj.amountsaved;
                }

            }
            else
            {
                savingsdict = null;
            }

            return savingsdict;
        }

        public int InsertTD(double savingGoal, double amountSaved, string description, DateTime endDate, DateTime startDate, string userID, int status, int bankaccnum)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;    // Execute NonQuery return an integer value
            SqlCommand sqlCmd = new SqlCommand();
            // Step1 : Create SQL insert command to add record to TDMaster using     

            //         parameterised query in values clause
            //
            sqlStr.AppendLine("INSERT INTO Savings (savingGoal, amountSaved, description, endDate,");
            sqlStr.AppendLine("startDate,userID, status, bankaccnum)");
            sqlStr.AppendLine("VALUES (@parasavingGoal,@paraamountSaved, @paradescription, @paraendDate,");
            sqlStr.AppendLine("GETDATE(),@parauserID,@parastatus,@parabankaccnum)");


            // Step 2 :Instantiate SqlConnection instance and SqlCommand instance

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            // Step 3 : Add each parameterised query variable with value
            //          complete to add all parameterised queries
            sqlCmd.Parameters.AddWithValue("@parasavingGoal", savingGoal);
            sqlCmd.Parameters.AddWithValue("@paraamountSaved", amountSaved);
            sqlCmd.Parameters.AddWithValue("@paradescription", description);
            sqlCmd.Parameters.AddWithValue("@paraendDate", endDate);
            sqlCmd.Parameters.AddWithValue("@parastartDate", startDate);
            sqlCmd.Parameters.AddWithValue("@parauserID", userID);
            sqlCmd.Parameters.AddWithValue("@parastatus", status);
            sqlCmd.Parameters.AddWithValue("@parabankaccnum", bankaccnum);

            // Step 4 Open connection the execute NonQuery of sql command   

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            // Step 5 :Close connection
            myConn.Close();

            return result;

        }
        public Savings getTDbyUserSavingID(int savingsID)
        {
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();

            StringBuilder sqlStr = new StringBuilder();
            sqlStr.AppendLine("SELECT * From Savings where savingsID = @parasavingsID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            SqlDataAdapter da = new SqlDataAdapter(sqlStr.ToString(), myConn);

            da.SelectCommand.Parameters.AddWithValue("parasavingsID", savingsID);

            da.Fill(ds, "Savings");

            Savings obj = new Savings();
            int rec_cnt = ds.Tables["Savings"].Rows.Count;
            if (rec_cnt > 0)
            {
                DataRow row = ds.Tables["Savings"].Rows[0];
                obj.savingsID = Convert.ToInt32(row["savingsID"].ToString());
                obj.savingGoal = Convert.ToDouble(row["savingGoal"]);
                obj.amountsaved = Convert.ToDouble(row["amountsaved"]);
                obj.description = row["description"].ToString();
                obj.startDate = Convert.ToDateTime(row["startDate"]);
                obj.endDate = Convert.ToDateTime(row["endDate"]);
                obj.userID = row["userID"].ToString();
                obj.bankaccnum = Convert.ToInt32(row["bankaccnum"].ToString());
                obj.status = Convert.ToInt32(row["status"]);

            }
            else
            {
                obj = null;
            }

            return obj;
        }
        public int UpdateTDforsavingGoal(int savingsID, double savingGoal)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;    // Execute NonQuery return an integer value
            SqlCommand sqlCmd = new SqlCommand();
            // Step1 : Create SQL update command to change column of tdRenewMode for the
            //        selected record in TDMaster using     

            sqlStr.AppendLine("UPDATE Savings SET savingGoal = @parasavingGoal");
            sqlStr.AppendLine("WHERE savingsID = @parasavingsID");

            // Step 2 :Instantiate SqlConnection instance and SqlCommand instance

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            // Step 3 : Add each parameterised query variable with value
            //          complete to add all parameterised queries
            sqlCmd.Parameters.AddWithValue("@parasavingGoal", savingGoal);
            sqlCmd.Parameters.AddWithValue("@parasavingsID", savingsID);

            // Step 4 Open connection the execute NonQuery of sql command   

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            // Step 5 :Close connection
            myConn.Close();

            return result;

        }
        public int UpdateTDforendDate(int savingsID, DateTime endDate)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;    // Execute NonQuery return an integer value
            SqlCommand sqlCmd = new SqlCommand();
            // Step1 : Create SQL update command to change column of tdRenewMode for the
            //        selected record in TDMaster using     

            sqlStr.AppendLine("UPDATE Savings SET endDate = @paraendDate");
            sqlStr.AppendLine("WHERE savingsID = @parasavingsID");

            // Step 2 :Instantiate SqlConnection instance and SqlCommand instance

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            // Step 3 : Add each parameterised query variable with value
            //          complete to add all parameterised queries
            sqlCmd.Parameters.AddWithValue("@paraendDate", endDate);
            sqlCmd.Parameters.AddWithValue("@parasavingsID", savingsID);

            // Step 4 Open connection the execute NonQuery of sql command   

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            // Step 5 :Close connection
            myConn.Close();

            return result;

        }
        public int DeleteTDbysavingsID(int savingsID)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;    // Execute NonQuery return an integer value
            SqlCommand sqlCmd = new SqlCommand();
            // Step1 : Create SQL update command to change column of tdRenewMode for the
            //        selected record in TDMaster using     

            sqlStr.AppendLine("DELETE FROM Savings");
            sqlStr.AppendLine("WHERE savingsID = @parasavingsID");

            // Step 2 :Instantiate SqlConnection instance and SqlCommand instance

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            // Step 3 : Add each parameterised query variable with value
            //          complete to add all parameterised queries
            sqlCmd.Parameters.AddWithValue("@parasavingsID", savingsID);

            // Step 4 Open connection the execute NonQuery of sql command   

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            // Step 5 :Close connection
            myConn.Close();

            return result;

        }
        public Savings getTDbyUserIDobj(string userID, int savingsID)
        {
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();
            //
            // Step 3 :Create SQLcommand to select all columns from TDMaster by parameterised customer id
            //          where TD is not matured yet

            StringBuilder sqlStr = new StringBuilder();
            sqlStr.AppendLine("SELECT * From Savings where userID = @parauserID and savingsID = @parasavingsID");
            //sqlStr.AppendLine("where userID = @paraCustId");

            SqlConnection myConn = new SqlConnection(DBConnect);
            SqlDataAdapter da = new SqlDataAdapter(sqlStr.ToString(), myConn);

            // Step 5 :add value to parameter 

            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            da.SelectCommand.Parameters.AddWithValue("parasavingsID", savingsID);

            // Step 6: fill dataset
            da.Fill(ds, "SavingsBalance");

            // Step 7: Iterate the rows from TableTD above to create a collection of TD
            //         for this particular customer 

            Savings obj = new Savings();
            int rec_cnt = ds.Tables["SavingsBalance"].Rows.Count;
            if (rec_cnt > 0)
            {
                DataRow row = ds.Tables["SavingsBalance"].Rows[0];
                obj.savingsID = Convert.ToInt32(row["savingsID"].ToString());
                obj.savingGoal = Convert.ToDouble(row["savingGoal"]);
                obj.amountsaved = Convert.ToDouble(row["amountsaved"]);
                obj.description = row["description"].ToString();
                obj.startDate = Convert.ToDateTime(row["startDate"]);
                obj.endDate = Convert.ToDateTime(row["endDate"]);
                obj.userID = row["userID"].ToString();
                obj.bankaccnum = Convert.ToInt32(row["bankaccnum"].ToString());
                obj.status = Convert.ToInt32(row["status"]);
            }
            else
            {
                obj = null;
            }

            return obj;
        }
        public Savings getTDbyUserID(string userID)
        {
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();
            //
            // Step 3 :Create SQLcommand to select all columns from TDMaster by parameterised customer id
            //          where TD is not matured yet

            StringBuilder sqlStr = new StringBuilder();
            sqlStr.AppendLine("SELECT * From Savings where userID = @parauserID");
            //sqlStr.AppendLine("where userID = @paraCustId");

            SqlConnection myConn = new SqlConnection(DBConnect);
            SqlDataAdapter da = new SqlDataAdapter(sqlStr.ToString(), myConn);

            // Step 5 :add value to parameter 

            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

            // Step 6: fill dataset
            da.Fill(ds, "SavingsBalance");

            // Step 7: Iterate the rows from TableTD above to create a collection of TD
            //         for this particular customer 

            Savings obj = new Savings();
            int rec_cnt = ds.Tables["SavingsBalance"].Rows.Count;
            if (rec_cnt > 0)
            {
                DataRow row = ds.Tables["SavingsBalance"].Rows[0];
                obj.savingsID = Convert.ToInt32(row["savingsID"].ToString());
                obj.savingGoal = Convert.ToDouble(row["savingGoal"]);
                obj.amountsaved = Convert.ToDouble(row["amountsaved"]);
                obj.description = row["description"].ToString();
                obj.startDate = Convert.ToDateTime(row["startDate"]);
                obj.endDate = Convert.ToDateTime(row["endDate"]);
                obj.userID = row["userID"].ToString();
                obj.bankaccnum = Convert.ToInt32(row["bankaccnum"].ToString());
                obj.status = Convert.ToInt32(row["status"]);
            }
            else
            {
                obj = null;
            }

            return obj;
        }
        public void UpdateTDforsavingbalance(int savingsID, double amountsaved)
        {
            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand(); 

            sqlStr.AppendLine("UPDATE Savings SET amountsaved = @paraamountsaved");
            sqlStr.AppendLine("WHERE savingsID = @parasavingsID");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@paraamountsaved", amountsaved);
            sqlCmd.Parameters.AddWithValue("@parasavingsID", savingsID);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();
            myConn.Close();


        }
        public void UpdatestatusForuserID(int savingsID, int status)
        {
            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("UPDATE Savings SET status = @parastatus");
            sqlStr.AppendLine("WHERE savingsID = @parasavingsID");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@parastatus", status);
            sqlCmd.Parameters.AddWithValue("@parasavingsID", savingsID);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();
            myConn.Close();


        }
    }
}