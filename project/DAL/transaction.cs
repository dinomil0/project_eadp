﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class transaction
    {
        public int transactionID { set; get; }
        public decimal type { set; get; }
        public int merchant_Number { set; get; }
        public decimal amount { set; get; }
        public string nric { set; get; }
    }
}