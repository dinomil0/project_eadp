﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class TransferPayYouVerify : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userID = Session["custNRIC"].ToString();
            if (Session["custNRIC"] == null)
            {
                Response.Redirect("landing.aspx");
            }
            Savings savTD = new Savings();
            SavingsDAO updTD = new SavingsDAO();
            //savTD = updTD.getTDbyUserSavingID(Convert.ToInt32(Session["SSsavingID"]));

            LblType.Text = Session["PayYouType"].ToString();

            if (LblType.Text == "Phone Number")
            {
                VerifyPhone.Visible = true;
                LblRecipMobileNum.Text = Session["PayYouPhonenum"].ToString();
                LblRecipNickNameMobileNum.Text = Session["PayYounamePhoneNum"].ToString();
                LblRecipBankAccnumMobileNum.Text = Session["PayYouRecipBankaccnumPhoneNum"].ToString();
                LblRecipBankAccnameMobileNum.Text = Session["PayYouRecipBankaccnamePhoneNum"].ToString();
                LblTransferAmt.Text = "$" + Session["PayYouamountPhoneNum"].ToString();
                LblAccount.Text = Session["PayYoubankaccPhoneNum"].ToString();

            }
            else if (LblType.Text == "NRIC")
            {
                VerifyNRIC.Visible = true;
                LblRecipNRIC.Text = Session["PayYouNRIC"].ToString();
                LblRecipNickNameNRIC.Text = Session["PayYounameNRIC"].ToString();
                LblRecipBankaccnumNRIC.Text = Session["PayYouRecipBankaccnumNRIC"].ToString();
                LblRecipBankaccnameNRIC.Text = Session["PayYouRecipBankaccnameNRIC"].ToString();
                LblTransferAmt.Text = "$" + Session["PayYouamountNRIC"].ToString();
                LblAccount.Text = Session["PayYoubankaccNRIC"].ToString();
            }
            else
            {
                Lbl_verifyerror.Text = "Error Found";
            }




        }
        protected void cancelButtonPayYouVerify_Click(object sender, EventArgs e)
        {
            Response.Redirect("TransferPayYouInput.aspx");
        }

        protected void submitButtonPayYouVerify_Click(object sender, EventArgs e)
        {
            DateTime date = DateTime.Now;
            double amount = Convert.ToDouble(LblTransferAmt.Text.ToString().Substring(1));
            //Session["userID"] = "S1234567A";
            string userID = Session["custNRIC"].ToString();
            string sendto = "";
            int frombankaccnum = Convert.ToInt32(LblAccount.Text.ToString());
            int tobankaccnum = 0;
            if (LblType.Text == "Phone Number")
            {
                sendto = LblRecipMobileNum.Text.ToString();
                tobankaccnum = Convert.ToInt32(LblRecipBankAccnumMobileNum.Text.ToString());
            }
            else if (LblType.Text == "NRIC")
            {
                sendto = LblRecipNRIC.Text.ToString();
                tobankaccnum = Convert.ToInt32(LblRecipBankaccnumNRIC.Text.ToString());
            }

            try
            {
                TransactionHistoryDAO newdata = new TransactionHistoryDAO();
                int insCnt = newdata.InsertTransactionsWithdrawal(date, "Transfer", "Withdrawal", amount, "Pending", userID, sendto, frombankaccnum, LblType.Text, tobankaccnum, frombankaccnum);
                if (insCnt == 1)
                {
                    Session["Timer"] = DateTime.Now.AddSeconds(30).ToString();
                    //Get acc balance and minus
                    Accounts accObj = new Accounts();
                    AccountsDAO accDAO = new AccountsDAO();

                    accObj = accDAO.getbankaccbalance(Convert.ToInt32(LblAccount.Text.ToString()));
                    double amountdeduct = Convert.ToDouble(LblTransferAmt.Text.ToString().Substring(1));
                    accObj.balance = accObj.balance - amountdeduct;

                    // Update acc balance for withdrawal
                    AccountsDAO updateDAO = new AccountsDAO();
                    updateDAO.updatebankaccbalance(Convert.ToInt32(LblAccount.Text.ToString()), accObj.balance);
                    Response.Redirect("TransferPayYouCompletion.aspx");
                }
                else
                {
                    Lbl_confirm.Text = "Transaction failed";
                }
                submitButtonPayYouVerify.Enabled = false;
            }
            catch (Exception f)
            {
                Lbl_confirm.Text = f.ToString();
            }





        }
    }
}