﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class TransactionHistory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }
            if (!IsPostBack)
            {
                var userID = Session["custNRIC"].ToString();
                customer cusObj = new customer();
                customerDAO cusDao = new customerDAO();

                cusObj = cusDao.GetCustomerLoginCredential(userID);

                AccountsDAO accDAO = new AccountsDAO();
                List<Accounts> bankaccnumListphone = new List<Accounts>();

                int bankaccnum = Convert.ToInt32(bankacclist.SelectedItem);

                bankaccnumListphone = accDAO.getbackaccnumbyuserID(userID);

                bankacclist.Items.Clear();
                bankacclist.Items.Insert(0, new ListItem("--Select--", "0"));
                bankacclist.AppendDataBoundItems = true;
                bankacclist.DataTextField = "bankaccname";
                bankacclist.DataValueField = "bankaccnum";
                bankacclist.DataSource = bankaccnumListphone;
                bankacclist.DataBind();

                if (cusObj == null)
                {
                    TH_err.Text = "Transaction History not found!";
                }
                else
                {
                    //  Call timeDeposit to get all TD for the customer 
                    //TransactionHistoryDAO THDAO = new TransactionHistoryDAO();
                    //List<TransactionHistory> THList = new List<TransactionHistory>();

                    DAL.TransactionHistoryDAO THDAO = new DAL.TransactionHistoryDAO();
                    List<DAL.TransactionHistory> THList = new List<DAL.TransactionHistory>();
                    THList = THDAO.getTDbyuserIDList(userID, Convert.ToInt32(bankaccnum));
                    THGridView.DataSource = THList;
                    THGridView.DataBind();
                }

            }
        }
        protected void THGridView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void THGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "pstatus")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = THGridView.Rows[index];
                Session["SSTransactionID"] = row.Cells[0].Text;
                Response.Redirect("StatusPendingWithdrawal.aspx");
            }
            else if (e.CommandName == "moreinfo")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = THGridView.Rows[index];
                Session["SSTransactionID"] = row.Cells[0].Text;
                //OpenNewWindow("TransactionHistoryMoreInfo.aspx");
                Response.Redirect("TransactionHistoryMoreInfo.aspx");
            }
            else if (e.CommandName == "tagain")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = THGridView.Rows[index];
                Session["SSTransactionID"] = row.Cells[0].Text;
                Response.Redirect("TransferAgainVerify.aspx");
            }

        }
        public void OpenNewWindow(string url)

        {
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", url));

        }
        protected void THGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString();
                if (status == "Successful")
                {
                    e.Row.Cells[8].Enabled = false;
                    Label SecondsLeft = (Label)e.Row.Cells[6].FindControl("lblSecsLeft");
                    SecondsLeft.Text = "0";
                }
                else
                {
                    //Add 15mins
                    //if (DateTime.Now > Convert.ToDateTime(e.Row.Cells[1].Text).AddMinutes(15))
                    if (DateTime.Now > Convert.ToDateTime(e.Row.Cells[1].Text).AddSeconds(30))
                    {
                        Label SecondsLeft = (Label)e.Row.Cells[6].FindControl("lblSecsLeft");
                        SecondsLeft.Text = "0";
                    }
                    else
                    {
                        string thedateS = e.Row.Cells[1].Text;
                        DateTime thedateDT = Convert.ToDateTime(thedateS);
                        //Add 15mins
                        //DateTime thedateDTadd15 = thedateDT.AddMinutes(15);
                        DateTime thedateDTadd15 = thedateDT.AddSeconds(30);
                        DateTime now = DateTime.Now;
                        TimeSpan difference = thedateDTadd15.Subtract(now); // could also write `now - otherTime`
                        Label SecondsLeft = (Label)e.Row.Cells[6].FindControl("lblSecsLeft");
                        SecondsLeft.Text = Math.Round(difference.TotalSeconds).ToString();

                    }
                }
                string checkdescription = DataBinder.Eval(e.Row.DataItem, "Description").ToString();
                string checktype = DataBinder.Eval(e.Row.DataItem, "Type").ToString();
                if (checkdescription != "Transfer" && checktype != "Deposit")
                {
                    e.Row.Cells[9].Enabled = false;
                }

            }
        }

        protected void bankacclist_SelectedIndexChanged(object sender, EventArgs e)
        {
            Accounts accObj = new Accounts();
            AccountsDAO accDAO = new AccountsDAO();
            TransactionHistory thObj = new TransactionHistory();
            TransactionHistoryDAO thDAO = new TransactionHistoryDAO();
            //Session["userID"] = "S1234567A";
            string userID = Session["custNRIC"].ToString();

            accObj = accDAO.getbalanceBybankaccnum(Convert.ToInt32(bankacclist.SelectedValue));

            if (bankacclist.SelectedValue == "0")
            {
                //THGridView.Items.Clear();
                TH_err.Text += "Please select a valid option";
            }
            else if (accObj.bankaccnum == Convert.ToInt32(bankacclist.SelectedValue))
            {
                bankaccbalance.Text = "$" + accObj.balance.ToString("F");
                TransactionHistoryDAO THDAO = new TransactionHistoryDAO();
                List<DAL.TransactionHistory> THList = new List<DAL.TransactionHistory>();
                THList = THDAO.getTDbyuserIDList(userID, Convert.ToInt32(bankacclist.SelectedValue));
                THGridView.DataSource = THList;
                THGridView.DataBind();
            }
            else
            {
                TH_err.Text += "Account not found in user";
            }
        }

        protected void Ddlmonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            string userID = Session["custNRIC"].ToString();
            if (bankacclist.SelectedValue != null)
            {
                TransactionHistoryDAO THDAO = new TransactionHistoryDAO();
                List<DAL.TransactionHistory> THList = new List<DAL.TransactionHistory>();
                THList = THDAO.getTDbyuserIDListmonths(userID, Convert.ToInt32(bankacclist.SelectedValue), Convert.ToInt32(Ddlmonths.SelectedValue));
                THGridView.DataSource = THList;
                THGridView.DataBind();

            }
           

        }
        
        public void showModal(object sender, EventArgs e)
        {
            int rowIndex = Convert.ToInt32(((sender as LinkButton).NamingContainer as GridViewRow).RowIndex);
            GridViewRow row = THGridView.Rows[rowIndex];
            
            DAL.TransactionHistory thobj = new DAL.TransactionHistory();
            TransactionHistoryDAO THDao = new TransactionHistoryDAO();

            thobj = THDao.getTDbyUserTransactionID(Convert.ToInt32(row.Cells[0].Text));
            LblReferencenum.Text = thobj.transactionID.ToString();
            Lbldate.Text = thobj.date.ToString("dd/MM/yyyy");
            Lbldescription.Text = thobj.description.ToString();
            LblType.Text = thobj.type.ToString();
            LblAmount.Text = "$" + thobj.amount.ToString("F");
            LblStatus.Text = thobj.status.ToString();
            if (Lbldescription.Text == "Transfer")
            {
                PanelTransfer.Visible = true;
                Lblsendto.Text = thobj.sendto.ToString();
                Lblsendusing.Text = thobj.sendusing.ToString();
            }
            //if (thobj.merchantcode != null)
            //{
            //    PanelMerchantCode.Visible = true;
            //    Lblmerchantcode.Text = thobj.merchantcode.ToString();
            //}

            Lblfrombankaccnum.Text = thobj.frombankaccnum.ToString();
            LbluserID.Text = thobj.userID.ToString();
            ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal()", true);
        }

        protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
        {
            string userID = Session["custNRIC"].ToString();
            if (bankacclist.SelectedValue != null)
            {
                TransactionHistoryDAO THDAO = new TransactionHistoryDAO();
                List<DAL.TransactionHistory> THList = new List<DAL.TransactionHistory>();
                THList = THDAO.getTDbyuserIDListyears(userID, Convert.ToInt32(bankacclist.SelectedValue), Convert.ToInt32(Ddlmonths.SelectedValue), Convert.ToInt32(ddlyear.SelectedValue));
                THGridView.DataSource = THList;
                THGridView.DataBind();

            }


        }
    }
}