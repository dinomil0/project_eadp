﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class WebForm8 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userID = Session["custNRIC"].ToString();
            if (Session["custNRIC"] == null)
            {
                Response.Redirect("landing.aspx");
            }
            GIRODAO GIRODAO = new GIRODAO();
            List<GIRO> GIROList = new List<GIRO>();
            GIROList = GIRODAO.getGIRObyuserIDList(userID);
            if (GIROList != null)
            {
                GridViewGIRO.DataSource = GIROList;
                GridViewGIRO.DataBind();
            }
            
            
        }

        public void showModal(object sender, EventArgs e)
        {
            int rowIndex = Convert.ToInt32(((sender as LinkButton).NamingContainer as GridViewRow).RowIndex);
            GridViewRow row = GridViewGIRO.Rows[rowIndex];
            LblgiroID.Text = row.Cells[0].Text.ToString();
            ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal()", true);
        }

        protected void confirmDeleteGIRO_Click(object sender, EventArgs e)
        {
            GIRODAO giroDAO = new GIRODAO();
            giroDAO.DeleteTDbyGIROID(Convert.ToInt32(LblgiroID.Text));
            Response.Redirect(Request.RawUrl);
        }

        protected void GridViewGIRO_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string userID = Session["custNRIC"].ToString();
            NotificationsDAO notiDAO = new NotificationsDAO();
            List<string> notiList = new List<string>();
            notiList = notiDAO.getAllNotificationsbyuserID(userID);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GIRODAO giroDAO = new GIRODAO();
                List<GIRO> giroList = new List<GIRO>();
                giroList = giroDAO.getGIRObyuserIDList(userID);
                foreach (GIRO giro in giroList)
                {
                    if (Convert.ToInt32(giro.date.ToString("MM")) < DateTime.Now.Month)
                    {
                        if (Convert.ToInt32(giro.date.ToString("dd")) <= DateTime.Now.Day)
                        {
                            TransactionHistoryDAO thDAO = new TransactionHistoryDAO();
                            var difference = DateTime.Now.Month - Convert.ToInt32(giro.date.ToString("MM"));
                            for (int i = 0; i < difference; i++)
                            {
                                thDAO.InsertTransactionsNormal(giro.date.AddMonths(i), "GIRO", "Withdrawal", giro.amount, "Successful", userID, giro.bankaccnum, giro.bankaccnum);
                            }
                            giroDAO.UpdateTDfordate(giro.giroID, giro.date.AddMonths(difference+1));
                        }
                        else
                        {
                            TransactionHistoryDAO thsDAO = new TransactionHistoryDAO();
                            var difference1 = DateTime.Now.Month - Convert.ToInt32(giro.date.ToString("MM"));
                            thsDAO.InsertTransactionsNormal(giro.date, "GIRO", "Withdrawal", giro.amount, "Successful", userID, giro.bankaccnum, giro.bankaccnum);
                            giroDAO.UpdateTDfordate(giro.giroID, giro.date.AddMonths(difference1));
                        }
                    }
                    else if (Convert.ToInt32(giro.date.ToString("MM")) == DateTime.Now.Month)
                    {
                        if (Convert.ToInt32(giro.date.ToString("dd")) <= DateTime.Now.Day)
                        {
                            TransactionHistoryDAO thDAO = new TransactionHistoryDAO();
                            thDAO.InsertTransactionsNormal(giro.date, "GIRO", "Withdrawal", giro.amount, "Successful", userID, giro.bankaccnum, giro.bankaccnum);
                            giroDAO.UpdateTDfordate(giro.giroID, giro.date.AddMonths(1));
                        }
                        else
                        {

                        }
                    }
                    else
                    {

                    }

                }
            }
        }
    }
}