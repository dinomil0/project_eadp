﻿<%@ Page Title="" Language="C#" MasterPageFile="~/landingMaster.Master" AutoEventWireup="true" CodeBehind="landing.aspx.cs" Inherits="project.landing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="d-flex loginBG">
        <div runat="server" class="border mx-auto my-auto shadow-sm p-3 mb-5 bg-white rounded opacitydiv">
            <div class="form-group">
                <asp:Label ID="lblUsername" runat="server" Text="NRIC/Phone Number"></asp:Label>
                <asp:TextBox type="text" class="form-control" id="tbUsername" placeholder="Enter Your NRIC/Phone Number" runat="server"></asp:TextBox>
                <%--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--%>
            </div>
            <div class="form-group">
                <asp:Label id="lblPassword" runat="server" Text="Password"></asp:Label>
                <asp:TextBox id="tbPassword" type="password" class="form-control" placeholder="Enter Your Password" runat="server"></asp:TextBox>
            </div>
            <div>
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            </div>
            <%--<div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Check me out</label>
            </div>--%>
            <div class="mt-2">
                <asp:Button id="btnLogin" type="submit" class="btn btn-primary" runat="server" Text="Login" OnClick="btnLogin_Click" />
            </div>
        </div>
    </section>
</asp:Content>
