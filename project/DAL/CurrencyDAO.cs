﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class CurrencyDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        public Currency getCurrencyWallet(string userID)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM CurrencyWallet");
            sqlCommand.AppendLine("where userID = @parauserID");

            Currency obj = new Currency();

            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            // fill dataset
            da.Fill(ds, "CurrencyWalletTable");
            int rec_cnt = ds.Tables["CurrencyWalletTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["CurrencyWalletTable"].Rows)
                {
                    obj.CurrencyAmtMYR = Convert.ToDouble(row["MYR"]);
                    obj.CurrencyAmtUSD = Convert.ToDouble(row["USD"]);
                    obj.CurrencyAmtEUR = Convert.ToDouble(row["EUR"]);
                    obj.CurrencyAmtGBP = Convert.ToDouble(row["GBP"]);
                }

            }
            else
            {
                obj = null;
            }

            return obj;
        }
        public void updatebalancebyuserIDmyr(string userID,string CurrencyName, double MYR)
        {
            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("UPDATE CurrencyWallet SET MYR = @paraMYR");
            sqlStr.AppendLine("where userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);


            sqlCmd.Parameters.AddWithValue("@parauserID", userID);
            sqlCmd.Parameters.AddWithValue("@paraCurrencyName", CurrencyName);
            sqlCmd.Parameters.AddWithValue("@paraMYR", MYR);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();
            
        }
        public void updatebalancebyuserIDusd(string userID, string CurrencyName, double USD)
        {
            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("UPDATE CurrencyWallet SET USD = @paraUSD");
            sqlStr.AppendLine("where userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);


            sqlCmd.Parameters.AddWithValue("@parauserID", userID);
            sqlCmd.Parameters.AddWithValue("@paraCurrencyName", CurrencyName);
            sqlCmd.Parameters.AddWithValue("@paraUSD", USD);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();

        }
        public void updatebalancebyuserIDeur(string userID, string CurrencyName, double EUR)
        {
            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("UPDATE CurrencyWallet SET EUR = @paraEUR");
            sqlStr.AppendLine("where userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);


            sqlCmd.Parameters.AddWithValue("@parauserID", userID);
            sqlCmd.Parameters.AddWithValue("@paraCurrencyName", CurrencyName);
            sqlCmd.Parameters.AddWithValue("@paraEUR", EUR);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();

        }
        public void updatebalancebyuserIDgbp(string userID, string CurrencyName, double GBP)
        {
            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("UPDATE CurrencyWallet SET GBP = @paraGBP");
            sqlStr.AppendLine("where userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);


            sqlCmd.Parameters.AddWithValue("@parauserID", userID);
            sqlCmd.Parameters.AddWithValue("@paraCurrencyName", CurrencyName);
            sqlCmd.Parameters.AddWithValue("@paraGBP", GBP);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();

        }

    }
}