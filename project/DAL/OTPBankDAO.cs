﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class OTPBankDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        public OTPBank getOTP(string userID)
        {
            
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM OTP");
            sqlCommand.AppendLine("where userID = @parauserID");

            OTPBank obj = new OTPBank();

            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            // fill dataset
            da.Fill(ds, "OTPTable");
            int rec_cnt = ds.Tables["OTPTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["OTPTable"].Rows)
                {
                    obj.otpID = Convert.ToInt32(row["otpID"]);
                    obj.otp = Convert.ToInt32(row["otp"]);
                    obj.date = Convert.ToDateTime(row["date"]);
                    obj.dateadd15 = Convert.ToDateTime(row["dateadd15"]);
                    obj.phonenum = row["phonenum"].ToString();
                    obj.userID = row["userID"].ToString();
                }
            }
            else
            {
                obj = null;
            }

            return obj;
        }
        public void InsertOTP(int otp, DateTime date, DateTime dateadd15, string phonenum, string userID)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("INSERT INTO OTP (otp, date, dateadd15, phonenum, userID)");
            sqlStr.AppendLine("VALUES (@paraotp,@paradate,@paradateadd15,@paraphonenum,@parauserID)");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@paraotp", otp);
            sqlCmd.Parameters.AddWithValue("@paradate", date);
            sqlCmd.Parameters.AddWithValue("@paradateadd15", dateadd15);
            sqlCmd.Parameters.AddWithValue("@paraphonenum", phonenum);
            sqlCmd.Parameters.AddWithValue("@parauserID", userID);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();

        }
        public void DeleteOTP(string userID)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("DELETE FROM OTP");
            sqlStr.AppendLine("WHERE userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@parauserID", userID);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();
        }
    }
}