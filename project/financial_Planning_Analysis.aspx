﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="financial_Planning_Analysis.aspx.cs" Inherits="project.financial_Planning_Analysis" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div id="fp_Form">
        <div class="container my-5">
            <asp:Label class="h5" runat="server" Text="Financial Plan Analysis"></asp:Label>
            <hr />
            <asp:Label ID="lb_Fp_Error_Title" class="text-danger" runat="server"></asp:Label>
            <div class="container mt-3">
                <asp:Label ID="lb_Fp_One" runat="server">
                    <i class="fas fa-coins fa-2x"></i>
                    &nbsp;&nbsp;&nbsp;&nbsp;Understanding Financial Status
                    <asp:LinkButton ID="btn_Fp_Insert_Data" class="btn" data-toggle="modal" data-target="#md_Fp_Insert_Data" runat="server">
                        <i class="fas fa-plus-circle"></i>
                    </asp:LinkButton>
                    &nbsp;&nbsp;
                    <asp:LinkButton ID="btn_Fp_Show_Data" class="btn btn-sm btn-primary" data-toggle="collapse" data-target="#div_Fp_Data" aria-expanded="false" aria-controls="div_Fp_Data" Text="Details" runat="server"></asp:LinkButton> 
                </asp:Label>
                <hr class="w-25 ml-5 pr-1" />
                <div id="div_Fp_Data" class="container collapse">
                    <asp:GridView ID="gv_Fp_Information" class="table table-responsive-sm" runat="server" AutoGenerateColumns="False" OnRowDataBound="gv_Fp_Information_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <asp:Label ID="lb_Fp_Read_Name" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type">
                                <ItemTemplate>
                                    <asp:Label ID="lb_Fp_Read_Type" runat="server" Text='<%# Eval("type") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Manual Input">
                                <ItemTemplate>
                                    <asp:Label ID="lb_Fp_Read_Manual_Input" runat="server" Text='<%# Eval("manual_Input") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount">
                                <ItemTemplate>
                                    <asp:Label ID="lb_Fp_Read_Amount" runat="server" Text='<%# Eval("amount") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btn_Fp_Read_Update"  Text="Update" OnClick="btn_Fp_Read_Update_Click" runat="server"></asp:LinkButton>
                                </ItemTemplate>
                                <ControlStyle CssClass="btn btn-sm btn-info d-flex justify-content-center" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btn_Fp_Read_Delete" OnClick="btn_Fp_Read_Delete_Click" runat="server">
                                        <i class="fas fa-trash-alt"></i>
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <ControlStyle CssClass="btn btn-sm btn-secondary d-flex justify-content-center py-2" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle Cssclass="thead-dark" />
                    </asp:GridView>
                </div>
                <div class="container py-2 border border-dark rounded">
                    <strong><asp:Label ID="lb_Fp_Information_Title" runat="server" Text="Calculations:"></asp:Label></strong>
                    <hr class="w-25 ml-0" />
                    <asp:Label ID="lb_Fp_Information" class="mt-2" runat="server"></asp:Label>
                    <strong><asp:Label ID="lb_Fp_Information_Summary_Title" runat="server" Text="Summary:"></asp:Label></strong>
                    <hr class="w-25 ml-0" />
                    <asp:Label ID="lb_Fp_Information_Summary" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="container my-5">
            <div class="container mt-3">
                <asp:Label ID="lb_Fp_Two" runat="server">
                    <i class="fab fa-optin-monster fa-2x"></i>
                    &nbsp;&nbsp;&nbsp;&nbsp;Choosing the Best Option
                    <asp:LinkButton ID="btn_Fp_Option" class="btn" data-toggle="collapse" data-target="#div_Fp_Option" aria-expanded="false" aria-controls="div_Fp_Option" runat="server">
                        <i class="fas fa-info-circle"></i>
                    </asp:LinkButton>  
                </asp:Label>
                <hr class="w-25 ml-5" />
                <div id="div_Fp_Option" class="container py-3 collapse border border-dark rounded ">
                    <asp:Label ID="lb_Fp_Plan_Title" class="mt-3" runat="server">
                        Plan Recap:
                    </asp:Label>
                    <hr class="w-25 ml-0" />
                    <asp:Label ID="lb_Fp_Plan" runat="server">
                    </asp:Label>
                    <hr />
                    <asp:Label ID="lb_Fp_Remark_Title" class="mt-3" runat="server">
                        Additional Information:
                    </asp:Label>
                    <hr class="w-25 ml-0" />
                    <asp:Label ID="lb_Fp_Remark" runat="server">
                    </asp:Label>
                </div>
                <div class="container mt-3">
                    <asp:GridView ID="gv_Fp_Option" class="table table-responsive-sm" runat="server" AutoGenerateColumns="False">
                        <Columns>
                            <asp:TemplateField HeaderText="Rank (#)">
                                <ItemTemplate>
                                    <asp:Label ID="lb_Fp_Read_Plan_Count" runat="server" Text='<%# Eval("count") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Plan">
                                <ItemTemplate>
                                    <asp:Label ID="lb_Fp_Read_Plan_Name" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Age">
                                <ItemTemplate>
                                    <asp:Label ID="lb_Fp_Read_Plan_Age" runat="server" Text='<%# Eval("matured_Age") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Duration">
                                <ItemTemplate>
                                    <asp:Label ID="lb_Fp_Read_Plan_Duration" runat="server" Text='<%# Eval("duration") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Monthly Contribution (S$)">
                                <ItemTemplate>
                                    <asp:Label ID="lb_Fp_Read_Plan_Monthly_Contribution" runat="server" Text='<%# Eval("monthly_Contribution") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btn_Fp_Read_Insert_Plan" runat="server" Text="Select" OnClick="btn_Fp_Read_Insert_Plan_Click"></asp:LinkButton>
                                </ItemTemplate>
                                <ControlStyle CssClass="btn btn-info d-flex justify-content-center" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle Cssclass="thead-dark" />
                    </asp:GridView>
                </div>
                <div class="d-flex justify-content-end mt-3">
                    <asp:Button id="btn_Fp_Back" class="btn btn-secondary" Text="Back" runat="server" OnClick="btn_Fp_Back_Click" />
                </div>
            </div>
        </div>

        <!--Inserting Financial Data-->
        <div class="modal fade" id="md_Fp_Insert_Data" tabindex="-1" role="dialog" aria-labelledby="md_Fp_Insert_Label" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="md_Fp_Insert_Label">Adding Additional Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lb_Fp_Insert_Error" class="text-danger" runat="server"></asp:Label>
                        <div class="form-group row mt-3">
                            <asp:Label ID="lb_Fp_Insert_Name" class="col-form-label col-sm-4" runat="server" Text="Data Name:"></asp:Label>
                            <div class="col-sm-8">
                                <asp:Textbox ID="tb_Fp_Insert_Name" class="form-control" runat="server"></asp:Textbox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Fp_Insert_Type" class="col-form-label col-sm-4" runat="server" Text="Data Type:"></asp:Label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="ddl_Fp_Insert_Type" class="form-control" runat="server">
                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                    <asp:ListItem Value="Asset">Asset</asp:ListItem>
                                    <asp:ListItem Value="Liability">Liability</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Fp_Insert_Amount" class="col-form-label col-sm-4" runat="server" Text="Amount:"></asp:Label>
                            <div class="col-sm-8">
                                <asp:Textbox ID="tb_Fp_Insert_Amount" class="form-control" runat="server"></asp:Textbox>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btn_Fp_Insert_Data_Submit" class="btn btn-success" Onclick="btn_Fp_Insert_Data_Submit_Click" Text="Insert Data"  runat="server" />
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!--Updating Financial Data-->
        <div class="modal fade" id="md_Fp_Update_Data" tabindex="-1" role="dialog" aria-labelledby="md_Fp_Update_Label" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="md_Fp_Update_Label">Updating Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lb_Fp_Update_Error" class="text-danger" runat="server"></asp:Label>
                        <div class="form-group row mt-3">
                            <asp:Label ID="lb_Fp_Update_Name" class="col-form-label col-sm-4" runat="server" Text="Data Name:"></asp:Label>
                            <div class="col-sm-8">
                                <asp:Textbox ID="tb_Fp_Update_Name" class="form-control" ReadOnly="true" runat="server"></asp:Textbox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Fp_Update_Type" class="col-form-label col-sm-4" runat="server" Text="Data Type:"></asp:Label>
                            <div class="col-sm-8">
                                <asp:Textbox ID="tb_Fp_Update_Type" class="form-control" ReadOnly="true" runat="server"></asp:Textbox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Fp_Update_Amount" class="col-form-label col-sm-4" runat="server" Text="Amount:"></asp:Label>
                            <div class="col-sm-8">
                                <asp:Textbox ID="tb_Fp_Update_Amount" class="form-control" runat="server"></asp:Textbox>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btn_Fp_Update_Data_Submit" class="btn btn-success" Onclick="btn_Fp_Update_Data_Submit_Click" Text="Update Data"  runat="server" />
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!--Deleting Financial Data-->
        <div class="modal fade" id="md_Fp_Delete_Data" tabindex="-1" role="dialog" aria-labelledby="md_Fp_Update_Label" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="md_Fp_Delete_Label">Deleting Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lb_Fp_Delete_Error" class="text-danger" runat="server"></asp:Label>
                        <div class="form-group row mt-3">
                            <asp:Label ID="lb_Fp_Delete_Name" class="col-form-label col-sm-4" runat="server" Text="Data Name:"></asp:Label>
                            <div class="col-sm-8">
                                <asp:Textbox ID="tb_Fp_Delete_Name" class="form-control" ReadOnly="true" runat="server"></asp:Textbox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Fp_Delete_Type" class="col-form-label col-sm-4" runat="server" Text="Data Type:"></asp:Label>
                            <div class="col-sm-8">
                                <asp:Textbox ID="tb_Fp_Delete_Type" class="form-control" ReadOnly="true" runat="server"></asp:Textbox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Fp_Delete_Amount" class="col-form-label col-sm-4" runat="server" Text="Amount:"></asp:Label>
                            <div class="col-sm-8">
                                <asp:Textbox ID="tb_Fp_Delete_Amount" class="form-control" ReadOnly="true" runat="server"></asp:Textbox>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btn_Fp_Delete_Data_Submit" class="btn btn-success" Onclick="btn_Fp_Delete_Data_Submit_Click" Text="Delete Data"  runat="server" />
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!--Creating Financial Plan-->
        <div class="modal fade" id="md_Fp_Insert_Plan" tabindex="-1" role="dialog" aria-labelledby="md_Fp_Insert_Plan_Label" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="md_Fp_Insert_Plan_Label">Plan Confirmation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row mt-3">
                            <asp:Label ID="lb_Fp_Insert_Plan_Rank" class="col-form-label col-sm-6" runat="server" Text="Plan Rank (#):"></asp:Label>
                            <div class="col-sm-6">
                                <asp:Textbox ID="tb_Fp_Insert_Plan_Rank" class="form-control" ReadOnly="true" runat="server"></asp:Textbox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Fp_Insert_Plan_Name" class="col-form-label col-sm-6" runat="server" Text="Plan Name:"></asp:Label>
                            <div class="col-sm-6">
                                <asp:Textbox ID="tb_Fp_Insert_Plan_Name" class="form-control" ReadOnly="true" runat="server"></asp:Textbox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Fp_Insert_Plan_Age" class="col-form-label col-sm-6" runat="server" Text="Age:"></asp:Label>
                            <div class="col-sm-6">
                                <asp:Textbox ID="tb_Fp_Insert_Plan_Age" class="form-control" ReadOnly="true" runat="server"></asp:Textbox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Fp_Insert_Plan_Monthly_Contribution" class="col-form-label col-sm-6" runat="server" Text="Monthly Contribution (S$):"></asp:Label>
                            <div class="col-sm-6">
                                <asp:Textbox ID="tb_Fp_Insert_Plan_Monthly_Contribution" class="form-control" ReadOnly="true" runat="server"></asp:Textbox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Fp_Insert_Plan_Amount" class="col-form-label col-sm-6" runat="server" Text="Amount (S$):"></asp:Label>
                            <div class="col-sm-6">
                                <asp:Textbox ID="tb_Fp_Insert_Plan_Amount" class="form-control" ReadOnly="true" runat="server"></asp:Textbox>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btn_Fp_Insert_Plan_Submit" class="btn btn-success" Onclick="btn_Fp_Insert_Plan_Submit_Click" Text="Create Plan"  runat="server" />
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <script type='text/javascript'>
            function initiate_Update_Modal() {
                $('[id*=md_Fp_Update_Data]').modal('show');
            } 
            function initiate_Delete_Modal() {
                $('[id*=md_Fp_Delete_Data]').modal('show');
            }
            function initiate_Insert_Plan_Modal() {
                $('[id*=md_Fp_Insert_Plan]').modal('show');
            }
        </script>
    
</asp:Content>
