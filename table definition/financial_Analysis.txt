CREATE TABLE [dbo].[Financial_Analysis] (
    [userID]        VARCHAR (50)    NOT NULL,
    [name]          VARCHAR (50)    NOT NULL,
    [update_Detail] DATETIME        NULL,
    [amount]        DECIMAL (10, 2) NULL,
    PRIMARY KEY CLUSTERED ([userID] ASC, [name] ASC)
);



//DATA

INSERT INTO [dbo].[Financial_Analysis] ([userID], [name], [update_Detail], [amount]) VALUES (N'S9123456A', N'Working Capital', N'2019-07-28 21:37:04', CAST(8509.53 AS Decimal(10, 2)))
INSERT INTO [dbo].[Financial_Analysis] ([userID], [name], [update_Detail], [amount]) VALUES (N'S9123456A', N'Working Capital Ratio', N'2019-07-28 21:37:04', CAST(6.07 AS Decimal(10, 2)))
INSERT INTO [dbo].[Financial_Analysis] ([userID], [name], [update_Detail], [amount]) VALUES (N'S9876543Z', N'Working Capital', N'2019-07-28 09:50:28', CAST(4177.40 AS Decimal(10, 2)))
INSERT INTO [dbo].[Financial_Analysis] ([userID], [name], [update_Detail], [amount]) VALUES (N'S9876543Z', N'Working Capital Ratio', N'2019-07-28 09:50:28', CAST(2.95 AS Decimal(10, 2)))
