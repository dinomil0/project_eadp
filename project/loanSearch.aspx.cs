﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project.bankSite
{
    public partial class personalLoan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["custNRIC"] = "S9123456A";
            if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }
            pnlError.Visible = false;
        }

        protected void loanSearch_Click(object sender, EventArgs e)
        {
            if (checkFields())
            {
                try
                {
                    loanDAO loanDAO = new loanDAO();
                    var loanlist = loanDAO.getLoans(Convert.ToString(loanType.SelectedValue), Convert.ToInt32(loanAmt.Text));
                    Session["loanList"] = loanlist;
                    //if (loanlist.Count() < 2)
                    //{
                    //    loanAmt.CssClass = "form-control is-invalid";
                    //}
                    //else
                    //{
                    //    loanAmt.CssClass = "form-control";
                    //}
                    foreach (loan loanobj in loanlist)
                    {
                        loanobj.loanTenor = Convert.ToInt32(loanTenor.Text);
                        loanobj.loanEIR = calEIR(Convert.ToInt32(loanTenor.Text), Convert.ToDouble(loanobj.loanInt), Convert.ToInt32(rbPeriod.SelectedValue));

                        var interest = Convert.ToInt32(loanAmt.Text) * Convert.ToDouble(loanobj.loanInt) * Convert.ToInt32(loanTenor.Text);
                        var principalNinterest = (Convert.ToInt32(loanAmt.Text) + interest);
                        var repaymenttime = Convert.ToInt32(rbPeriod.SelectedValue) * Convert.ToInt32(loanTenor.Text);
                        loanobj.loanRepayment = Math.Round(principalNinterest / repaymenttime, 2);
                        Session["loanAmount"] = Convert.ToInt32(loanAmt.Text);
                    }
                    gvLoans.DataSource = loanlist;
                    gvLoans.DataBind();
                    pnlGV.Visible = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }
        public double calEIR(int tenor, double intRate, int compounding)
        {
            var EIR = Math.Round(Math.Pow(1 + (intRate / compounding), compounding) -1 , 2);
            return EIR;
        }
        protected void gvLoans_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        public bool checkFields()
        {
            
            if (string.IsNullOrEmpty(loanTenor.Text) || Convert.ToInt32(loanTenor.Text) <= 0)
            {
                ShowMessage("Loan Tenor cannot be empty", "danger");
                return false;
            }
            if (string.IsNullOrEmpty(loanAmt.Text))
            {
                ShowMessage("Loan amount cannot be empty", "danger");
                return false;
            }
            if (loanType.SelectedIndex == 0)
            {
                ShowMessage("Please Choose a loan type", "danger");
                return false;
            }
            else if (loanType.SelectedIndex == 1)
            {
                if (Int32.TryParse(loanAmt.Text, out var amt))
                {
                    if (amt < 1000)
                    {
                        ShowMessage("Loan amount cannot be lower than $1000", "danger");
                        return false;
                    }
                }
                if (Convert.ToInt32(loanTenor.Text)  < 1 || Convert.ToInt32(loanTenor.Text) > 7)
                {
                    ShowMessage("Personal loan tenor cannot be less than 1 year and more than 7 years", "danger");
                    return false;
                }

            }
            else if (loanType.SelectedIndex == 2)
            {
                if(Convert.ToInt32(loanTenor.Text) < 1 || Convert.ToInt32(loanTenor.Text) > 7)
                {
                    ShowMessage("Car loan tenor cannot be less than 1 year and more than 7 years", "danger");
                    return false;
                }
                
            }
            return true;
        }

        public void ShowMessage(string Message, string type)
        {
            lblError.Text = Message;
            pnlError.CssClass = string.Format("alert alert-{0} alert-dismissable", type.ToString().ToLower());
            pnlError.Attributes.Add("role", "alert");
            pnlError.Visible = true;
            lblError.Text = Message;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            pnlError.Visible = false;
        }


        protected void gvLoans_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "applyLoan")
            {
                //"S9123456A";
                //customer custObj = new customer();
                //customerDAO custDAO = new customerDAO();
                //int index = Convert.ToInt32(e.CommandArgument);
                //GridViewRow row = gvLoans.Rows[index];

                //custObj = custDAO.GetCustomerLoginCredential(Convert.ToString(Session["custNRIC"]));

                //var bd = DateTime.Now.Year - custObj.customerDOB.Year;

                //var name = row.Cells[0].Text.Split(' ');

                //var bankName = name[0];

                //if (bd >= 21)
                //{
                //    loanApplicationDAO loanApplicationDAO = new loanApplicationDAO();
                //    loanApplicationDAO.applyLoan(custObj, Convert.ToDouble(row.Cells[2].Text), Convert.ToInt32(row.Cells[3].Text), Convert.ToInt32(Session["loanAmount"]), Convert.ToDouble(row.Cells[4].Text), bankName);
                //}
                //else
                //{

                //    ShowMessage("Below 21 years old", "danger");
                //}
            }
            else if (e.CommandName == "Details")
            {
                
            }
        }

        protected void gvLoans_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            
        }

        protected void loanType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (loanType.SelectedIndex != 0)
            {
                pnlPeriod.Visible = true;
            }
            else pnlPeriod.Visible = false;
        }
        public void showModal(object sender, EventArgs e)
        {
            try
            {
                int rowindex = Convert.ToInt32(((sender as LinkButton).NamingContainer as GridViewRow).RowIndex);
                GridViewRow row = gvLoans.Rows[rowindex];
                Session["GVRow"] = row;
                var loanName = row.Cells[0].Text;
                var loanIntRate = Convert.ToDouble(row.Cells[1].Text.Substring(0, 4));
                var loanEIR = Convert.ToDouble(row.Cells[2].Text.Substring(0, 4));
                var loanTenor = Convert.ToInt32(row.Cells[3].Text);
                var repaymentAmt = Convert.ToDouble(row.Cells[4].Text.Substring(1));
                var interval = Convert.ToInt32(rbPeriod.SelectedValue);
                var principal = Convert.ToDouble(loanAmt.Text);

                //var interestAmt = (loanIntRate * 100) * (loanTenor * Convert.ToInt32(rbPeriod.SelectedValue));
                var interestAmt = Math.Round(Convert.ToInt32(loanAmt.Text) * (Convert.ToDouble(loanEIR) / 100) * Convert.ToInt32(loanTenor), 2);

                Session["pieChart"] = pieChart(interestAmt, principal);

                lblLoanName.Text = loanName;
                lblLoanTenor.Text = loanTenor.ToString() + " Years";
                lblEIR.Text = (loanEIR).ToString("F") + "%";
                lblRepaymentAmt.Text = "$" + repaymentAmt.ToString("F");
                lblTotalInt.Text = "$" + interestAmt.ToString("F");
                lblTotalAmt.Text = "$" + (principal + interestAmt).ToString("F");

                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal()", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public string pieChart(double interest, double principal)
        {
            double[] list = { interest, principal };
            return new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(list);
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                customer custObj = new customer();
                customerDAO custDAO = new customerDAO();
                GridViewRow row = (GridViewRow)Session["GVRow"];

                custObj = custDAO.GetCustomerLoginCredential(Convert.ToString(Session["custNRIC"]));
                TransactionHistoryDAO transactionHistoryDAO = new TransactionHistoryDAO();
                var salary = Convert.ToDouble(transactionHistoryDAO.read_Salary(Convert.ToString(Session["custNRIC"]))) * 12;

                var bd = DateTime.Now.Year - custObj.customerDOB.Year;

                var bname = row.Cells[0].Text.Split(' ');

                var bankName = bname[0];

                if (bd >= 21)
                {
                    double minsalary = 0;
                    var loanList = (List<loan>)Session["loanList"];
                    foreach(var loans in loanList)
                    {
                        if (loans.loanName == row.Cells[0].Text)
                        {
                            minsalary = loans.loanMinSalary;
                            break;
                        }
                    }
                    if (salary >= minsalary)
                    {
                        loanApplicationDAO loanApplicationDAO = new loanApplicationDAO();
                        var loanApplist = loanApplicationDAO.retrieveLoans();
                        var check = false;
                        if (loanApplist != null)
                        {
                            foreach (var loans in loanApplist)
                            {
                                var loanNRIC = loans.custNRIC.Substring(0, 9);
                                if (loanNRIC == Convert.ToString(Session["custNRIC"]))
                                {
                                    check = true;
                                    break;
                                }
                            }
                        }
                        if (check == false)
                        {
                            loanApplicationDAO.applyLoan(custObj, Convert.ToDouble(row.Cells[2].Text.Substring(0, 4)), Convert.ToInt32(row.Cells[3].Text), Convert.ToInt32(Session["loanAmount"]), Convert.ToDouble(row.Cells[4].Text.Substring(1)), bankName, salary);
                            ShowMessage("Loan Applied", "success");
                        }
                        else
                        {
                            ShowMessage("You have pending loans applications", "danger");
                        }
                    }
                    else
                    {
                        ShowMessage("You didnt meet the minimum salary requirement( $" + minsalary + ")", "danger");
                    }
                }
                else
                {
                    ShowMessage("Below 21 years old", "danger");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}