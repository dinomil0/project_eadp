﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class financial_PlanDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

        //Creating Financial Plan
        public void insert_Financial_Plan(string name, int age, decimal monthly_Contribution, decimal amount, string userID)
        {
            try
            {
                StringBuilder sqlString = new StringBuilder();
                SqlCommand sqlCommand = new SqlCommand();
                sqlString.AppendLine("INSERT INTO FINANCIAL_PLAN (NAME, MATURED_AGE, MONTHLY_CONTRIBUTION, AMOUNT, USERID)");
                sqlString.AppendLine("VALUES (@PARANAME, @PARAMATURED_AGE, @PARAMONTHLY_CONTRIBUTION, @PARAAMOUNT, @PARAUSERID)");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                sqlCommand = new SqlCommand(sqlString.ToString(), sqlConnection);
                sqlCommand.Parameters.AddWithValue("@PARANAME", name);
                sqlCommand.Parameters.AddWithValue("@PARAMATURED_AGE", age);
                sqlCommand.Parameters.AddWithValue("@PARAMONTHLY_CONTRIBUTION", monthly_Contribution);
                sqlCommand.Parameters.AddWithValue("@PARAAMOUNT", amount);
                sqlCommand.Parameters.AddWithValue("@PARAUSERID", userID);
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch
            {
                //Exception
            }
        }

        //Reading Financial Plan
        public List<financial_Plan> read_Financial_Plan(string userID)
        {
            List<financial_Plan> financial_Plan_List = new List<financial_Plan>();
            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                SqlDataAdapter da;
                StringBuilder sqlString = new StringBuilder();
                sqlString.AppendLine("SELECT * FROM FINANCIAL_PLAN");
                sqlString.AppendLine("WHERE USERID = @PARAUSERID");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlString.ToString(), sqlConnection);
                da.SelectCommand.Parameters.AddWithValue("@PARAUSERID", userID);
                da.Fill(ds, "financial_PlanTable");
                int rec_cnt = ds.Tables["financial_PlanTable"].Rows.Count;
                if (rec_cnt == 0)
                {
                    financial_Plan_List = null;
                }
                else
                {
                    foreach (DataRow row in ds.Tables["financial_PlanTable"].Rows)
                    {
                        financial_Plan each_Financial_Plan = new financial_Plan();
                        each_Financial_Plan.plan_Id = Convert.ToInt32(row["plan_Id"]);
                        each_Financial_Plan.name = Convert.ToString(row["name"]);
                        each_Financial_Plan.matured_Age = Convert.ToInt32(row["matured_Age"]);
                        each_Financial_Plan.monthly_Contribution = Convert.ToDecimal(row["monthly_Contribution"]);
                        each_Financial_Plan.amount = Convert.ToDecimal(row["amount"]);
                        financial_Plan_List.Add(each_Financial_Plan);
                    }
                }
            }
            catch
            {
                //Exception
            }
            return financial_Plan_List;
        }

        //Deleting Financial Plan
        public void delete_Financial_Plan(int plan_Id)
        {
            try
            {
                StringBuilder sqlString = new StringBuilder();
                SqlCommand sqlCommand = new SqlCommand();
                sqlString.AppendLine("DELETE FROM FINANCIAL_PLAN");
                sqlString.AppendLine("WHERE PLAN_ID = @PARAPLAN_ID");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                sqlCommand = new SqlCommand(sqlString.ToString(), sqlConnection);
                sqlCommand.Parameters.AddWithValue("@PARAPLAN_ID", plan_Id);
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch
            {
                //Exception
            }
        }
    }
}