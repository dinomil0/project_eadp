﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class Class_ListDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        public void Add_Cart(string userID, string ProdId, string ProdName, int qty, double price)
        {
            Class_ProductDAO productDAO = new Class_ProductDAO();
            Class_Order Order = new Class_Order();
            Order.ProdId = ProdId;
            Order.ordId = ProdId + Order.getCount();
            Order.reference = productDAO.getProduct(ProdId).ProdName;
            Order.ProdName = ProdName;
            Order.price = Convert.ToString(price);
            Order.BuyQty = qty;
            Order.total = Convert.ToString(price * qty);
            Order.debit = Convert.ToString(price * qty);
            Order.userID = userID;
            Order.sellerID = productDAO.getSellerID(ProdId);

            StringBuilder sqlCommand = new StringBuilder();
            SqlCommand excommand = new SqlCommand();
            sqlCommand.AppendLine("INSERT INTO Cart");
            sqlCommand.AppendLine("VALUES (@paraordId, @paraProdId, @parauserID, @paraSellerID, @parareference, @paraProdName, @paraprice, @paraBuyQty, @paratotal, @paradebit)");
            SqlConnection myConn = new SqlConnection(DBConnect);
            excommand = new SqlCommand(sqlCommand.ToString(), myConn);
            excommand.Parameters.AddWithValue("paraordId", Order.ordId);
            excommand.Parameters.AddWithValue("paraProdId", Order.ProdId);
            excommand.Parameters.AddWithValue("parauserID", Order.userID);
            excommand.Parameters.AddWithValue("paraSellerID", Order.sellerID);
            excommand.Parameters.AddWithValue("parareference", Order.reference);
            excommand.Parameters.AddWithValue("paraProdName", Order.ProdName);
            excommand.Parameters.AddWithValue("paraprice", Order.price);
            excommand.Parameters.AddWithValue("paraBuyQty", Order.BuyQty);
            excommand.Parameters.AddWithValue("paratotal", Convert.ToDouble(Order.total));
            excommand.Parameters.AddWithValue("paradebit", Convert.ToDouble(Order.debit));
            myConn.Open();
            excommand.ExecuteNonQuery();
            myConn.Close();
        }
        public void Less_Cart(string userID, string ordId)
        {
            StringBuilder sqlCommand = new StringBuilder();
            SqlCommand excommand = new SqlCommand();
            sqlCommand.AppendLine("DELETE FROM Cart");
            sqlCommand.AppendLine("WHERE ordId = @paraordId AND userID = @parauserID");
            SqlConnection myConn = new SqlConnection(DBConnect);
            excommand = new SqlCommand(sqlCommand.ToString(), myConn);
            excommand.Parameters.AddWithValue("paraordId", ordId);
            excommand.Parameters.AddWithValue("parauserID", userID);
            myConn.Open();
            excommand.ExecuteNonQuery();
            myConn.Close();
        }
        public void Checkout(string userID, string type, int bankaccnum)
        {
            Class_ProductDAO productDAO = new Class_ProductDAO();
            Class_OrderDAO orderDAO = new Class_OrderDAO();
            Class_TransactionDAO transDAO = new Class_TransactionDAO();

            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Cart");
            sqlCommand.AppendLine("WHERE userID = @parauserID");
            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            da.Fill(ds, "CartTable");

            foreach (DataRow row in ds.Tables["CartTable"].Rows)
            {
                Class_ProductDAO prodDAO = new Class_ProductDAO();
                Class_Order order = new Class_Order();
                Class_Transaction transactionBuyer = new Class_Transaction();

                order.ordId = Convert.ToString(row["ordId"]);
                order.ProdId = Convert.ToString(row["ProdId"]);
                order.userID = Convert.ToString(row["userID"]);
                order.sellerID = Convert.ToString(row["sellerID"]);
                order.reference = Convert.ToString(row["reference"]);
                order.price = Convert.ToString(row["price"]);
                order.BuyQty = Convert.ToInt16(row["BuyQty"]);
                if (type == "VC")
                {
                    order.total = Convert.ToString(Convert.ToDouble(row["total"]) * 4) + "VC";
                }
                else if (type == "Card")
                {
                    order.total = "$" + Convert.ToString(row["total"]);
                }
                order.debit = order.total;
                order.date = DateTime.Now;
                order.status = "Pending";

                transactionBuyer.transID = order.ProdId + transactionBuyer.getCount();
                transactionBuyer.ordId = order.ordId;
                transactionBuyer.date = DateTime.Now;
                transactionBuyer.reference = order.reference;
                transactionBuyer.price = Convert.ToString(order.price);
                transactionBuyer.qty = order.BuyQty;
                transactionBuyer.total = Convert.ToString(row["total"]);
                if (type == "Card")
                {
                    transactionBuyer.debit = order.debit.Substring(1);
                }
                else if (type == "VC")
                {
                    transactionBuyer.debit = order.debit;
                }
                transactionBuyer.credit = "0.00";
                transactionBuyer.userID = userID;
                transactionBuyer.status = "Cleared";
                orderDAO.Add_Order(order);
                transDAO.Add_Transaction(transactionBuyer, type, bankaccnum);
                prodDAO.Less_Qty(order.ProdId, order.BuyQty);
            }
            Class_ListDAO listDAO = new Class_ListDAO();
            listDAO.clear_cart(userID);
        }
        public List<String> test_checkout(string userID)
        {
            List<String> ProdIdList = new List<String>();

            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Cart");
            sqlCommand.AppendLine("WHERE userID = @parauserID");
            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            da.Fill(ds, "CartTable");
            foreach (DataRow row in ds.Tables["CartTable"].Rows)
            {
                Class_Order order = new Class_Order();
                order.ProdId = Convert.ToString(row["ProdId"]);
                order.BuyQty = Convert.ToInt16(row["BuyQty"]);

                Class_ProductDAO productDAO = new Class_ProductDAO();
                if (productDAO.testLess_Qty(order.ProdId, order.BuyQty))
                {
                    continue;
                }
                else
                {
                    ProdIdList.Add(order.ProdId);
                }
            }
            return ProdIdList;
        }
        public void Add_SellerTrans(string ordId)
        {
            Class_TransactionDAO transDAO = new Class_TransactionDAO();

            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Orders");
            sqlCommand.AppendLine("WHERE ordId = @paraordId");
            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("paraordId", ordId);
            da.Fill(ds, "OrderTable");

            foreach (DataRow row in ds.Tables["OrderTable"].Rows)
            {
                Class_Order order = new Class_Order();
                Class_OrderDAO orderDAO = new Class_OrderDAO();
                Class_Transaction transactionSeller = new Class_Transaction();

                order.ordId = Convert.ToString(row["ordId"]);
                order.ProdId = Convert.ToString(row["ProdId"]);
                order.sellerID = Convert.ToString(row["sellerID"]);
                order.price = Convert.ToString(row["price"]);
                order.BuyQty = Convert.ToInt16(row["BuyQty"]);
                order.total = Convert.ToString(row["total"]);
                order.debit = Convert.ToString(row["debit"]);
                orderDAO.Update_Status(order.ordId, "Cleared");

                transactionSeller.transID = "S" + order.ProdId + transactionSeller.getCount();
                transactionSeller.ordId = order.ordId;
                transactionSeller.date = DateTime.Now;
                transactionSeller.reference = "SELL" + order.ordId;
                transactionSeller.price = order.price;
                transactionSeller.qty = order.BuyQty;
                if (order.total.Substring(order.total.Length - 2) == "VC")
                {
                    transactionSeller.total = Convert.ToString(Convert.ToDouble(order.total.Substring(0, order.total.Length-2)) / 4);
                }
                else
                {
                    transactionSeller.total = Convert.ToString(order.total).Substring(1);
                }
                transactionSeller.debit = "0.00";
                transactionSeller.credit = transactionSeller.total;
                transactionSeller.userID = order.sellerID;
                transactionSeller.status = "Cleared";
                Class_AccountDAO accDAO = new Class_AccountDAO();
                List<Class_Account.Account> accList = accDAO.getAccount(order.sellerID);
                long bankaccnum = Convert.ToInt64(accList[0].bankaccnum);
                transDAO.Add_Transaction(transactionSeller, "Card", bankaccnum);
            }

        }
        public void clear_cart(string userID)
        {
            StringBuilder sqlCommand = new StringBuilder();
            SqlCommand excommand = new SqlCommand();
            sqlCommand.AppendLine("DELETE FROM Cart");
            sqlCommand.AppendLine("WHERE userID = @parauserID");
            SqlConnection myConn = new SqlConnection(DBConnect);
            excommand = new SqlCommand(sqlCommand.ToString(), myConn);
            excommand.Parameters.AddWithValue("parauserID", userID);
            myConn.Open();
            excommand.ExecuteNonQuery();
            myConn.Close();
        }
    }
}