﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class WebForm5 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Session["custNRIC"] = "S9123456A";
                
                if (Session["custNRIC"] == null)
                {
                    Response.Redirect("landing.aspx");
                }
                var userID = Session["custNRIC"].ToString();
                if (Session["SSTransactionID"] != null)
                {

                    DAL.TransactionHistory trans = new DAL.TransactionHistory();
                    TransactionHistoryDAO transDAO = new TransactionHistoryDAO();
                    trans = transDAO.getTDbyUserTransactionIDmoreinfo(Convert.ToInt32(Session["SSTransactionID"]));

                    LblType.Text = trans.sendusing.ToString();
                    if (LblType.Text == "Phone Number")
                    {
                        LblRecipMobileNum.Text = trans.sendto.ToString();
                        LblRecipBankAccnumMobileNum.Text = trans.tobankaccnum.ToString();

                        customer cus = new customer();
                        customerDAO cusDAO = new customerDAO();
                        cus = cusDAO.getNamebyphonenum(Convert.ToInt32(trans.sendto.ToString()));

                        LblRecipNickNameMobileNum.Text = cus.name.ToString();

                        Accounts accs = new Accounts();
                        AccountsDAO accsDAO = new AccountsDAO();
                        accs = accsDAO.getbalanceBybankaccnum(Convert.ToInt32(trans.tobankaccnum.ToString()));

                        LblRecipBankAccnameMobileNum.Text = accs.bankaccname.ToString();
                        VerifyPhone.Visible = true;
                        VerifyNRIC.Visible = false;


                    }
                    else if(LblType.Text == "NRIC")
                    {
                        LblRecipNRIC.Text = trans.sendto.ToString();
                        LblRecipBankaccnumNRIC.Text = trans.tobankaccnum.ToString();

                        customer cus = new customer();
                        customerDAO cusDAO = new customerDAO();
                        cus = cusDAO.getNamebynric(trans.sendto.ToString());

                        LblRecipNickNameNRIC.Text = cus.name.ToString();

                        Accounts acc = new Accounts();
                        AccountsDAO accDAO = new AccountsDAO();
                        acc = accDAO.getbalanceBybankaccnum(Convert.ToInt32(trans.tobankaccnum.ToString()));

                        LblRecipBankaccnameNRIC.Text = acc.bankaccname.ToString();
                        VerifyNRIC.Visible = true;
                        VerifyPhone.Visible = false;
                    }

                    TbTransamount.Text = trans.amount.ToString("F");
                    Accounts accounts = new Accounts();
                    AccountsDAO accountsDAO = new AccountsDAO();
                    accounts = accountsDAO.getbalanceBybankaccnum(Convert.ToInt32(trans.bankaccnum.ToString()));

                    List<Accounts> bankaccnumListphone = new List<Accounts>();
                    int bankaccnum = Convert.ToInt32(bankacclist.SelectedItem);

                    bankaccnumListphone = accountsDAO.getbackaccnumbyuserID(userID);

                    bankacclist.Items.Clear();
                    bankacclist.Items.Insert(0, new ListItem("--Select--", "0"));
                    bankacclist.AppendDataBoundItems = true;
                    bankacclist.DataTextField = "bankaccname";
                    bankacclist.DataValueField = "bankaccnum";
                    bankacclist.DataSource = bankaccnumListphone;
                    bankacclist.DataBind();
                    var bankaccname = accounts.bankaccname.ToString();

                    if (bankaccname != null)
                    {
                        bankacclist.SelectedValue = bankaccname;
                    }
                    

                }
                else
                {
                    Response.Redirect("TransactionHistory.aspx");
                }
            }
        }

        protected void bankacclist_SelectedIndexChanged(object sender, EventArgs e)
        {
            Accounts accObj = new Accounts();
            AccountsDAO accDAO = new AccountsDAO();
            TransactionHistory thObj = new TransactionHistory();
            TransactionHistoryDAO thDAO = new TransactionHistoryDAO();
            string userID = Session["custNRIC"].ToString();

            accObj = accDAO.getbalanceBybankaccnum(Convert.ToInt32(bankacclist.SelectedValue));

            if (bankacclist.SelectedValue == "0")
            {
                Lbl_verifyerror.Text += "Please select a valid option";
            }
            else if (accObj.bankaccnum == Convert.ToInt32(bankacclist.SelectedValue))
            {
                Lblbankaccbalance.Text = "$" + accObj.balance.ToString("F");
            }
            else
            {
                Lbl_verifyerror.Text += "Account not found in user";
            }
        }

        protected void cancelButtonPayYouVerify_Click(object sender, EventArgs e)
        {
            Response.Redirect("TransactionHistory.aspx");
        }

        protected void submitButtonPayYouVerify_Click(object sender, EventArgs e)
        {
            DateTime date = DateTime.Now;
            double amount = Convert.ToDouble(TbTransamount.Text.ToString());
            //Session["userID"] = "S1234567A";
            string userID = Session["custNRIC"].ToString();
            string sendto = "";
            int frombankaccnum = Convert.ToInt32(bankacclist.SelectedValue);
            int tobankaccnum = 0;
            if (LblType.Text == "Phone Number")
            {
                sendto = LblRecipMobileNum.Text.ToString();
                tobankaccnum = Convert.ToInt32(LblRecipBankAccnumMobileNum.Text.ToString());
                Session["PayYouType"] = LblType.Text.ToString();
                // Session PhoneNum
                Session["PayYouPhonenum"] = LblRecipMobileNum.Text;
                Session["PayYounamePhoneNum"] = LblRecipNickNameMobileNum.Text;
                Session["PayYouRecipBankaccnumPhoneNum"] = LblRecipBankAccnumMobileNum.Text;
                Session["PayYouRecipBankaccnamePhoneNum"] = LblRecipBankAccnameMobileNum.Text;
                Session["PayYouamountPhoneNum"] = TbTransamount.Text;
                Session["PayYoubankaccPhoneNum"] = bankacclist.SelectedValue;
            }
            else if (LblType.Text == "NRIC")
            {
                sendto = LblRecipNRIC.Text.ToString();
                tobankaccnum = Convert.ToInt32(LblRecipBankaccnumNRIC.Text.ToString());
                Session["PayYouNRIC"] = LblRecipNRIC.Text;
                Session["PayYounameNRIC"] = LblRecipNickNameNRIC.Text;
                Session["PayYouRecipBankaccnumNRIC"] = LblRecipBankaccnumNRIC.Text;
                Session["PayYouRecipBankaccnameNRIC"] = LblRecipBankaccnameNRIC.Text;
                Session["PayYouamountNRIC"] = TbTransamount.Text;
                Session["PayYoubankaccNRIC"] = bankacclist.SelectedValue.ToString();
            }
            try
            {
                TransactionHistoryDAO newdata = new TransactionHistoryDAO();
                int insCnt = newdata.InsertTransactionsWithdrawal(date, "Transfer", "Withdrawal", amount, "Pending", userID, sendto, frombankaccnum, LblType.Text, tobankaccnum, frombankaccnum);
                if (insCnt == 1)
                {
                    Session["Timer"] = DateTime.Now.AddSeconds(30).ToString();
                    Response.Redirect("TransferPayYouCompletion.aspx");
                }
                else
                {
                    Lbl_confirm.Text = "Transaction failed";
                }
                submitButtonPayYouVerify.Enabled = false;
            }
            catch (Exception f)
            {
                Lbl_confirm.Text = f.ToString();
            }
        }
    }
}