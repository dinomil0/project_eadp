﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace project
{
    public partial class bankHome : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }
            string userID = Session["custNRIC"].ToString();
            customer cusObj = new customer();
            customerDAO cusDao = new customerDAO();

            //get Total account balance and amount owed
            Accounts accObj = new Accounts();
            AccountsDAO accDAO = new AccountsDAO();
            List<double> bankacctotal = new List<double>();

            bankacctotal = accDAO.getTotalbalancefromALLbankacc(userID);
            LblTotalMoney.Text = "$ " + bankacctotal[0].ToString("F");
            LblTotalOwe.Text = "$ " + bankacctotal[1].ToString();

            //bargraph data

            cusObj = cusDao.GetCustomerLoginCredential(userID);

            if (cusObj == null)
            {
                lbl_Savings.Text = "Transaction History not found!";
            }
            else
            {
                //  Call timeDeposit to get all TD for the customer 
                SavingsDAO saveDAO = new SavingsDAO();
                List<Savings> SaveList = new List<Savings>();
                SaveList = saveDAO.getTDbyuserID(userID);
                SavingsGridView.DataSource = SaveList;
                SavingsGridView.DataBind();
            }
            List<Accounts> bankaccdetails = new List<Accounts>();
            AccountsDAO accsDAO = new AccountsDAO();
            bankaccdetails = accsDAO.getAccdetailsuserID(userID);
            int count = bankaccdetails.Count();
            DataTable table = new DataTable();
            table.Columns.Add("bankaccname");
            table.Columns.Add("cardNum");
            table.Columns.Add("balance");
            for (int i = 0; i < count; i++)
            {
                Accounts acc = new Accounts();
                acc = bankaccdetails[i];
                var cardnum = Regex.Replace(acc.cardNum.ToString(), @"\b[0-9]{12}(?=[0-9]{4}\b)", new string('X', 12));
                table.Rows.Add(acc.bankaccname, cardnum, "$" + acc.balance.ToString("F"));
            }
            bankAccDetails.DataSource = table;
            bankAccDetails.DataBind();

            //Savings Date Filter
            NewSavingDate.Attributes.Add("min", (DateTime.Now.ToString("yyyy-MM-dd")));

            
        }
        public string bargraph()
        {
            //Session["userID"] = "S1234567A";
            string userID = Session["custNRIC"].ToString();
            TransactionHistoryDAO thDAO = new TransactionHistoryDAO();
            List<double> bargraph = new List<double>();
            bargraph = thDAO.getbargraphdata(userID);
            string val = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(bargraph);
            return val;
        }

        
        public string Gaugegraph()
        {
            //Session["userID"] = "S1234567A";
            string userID = Session["custNRIC"].ToString();
            SavingsDAO savDAO = new SavingsDAO();
            Dictionary<string, double> savDict = new Dictionary<string, double>();
            //List<List<string>> savList = new List<List<string>>();
            savDict = savDAO.getGaugechartdata(userID);
            string val = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(savDict);
            return val;
        }
        protected void SavingsGridView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void AddSavings_Click1(object sender, EventArgs e)
        {
            Response.Redirect("newsavingGoal.aspx");
        }

        protected void SavingsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var userID = Session["custNRIC"].ToString();
            if (e.CommandName == "update")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = SavingsGridView.Rows[index];
                Session["SSsavingID"] = row.Cells[0].Text;
                Response.Redirect("updatesavingGoal.aspx");
            }
            else if (e.CommandName == "delete")
            {
                Savings savings = new Savings();
                SavingsDAO saveDAO = new SavingsDAO();

                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = SavingsGridView.Rows[index];

                int savID = Convert.ToInt32(row.Cells[0].Text.ToString());
                savings = saveDAO.getTDbyUserIDobj(userID, savID);
                //get bankacc
                //Insert Transactions deposit
                TransactionHistoryDAO newdata = new TransactionHistoryDAO();
                int insCnt = newdata.InsertTransactionsNormal(DateTime.Now, "Savings",
                                                        "Deposit", Convert.ToDouble(row.Cells[2].Text),
                                                        "Successful", userID, Convert.ToInt32(savings.bankaccnum), Convert.ToInt32(savings.bankaccnum));
                // Update acc balance for deposit
                //Get acc balance and plus
                Accounts accsObj = new Accounts();
                AccountsDAO accsDAO = new AccountsDAO();

                accsObj = accsDAO.getbankaccbalance(Convert.ToInt32(savings.bankaccnum));
                double addamount = Convert.ToDouble(row.Cells[2].Text);
                accsObj.balance = accsObj.balance + addamount;

                //update balance
                AccountsDAO updateDAOdeposit = new AccountsDAO();
                updateDAOdeposit.updatebankaccbalance(Convert.ToInt32(savings.bankaccnum), accsObj.balance);

                //Delete
                saveDAO.DeleteTDbysavingsID(Convert.ToInt32(row.Cells[0].Text));

                Response.Redirect("bankHome.aspx");
            }
            else if (e.CommandName == "transfer")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = SavingsGridView.Rows[index];
                Session["SSsavingID"] = row.Cells[0].Text;
                Response.Redirect("TransferToSavings.aspx");
            }
        }
        public void showModal(object sender, EventArgs e)
        {
            var userID = Session["custNRIC"].ToString();
            AccountsDAO accDAO = new AccountsDAO();
            List<Accounts> bankaccnumList = new List<Accounts>();

            bankaccnumList = accDAO.getbackaccnumbyuserID(userID);

            ddlbankacclist.Items.Clear();
            ddlbankacclist.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlbankacclist.AppendDataBoundItems = true;
            ddlbankacclist.DataTextField = "bankaccname";
            ddlbankacclist.DataValueField = "bankaccnum";
            ddlbankacclist.DataSource = bankaccnumList;
            ddlbankacclist.DataBind();

            ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal()", true);
        }

        protected void closebuttonmodalSavings_Click(object sender, EventArgs e)
        {
            Response.Redirect("bankHome.aspx");
        }

        protected void submitbuttonmodalSavings_Click(object sender, EventArgs e)
        {
            var userID = Session["custNRIC"].ToString();
            try
            {
                if (validate_Input())
                {
                    double savingGoal = Convert.ToDouble(NewSavingGoalamt.Text.ToString());
                    double amountSaved = Convert.ToDouble(NewSavingamtnone.Text.ToString());
                    String description = NewSavingdescription.Text.ToString();
                    //DateTime endDate = Convert.ToDateTime(NewSavingGoalamt.Text);
                    DateTime endDate = Convert.ToDateTime(NewSavingDate.Text.ToString());
                    DateTime startDate = DateTime.Now.Date;
                    try
                    {
                        SavingsDAO newdata = new SavingsDAO();
                        int insCnt = newdata.InsertTD(savingGoal, amountSaved, description, endDate, startDate, userID, 0, Convert.ToInt32(ddlbankacclist.SelectedValue));
                        if (insCnt == 1)
                        {
                            lbl_Savings.Text = "Savings Created";
                            Response.Redirect(Request.RawUrl);
                        }
                        else
                        {
                            lbl_Savings.Text = "Savings failed";
                        }
                        submitbuttonmodalSavings.Enabled = false;
                    }
                    catch (Exception f)
                    {
                        lbl_Savings.Text = f.ToString();
                    }
                }
                else
                {
                    lbl_Savings.Visible = true;
                }
            }
            catch
            {


            }
        }
        protected Boolean validate_Input()
        {
            double amount;
            DateTime date;
            try
            {
                lbl_Savings.Text = null;
                if (String.IsNullOrEmpty(NewSavingdescription.Text))
                {
                    lbl_Savings.Text += "Please enter the description" + "<br/>";
                }
                if (!DateTime.TryParse(NewSavingDate.Text, out date))
                {
                    lbl_Savings.Text += "Please enter the correct date" + "<br/>";
                }
                if (!double.TryParse(NewSavingGoalamt.Text, out amount))
                {
                    lbl_Savings.Text += "Please enter a valid amount!" + "<br/>";
                }
                if (String.IsNullOrEmpty(lbl_Savings.Text))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        protected void SavingsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string userID = Session["custNRIC"].ToString();
            NotificationsDAO notiDAO = new NotificationsDAO();
            List<string> notiList = new List<string>();
            notiList = notiDAO.getAllNotificationsbyuserID(userID);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                SavingsDAO savDAO = new SavingsDAO();
                List<Savings> savList = new List<Savings>();
                savList = savDAO.getTDbyuserID(userID);

                foreach (Savings savings in savList)
                {
                    if (savings.status == 0)
                    {
                        if ((Convert.ToDateTime(savings.endDate) - DateTime.Now).TotalDays < 30)
                        {
                            var difference = (Convert.ToDateTime(savings.endDate) - DateTime.Now).TotalDays;
                            notiDAO.InsertNotifications("Your Savings Goal for " + savings.description + " is ending in " + Math.Round(difference, 0) + " days", userID);
                            savDAO.UpdatestatusForuserID(savings.savingsID, 1);
                        }
                    }
                    else
                    {
                        
                    }
                        
                }
            }
        }
    }
}