﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class updatesavingGoal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userID = Session["custNRIC"].ToString();
            if (Session["custNRIC"] == null)
            {
                Response.Redirect("landing.aspx");
            }
            if (Session["SSsavingID"] != null)
            {
                LblsavingsID.Text = Session["SSsavingID"].ToString();

                Savings savTD = new Savings();
                SavingsDAO updTD = new SavingsDAO();
                savTD = updTD.getTDbyUserSavingID(Convert.ToInt32(Session["SSsavingID"]));


                LbluserID.Text = savTD.userID.ToString();
                LblsavingGoal.Text = savTD.savingGoal.ToString();
                LblsavingSaved.Text = savTD.amountsaved.ToString();
                string startdate = savTD.startDate.ToString("dd/MM/yyyy");
                LblstartDate.Text = startdate;
                string enddate = savTD.endDate.ToString("dd/MM/yyyy");
                LblendDate.Text = enddate;
                Lbldescription.Text = savTD.description.ToString();
                ViewState["currRenewMode"] = savTD.RenewMode;
            }
            else
            {
                Response.Redirect("bankHome.aspx");
            }

            //Filter
            NewEndDate.Attributes.Add("min", (DateTime.Now.ToString("yyyy-MM-dd")));

        }

        protected void BtnUpdate_Click(object sender, EventArgs e)
        {
            Savings savTD = new Savings();
            SavingsDAO uptTD = new SavingsDAO();
            bool result;
            int updsavingGoal;

            double amt;
            result = double.TryParse(newsavingGoal.Text, out amt);
            if (result == false)
            {
                LblResult.Text += "Please input your new savings goal";
            }
            else
            {
                if (newsavingGoal.Text == LblsavingGoal.Text)
                {
                    LblResult.Text = "You have not changed your savings amount";
                }
                else
                {
                    LblsavingGoal.Text = newsavingGoal.Text;
                    updsavingGoal = uptTD.UpdateTDforsavingGoal(Convert.ToInt32(LblsavingsID.Text), Convert.ToDouble(LblsavingGoal.Text));
                    if (updsavingGoal != 1)
                    {
                        LblResult.Text = "You have failed to update your savings goal";
                    }
                    else
                    {
                        LblResult.Text += "You have successfully changed your savings goal" + "<br/>";
                    }

                }
                Response.Redirect("bankHome.aspx");
            }
            DateTime newdate;
            result = DateTime.TryParse(NewEndDate.Text, out newdate);
            if (result == false)
            {
                LblResult.Text += "Please input your new end date";
            }
            else
            {
                if (NewEndDate.Text == LblendDate.Text)
                {
                    LblResult.Text = "You have not changed your end date";
                }
                else
                {
                    //string changedDate = Convert.ToDateTime(NewEndDate.Text).ToString("dd/mm/yyyy");
                    LblendDate.Text = NewEndDate.Text;
                    updsavingGoal = uptTD.UpdateTDforendDate(Convert.ToInt32(LblsavingsID.Text), Convert.ToDateTime(LblendDate.Text));
                    if (updsavingGoal != 1)
                    {
                        LblResult.Text = "You have failed to update your end date";
                    }
                    else
                    {
                        LblResult.Text += "You have successfully changed your end date" + "<br/>";
                    }

                }
                Response.Redirect("bankHome.aspx");
            }
        }

        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("bankHome.aspx");
        }

    }
}