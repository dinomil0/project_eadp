﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class commerce_Enquiries : System.Web.UI.Page
    {
        string userID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            userID = Session["custNRIC"].ToString();
            Class_InquiryDAO inquiryDAO = new Class_InquiryDAO();
            List<Class_Inquiry> enquiries = inquiryDAO.get_buyinquiry(userID);
            int count = enquiries.Count();
            if (count > 0)
            {
                DataTable table = new DataTable();
                table.Columns.Add("InquiryId");
                table.Columns.Add("ordId");
                table.Columns.Add("UserInput");
                table.Columns.Add("SellerInput");
                table.Columns.Add("InquiryDesc");
                for (int i = 0; i < count; i++)
                {
                    Class_Inquiry inquiry = new Class_Inquiry();
                    if (enquiries.Any())
                    {
                        inquiry = enquiries[i];
                        table.Rows.Add(inquiry.InquiryId, inquiry.ordId, inquiry.UserInput, inquiry.SellerInput, inquiry.InquiryDesc);
                    }
                }
                BuyInquiryList.DataSource = table;
                BuyInquiryList.DataBind();
            }
            List<Class_Inquiry> enquiries1 = inquiryDAO.get_sellinquiry(userID);
            int count1 = enquiries1.Count();
            if (count1 > 0)
            {
                DataTable table1 = new DataTable();
                table1.Columns.Add("InquiryId");
                table1.Columns.Add("ordId");
                table1.Columns.Add("UserInput");
                table1.Columns.Add("SellerInput");
                table1.Columns.Add("InquiryDesc");
                for (int i = 0; i < count1; i++)
                {
                    Class_Inquiry inquiry = new Class_Inquiry();
                    if (enquiries1.Any())
                    {
                        inquiry = enquiries1[i];
                        table1.Rows.Add(inquiry.InquiryId, inquiry.ordId, inquiry.UserInput, inquiry.SellerInput, inquiry.InquiryDesc);
                    }
                }
                SellInquiryList.DataSource = table1;
                SellInquiryList.DataBind();
            }
        }
        Class_InquiryDAO inquiryDAO = new Class_InquiryDAO();
        protected void btn_buyRefund_Click(object sender, CommandEventArgs e)
        {
            string InquiryId = e.CommandArgument.ToString();
            inquiryDAO.update_inquiry(InquiryId, "buyer", "Refund");
            inquiryDAO.check_inquiry(InquiryId);
            Response.Redirect("commerce_Enquiries.aspx");
        }

        protected void btn_buyReplace_Click(object sender, CommandEventArgs e)
        {
            string InquiryId = e.CommandArgument.ToString();
            inquiryDAO.update_inquiry(InquiryId, "buyer", "Replace");
            inquiryDAO.check_inquiry(InquiryId);
            Response.Redirect("commerce_Enquiries.aspx");
        }

        protected void btn_sellRefund_Click(object sender, CommandEventArgs e)
        {
            string InquiryId = e.CommandArgument.ToString();
            inquiryDAO.update_inquiry(InquiryId, "seller", "Refund");
            inquiryDAO.check_inquiry(InquiryId);
            Response.Redirect("commerce_Enquiries.aspx");
        }

        protected void btn_sellReplace_Click(object sender, CommandEventArgs e)
        {
            string InquiryId = e.CommandArgument.ToString();
            inquiryDAO.update_inquiry(InquiryId, "seller", "Replace");
            inquiryDAO.check_inquiry(InquiryId);
            Response.Redirect("commerce_Enquiries.aspx");
        }

        protected void btn_Accept_Click(object sender, CommandEventArgs e)
        {
            string InquiryId = e.CommandArgument.ToString();
            inquiryDAO.remove_inquiry(InquiryId);
        }
    }
}