﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="newsavingGoal.aspx.cs" Inherits="project.newsavingGoal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<section id="newSavingGoal" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
    <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="bankHome.aspx">Bank Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">New Saving Goal</li>
          </ol>
    </nav>
    <h2 class="text-center display-4">New Saving Goal</h2>
    <div class="container">
    <div class="form-group">
        <asp:Label ID="LblNewSavingGoal" runat="server" Text="Saving Goal:"></asp:Label>
        <asp:TextBox ID="NewSavingGoalamt" runat="server" class="form-control" placeholder="Enter saving goal amount"></asp:TextBox>
    </div>
    <div class="form-group">
        <asp:Label ID="LblNewSavingamt" runat="server" Text="Saved Amount:"></asp:Label>
        <asp:Label ID="NewSavingamtnone" runat="server" Text="0"></asp:Label>
    </div>
    <div class="form-group">
        <asp:Label ID="LblNewSavingdescription" runat="server" Text="Description:"></asp:Label>
        <asp:TextBox ID="NewSavingdescription" runat="server" class="form-control" placeholder="What are you saving for?"></asp:TextBox>
    </div>
    <div class="form-group">
    <asp:Label ID="LblNewSavingDate" runat="server" Text="End Date:"></asp:Label>
    <div class='input-group date' id='datetimepicker'>
        <asp:TextBox ID="NewSavingDate" runat="server" type="date" class="form-control"></asp:TextBox>
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
    <small id="" class="form-text text-muted">When can you save by?</small>
    </div>
    <div class="modal-footer">
        <asp:Button ID="closebuttonmodalSavings" runat="server" Text="Back" type="reset" data-dismiss="modal" class="btn btn-info" OnClick="closebuttonmodalSavings_Click"/> &nbsp;&nbsp;
        <!-- Submit Button -->
        <asp:Button ID="submitbuttonmodalSavings" runat="server" Text="Submit" type="submit" data-dismiss="modal" class="btn btn-success" OnClick="submitbuttonmodalSavings_Click"/>
    </div>
    <asp:Label ID="lbl_Savings" runat="server" Text=""></asp:Label>
</div>
</section>

</asp:Content>
