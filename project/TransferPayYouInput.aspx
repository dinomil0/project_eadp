﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="TransferPayYouInput.aspx.cs" Inherits="project.TransferPayYouInput" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
    hr{
        border:1px solid orange
    }
    .auto-style2 {
        margin-left: 21px;
    }
    .auto-style3 {
        margin-left: 0px
    }
    .auto-style5 {
        margin-left: 34px;
    }
    .auto-style6 {
        margin-left: 16px;
    }
    .auto-style7 {
        margin-left: 73px;
    }
    .auto-style8 {
        margin-left: 31px;
    }
    .auto-style9 {
        margin-left: 25px;
    }
</style>
<section id="TransferPayNowInput" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
  
    <div class="container">
        <label>
            <h1>PayYou</h1>
            <h3>Make a transaction</h3>
        </label>
        <ul class="progress-indicator">
            <li class="completed">
                <span class="bubble"></span>
                <i class="fa fa-check-circle"></i>
                1.Input Details
            </li>
            <li class="">
                <span class="bubble"></span>
                <i class="fa fa-check-circle"></i>
                2.Verify Details
            </li>
            <li>
                <span class="bubble"></span>
                3.Completion
            </li>
        </ul>
        <hr />
        <h5>Receipient Details</h5>
        	
            <!-- Type Button -->
            <asp:Label ID="Lbl_err" runat="server" Text=""></asp:Label>
            <br />
            <label for="type_transaction" class="control-label">Type:</label>
                <%--<asp:DropDownList CssClass="" ID="InputType" runat="server" onchange="HideShowDiv();" OnSelectedIndexChanged="InputType_SelectedIndexChanged" AutoPostBack="True">--%>
                <asp:DropDownList CssClass="btn btn-secondary dropdown-toggle" ID="InputType" runat="server" OnSelectedIndexChanged="InputType_SelectedIndexChanged" AutoPostBack="True">
                    <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                    <asp:ListItem Value="Phone Number">Phone Number</asp:ListItem>
                    <asp:ListItem Value="NRIC">NRIC</asp:ListItem>
                </asp:DropDownList>
           <%-- Phone Number Show Hide Details --%>
            <asp:Panel ID="panelPhone" runat="server" Visible="False">
               <%-- Country Code :&nbsp;&nbsp; <asp:TextBox ID="txtCC" runat="server" CssClass="auto-style9" Width="173px"></asp:TextBox>--%>
               <%-- <br />--%>
                Phone Number: &nbsp;&nbsp;<asp:TextBox ID="txtPhoneNumber" runat="server"></asp:TextBox>
                <%--<asp:Button ID="searchButtonPhoneNumber" runat="server" Text="Search" type="submit" class="btn btn-primary" OnClick="searchButtonPhoneNumber_Click" UseBehaviorSubmit="false" OnClientClick="return false;"/>--%>
                <asp:Button ID="searchButtonPhoneNumber" runat="server" Text="Search" type="submit" OnClick="searchButtonPhoneNumber_Click" CssClass="auto-style2 btn btn-success" style="margin-left: 10px" />
                <br />
                <asp:Label runat="server" Text="Receipient's Nickname:"></asp:Label>
                <asp:Label ID="PNNickname" runat="server" Text=""></asp:Label>
                <br />
                <asp:Label runat="server" Text="Receipient's Bank Account Number:"></asp:Label>
                <asp:Label ID="PNBankaccnum" runat="server" Text=""></asp:Label>
                <br />
                <asp:Label runat="server" Text="Receipient's Bank Account Name:"></asp:Label>
                <asp:Label ID="PNBankaccname" runat="server" Text=""></asp:Label>
                <div style="">
                <hr />
                <h5>Transfer Details</h5>
                <div class="d-flex">
                      <label for="type_transaction" class="label label-default">Transfer Amount:</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">$</span>
                      </div>
                        <asp:TextBox ID="txtTransferAmtPhone" runat="server" CssClass="auto-style3" Width="172px" ></asp:TextBox><br /> 
                     </div>
                </div>
               
                <label for="type_transaction" class="control-label">From account:</label>
                <asp:DropDownList ID="AccountsListDropdownPhone" runat="server" CssClass="auto-style5 btn btn-secondary dropdown-toggle" Width="226px" OnSelectedIndexChanged="AccountsListDropdownPhone_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList>
                <br />
                SGD Balance &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="LblSGDBalPhone" runat="server" Text=""></asp:Label>
            </div>
            </asp:Panel>
    
           <asp:Panel ID="panelNRIC" runat="server" Visible="False">
             <%-- NRIC Show Hide Details --%>
           <div id="NRICShowHide" style="">
            <%--<div id="NRICShowHide" style="">--%>
                NRIC :&nbsp;&nbsp; <asp:TextBox ID="txtNRIC" runat="server" CssClass=""></asp:TextBox>
                <asp:Button ID="searchButtonNRIC" runat="server" Text="Search" type="submit" OnClick="searchButtonNRIC_Click" CssClass="auto-style6 btn btn-success" />
                <br /><asp:Label runat="server" Text="Receipient's Nickname:"></asp:Label>
                <asp:Label ID="NRICNickname" runat="server" Text=""></asp:Label>
                <br />
               <asp:Label runat="server" Text="Receipient's Bank Account Number:"></asp:Label>
                <asp:Label ID="NRICBankaccnum" runat="server" Text=""></asp:Label>
                <br />
                <asp:Label runat="server" Text="Receipient's Bank Account Name:"></asp:Label>
                <asp:Label ID="NRICBankaccname" runat="server" Text=""></asp:Label>
                <div style="">
            </div>
               <div style="">
                <hr />
                <h5>Transfer Details</h5>
                <div class="d-flex">
                      <label for="type_transaction" class="label label-default">Transfer Amount:</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">$</span>
                      </div>
                        <asp:TextBox ID="txtTransferAmtNRIC" runat="server" CssClass="auto-style3" Width="172px" ></asp:TextBox><br /> 
                     </div>
                </div>
                <label for="type_transaction" class="control-label">From account:</label>
                <%-- Get from database --%>
               <asp:DropDownList ID="AccountsListDropdownNRIC" runat="server" OnSelectedIndexChanged="AccountsListDropdownNRIC_SelectedIndexChanged" CssClass="auto-style8 btn btn-secondary dropdown-toggle" Width="226px" AutoPostBack="True">
                </asp:DropDownList>
                <br />
                SGD Balance: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="LblSGDBalNRIC" runat="server" Text=""></asp:Label>
            </div>
            </asp:Panel>
        <br />


            
            

        <div class="d-flex">
	        <div class="form-group">
                <!-- Cancel Button -->
                <asp:Button ID="cancelButtonPayYouInput" runat="server" Text="Cancel" type="reset" class="btn btn-danger" OnClick="cancelButtonPayYouInput_Click"/> &nbsp;&nbsp;
                <!-- Submit Button -->
                <asp:Button ID="submitButtonPayYouInput" runat="server" Text="Confirm" type="submit" class="btn btn-primary" OnClick="submitButtonPayYouInput_Click" />
            </div>
        </div>
    			


    </div>
</section>


</asp:Content>

