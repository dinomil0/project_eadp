﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="ConvertCurrencies.aspx.cs" Inherits="project.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<section id="ConvertCurrencies" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
    <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="CurrencyWallet.aspx">Currency Wallet</a></li>
            <li class="breadcrumb-item active" aria-current="page">Convert to Currencies</li>
          </ol>
    </nav>
    <div class="row">
        <div class="col-4 mx-auto my-auto ">
            <div class="d-flex text-center">
              <div>
                  <h4> Transfer from: </h4>
              </div>
              <div>
                  <asp:DropDownList ID="Ddlchosenbankacc" CssClass="btn btn-secondary dropdown-toggle" runat="server" OnSelectedIndexChanged="Ddlchosenbankacc_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
              </div>
          </div>
            <asp:Label runat="server" Text="Balance:"></asp:Label>
            <asp:Label ID="LblTransferBalance" runat="server" Text=""></asp:Label>
        </div>
          <div class="col-4 text-center mt-5 ">
            <div class="input-group" style="left: 0px; top: 0px">
                <div class="input-group-prepend mx-auto my-auto">
                    <span class="input-group-text">$</span>
                    <asp:TextBox ID="TxtAmountinput" type="number" runat="server" step=".01" OnTextChanged="TxtAmountinput_TextChanged" AutoPostBack="true" ></asp:TextBox>
                    <%--<input type="text" id="TxtAmountinput" class="form-control" aria-label="Amount (to the nearest dollar)"/>--%>
                </div>
            </div>
              <asp:Button ID="BtnConvertCurrencies" runat="server" Text="Transfer" class="btn btn-success mt-2" OnClick="BtnConvertCurrencies_Click" OnClientClick="return confirm('Confirm transfer?')"/>
              <h5>
                  <asp:Label ID="Lbltransfercurrencies" runat="server" Text=""></asp:Label></h5>
          </div>
           <div class="col-4">
               <div class="d-flex text-center mt-5">
                  <div>
                      <h4> Transfer To: </h4>
                  </div>
                <div>
                    <h4><asp:Label ID="Lblconvertedcurrencies" runat="server" Text=""></asp:Label></h4>
                </div>
                </div>
               <asp:Label runat="server" Text="Balance:"></asp:Label>
                <asp:Label ID="LblcurrencyBalance" runat="server" Text=""></asp:Label>
           </div>
    <asp:Label ID="Lbl_error" runat="server" Text=""></asp:Label>
</section>

</asp:Content>
