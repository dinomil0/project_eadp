﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class financial_Planning_New : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }

                if (!IsPostBack)
                {
                    int customer_Age;
                    customerDAO read_CustomerDAO = new customerDAO();
                    customer_Age = read_CustomerDAO.read_Customer_Age(Session["custNRIC"].ToString());

                    //Retrieving Suggested Plans based on Customer's Age
                    Dictionary<int, List<string>> plan_Dict = new Dictionary<int, List<string>>
                {
                    {29, new List<string> {"Party", }},
                    {39, new List<string> {"Education"} },
                    {49, new List<string> {"Sole Proprietorship", "Entrepreneur"} },
                    {69, new List<string> {"Retirement"} },
                    {79, new List<string> {"Holiday Trip", "Other"} },
                };
                    ddl_Fp_Description.Items.Add(new ListItem("--Select--", "--Select--"));
                    foreach (KeyValuePair<int, List<string>> plan_Range in plan_Dict)
                    {
                        if (Convert.ToInt32(Session["custAge"]) < plan_Range.Key)
                        {
                            foreach (string plan_Item in plan_Range.Value)
                            {
                                ddl_Fp_Description.Items.Add(new ListItem(plan_Item, plan_Item));
                            }
                        }
                    }

                    //Retrieving ddl_Fp_Age Values based on Customer's Age
                    Dictionary<int, string> age_Dict = new Dictionary<int, string>
                {
                    {29, "20 - 29" },
                    {39, "30 - 39" },
                    {49, "40 - 49" },
                    {59, "50 - 59" },
                    {69, "60 - 69" },
                    {79, "70 - 79" },
                };
                    ddl_Fp_Age.Items.Add(new ListItem("--Select--", "--Select--"));
                    foreach (KeyValuePair<int, string> age_Range in age_Dict)
                    {
                        if (customer_Age < age_Range.Key)
                        {
                            ddl_Fp_Age.Items.Add(new ListItem(age_Range.Value, (age_Range.Key).ToString()));
                        }
                    }
                    Session["custAge"] = customer_Age;
                }

                //Retrieving Previous Data Customer entered
                if (Session["financial_Plan"] != null)
                {
                    List<object> render_Data = (List<object>)Session["financial_Plan"];
                    ddl_Fp_Description.SelectedValue = render_Data[0].ToString();
                    if (ddl_Fp_Description.SelectedValue == "Other")
                    {
                        pn_Fp_Other.Visible = true;
                    }
                    tb_Fp_Other.Text = render_Data[1].ToString();
                    ddl_Fp_Age.SelectedValue = render_Data[2].ToString();
                    tb_Fp_Duration.Text = render_Data[3].ToString();
                    tb_Fp_Amount.Text = render_Data[4].ToString();
                    Session.Remove("financial_Plan");
                }            
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        protected void ddl_Fp_Description_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddl_Fp_Description.SelectedValue == "Other")
                {
                    pn_Fp_Other.Visible = true;
                }
                else
                {
                    pn_Fp_Other.Visible = false;
                }
            }
            catch
            {
                //Exception
            }
        }

        protected void ddl_Fp_Age_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string years_Left = null;
                int customer_Age = Convert.ToInt32(Session["custAge"]);
                int maximum_Years_Left = Convert.ToInt32(ddl_Fp_Age.SelectedValue) - customer_Age;
                int minimum_Years_Left = Convert.ToInt32(ddl_Fp_Age.SelectedValue) - 9;
                if (minimum_Years_Left <= customer_Age)
                {
                    years_Left += "1 - " + maximum_Years_Left + " years";
                }
                else
                {
                    years_Left += Convert.ToString((minimum_Years_Left - customer_Age)) + " - " + Convert.ToString(maximum_Years_Left) + " years";
                }

                tb_Fp_Duration.Text = years_Left;
            }
            catch
            {
                //Exception
            }
        }

        protected void btn_Fp_Confirm_Click(object sender, EventArgs e)
        {
            try
            {
                if (validate_Input())
                {
                    List<object> store_Data = new List<object> { ddl_Fp_Description.SelectedValue, tb_Fp_Other.Text, ddl_Fp_Age.SelectedValue, tb_Fp_Duration.Text, Convert.ToDecimal(tb_Fp_Amount.Text) };
                    Session["financial_Plan"] = store_Data;
                    Response.Redirect("financial_Planning_Analysis.aspx");
                }
                else
                {
                    lb_Fp_Error.Visible = true;
                }

            }
            catch
            {
                //Exception
            }
        }

        protected void btn_Fp_Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("financial_Planning_Overview.aspx");
        }

        protected Boolean validate_Input()
        {
            decimal amount;
            try
            {
                lb_Fp_Error.Text = null;
                if (ddl_Fp_Description.SelectedValue == "--Select--")
                {
                    lb_Fp_Error.Text += "Please Select a Valid Plan! <br>";
                }
                if (ddl_Fp_Description.SelectedValue == "Other" && String.IsNullOrEmpty(tb_Fp_Other.Text))
                {
                    lb_Fp_Error.Text += "Please Enter a Valid Plan! <br>";
                }
                if (ddl_Fp_Age.SelectedValue == "--Select--")
                {
                    lb_Fp_Error.Text += "Please Select a Valid Age! <br>";
                }
                if (!decimal.TryParse(tb_Fp_Amount.Text, out amount))
                {
                    lb_Fp_Error.Text += "Please Enter a Valid Amount! <br>";
                }
                if (!cb_Fp_Recognition.Checked)
                {
                    lb_Fp_Error.Text += "Please Agree to the Terms & Conditions! <br>";
                }

                if (String.IsNullOrEmpty(lb_Fp_Error.Text))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}