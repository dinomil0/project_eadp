﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class Class_Transaction
    {
        static int count = 0;
        public Class_Transaction()
        {
            count++;
        }
        public string getCount()
        {
            return count.ToString();
        }
        public string transID { get; set; }
        public string ordId { get; set; }
        public DateTime date { get; set; }
        public string reference { get; set; }

        public string price { get; set; }

        public int qty { get; set; }

        public string total { get; set; }

        public string debit { get; set; }

        public string credit { get; set; }
        public string userID { get; set; }
        public string status { get; set; }
    }
}