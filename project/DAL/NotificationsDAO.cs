﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class NotificationsDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        public List<string> getAllNotificationsbyuserID(string userID)
        {
            List<string> NotificationsList = new List<string>();
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();
            StringBuilder sqlStr = new StringBuilder();

            sqlStr.AppendLine("SELECT * From Notifications where userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            SqlDataAdapter da = new SqlDataAdapter(sqlStr.ToString(), myConn);

            da.SelectCommand.Parameters.AddWithValue("@parauserID", userID);
;
            da.Fill(ds, "NotificationsList");
            int rec_cnt = ds.Tables["NotificationsList"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["NotificationsList"].Rows)
                {
                    string details = row["details"].ToString();

                    NotificationsList.Add(details);
                }
            }
            else
            {
                NotificationsList = null;
            }

            return NotificationsList;

        }
        public void InsertNotifications(string details, string userID)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("INSERT INTO Notifications (details, userID)");
            sqlStr.AppendLine("VALUES (@paradetails,@parauserID)");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@paradetails", details);
            sqlCmd.Parameters.AddWithValue("@parauserID", userID);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();

        }
    }
}