﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class Class_AccountDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        public Class_Account.Account get_byAccNum(int accNum)
        {
            Class_Account.Account acc = new Class_Account.Account();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Accounts");
            sqlCommand.AppendLine("WHERE bankaccnum = @parabankaccnum");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parabankaccnum", accNum);

            da.Fill(ds, "AccountTable");

            foreach (DataRow row in ds.Tables["AccountTable"].Rows)
            {
                acc.bal = Convert.ToDouble(row["balance"]);
                acc.bankaccnum = Convert.ToInt64(row["bankaccnum"]);
                acc.bankaccname = Convert.ToString(row["bankaccname"]);
                acc.cardaccnum = Convert.ToInt64(row["cardNum"]);
                acc.cvv = Convert.ToInt16(row["cvv"]);
                acc.DOE = Convert.ToDateTime(row["DOE"]);
            }
            return acc;
        }
        public List<Class_Account.Account> getAccount(string userID)
        {
            List<Class_Account.Account> accList = new List<Class_Account.Account>();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Accounts");
            sqlCommand.AppendLine("WHERE userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

            da.Fill(ds, "SavingAccTable");

            foreach (DataRow row in ds.Tables["SavingAccTable"].Rows)
            {
                Class_Account.Account acc = new Class_Account.Account();
                acc.userID = Convert.ToString(row["userID"]);
                acc.bal = Convert.ToInt32(row["balance"]);
                acc.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
                acc.bankaccname = Convert.ToString(row["bankaccname"]);
                acc.cardaccnum = Convert.ToInt64(row["cardNum"]);
                string[] list = acc.bankaccname.Split(' ');
                string test = list[1];
                if (list[1] != "Loans")
                {
                    accList.Add(acc);
                }
            }
            return accList;
        }
        public Class_Account.VCAccount getVCAccount(string userID)
        {
            Class_Account.VCAccount VCAcc = new Class_Account.VCAccount();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM AccountVC");
            sqlCommand.AppendLine("WHERE userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

            da.Fill(ds, "AccountVCTable");

            foreach (DataRow row in ds.Tables["AccountVCTable"].Rows)
            {
                VCAcc.userID = Convert.ToString(row["userID"]);
                VCAcc.VCbal = Convert.ToInt32(row["VCBal"]);
            }

            return VCAcc;
        }
        public bool useCard(string userID, long bankaccnum, double debit, double credit)
        {
            Class_Account.Account acc = new Class_Account.Account();
            if (debit == 0)
            {
                SqlDataAdapter da;
                DataSet ds = new DataSet();

                StringBuilder sqlCommand = new StringBuilder();
                sqlCommand.AppendLine("SELECT * FROM Accounts");
                sqlCommand.AppendLine("WHERE bankaccnum = @parabankaccnum");

                SqlConnection myConn = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
                da.SelectCommand.Parameters.AddWithValue("parabankaccnum", bankaccnum);

                da.Fill(ds, "AccountTable");

                foreach (DataRow row in ds.Tables["AccountTable"].Rows)
                {
                    acc.bal = Convert.ToDouble(row["balance"]);
                }
                acc.bal += credit;
                StringBuilder addCommand = new StringBuilder();
                SqlCommand excommand = new SqlCommand();
                addCommand.AppendLine("UPDATE Accounts");
                addCommand.AppendLine("SET balance = @parabal");
                addCommand.AppendLine("WHERE bankaccnum = @parabankaccnum");
                SqlConnection myConn2 = new SqlConnection(DBConnect);
                excommand = new SqlCommand(addCommand.ToString(), myConn2);
                excommand.Parameters.AddWithValue("parabal", acc.bal);
                excommand.Parameters.AddWithValue("parabankaccnum", bankaccnum);
                myConn2.Open();
                excommand.ExecuteNonQuery();
                myConn2.Close();
                return true;
            }
            else if (credit == 0)
            {
                SqlDataAdapter da;
                DataSet ds = new DataSet();

                StringBuilder sqlCommand = new StringBuilder();
                sqlCommand.AppendLine("SELECT * FROM Accounts");
                sqlCommand.AppendLine("WHERE bankaccnum = @parabankaccnum");

                SqlConnection myConn = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
                da.SelectCommand.Parameters.AddWithValue("parabankaccnum", bankaccnum);

                da.Fill(ds, "AccountTable");

                foreach (DataRow row in ds.Tables["AccountTable"].Rows)
                {
                    acc.bal = Convert.ToDouble(row["balance"]);
                }
                acc.bal -= debit;
                if (acc.bal < 0)
                {
                    return false;
                }
                else if (acc.bal > 0)
                {
                    StringBuilder addCommand = new StringBuilder();
                    SqlCommand excommand = new SqlCommand();
                    addCommand.AppendLine("UPDATE Accounts");
                    addCommand.AppendLine("SET balance = @parabal");
                    addCommand.AppendLine("WHERE bankaccnum = @parabankaccnum");
                    SqlConnection myConn2 = new SqlConnection(DBConnect);
                    excommand = new SqlCommand(addCommand.ToString(), myConn2);
                    excommand.Parameters.AddWithValue("parabal", acc.bal);
                    excommand.Parameters.AddWithValue("parabankaccnum", bankaccnum);
                    myConn2.Open();
                    excommand.ExecuteNonQuery();
                    myConn2.Close();
                    return true;
                }
            }
            return false;
        }
        //public bool useCard(string userID, double debit, double credit)
        //{
        //    Class_Account.Account acc = new Class_Account.Account();
        //    if (debit == 0)
        //    {
        //        SqlDataAdapter da;
        //        DataSet ds = new DataSet();

        //        StringBuilder sqlCommand = new StringBuilder();
        //        sqlCommand.AppendLine("SELECT * FROM Accounts");
        //        sqlCommand.AppendLine("WHERE userID = @parauserID");

        //        SqlConnection myConn = new SqlConnection(DBConnect);
        //        da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
        //        da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

        //        da.Fill(ds, "AccountTable");

        //        foreach (DataRow row in ds.Tables["AccountTable"].Rows)
        //        {
        //            acc.bal = Convert.ToDouble(row["balance"]);
        //        }
        //        acc.bal += credit;
        //        StringBuilder addCommand = new StringBuilder();
        //        SqlCommand excommand = new SqlCommand();
        //        addCommand.AppendLine("UPDATE Accounts");
        //        addCommand.AppendLine("SET balance = @parabal");
        //        addCommand.AppendLine("WHERE userID = @parauserID");
        //        SqlConnection myConn2 = new SqlConnection(DBConnect);
        //        excommand = new SqlCommand(addCommand.ToString(), myConn2);
        //        excommand.Parameters.AddWithValue("parabal", acc.bal);
        //        excommand.Parameters.AddWithValue("parauserID", userID);
        //        myConn2.Open();
        //        excommand.ExecuteNonQuery();
        //        myConn2.Close();
        //        return true;
        //    }
        //    else if (credit == 0)
        //    {
        //        SqlDataAdapter da;
        //        DataSet ds = new DataSet();

        //        StringBuilder sqlCommand = new StringBuilder();
        //        sqlCommand.AppendLine("SELECT * FROM Accounts");
        //        sqlCommand.AppendLine("WHERE userID = @parauserID");

        //        SqlConnection myConn = new SqlConnection(DBConnect);
        //        da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
        //        da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

        //        da.Fill(ds, "AccountTable");

        //        foreach (DataRow row in ds.Tables["AccountTable"].Rows)
        //        {
        //            acc.bal = Convert.ToDouble(row["balance"]);
        //        }
        //        if ((acc.bal - debit) < 0)
        //        {
        //            return false;
        //        }
        //        else if ((acc.bal - debit) > 0)
        //        {
        //            StringBuilder addCommand = new StringBuilder();
        //            SqlCommand excommand = new SqlCommand();
        //            addCommand.AppendLine("UPDATE Accounts");
        //            addCommand.AppendLine("SET balance = @parabal");
        //            addCommand.AppendLine("WHERE userID = @parauserID");
        //            SqlConnection myConn2 = new SqlConnection(DBConnect);
        //            excommand = new SqlCommand(addCommand.ToString(), myConn2);
        //            excommand.Parameters.AddWithValue("parabal", acc.bal);
        //            excommand.Parameters.AddWithValue("parauserID", userID);
        //            myConn2.Open();
        //            excommand.ExecuteNonQuery();
        //            myConn2.Close();
        //            return true;
        //        }
        //    }
        //    return false;
        //}
        public bool useVC(string userID, double debit, double credit)
        {
            Class_Account.VCAccount acc = new Class_Account.VCAccount();
            if (debit == 0)
            {
                SqlDataAdapter da;
                DataSet ds = new DataSet();

                StringBuilder sqlCommand = new StringBuilder();
                sqlCommand.AppendLine("SELECT * FROM AccountVC");
                sqlCommand.AppendLine("WHERE userID = @parauserID");

                SqlConnection myConn = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
                da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

                da.Fill(ds, "AccountVCTable");

                foreach (DataRow row in ds.Tables["AccountVCTable"].Rows)
                {
                    acc.VCbal = Convert.ToInt64(row["VCbal"]);
                }
                acc.VCbal += Convert.ToInt16(credit);
                StringBuilder addCommand = new StringBuilder();
                SqlCommand excommand = new SqlCommand();
                addCommand.AppendLine("UPDATE AccountVC");
                addCommand.AppendLine("SET VCbal = @parabal");
                addCommand.AppendLine("WHERE userID = @parauserID");
                SqlConnection myConn2 = new SqlConnection(DBConnect);
                excommand = new SqlCommand(addCommand.ToString(), myConn2);
                excommand.Parameters.AddWithValue("parabal", acc.VCbal);
                excommand.Parameters.AddWithValue("parauserID", userID);
                myConn2.Open();
                excommand.ExecuteNonQuery();
                myConn2.Close();
                return true;
            }
            else if (credit == 0)
            {
                SqlDataAdapter da;
                DataSet ds = new DataSet();

                StringBuilder sqlCommand = new StringBuilder();
                sqlCommand.AppendLine("SELECT * FROM AccountVC");
                sqlCommand.AppendLine("WHERE userID = @parauserID");

                SqlConnection myConn = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
                da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

                da.Fill(ds, "AccountVCTable");

                foreach (DataRow row in ds.Tables["AccountVCTable"].Rows)
                {
                    acc.VCbal = Convert.ToInt32(row["VCbal"]);
                }
                if ((acc.VCbal - debit) < 0)
                {
                    return false;
                }
                else if ((acc.VCbal - debit) > 0)
                {
                    StringBuilder addCommand = new StringBuilder();
                    SqlCommand excommand = new SqlCommand();
                    addCommand.AppendLine("UPDATE AccountVC");
                    addCommand.AppendLine("SET VCbal = @parabal");
                    addCommand.AppendLine("WHERE userID = @parauserID");
                    SqlConnection myConn2 = new SqlConnection(DBConnect);
                    excommand = new SqlCommand(addCommand.ToString(), myConn2);
                    excommand.Parameters.AddWithValue("parabal", acc.VCbal);
                    excommand.Parameters.AddWithValue("parauserID", userID);
                    myConn2.Open();
                    excommand.ExecuteNonQuery();
                    myConn2.Close();
                    return true;
                }
            }
            return false;
        }
        public bool VCconversion(string userID, int bankaccnum, int amt, int vc)
        {
            Class_Account.Account savingAcc = new Class_Account.Account();
            Class_AccountDAO accDAO = new Class_AccountDAO();
            savingAcc = accDAO.get_byAccNum(bankaccnum);
            if (savingAcc.bal - amt >= 0)
            {
                StringBuilder sqlCommand = new StringBuilder();
                SqlCommand excommand = new SqlCommand();
                sqlCommand.AppendLine("UPDATE AccountVC");
                sqlCommand.AppendLine("SET VCBal = VCBal + @paravc");
                sqlCommand.AppendLine("WHERE userID = @parauserID;");
                SqlConnection myConn = new SqlConnection(DBConnect);
                excommand = new SqlCommand(sqlCommand.ToString(), myConn);
                excommand.Parameters.AddWithValue("paravc", vc);
                excommand.Parameters.AddWithValue("parauserID", userID);
                myConn.Open();
                excommand.ExecuteNonQuery();
                myConn.Close();
                return true;
            }
            return false;
        }
    }
}