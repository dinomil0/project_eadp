﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class financial_Data
    {
        public string userID { set; get; }
        public string name { set; get; }
        public string type { set; get; }
        public string manual_Input { set; get; }
        public decimal amount { set; get; }
    }
}