﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class WebForm9 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }
            if (!IsPostBack)
            {
                AccountsDAO accDAO = new AccountsDAO();
                List<Accounts> bankaccnumListphone = new List<Accounts>();

                string userID = Session["custNRIC"].ToString();
                int bankaccnum = Convert.ToInt32(Ddlbankaccs.SelectedItem);

                bankaccnumListphone = accDAO.getbackaccnumbyuserID(userID);

                Ddlbankaccs.Items.Clear();
                Ddlbankaccs.Items.Insert(0, new ListItem("--Select--", "0"));
                Ddlbankaccs.AppendDataBoundItems = true;
                Ddlbankaccs.DataTextField = "bankaccname";
                Ddlbankaccs.DataValueField = "bankaccnum";
                Ddlbankaccs.DataSource = bankaccnumListphone;
                Ddlbankaccs.DataBind();

                NewGIRODate.Attributes.Add("min", (DateTime.Now.ToString("yyyy-MM-dd")));

            }
        }

        protected void submitbuttonGIRO_Click(object sender, EventArgs e)
        {
            string userID = Session["custNRIC"].ToString();
            if (Ddlbankaccs.SelectedValue != null && DdlBillingOrganisation.SelectedValue != null && TbGIROAmt != null)
            {
                GIRODAO giroDAO = new GIRODAO();
                DateTime now = DateTime.Now;
                giroDAO.InsertGIRO(Convert.ToDateTime(NewGIRODate.Text), DdlBillingOrganisation.SelectedValue, Convert.ToDouble(TbGIROAmt.Text), Convert.ToInt32(Ddlbankaccs.SelectedValue), userID);
                Response.Redirect("GIRO.aspx");
            }
        }

        protected void closebuttonGIRO_Click(object sender, EventArgs e)
        {
            Response.Redirect("GIRO.aspx");
        }
    }
}