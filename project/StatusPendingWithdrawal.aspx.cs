﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class StatusPendingWithdrawal : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            var userID = Session["custNRIC"].ToString();
            if (Session["custNRIC"] == null)
            {
                Response.Redirect("landing.aspx");
            }
            if (!IsPostBack)
            {
                if (Session["SSTransactionID"] != null)
                {
                    LblWithdrawalReferenceNum.Text = Session["SSTransactionID"].ToString();

                    DAL.TransactionHistory trans = new DAL.TransactionHistory();
                    TransactionHistoryDAO transDAO = new TransactionHistoryDAO();
                    trans = transDAO.getTDbyUserTransactionID(Convert.ToInt32(Session["SSTransactionID"]));

                    LblWithdrawalReferenceNum.Text = trans.transactionID.ToString();
                    LblUserID.Text = trans.userID.ToString();
                    string dateforwithdrawal = trans.date.ToString("dd/MM/yyyy");
                    LblWithdrawalDate.Text = dateforwithdrawal;
                    LblWithdrawalDescription.Text = trans.description.ToString();
                    LblWithdrawalType.Text = trans.type.ToString();
                    LblWithdrawalAmt.Text = "$" + trans.amount.ToString("F");
                    LblWithdrawalSendTo.Text = trans.sendto.ToString();
                    LblWithdrawalSendToBankAcc.Text = trans.tobankaccnum.ToString();
                    LblWithdrawalSendUsing.Text = trans.sendusing.ToString();
                    LblWithdrawalStatus.Text = trans.status.ToString();
                    LblWithdrawalbankacc.Text = trans.frombankaccnum.ToString();
                }
                else
                {
                    Response.Redirect("TransactionHistory.aspx");
                }
            }
        }

        protected void BtnWithdrawalConfirm_Click(object sender, EventArgs e)
        {
            TransactionHistory THtd = new TransactionHistory();
            TransactionHistoryDAO delTH = new TransactionHistoryDAO();

            delTH.DeleteTDbytransactionID(Convert.ToInt32(LblWithdrawalReferenceNum.Text.ToString()));

            //Get acc balance and add
            Accounts accObj = new Accounts();
            AccountsDAO accDAO = new AccountsDAO();

            accObj = accDAO.getbankaccbalance(Convert.ToInt32(LblWithdrawalbankacc.Text.ToString()));
            double amount = Convert.ToDouble(LblWithdrawalAmt.Text.ToString().Substring(1));
            accObj.balance = accObj.balance + amount;

            // Update acc balance after withdrawal
            AccountsDAO updateDAO = new AccountsDAO();
            updateDAO.updatebankaccbalance(Convert.ToInt32(LblWithdrawalbankacc.Text.ToString()), accObj.balance);
            Response.Redirect("TransactionHistory.aspx");
        }

        protected void BtnWithdrawalBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("TransactionHistory.aspx");
        }
        protected void timerTest_tick(object sen, EventArgs e)
        {
            string nric = Session["custNRIC"].ToString();
            if (DateTime.Compare(DateTime.Now, DateTime.Parse(Session["Timer"].ToString())) < 0)
            {
                litMsg.Text = ((Int32)DateTime.Parse(Session["Timer"].ToString()).Subtract(DateTime.Now).TotalMinutes)
                .ToString() + " Minute " + (((Int32)DateTime.Parse(Session["Timer"].ToString()).Subtract(DateTime.Now).TotalSeconds) % 60)
                .ToString() + " Seconds";

            }
            else
            {
                // Deposit Transaction for another user
                customer cus = new customer();
                customerDAO checkuserID = new customerDAO();

                string userID = "";
                string sendby = "";

                if (LblWithdrawalSendUsing.Text == "Phone Number")
                {
                    cus = checkuserID.getUserIDbyphonenum(Convert.ToInt32(LblWithdrawalSendTo.Text.ToString()));
                    userID = cus.userID;
                }
                else if (LblWithdrawalSendUsing.Text == "NRIC")
                {
                    cus = checkuserID.getNamebynric(LblWithdrawalSendTo.Text.ToString());
                    userID = cus.userID;
                }

                // add deposit transaction
                TransactionHistoryDAO newdata = new TransactionHistoryDAO();
                int insCnt = newdata.InsertTransactionsDeposit(Convert.ToDateTime(LblWithdrawalDate.Text), LblWithdrawalDescription.Text,
                                                        "Deposit", Convert.ToDouble(LblWithdrawalAmt.Text.ToString().Substring(1)),
                                                        "Successful", userID,
                                                        Convert.ToInt32(LblWithdrawalbankacc.Text), Convert.ToInt32(LblWithdrawalSendToBankAcc.Text), Convert.ToInt32(LblWithdrawalSendToBankAcc.Text));
                // Update acc balance for deposit
                //Get acc balance and plus
                Accounts accsObj = new Accounts();
                AccountsDAO accsDAO = new AccountsDAO();

                accsObj = accsDAO.getbankaccbalance(Convert.ToInt32(LblWithdrawalSendToBankAcc.Text));
                double addamount = Convert.ToDouble(LblWithdrawalAmt.Text.ToString().Substring(1));
                accsObj.balance = accsObj.balance + addamount;

                AccountsDAO updateDAOdeposit = new AccountsDAO();
                updateDAOdeposit.updatebankaccbalance(Convert.ToInt32(LblWithdrawalSendToBankAcc.Text), accsObj.balance);

                // Update status for current user
                TransactionHistory THObj = new TransactionHistory();
                TransactionHistoryDAO uptTHDAO = new TransactionHistoryDAO();
                uptTHDAO.UpdateTDforstatus(Convert.ToInt32(LblWithdrawalReferenceNum.Text), "Successful");

                //add notifications
                NotificationsDAO notiDAO = new NotificationsDAO();
                notiDAO.InsertNotifications("You have successfully transferred $" + Math.Round(addamount,2).ToString() + " to " + LblWithdrawalSendTo.Text + " with Bank Account Number " + LblWithdrawalbankacc.Text, nric);
                Response.Redirect("TransactionHistory.aspx");

            }
        }
    }
}