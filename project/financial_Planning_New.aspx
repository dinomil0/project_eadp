﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="financial_Planning_New.aspx.cs" Inherits="project.financial_Planning_New" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="fp_Form">
        <div class="container mt-3">
            <asp:Label ID="lb_Fp_Title" class="h5" Text="New Financial Plan" runat="server"></asp:Label>
            <hr />
            <asp:Label ID="lb_Fp_Error" class="text-danger" Visible="False" runat="server"></asp:Label>
            <div class="container mt-3">
                <div class="form-group row">
                    <asp:Label ID="lb_Fp_Description" class="col-form-label col-sm-3" Text="Choose a Plan:" runat="server"></asp:Label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddl_Fp_Description" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddl_Fp_Description_SelectedIndexChanged" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <asp:Panel ID="pn_Fp_Other" Visible="False" runat="server">
                    <div class="form-group row">
                        <asp:Label ID="lb_Fp_Other" class="col-form-label col-sm-3" runat="server"></asp:Label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="tb_Fp_Other" class="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </asp:Panel>
                <div class="form-group row">
                    <asp:Label ID="lb_Fp_Age" class="col-form-label col-sm-3" Text="Age @ Plan Maturity:" runat="server"></asp:Label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddl_Fp_Age" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddl_Fp_Age_SelectedIndexChanged" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group row">
                    <asp:Label ID="lb_Fp_Duration" class="col-form-label col-sm-3" Text="Number of Years Left:" runat="server"></asp:Label>
                    <div class="col-sm-9">
                        <asp:Textbox ID="tb_Fp_Duration" class="form-control" ReadOnly="True" runat="server"></asp:Textbox>
                    </div>
                </div>
                <div class="form-group row">
                    <asp:Label ID="lb_Fp_Amount" class="col-form-label col-sm-3" runat="server" Text="Minimum Amount Needed:"></asp:Label>
                    <div class="col-sm-9">
                        <asp:Textbox ID="tb_Fp_Amount" class="form-control" runat="server"></asp:Textbox>
                    </div>
                </div>
                <div class="form-check">
                    <asp:CheckBox ID="cb_Fp_Recognition" class="form-check-input" runat="server" />
                    <asp:Label id="lb_Fp_Recognition" class="form-check-label" runat="server">
                        I hereby acknowledge that the information provided above is as accurate to my knowledge,
                        and that I will not hold anyone accountable if there are any flaws in my Financial Plans.
                    </asp:Label>
                </div>
                <div class="d-flex justify-content-end">
                    <asp:Button id="btn_Fp_Confirm" class="btn btn-success" Text="Confirm" OnClick="btn_Fp_Confirm_Click" runat="server" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:Button id="btn_Fp_Cancel" class="btn btn-secondary" Text="Cancel" OnClick="btn_Fp_Cancel_Click" runat="server" />
                </div>
            </div>
       </div>
    
</asp:Content>
