﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class Class_Order
    {
        static int count = 0;
        public Class_Order()
        {
            count++;
        }
        public string getCount()
        {
            return count.ToString();
        }
        public string ProdId { get; set; }
        public virtual string ordId { get; set; }
        public DateTime date { get; set; }
        public string reference { get; set; }
        public string price { get; set; }
        public int BuyQty { get; set; }
        public string ProdName { get; set; }
        public string total { get; set; }
        public string debit { get; set; }
        public string status { get; set; }
        public string userID { get; set; }
        public string sellerID { get; set; }
    }
}