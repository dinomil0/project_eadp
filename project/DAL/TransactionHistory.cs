﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class TransactionHistory
    {

        public TransactionHistory()
        {

        }
        public int transactionID { get; set; }
        public string userID { get; set; }
        public DateTime date { get; set; }
        public string description { get; set; }
        public string type { get; set; }
        public double amount { get; set; }
        public string status { get; set; }
        public string address { get; set; }
        public string sendto { get; set; }
        public int frombankaccnum { get; set; }
        public string sendusing { get; set; }
        public int tobankaccnum { get; set; }
        public int bankaccnum { get; set; }


    }
}