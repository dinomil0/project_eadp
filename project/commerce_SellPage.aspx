﻿<%@ Page Title="" Language="C#" MasterPageFile="~/commerceMaster.Master" AutoEventWireup="true" CodeBehind="commerce_SellPage.aspx.cs" Inherits="project.commerce_SellPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="form1">
    <section class="d-flex justify-content-center">
        <div class="col-3">
            &nbsp
        </div>
        <div class="col-6 border border-success mt-5">
            <h4 class="mt-2">List a new product</h4>
            <div class="d-flex justify-content-center">
                <div>
                    <table style="width: 100%;" class="mt-5">
                        <tr>
                            <td>Enter Product Name: </td>
                            <td><asp:TextBox ID="Tb_ProductName" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Enter Product Description: </td>
                            <td><asp:TextBox ID="Tb_ProductDesc" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Enter Product Price: </td>
                            <td><asp:TextBox ID="Tb_ProductPrice" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Enter Quantity of Product: </td>
                            <td><asp:TextBox ID="Tb_ProductQuantity" runat="server"></asp:TextBox></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="d-flex mt-2 m-3 justify-content-center">
                <asp:FileUpload ID="file_imgUpload" runat="server" class="mt-2 ml-2"/>
            </div>
            <div class="d-flex justify-content-center">
                <asp:Button ID="btn_addtolist" runat="server" Text="List" class="float-right btn btn-outline-success mb-2" OnClick="btn_addtolist_Click"/>
            </div>
        </div>
        <div class="col-3">
            &nbsp
        </div>
    </section>
</div>
</asp:Content>
