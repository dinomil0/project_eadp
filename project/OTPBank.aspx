﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="OTPBank.aspx.cs" Inherits="project.WebForm6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section id="OTPBank" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
      <div class="form-group">
        <label for="exampleInputEmail1">OTP</label>
          <asp:TextBox ID="tbotp" class="form-control" runat="server"></asp:TextBox>
      </div>
        <asp:Button ID="OTPgenerator" class="btn btn-secondary" runat="server" Text="Get OTP via phone number" OnClick="OTPgenerator_Click" />
        <asp:Button ID="OTPsubmit" runat="server" class="btn btn-success"  Text="Confirm" OnClick="OTPsubmit_Click" />
        <%--<asp:Label ID="otpPIN" runat="server" Text=""></asp:Label>--%>
    </section>


</asp:Content>
