﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="adminTrend.aspx.cs" Inherits="project.adminTrend" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
        <section class="border border-secondary bg-light shadow p-3 mb-5 rounded container mt-3">
            <div>
            <h3>
                Loan Trends
            </h3>
            </div>
            <div class="d-flex justify-content-end">
                <p class="mt-2 pr-2">Select Month:</p>
                <asp:DropDownList ID="ddlMonth" class="form-control col-2" runat="server" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </div>
                <div class="chart-container" style="height:50vh;">
                <canvas id="myChart"></canvas>
                <script>
                DepositWithdrawal = JSON.parse('<%=Session["allGroup"]%>');
                var ctx = document.getElementById('myChart').getContext('2d');
                    var chart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ['21 - 25', '26 - 35', '36 - 45', '46 - 55', '56 or older'],
                            datasets: [
                                {
                                    label: 'Personal',
                                    backgroundColor: 'rgb(255,0,0)',
                                    borderColor: 'red',
                                    data: [DepositWithdrawal[0], DepositWithdrawal[3], DepositWithdrawal[6], DepositWithdrawal[9], DepositWithdrawal[12]]
                                },
                                {
                                    label: 'Auto',
                                    backgroundColor: 'rgb(255, 218, 185)',
                                    borderColor: 'rgb(255, 99, 132)',
                                    data: [DepositWithdrawal[1], DepositWithdrawal[4], DepositWithdrawal[7], DepositWithdrawal[10], DepositWithdrawal[13]]
                                },
                                {
                                    label: 'Housing',
                                    backgroundColor: 'rgb(0, 0, 255).',
                                    borderColor: 'blue',
                                    data: [DepositWithdrawal[2], DepositWithdrawal[5], DepositWithdrawal[8], DepositWithdrawal[11], DepositWithdrawal[15]]
                                }
                            ]
                        },
                        options: {
                            title: {
                                    display: true,
                                    text: 'Age Group Vs Types of Loans',
                                    fontSize: 25
                                },
                            maintainAspectRatio: false,
                            scales: {
                            yAxes: [{
                                        
                                gridLines: {
                                    display: true,
                                    color: "rgba(255,99,132,0.2)"
                                }
                                }],
                            xAxes: [{
                                gridLines: {
                                display: false
                                }
                                }],
                            legend: {
                                display:true,
                                position:'right',
                            labels:{
                                fontColor: '#000'
                                }
                            },
                            tooltips:{
                                enabled:true
                            },
                            }}
                    });
                </script>
            </div>
        </section>
    
</asp:Content>
