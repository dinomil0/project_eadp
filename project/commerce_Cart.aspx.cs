﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class commerce_Cart : System.Web.UI.Page
    {
        Double sum = 0;
        string userID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            userID = Session["custNRIC"].ToString();
            //Total up the price of all products
            Class_ProductDAO prodDAO = new Class_ProductDAO();
            List<Class_Order> orders = prodDAO.get_cart(userID);
            int count = orders.Count();
            DataTable table = new DataTable();
            table.Columns.Add("ProdImage");
            table.Columns.Add("ordId");
            table.Columns.Add("ProdName");
            table.Columns.Add("Price");
            table.Columns.Add("BuyQty");
            table.Columns.Add("Total");
            for (int i = 0; i < count; i++)
            {
                Class_Product product = new Class_Product();
                Class_Order order = new Class_Order();
                order = orders[i];
                //Convert order.total into local currency
                product = prodDAO.getProduct(order.ProdId);
                string strBase64 = Convert.ToBase64String(product.ProdImage);
                string url = "data:img/png;base64," + strBase64;
                table.Rows.Add(url, order.ordId, order.ProdName, order.price, order.BuyQty, order.total);
                sum += Convert.ToDouble(order.total);
                //Convert sum into local currency
            }
            Lbl_Total.Text = "$" + Convert.ToString(sum) + " [ " + Convert.ToString(sum * 4) + "VC ]";
            Cart.DataSource = table;
            Cart.DataBind();
        }

        protected void btn_checkout_Click(object sender, EventArgs e)
        {
            Response.Redirect("commerce_Payment.aspx?Total=" + Convert.ToString(sum));
        }

        protected void Remove_Click(object sender, CommandEventArgs e)
        {
            Class_ListDAO listDAO = new Class_ListDAO();
            string ordId = e.CommandArgument.ToString();
            listDAO.Less_Cart(userID, ordId);
            Response.Redirect("commerce_Cart.aspx");
        }

        protected void btn_clear_Click(object sender, EventArgs e)
        {
            Class_ListDAO listDAO = new Class_ListDAO();
            listDAO.clear_cart(userID);
            Response.Redirect("commerce_Cart.aspx");
        }
    }
}