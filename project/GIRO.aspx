﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="GIRO.aspx.cs" Inherits="project.WebForm8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style>
    a {
    text-decoration: none !important;
    }
</style>
    <div class="container mt-5">
         <div class="d-flex text-center bg-light mt-5">
        <a href="newGIRO.aspx" class=" badge-light col-4 shadow bg-light mr-2" style="border-top: orange solid 4px">
            <h4 class="mt-3">Set up</h4>
            <p class="mt-3">Set up your GIRO payments conveniently anytime, anywhere</p>
        </a>
        <a href="TransactionHistory.aspx" class=" badge-light col-4 shadow bg-light mr-2" style="border-top: orange solid 4px">
            <h4 class="mt-3">Check</h4>
            <p class="mt-3">Check your GIRO payments history</p>
        </a>
        <a  data-toggle="collapse" href="#gridviewshow" role="button" class=" badge-light col-4 shadow bg-light mr-2" style="border-top: orange solid 4px">
            <h4 class="mt-3">Manage</h4>
            <p class="mt-3">View or terminate your GIRO arrangements</p>
        </a>
    </div>
    </div>
    <div class="collapse" id="gridviewshow">
      <div class="card card-body">
          <asp:GridView ID="GridViewGIRO" class="table table-responsive-sm table table-bordered mt-5" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewGIRO_RowDataBound">
              <Columns>
                  <asp:BoundField DataField="giroID" HeaderText="GIRO ID" />
                  <asp:BoundField DataField="date" HeaderText="Recurring Date" DataFormatString="{0:dd/MM/yyyy}" />
                  <asp:BoundField DataField="billingOrganisation" HeaderText="Billing Organisation" />
                  <asp:BoundField DataField="amount" HeaderText="Amount" DataFormatString="{0:n}"/>
                   <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton runat="server" onclick="showModal">Terminate GIRO</asp:LinkButton>
                        </ItemTemplate>
                        <ControlStyle CssClass="btn btn-danger" />
                    </asp:TemplateField>
                  
              </Columns>
          </asp:GridView>
      </div>
    </div>
    <!-- Modal -->
        <div class="modal fade" id="GIROModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <div class="text-center">
                    <i class="fas fa-exclamation-circle text-warning fa-5x"></i>
                      <br />
                      <h4>Are you sure you want to terminate the GIRO?</h4>
                  </div>
                  <asp:Label ID="LblgiroID" runat="server" Text="" Visible="false"></asp:Label>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <asp:Button ID="confirmDeleteGIRO" runat="server" Text="Confirm" class="btn btn-danger" onclick="confirmDeleteGIRO_Click"/>
              </div>
            </div>
          </div>
        </div>
<script type='text/javascript'>
    function openModal() {
        $('[id*=GIROModal]').modal('show');
    } 
</script>

</asp:Content>
