﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Text;

namespace project.DAL
{
    public class trendDAO
    {
        public List<trend> getTrends(DateTime date)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

            StringBuilder sqlStr = new StringBuilder();
            StringBuilder sqlCommand = new StringBuilder();

            sqlCommand.AppendLine("Select * from loanTrend");

            SqlConnection myConn = new SqlConnection(DBConnect);
            SqlDataAdapter da = new SqlDataAdapter(sqlCommand.ToString(), myConn);

            DataSet ds = new DataSet();

            da.Fill(ds, "trend");
            var applicationList = new List<loanApplication>();
            int rec_cnt = ds.Tables["trend"].Rows.Count;
            if (rec_cnt > 0)
            {
                trend trend21P = new trend
                {
                    loanType = "Personal",
                    ageGroup = "21"
                };
                trend trend21A = new trend
                {
                    loanType = "Auto",
                    ageGroup = "21"
                };
                trend trend21H = new trend
                {
                    loanType = "Housing",
                    ageGroup = "21"
                };

                trend trend26P = new trend
                {
                    loanType = "Personal",
                    ageGroup = "26"
                };
                trend trend26A = new trend
                {
                    loanType = "Auto",
                    ageGroup = "26"
                };
                trend trend26H = new trend
                {
                    loanType = "Housing",
                    ageGroup = "26"
                };

                trend trend36P = new trend
                {
                    loanType = "Personal",
                    ageGroup = "36"
                };
                trend trend36A = new trend
                {
                    loanType = "Auto",
                    ageGroup = "36"
                };
                trend trend36H = new trend
                {
                    loanType = "Housing",
                    ageGroup = "36"
                };

                trend trend46P = new trend
                {
                    loanType = "Personal",
                    ageGroup = "46"
                };
                trend trend46A = new trend
                {
                    loanType = "Auto",
                    ageGroup = "46"
                };
                trend trend46H = new trend
                {
                    loanType = "Housing",
                    ageGroup = "46"
                };

                trend trend56P = new trend
                {
                    loanType = "Personal",
                    ageGroup = "56"
                };
                trend trend56A = new trend
                {
                    loanType = "Auto",
                    ageGroup = "56"
                };
                trend trend56H = new trend
                {
                    loanType = "Housing",
                    ageGroup = "56"
                };

                for (int i = 0; i < rec_cnt; i++)
                {

                    DataRow row = ds.Tables["trend"].Rows[i];
                    var enquiredate = Convert.ToDateTime(row["dateEnquire"]).Month;
                    var dateMonth = date.Month;
                    if (enquiredate == dateMonth)
                    {
                        var agegroup = row["ageGroup"].ToString().Substring(0, 2);
                        if (agegroup == "21")
                        {
                            if (row["loanType"].ToString().Substring(0,1) == "P")
                            {
                                trend21P.count += 1;
                            }
                            else if (row["loanType"].ToString().Substring(0, 1) == "A")
                            {
                                trend21A.count += 1;
                            }
                            else
                            {
                                trend21H.count += 1;
                            }
                        }
                        else if (agegroup == "26")
                        {
                            if (row["loanType"].ToString().Substring(0, 1) == "P")
                            {
                                trend26P.count += 1;
                            }
                            else if (row["loanType"].ToString().Substring(0, 1) == "A")
                            {
                                trend26A.count += 1;
                            }
                            else
                            {
                                trend26H.count += 1;
                            }
                        }
                        else if (agegroup == "36")
                        {
                            if (row["loanType"].ToString().Substring(0, 1) == "P")
                            {
                                trend36P.count += 1;
                            }
                            else if (row["loanType"].ToString().Substring(0, 1) == "A")
                            {
                                trend36A.count += 1;
                            }
                            else
                            {
                                trend36H.count += 1;
                            }
                        }
                        else if (agegroup == "46")
                        {
                            if (row["loanType"].ToString().Substring(0, 1) == "P")
                            {
                                trend46P.count += 1;
                            }
                            else if (row["loanType"].ToString().Substring(0, 1) == "A")
                            {
                                trend46A.count += 1;
                            }
                            else
                            {
                                trend46H.count += 1;
                            }
                        }
                        else
                        {
                            if (row["loanType"].ToString().Substring(0, 1) == "P")
                            {
                                trend56P.count += 1;
                            }
                            else if (row["loanType"].ToString() == "Auto")
                            {
                                trend56A.count += 1;
                            }
                            else
                            {
                                trend56H.count += 1;
                            }
                        }

                    }
                }


                List<trend> allgroup = new List<trend>
                {
                    trend21P,
                    trend21A,
                    trend21H,
                    trend26P,
                    trend26A,
                    trend26H,
                    trend36P,
                    trend36A,
                    trend36H,
                    trend46P,
                    trend46A,
                    trend46H,
                    trend56P,
                    trend56A,
                    trend56H,
                };
                return allgroup;

            }
            else return null;

        }
        public void useless()
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

            StringBuilder sqlStr = new StringBuilder();
            SqlCommand sqlCmd = new SqlCommand();

            var dateArray = new string[] { "01/06/19", "01/07/19", "01/07/19", "01/08/19", "01/08/19", "01/06/19" };
            var typeArray = new string[] { "Housing", "Personal", "Auto"};
            var ageArray = new string[] { "21-25", "26-25", "36-45", "46-55", "56 or older" };

            var randDate = new Random().Next(0, 5);
            var randType = new Random().Next(0, 2);
            var randAge = new Random().Next(0, 4);


            sqlStr.AppendLine("INSERT INTO loanTrend(loanType, ageGroup, dateEnquire)");
            sqlStr.AppendLine("VALUES(@paraType, @paraAge, @paraDate)");

            SqlConnection myConn = new SqlConnection(DBConnect);
            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@paraType", typeArray[randType]);
            sqlCmd.Parameters.AddWithValue("@paraAge", ageArray[randAge]);
            sqlCmd.Parameters.AddWithValue("@paraDate", Convert.ToDateTime(dateArray[randDate]));

            myConn.Open();
            sqlCmd.ExecuteNonQuery();
            myConn.Close();
        }
    }
    
    
}