﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="TransferPayYouVerify.aspx.cs" Inherits="project.TransferPayYouVerify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
    hr{
        border:1px solid orange
    }

        .auto-style2 {
            width: 606px;
        }
        .auto-style4 {
            width: 606px;
        }

        .auto-style5 {
            width: 606px;
        }

    </style>
<section id="TransferPayNowVerify" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
  
    <div class="container">
        <label>
            <h1>PayYou</h1>
            <h3>Make a transaction</h3>
        </label>
        <ul class="progress-indicator">
            <li class="completed">
                <span class="bubble"></span>
                <i class="fa fa-check-circle"></i>
                1.Input Details
            </li>
            <li class="completed">
                <span class="bubble"></span>
                <i class="fa fa-check-circle"></i>
                2.Verify Details
            </li>
            <li>
                <span class="bubble"></span>
                3.Completion
            </li>
        </ul>
        <hr />
        
        
        <h5>Recipient Details </h5>
            <asp:Label ID="Lbl_verifyerror" runat="server" Text=""></asp:Label>
            <table class="table">
                <tr>
                    <td class="auto-style2">Type:</td>
                    <td>
                        <asp:Label ID="LblType" runat="server"  ></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="VerifyPhone" runat="server" Visible="false">
               <table class="table">
                    <tr>
                    <td class="auto-style2">Recipient's Mobile Number:</td>
                    <td>
                        <asp:Label ID="LblRecipMobileNum" runat="server"  ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">Recipient's Nickname:</td>
                    <td>
                        <asp:Label ID="LblRecipNickNameMobileNum" runat="server"  ></asp:Label>
                    </td>
                </tr>
                   <tr>
                    <td class="auto-style2">Recipient's Bank Account Number:</td>
                    <td>
                        <asp:Label ID="LblRecipBankAccnumMobileNum" runat="server"  ></asp:Label>
                    </td>
                </tr>
                   <tr>
                    <td class="auto-style2">Recipient's Bank Account Name:</td>
                    <td>
                        <asp:Label ID="LblRecipBankAccnameMobileNum" runat="server"  ></asp:Label>
                    </td>
                </tr>
               </table>
                </asp:Panel>
            <asp:Panel ID="VerifyNRIC" runat="server" Visible="false">
                <table class="table">
                    <tr>
                    <td class="auto-style5">Recipient's NRIC:</td>
                    <td class="auto-style6">
                        <asp:Label ID="LblRecipNRIC" runat="server"  ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">Recipient's Nickname:</td>
                    <td>
                        <asp:Label ID="LblRecipNickNameNRIC" runat="server"  ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">Recipient's Bank Account Number:</td>
                    <td>
                        <asp:Label ID="LblRecipBankaccnumNRIC" runat="server"  ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">Recipient's Bank Account Name:</td>
                    <td>
                        <asp:Label ID="LblRecipBankaccnameNRIC" runat="server"  ></asp:Label>
                    </td>
                </tr>
               </table>
            </asp:Panel>
            <hr />
            <h5>Transfer Details</h5>
            <table class="table">
                <tr>
                    <td class="auto-style4">Transfer Amount:</td>
                    <td>
                        <asp:Label ID="LblTransferAmt" runat="server"  ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">From Account Number:</td>
                    <td>
                        <asp:Label ID="LblAccount" runat="server"  ></asp:Label>
                    </td>
                </tr>
            </table>
            <div class="d-flex">
	            <div class="form-group">
                    <!-- Cancel Button -->
                    <asp:Button ID="cancelButtonPayYouVerify" runat="server" Text="Cancel" type="reset" class="btn btn-secondary" OnClick="cancelButtonPayYouVerify_Click"/> &nbsp;&nbsp;
                    <!-- Submit Button -->
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#savingsModal">
                      Confirm
                    </button>

        <!-- Modal -->
        <div class="modal fade" id="savingsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <div class="text-center">
                    <i class="fas fa-exclamation-circle text-warning fa-5x"></i>
                      <br />
                      <h4>Are you sure you want to transfer?</h4>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <asp:Button ID="submitButtonPayYouVerify" runat="server" Text="Confirm" type="submit" class="btn btn-success" OnClick="submitButtonPayYouVerify_Click"/>
              </div>
            </div>
          </div>
        </div>

                </div>
                <asp:Label ID="Lbl_confirm" runat="server" Text=""></asp:Label>
            </div>
    			
    </div>
</section>
</asp:Content>
