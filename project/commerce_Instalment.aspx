﻿<%@ Page Title="" Language="C#" MasterPageFile="~/commerceMaster.Master" AutoEventWireup="true" CodeBehind="commerce_Instalment.aspx.cs" Inherits="project.commerce_Instalment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="form1">
    <section class="border border-success d-flex justify-content-center flex-column w-50 m-auto">
        <div class="d-flex justify-content-center">
            <asp:Label ID="Lbl_Payment" runat="server" Text="Your Total is SGD$50 [2000VC]"></asp:Label>
        </div>
        <div class="d-flex justify-content-center">
            <p class="">Pay securely with a Credit/Debit Card or Virtual Currency</p>
        </div>
        <div class="d-flex">
            <div class="col-6">
                <asp:LinkButton ID="instal_PayCard" runat="server" OnCommand="commerce_PayCard_Click" CssClass="float-right"><i class="far fa-credit-card fa-7x text-success"></i></asp:LinkButton>
            </div>
            <div class="col-6">
                <asp:LinkButton ID="instal_PayVC" runat="server" OnCommand="commerce_PayVC_Click" CssClass="float-left"><i class="fas fa-coins fa-7x text-success"></i></asp:LinkButton>
            </div>
        </div>
        <br />
        <asp:Panel ID="PanelCardInstalment" runat="server" Visible="False" class="d-flex justify-content-center">
            <div class="">
                <div class="d-flex justify-content-center">
                    <table style="width: 100%;">
                        <tr>
                            <td>Period (Months)</td>
                            <td><asp:DropDownList ID="ddl_CardPeriod" runat="server" OnSelectedIndexChanged="ddl_CardPeriod_SelectedIndexChanged" AutoPostBack="True">
                                <asp:ListItem>--Select--</asp:ListItem>
                                <asp:ListItem Value="0.03">5</asp:ListItem>
                                <asp:ListItem Value="0.05">8</asp:ListItem>
                                <asp:ListItem Value="0.07">10</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>Interest Rate: </td>
                            <td><asp:Label ID="Lbl_CardInterest" runat="server" Text="Interest Rate"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Monthly Repayment: </td>
                            <td><asp:Label ID="Lbl_CardMthlyRepay" runat="server" Text="Monthly Repayment"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Total Repayment: </td>
                            <td><asp:Label ID="Lbl_CardTotalRepay" runat="server" Text="Total Repayment"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Account: </td>
                            <td><asp:DropDownList ID="ddl_bankNumber" runat="server" class="w-50" AutoPostBack="True">
                                <asp:ListItem Selected="True">----Select-----</asp:ListItem></asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="d-flex justify-content-center mt-4 mb-3">
                    <asp:Button ID="btn_instalpaycard" runat="server" Text="Pay" class="btn btn-outline-success" OnClick="btn_instalpaycard_Click"/>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="PanelVCInstalment" runat="server" Visible="False" class="d-flex justify-content-center">
            <div class="">
                <div class="d-flex justify-content-center">
                    <table style="width: 100%;">
                        <tr>
                            <td>Period (Months): </td>
                            <td><asp:DropDownList ID="ddl_VCPeriod" runat="server" OnSelectedIndexChanged="ddl_VCPeriod_SelectedIndexChanged" AutoPostBack="True">
                                <asp:ListItem>--Select--</asp:ListItem>
                                <asp:ListItem Value="0.03">5</asp:ListItem>
                                <asp:ListItem Value="0.05">8</asp:ListItem>
                                <asp:ListItem Value="0.07">10</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>Interest Rate: </td>
                            <td><asp:Label ID="Lbl_VCInterest" runat="server" Text="Interest Rate"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Monthly Repayment: </td>
                            <td><asp:Label ID="Lbl_VCMthlyRepay" runat="server" Text="Monthly Repayment"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Total Repayment: </td>
                            <td><asp:Label ID="Lbl_VCTotalRepay" runat="server" Text="Total Repayment"></asp:Label></td>
                        </tr>
                    </table>
                </div>
                <div class="d-flex justify-content-center mt-4 mb-3">
                    <asp:Button ID="btn_instalpayvc" runat="server" Text="Pay" class="btn btn-outline-success" OnClick="btn_instalpayvc_Click"/>
                </div>
            </div>
        </asp:Panel>
        <asp:Label ID="Lbl_err" runat="server" Class="text-danger"></asp:Label>
    </section>
</div>
</asp:Content>
