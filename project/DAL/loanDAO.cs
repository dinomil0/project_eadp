﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Text;

namespace project.DAL
{
    public class loanDAO
    {
        public List<loan> getLoans(string loanType, double loanAmt)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("Select * from Loans where");
            sqlCommand.AppendLine("loanType = @LoanType AND (loanMin < @loanAmt OR loanMin = @loanAmt)");
            sqlCommand.AppendLine("order by loanInt");

            var loansList = new List<loan>();
            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("LoanType", loanType);
            da.SelectCommand.Parameters.AddWithValue("loanAmt", loanAmt);
            da.Fill(ds, "loanTable");
            int rec_cnt = ds.Tables["loanTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                for (int i = 0; i < rec_cnt; i++)
                {
                    loan loan = new loan();
                    DataRow row = ds.Tables["loanTable"].Rows[i];
                    loan.loanID = Convert.ToInt32(row["loanID"]);
                    loan.loanName = row["loanName"].ToString();
                    loan.loanInt = Convert.ToDouble(row["loanInt"]);
                    loan.loanFees = Convert.ToDouble(row["loanFees"]);
                    loan.loanRemark = row["loanRemark"].ToString();
                    loan.loanMinSalary = Convert.ToDouble(row["loanMinSalary"]);
                    loan.loanEIR = 0;
                    loan.loanTenor = 0;
                    loan.loanRepayment = 0;
                    loansList.Add(loan);
                }
            }
            else return null;
            return loansList;
        }

        public loan getLoanByName(string loanName)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("Select * from Loans where");
            sqlCommand.AppendLine("loanName = @loanName");

            var loans = new loan();
            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("loanName", loanName);
            da.Fill(ds, "loanTable");
            int rec_cnt = ds.Tables["loanTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                for (int i = 0; i < rec_cnt; i++)
                {
                    DataRow row = ds.Tables["loanTable"].Rows[i];
                    loans.loanID = Convert.ToInt32(row["loanID"]);
                    loans.loanName = row["loanName"].ToString();
                    loans.loanInt = Convert.ToDouble(row["loanInt"]);
                    loans.loanFees = Convert.ToDouble(row["loanFees"]);
                    loans.loanRemark = row["loanRemark"].ToString();
                    loans.loanMinSalary = Convert.ToDouble(row["loanMinSalary"]);
                    loans.loanEIR = 0;
                    loans.loanTenor = 0;
                    loans.loanRepayment = 0;
                }
            }
            else return null;
            return loans;
        }
    }
    
}