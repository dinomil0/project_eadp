﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class Class_Inquiry
    {
        static int count = 0;
        public Class_Inquiry()
        {
            count++;
        }
        public string get_count()
        {
            return Convert.ToString(count);
        }
        public string InquiryId { get; set; }
        public string ordId { get; set; }
        public string userID { get; set; }
        public string sellerID { get; set; }
        public string UserInput { get; set; }
        public string SellerInput { get; set; }
        public string InquiryDesc { get; set; }
    }
}