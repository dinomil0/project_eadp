﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class Class_OrderDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        public void Add_Order(Class_Order order)
        {
            StringBuilder sqlCommand = new StringBuilder();
            SqlCommand excommand = new SqlCommand();
            sqlCommand.AppendLine("INSERT INTO Orders");
            sqlCommand.AppendLine("VALUES (@paraordId, @paraProdId, @parauserID, @parasellerID, @parareference, @paraprice, @paraBuyQty, @paratotal, @paradebit, @parastatus, @paradate)");
            SqlConnection myConn = new SqlConnection(DBConnect);
            excommand = new SqlCommand(sqlCommand.ToString(), myConn);
            excommand.Parameters.AddWithValue("paraordId", order.ordId);
            excommand.Parameters.AddWithValue("paraProdId", order.ProdId);
            excommand.Parameters.AddWithValue("parauserID", order.userID);
            excommand.Parameters.AddWithValue("parasellerID", order.sellerID);
            excommand.Parameters.AddWithValue("parareference", order.reference);
            excommand.Parameters.AddWithValue("paraprice", order.price);
            excommand.Parameters.AddWithValue("paraBuyQty", order.BuyQty);
            excommand.Parameters.AddWithValue("paratotal", order.total);
            excommand.Parameters.AddWithValue("paradebit", order.debit);
            excommand.Parameters.AddWithValue("parastatus", order.status);
            excommand.Parameters.AddWithValue("paradate", order.date);
            myConn.Open();
            excommand.ExecuteNonQuery();
            myConn.Close();
        }
        public void Update_Status(string ordId, string status)
        {
            StringBuilder sqlCommand = new StringBuilder();
            SqlCommand excommand = new SqlCommand();
            sqlCommand.AppendLine("UPDATE Orders");
            sqlCommand.AppendLine("SET status = @parastatus");
            sqlCommand.AppendLine("WHERE ordId = @paraordId");
            SqlConnection myConn = new SqlConnection(DBConnect);
            excommand = new SqlCommand(sqlCommand.ToString(), myConn);
            excommand.Parameters.AddWithValue("parastatus", status);
            excommand.Parameters.AddWithValue("paraordId", ordId);
            myConn.Open();
            excommand.ExecuteNonQuery();
            myConn.Close();
        }
        public List<Class_Order> get_orders(string userID)
        {
            List<Class_Order> orderList = new List<Class_Order>();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Orders");
            sqlCommand.AppendLine("WHERE userID = @parauserID");
            sqlCommand.AppendLine("ORDER BY date DESC");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

            da.Fill(ds, "OrderTable");

            foreach (DataRow row in ds.Tables["OrderTable"].Rows)
            {
                Class_Order order = new Class_Order();
                order.date = Convert.ToDateTime(row["date"]);
                order.ordId = Convert.ToString(row["ordId"]);
                order.ProdId = Convert.ToString(row["ProdId"]);
                order.reference = Convert.ToString(row["reference"]);
                order.price = "$" + Convert.ToString(row["price"]);
                order.BuyQty = Convert.ToInt16(row["BuyQty"]);
                order.total = Convert.ToString(row["total"]);
                order.debit = Convert.ToString(row["debit"]);
                order.status = Convert.ToString(row["status"]);
                orderList.Add(order);
            }
            return orderList;
        }
        public bool checkordId(string userID, string ordId)
        {
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Orders");
            sqlCommand.AppendLine("WHERE userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

            da.Fill(ds, "OrderTable");

            foreach (DataRow row in ds.Tables["OrderTable"].Rows)
            {
                Class_Order order = new Class_Order();
                order.ordId = Convert.ToString(row["ordId"]);
                if (order.ordId == ordId)
                {
                    return true;
                }
            }
            return false;
        }
    }
}