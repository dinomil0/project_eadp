﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="CurrencyWallet.aspx.cs" Inherits="project.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="container">
        
        <%--<asp:Button ID="btnConvert" runat="server" CssClass="btn btn-success" Text="Convert Currencies" OnClick="btnConvert_Click" />--%>
        <br />
        <div class="form-group">
            <div class="table-responsive">
                <br />
                <asp:GridView ID="GridViewCurrencies" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" OnSelectedIndexChanged="GridViewCarLoan_SelectedIndexChanged" OnRowCommand="GridViewCurrencies_RowCommand">
                    <Columns>
                        <asp:BoundField DataField="CurrencyName" HeaderText="Currency Name" />
                        <asp:BoundField DataField="ConversionRate" HeaderText="Conversion Rate" />
                        <asp:BoundField DataField="Country" HeaderText="Country" />
                        <asp:BoundField DataField="Symbol" HeaderText="Symbol" />
                        <asp:ButtonField ButtonType="Button" CommandName="convertcurrencies" Text="Convert Currencies">
                        <ControlStyle CssClass="btn btn-success" />
                        </asp:ButtonField>
                    </Columns>
                </asp:GridView>
                <section id="CurrenciesTable" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
                    <h2 class="text-center">Currencies Wallet</h2>
                    <div class="row">
                        <div class="col-3">
                         <section id="MYRCurrencyamount" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
                            <div class="text-center">
                                <h4>
                                    <asp:Label runat="server" Text="MYR"></asp:Label>
                                </h4>
                            </div>
                            <div class="text-center">
                                <h1>
                                    <asp:Label ID="LblCurrencyAmtMYR" runat="server" Text=""></asp:Label>
                                </h1>
                            </div>
                         </section>
                    </div>          
                    <div class="col-3">
                        <section id="USDCurrencyamount" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
                            <div class="text-center">
                                <h4>
                                    <asp:Label runat="server" Text="USD"></asp:Label>
                                </h4>
                            </div>
                            <div class="text-center">
                                <h1>
                                    <asp:Label ID="LblCurrencyAmtUSD" runat="server" Text=""></asp:Label>
                                </h1>
                            </div>
                         </section>
                    </div>
                    <div class="col-3">
                        <section id="EURCurrencyamount" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
                            <div class="text-center">
                                <h4>
                                    <asp:Label runat="server" Text="EUR"></asp:Label>
                                </h4>
                            </div>
                            <div class="text-center">
                                <h1>
                                    <asp:Label ID="LblCurrencyAmtEUR" runat="server" Text=""></asp:Label>
                                </h1>
                            </div>
                         </section>
                    </div>
                    <div class="col-3">
                        <section id="GBPCurrencyamount" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
                            <div class="text-center">
                                <h4>
                                    <asp:Label runat="server" Text="GBP"></asp:Label>
                                </h4>
                            </div>
                            <div class="text-center">
                                <h1>
                                    <asp:Label ID="LblCurrencyAmtGBP" runat="server" Text=""></asp:Label>
                                </h1>
                            </div>
                         </section>
                    </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    

</asp:Content>
