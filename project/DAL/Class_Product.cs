﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class Class_Product
    {
        static int count = 0;
        public Class_Product()
        {
            count++;
        }
        public string get_count()
        {
            return Convert.ToString(count);
        }
        public string ProdId { get; set; }
        public string ProdName { get; set; }
        public string ProdDesc { get; set; }
        public double ProdPrice { get; set; }
        public int ProdQty { get; set; }
        public string SellerID { get; set; }
        public Byte[] ProdImage { get; set; }
    }
}