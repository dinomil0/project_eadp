﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class commerce_TopUpVC : System.Web.UI.Page
    {
        string userID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            
            //userID = Session["custNRIC"].ToString();
            userID = "S9123456A";
            Class_Account.VCAccount vcAcc = new Class_Account.VCAccount();
            Class_AccountDAO AccDAO = new Class_AccountDAO();
            vcAcc = AccDAO.getVCAccount(userID);
            Lbl_user.Text = "User: " + Convert.ToString(vcAcc.userID);
            Lbl_balvc.Text = "Balance:  " + Convert.ToString(vcAcc.VCbal) + "VC";
            Class_InquiryDAO inqDAO = new Class_InquiryDAO();
            bool result = inqDAO.check_VCInquiry(userID);
            if (result == true)
            {
                Class_Inquiry inquiry = new Class_Inquiry();
                inquiry = inqDAO.get_VCInquiry(userID);
                Lbl_Status.Text = "Pending";
                Lbl_Status.Visible = true;
                TB_CashOutAmt.Text = inquiry.UserInput;
                TB_CashOutAmt.ReadOnly = true;
                TA_Reason.InnerText = inquiry.InquiryDesc;
                TA_Reason.Disabled = true;
                btn_Submit.Enabled = false;
            }
            List<Class_Account.Account> accList = AccDAO.getAccount(userID);
            foreach (Class_Account.Account acc in accList)
            {
                if (ddl_bankNumber.Items.FindByValue(Convert.ToString(acc.bankaccnum)) == null)
                {
                    ddl_bankNumber.Items.Add(new ListItem(Convert.ToString(acc.bankaccname) + " " + Convert.ToString(acc.bankaccnum), Convert.ToString(acc.bankaccnum)));
                }
            }
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Lbl_selected.Text = rbl_VC.SelectedItem.Text;
            Lbl_price.Text = "SGD $" + rbl_VC.SelectedValue;
            Lbl_err.Text = "";
        }

        protected void btn_proceed_Click(object sender, EventArgs e)
        {
            if (rbl_VC.SelectedIndex == -1)
            {
                Lbl_err.Text = "Select an option";
                Lbl_err.Visible = true;
            }
            else
            {
                int amt = Convert.ToInt32(rbl_VC.SelectedValue);
                int vc = Convert.ToInt32(rbl_VC.SelectedValue) * 4;
                Class_AccountDAO AccDAO = new Class_AccountDAO();
                bool result = AccDAO.VCconversion(userID, Convert.ToInt32(ddl_bankNumber.SelectedValue), amt, vc);
                if (result == true)
                {
                    Class_Order order = new Class_Order();
                    Class_Transaction transaction = new Class_Transaction();
                    Class_TransactionDAO transDAO = new Class_TransactionDAO();
                    transaction.transID = "VC" + transaction.getCount();
                    transaction.ordId = "VCC" + order.getCount();
                    transaction.date = DateTime.Today;
                    transaction.reference = "VC Conversion";
                    transaction.price = Convert.ToString(amt);
                    transaction.qty = vc;
                    transaction.total = Convert.ToString(amt);
                    transaction.debit = transaction.total;
                    transaction.credit = "0.00";
                    transaction.userID = userID;
                    transaction.status = "Cleared";
                    transDAO.Add_Transaction(transaction, "Card", Convert.ToInt64(ddl_bankNumber.SelectedValue));
                    Lbl_err.Text = " ";
                    Lbl_err.Visible = false;
                    Response.Redirect("commerce_TopUpVC.aspx");
                }
                else
                {
                    Lbl_err.Text = "Insufficient funds in account";
                    Lbl_err.Visible = true;
                }
            }

        }
        protected void Req_CashOut(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal()", true);
        }
        protected void CashOut_Submit(object sender, EventArgs e)
        {
            long VCAmt;
            if (TB_CashOutAmt.Text == "")
            {
                Class_Account.VCAccount VCAcc = new Class_Account.VCAccount();
                Class_AccountDAO AccDAO = new Class_AccountDAO();
                VCAcc = AccDAO.getVCAccount(userID);
                VCAmt = VCAcc.VCbal;
                Class_Inquiry inquiry = new Class_Inquiry();
                Class_InquiryDAO inquiryDAO = new Class_InquiryDAO();
                inquiry.InquiryId = "COVC" + userID;
                inquiry.ordId = "COVC";
                inquiry.userID = userID;
                inquiry.UserInput = Convert.ToString(VCAmt) + "VC";
                inquiry.sellerID = "Aorium";
                inquiry.SellerInput = " ";
                inquiry.InquiryDesc = TA_Reason.InnerText;
                inquiryDAO.add_inquiry(inquiry);
            }
            else
            {
                int val;
                bool result = Int32.TryParse(TB_CashOutAmt.Text, out val);
                if (result == true)
                {
                    VCAmt = val;
                    Class_Inquiry inquiry = new Class_Inquiry();
                    Class_InquiryDAO inquiryDAO = new Class_InquiryDAO();
                    inquiry.InquiryId = "COVC" + userID;
                    inquiry.ordId = "COVC";
                    inquiry.userID = userID;
                    inquiry.UserInput = "Cash Out " + Convert.ToString(VCAmt);
                    inquiry.sellerID = "Aorium";
                    inquiry.SellerInput = " ";
                    inquiry.InquiryDesc = TA_Reason.InnerText;
                    inquiryDAO.add_inquiry(inquiry);
                }
                else
                {
                    Lbl_modalErr.Text = "Please enter a valid amount";
                    Lbl_modalErr.Visible = true;
                }
            }
        }
    }
}