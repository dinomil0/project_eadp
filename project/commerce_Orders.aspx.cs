﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class commerce_Orders : System.Web.UI.Page
    {
        string userID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            userID = Session["custNRIC"].ToString();
            Lbl_userID.Text = "User:" + userID;
            Class_OrderDAO orderDAO = new Class_OrderDAO();
            List<Class_Order> orderList = new List<Class_Order>();
            orderList = orderDAO.get_orders(userID);
            GridView1.DataSource = orderList;
            GridView1.DataBind();
        }
        protected void Clear_Click(object sender, CommandEventArgs e)
        {
            Class_OrderDAO orderDAO = new Class_OrderDAO();
            Class_ListDAO listDAO = new Class_ListDAO();
            int rowindex = Convert.ToInt16(e.CommandArgument.ToString());
            string ordId = GridView1.Rows[rowindex].Cells[1].Text;
            orderDAO.Update_Status(ordId, "Cleared");
            listDAO.Add_SellerTrans(ordId);

            Response.Redirect("commerce_Orders.aspx");
        }
        protected void Inquiry_Click(object sender, CommandEventArgs e)
        {
            int rowindex = Convert.ToInt16(e.CommandArgument.ToString());
            string status = GridView1.Rows[rowindex].Cells[8].Text;
            string ordId = GridView1.Rows[rowindex].Cells[1].Text;
            string ProdId = GridView1.Rows[rowindex].Cells[2].Text;
            Lbl_ordId.Text = ordId;
            Lbl_ProdId.Text = ProdId;
            ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal()", true);
        }
        protected void Inquiry_Submit(object sender, EventArgs e)
        {
            if (ddl_actions.SelectedIndex == 0)
            {
                Lbl_modalErr.Text = "Please select action.";
            }
            else
            {
                Class_ProductDAO prodDAO = new Class_ProductDAO();
                Class_Inquiry inquiry = new Class_Inquiry();
                Class_InquiryDAO inquiryDAO = new Class_InquiryDAO();
                inquiry.InquiryId = Lbl_ordId.Text + inquiry.get_count();
                inquiry.ordId = Lbl_ordId.Text;
                inquiry.userID = userID;
                inquiry.UserInput = ddl_actions.SelectedValue;
                inquiry.sellerID = prodDAO.getSellerID(Lbl_ProdId.Text);
                inquiry.SellerInput = " ";
                //inquiry.InquiryDesc = Request.Form["TA_complaint"];
                inquiry.InquiryDesc = TA_complaint.InnerText;
                inquiryDAO.add_inquiry(inquiry);
            }
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string status = DataBinder.Eval(e.Row.DataItem, "status").ToString();
                if (status == "Pending")
                {
                    e.Row.Cells[10].Enabled = false;
                }
                else if (status == "Cleared")
                {
                    e.Row.Cells[9].Enabled = false;
                }
            }
        }
    }
}