﻿<%@ Page Title="" Language="C#" MasterPageFile="~/commerceMaster.Master" AutoEventWireup="true" CodeBehind="commerce_TopUpVC.aspx.cs" Inherits="project.commerce_TopUpVC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="form1">
    <section class="mt-3">
        <div class="d-flex mx-auto my-auto w-50">
            <div class="col-4">
                <asp:Label ID="Lbl_user" runat="server" Text="Name" CssClass="h5"></asp:Label>
            </div>
            <div class="col-4">
                <asp:Label ID="Lbl_balvc" runat="server" Text="Balance" CssClass="h5"></asp:Label>
            </div>
            <div>
                <asp:Button ID="btn_CashOut" runat="server" Text="Cash Out" OnClick="Req_CashOut" class="btn btn-outline-danger"/>
            </div>
        </div>
        <hr class="w-75 justify-content-center"/>
        <div class="d-flex mx-auto my-auto w-50">
            <div class="mr-4 mt-4">
                <h4>Virtual Currency Top Up</h4>
                <div class="mt-3">
                    <asp:RadioButtonList ID="rbl_VC" runat="server" Width="190px" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                        <asp:ListItem Value="5">SGD $5 &nbsp, VC x 20</asp:ListItem> 
                        <asp:ListItem Value="10">SGD $10, VC x 40</asp:ListItem>
                        <asp:ListItem Value="20">SGD $20, VC x 80</asp:ListItem>
                        <asp:ListItem Value="50">SGD $50, VC x 200</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="mt-3">
                <asp:Panel ID="Panel1" runat="server" class="w-100">
                    <h5 class="mt-5">Review Purchase</h5>
                    <table class="">
                        <tr>
                            <td class="">Selected Product: </td>
                            <td>
                                <asp:Label ID="Lbl_selected" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="">Price: </td>
                            <td>
                                <asp:Label ID="Lbl_price" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Account: </td>
                            <td><asp:DropDownList ID="ddl_bankNumber" runat="server">
                                <asp:ListItem Selected="True">-----------------Select-----------------</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                    <div class=" mt-2 mb-2">
                        <asp:Button ID="btn_proceed" runat="server" Text="Convert" class="btn btn-outline-success" OnClick="btn_proceed_Click"/>
                    </div>
                    <small>Do take note that VC cannot be easily cashed out as part of company regulations.</small>
                    <asp:Label ID="Lbl_err" runat="server" Visible="False"></asp:Label>
                </asp:Panel>
            </div>
        </div>
    </section>
    <div class="modal fade" id="Inquiry" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <asp:Label ID="Lbl_Status" runat="server" Text="" Visible="false" CssClass="h5"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Request: </td>
                            <td>Cash Out VC</td>
                        </tr>
                    </table>
                </div>
                <div class="modal-body">
                    <table style="width: 100%;">
                        <tr>
                            <td>Amount: </td>
                            <td><asp:TextBox ID="TB_CashOutAmt" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Reason: </td>
                            <td><textarea id="TA_Reason" name="TA_Reason" cols="20" rows="2" runat="server"></textarea></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                    <asp:Button ID="btn_Submit" runat="server" Text="Submit" class="btn btn-outline-success" OnClick="CashOut_Submit"/>
                    <asp:Label ID="Lbl_modalErr" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <script type='text/javascript'>
        function openModal(){$('[id*=Inquiry]').modal('show');} 
    </script>
</div>
</asp:Content>