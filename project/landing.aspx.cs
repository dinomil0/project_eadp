﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class landing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (checkUsername())
            {
                Session["loginStatus"] = true;
                Response.Redirect("sidePage.aspx");
            }
        }

        private bool checkUsername()
        {
            var nricRegex = new Regex(@"^[STFG stfg]\d{7}[a-z A-Z]$");
            var adminRegex = new Regex(@"^[A-Z a-z]\d{7}[a-z A-Z]$");
            var phoneRegex = new Regex(@"[8|9]\d{7}");
            if (!string.IsNullOrEmpty(tbUsername.Text))
            {
                if (!string.IsNullOrEmpty(tbPassword.Text))
                {
                    customer custObj = new customer();
                    customerDAO custDao = new customerDAO();
                    custObj = custDao.GetCustomerLoginCredential(tbUsername.Text);
                    if (Int32.TryParse(tbUsername.Text, out int intUsername))
                    {
                        if (phoneRegex.IsMatch(tbUsername.Text))
                        {
                            if (custObj.phonenum == Convert.ToInt32(tbUsername.Text))
                                if (custObj.customerPassword == tbPassword.Text)
                                {
                                    Session["custNRIC"] = custObj.userID;
                                    return true;
                                }
                                else
                                {
                                    lblError.Text = "Incorrect Password";
                                }
                            else
                            {
                                lblError.Text = "User does not exist";
                            }
                        }
                        else
                        {
                            lblError.Text = "Enter valid phone number";
                            //return false;
                        }
                    }
                    else if (nricRegex.IsMatch(tbUsername.Text))
                    {
                        if (custObj.userID == tbUsername.Text)
                            if (custObj.customerPassword == tbPassword.Text)
                            {
                                Session["custNRIC"] = custObj.userID;
                                return true;
                            }
                            else
                            {
                                lblError.Text = "Incorrect Password";
                            }
                        else
                        {
                            lblError.Text = "User does not exist";
                        }
                    }
                    else if (adminRegex.IsMatch(tbUsername.Text))
                    {
                        if (custObj.userID == tbUsername.Text)
                            if (custObj.customerPassword == tbPassword.Text)
                            {
                                Session["custNRIC"] = custObj.userID;
                                return true;
                            }
                            else
                            {
                                lblError.Text = "Incorrect Password";
                            }
                        else
                        {
                            lblError.Text = "User does not exist";
                        }
                    }
                    else
                    {
                        lblError.Text = "Please enter valid NRIC or phone number";
                        //return false;
                    }
                }
                else
                {
                    lblError.Text = "Please enter your password";
                    //return false;
                }
            }
            else
            {
                lblError.Text = "Please enter your NRIC or phone number";
                //return false;
            }
            return false;
        }
    }
}