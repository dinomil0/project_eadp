﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class commerce_BuyPage : System.Web.UI.Page
    {
        string userID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            userID = Session["custNRIC"].ToString();
            //Get area code by session
            //Get currency details by area code
            Class_Product product = new Class_Product();
            Class_ProductDAO prodDAO = new Class_ProductDAO();
            string ProdId = Request.QueryString["ProdId"];
            product = prodDAO.getProduct(ProdId);
            string strBase64 = Convert.ToBase64String(product.ProdImage);
            string url = "data:img/png;base64," + strBase64;
            ProdImage.ImageUrl = url;
            Lbl_productname.Text = product.ProdName;
            LBl_productID.Text = product.ProdId;
            Lbl_ProdDesc.Text = product.ProdDesc;
            //Convert display price into local currency + symbol
            Lbl_ProdPrice.Text = "$" + Convert.ToString(product.ProdPrice);
        }
        protected void btn_cart_Click(object sender, EventArgs e)
        {
            short qty;
            bool result = Int16.TryParse(TB_Qty.Text, out qty);
            Class_ListDAO ListDAO = new Class_ListDAO();
            if (result)
            {
                qty = Convert.ToInt16(TB_Qty.Text);
                ListDAO.Add_Cart(userID, LBl_productID.Text, Lbl_productname.Text, qty, Convert.ToDouble(Lbl_ProdPrice.Text.Substring(1)));
                Response.Redirect("commerce_Cart.aspx");
            }
            else
            {
                Lbl_Err.Text = "Please enter a quantity";
                Lbl_Err.Visible = true;
            }

        }
        protected void btn_instal_Click(object sender, EventArgs e)
        {
            short qty;
            bool result = Int16.TryParse(TB_Qty.Text, out qty);
            if (!result)
            {
                Lbl_Err.Text = "Please enter a quantity";
                Lbl_Err.Visible = true;
            }
            else if (Convert.ToDouble(Lbl_ProdPrice.Text.Substring(1)) < 1000)
            {
                Lbl_Err.Text = "Payment is too low to use instalment";
                Lbl_Err.Visible = true;
            }
            else
            {
                Response.Redirect("commerce_Instalment.aspx?ProdId=" + LBl_productID.Text + "&qty=" + TB_Qty.Text + "&price=" + Convert.ToDouble(Lbl_ProdPrice.Text.Substring(1)));
            }
        }
    }
}