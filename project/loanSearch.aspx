﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="loanSearch.aspx.cs" Inherits="project.bankSite.personalLoan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section  style="background-image:url(../img/loanSearch.jpg);background-repeat:no-repeat;height:100vh;">
        <%----%>
            <section class="border border-secondary bg-light shadow p-3 mb-5 rounded container mt-5">
                <asp:Panel ID="pnlError" runat="server" Visible="false">
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    <asp:Button ID="btnClose" class="close btn btn-outline-light btn-sm" type="button" runat="server" Text="&times;" data-dismiss="alert" aria-label="Close" OnClick="btnClose_Click"/>
                </asp:Panel>
                <div class="form-group d-md-flex d-sm-block ">
                    <div class="col-md-3 col-sm-6 d-flex px-0 ml-md-5 justify-content-center">
                        <div class="">
                        <label class="pr-2 mt-2">I want a</label>
                        </div>
                        <div class="">
                            <asp:DropDownList ID="loanType" runat="server"  OnSelectedIndexChanged="loanType_SelectedIndexChanged" AutoPostBack="True" CssClass="form-control">
                                <asp:ListItem >Loan Type</asp:ListItem>
                                <asp:ListItem Value="Personal">Personal Loan</asp:ListItem>
                                <asp:ListItem Value="Auto">Auto Loan</asp:ListItem>
                                <asp:ListItem Value="Housing">Housing Loan</asp:ListItem>
                            </asp:DropDownList> 
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 d-inline-flex px-0 justify-content-center">
                        <label class="pl-md-2 pr-2 mt-2">of</label>
                        <div class="">
                            <div class="input-group mb-auto">
                            <div class="input-group-prepend">
                                <div class="input-group-text rounded-left">&#36;</div>
                            </div>
                            <asp:TextBox ID="loanAmt" class="form-control" type="number" runat="server" placeholder="Amount" min="0"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6  d-inline-flex px-0">
                        <label class="pl-md-2 pr-2 mt-2">With a Tenor of</label>
                        <div class="">
                            <div class="input-group">
                                <asp:TextBox ID="loanTenor" class="form-control" type="number" runat="server" placeholder="Tenor" min="0"></asp:TextBox>
                                <div class="input-group-prepend">
                                    <div class="input-group-text rounded-right">Year(s)</div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlPeriod" runat="server" Visible="False" class="d-flex ml-md-5  ">
                    <div class="col-md-2 col-sm-6">
                        <asp:Label runat="server">Instalment Interval:</asp:Label>
                    </div>
                    <div class="col-sm-6">
                        <asp:RadioButtonList ID="rbPeriod" runat="server">
                            <asp:ListItem Value="12" Selected="True">Monthly</asp:ListItem>
                            <asp:ListItem Value="4">Quarterly</asp:ListItem>
                            <asp:ListItem Value="2">Semi-Annual</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>       
                </asp:Panel>
              <div class="d-flex justify-content-center">
                  <asp:Button ID="loanSearch" class="btn btn-outline-success" runat="server" Text="Search for Loans" OnClick="loanSearch_Click" />
              </div>
            </section>
            <%--<section class="d-flex justify-content-center">--%>
            <section class="">
                <asp:Panel ID="pnlGV" runat="server" Visible="false" CssClass="border border-secondary bg-light shadow p-3 mb-5 rounded container mt-3">
                    <%--<asp:GridView ID="gvLoans" runat="server" OnSelectedIndexChanged="gvLoans_SelectedIndexChanged"></asp:GridView>--%>
                    <asp:GridView ID="gvLoans" class="table table-responsive-sm table-bordered" runat="server" AutoGenerateColumns="False" OnRowCommand="gvLoans_RowCommand" OnRowDataBound="gvLoans_RowDataBound">
                        <Columns>
                            <asp:BoundField HeaderText="Name" DataField="loanName"/>
                            <asp:BoundField HeaderText="Interest Rate" DataField="loanInt" DataFormatString="{0:P}"/>
                            <asp:BoundField HeaderText="Effective Interest Rate (EIR)" DataField="loanEIR" DataFormatString="{0:P}"/>
                            <asp:BoundField HeaderText="Tenor" DataField="loanTenor"/>
                            <asp:BoundField HeaderText="Repayment Amount" DataField="loanRepayment" DataFormatString="${0:n}"/>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Details" Text="Details" OnClick="showModal"></asp:LinkButton>
                                </ItemTemplate>
                                <ControlStyle CssClass="btn btn-info" />
                            </asp:TemplateField>
                            
                            <asp:BoundField DataField="loanRemark" HeaderText="Additional Information" />
                            
                        </Columns>
                        <HeaderStyle Cssclass="thead-dark"/>    
                    </asp:GridView>
                </asp:Panel>
            </section>
            <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Loan Details</h4>
                        <button type="button" class="close" data-dismiss="modal">
                            &times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            <div class="form-group">
                                <asp:Label ID="lbl1" runat="server" Text="Loan Name:"></asp:Label>
                                <asp:Label ID="lblLoanName" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="Tenor:"></asp:Label>
                                <asp:Label ID="lblLoanTenor" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label5" runat="server" Text="Effective Interest Rate:"></asp:Label>
                                <asp:Label ID="lblEIR" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text="Repayment Amount:"></asp:Label>
                                <asp:Label ID="lblRepaymentAmt" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label1" runat="server" Text="Total Interest:"></asp:Label>
                                <asp:Label ID="lblTotalInt" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text="Total Payment:"></asp:Label>
                                <asp:Label ID="lblTotalAmt" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="pie-container"  style="position: relative;">
                            <canvas id="PieChart"></canvas>
                        </div>
                        <script>
                        DepositWithdrawal = JSON.parse('<%=Session["pieChart"]%>');

                            var savingsGoal = 100;
                            var savingsSaved = 75;

                            var ctx = document.getElementById('PieChart').getContext('2d');
                            var progresspie = new Chart(ctx,
                                {
                                  type: 'bar',
                                    data:
                                    {
                                    labels: ['Total Payable'],
                                      datasets:
                                          [
                                            {
                                                label: 'Principal',
                                                data: [DepositWithdrawal[1]],
                                                backgroundColor: '#EC6778',
                                            },
                                            {
                                            label: 'Total Interest',
                                            data: [DepositWithdrawal[0]],
                                            backgroundColor: '#FFB449',
                                            },
                                              
                                          ]
                                    },
                                    options:
                                    {
                                        scales:
                                        {
                                          xAxes: [{ stacked: true }],
                                          yAxes: [{ stacked: true }]
                                        }
                                    }
                                }
                            );
                        </script>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnApply" runat="server" CssClass="btn btn-outline-success" Text="Apply Loan" OnClick="btnApply_Click" />
                        <%--<asp:Button ID="btnSave" runat="server" Text="Pay" OnClick="" CssClass="btn btn-info" />
                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>--%>
                    </div>
                </div>
            </div>
        </div>
            <script type='text/javascript'>
                function openModal(){$('[id*=myModal]').modal('show');} 
        </script>
        <%----%>
    </section>
</asp:Content>
