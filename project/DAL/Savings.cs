﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class Savings
    {
        public Savings()
        {

        }
        public int savingsID { get; set; }
        public double savingGoal { get; set; }
        public double amountsaved { get; set; }
        public DateTime endDate { get; set; }
        public DateTime startDate { get; set; }
        public string description { get; set; }
        public int bankaccnum { get; set; }
        public string userID { get; set; }
        public int status { get; set; }

        public int RenewMode { get; set; }
    }
}