﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="StatusPendingWithdrawal.aspx.cs" Inherits="project.StatusPendingWithdrawal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section id="StatusPendingWithdrawal" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
    <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="TransactionHistory.aspx">Transcation History</a></li>
            <li class="breadcrumb-item active" aria-current="page">Withdrawal of Transaction</li>
          </ol>
    </nav>
    <h2 class="text-center display-4">Withdrawal of Transaction</h2>
    <table class="table table-hover">
        <tr>
            <td class="auto-style1">User ID:</td>
            <td class="auto-style2">
                <asp:Label ID="LblUserID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Reference Number:</td>
            <td class="auto-style2">
                <asp:Label ID="LblWithdrawalReferenceNum" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Date:</td>
            <td class="auto-style2">
                <asp:Label ID="LblWithdrawalDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Description:</td>
            <td class="auto-style2">
                <asp:Label ID="LblWithdrawalDescription" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Type:</td>
            <td class="auto-style2">
                <asp:Label ID="LblWithdrawalType" runat="server"></asp:Label>
            </td>
            
        </tr>
        <tr>
            <td class="auto-style1">Amount:</td>
            <td>
                <asp:Label ID="LblWithdrawalAmt" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Send To:</td>
            <td>
                <asp:Label ID="LblWithdrawalSendTo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Send To Bank Account Number:</td>
            <td>
                <asp:Label ID="LblWithdrawalSendToBankAcc" runat="server"></asp:Label>
            </td>
        </tr>
            <tr>
            <td class="auto-style1">Send Using:</td>
            <td>
                <asp:Label ID="LblWithdrawalSendUsing" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Status:</td>
            <td class="auto-style2">
                <asp:Label ID="LblWithdrawalStatus" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">From Bank Account:</td>
            <td class="auto-style2">
                <asp:Label ID="LblWithdrawalbankacc" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Time Remaining:</td>
            <td class="auto-style2">
                <asp:ScriptManager ID="sp" runat="server">
            </asp:ScriptManager>
            <asp:Timer ID="timerTest" runat="server" Interval="1000" OnTick="timerTest_tick">
            </asp:Timer>
            <asp:UpdatePanel ID="up" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Literal ID="litMsg" runat="server"></asp:Literal>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="timerTest" EventName="tick" />
                </Triggers>
            </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">
                <asp:Button ID="BtnWithdrawalConfirm" runat="server" Text="Withdraw Transaction" class="btn btn-warning" OnClick="BtnWithdrawalConfirm_Click" OnClientClick="return confirm('Withdraw this Transaction?')"/>&nbsp;&nbsp;
                <asp:Button ID="BtnWithdrawalBack" runat="server" Text="Back" class="btn btn-secondary" OnClick="BtnWithdrawalBack_Click"/>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">
                <asp:Label ID="LblResult" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</section>

</asp:Content>
