﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="TransferPayYouCompletion.aspx.cs" Inherits="project.TransferPayYouCompletion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
    hr{
        border:1px solid orange
    }
        .auto-style1 {
            width: 606px;
        }
        .auto-style3 {
            width: 606px;
        }
        .auto-style4 {
            width: 606px;
        }
        .auto-style5 {
            width: 606px;
        }
    </style>
<section id="TransferPayNowVerify" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
  
    <div class="container">
        <label>
            <h1>PayYou</h1>
            <h3>Make a transaction</h3>
        </label>
        <ul class="progress-indicator">
            <li class="completed">
                <span class="bubble"></span>
                <i class="fa fa-check-circle"></i>
                1.Input Details
            </li>
            <li class="completed">
                <span class="bubble"></span>
                <i class="fa fa-check-circle"></i>
                2.Verify Details
            </li>
            <li class="completed">
                <span class="bubble"></span>
                3.Completion
            </li>
        </ul>
        <hr />
        
        <asp:Label ID="LblCompletion" runat="server" Text=""></asp:Label>
        <h5>Recipient Details</h5>
        
        
            <table class="table">
                <tr>
                    <td class="auto-style1">Type:</td>
                    <td>
                        <asp:Label ID="LblType" runat="server"  ></asp:Label>
                    </td>
                </tr>
                </table>
              <asp:Panel ID="VerifyPhone1" runat="server" Visible="false">
               <table class="table">
                    <tr>
                    <td class="auto-style3">Recipient's Mobile Number:</td>
                    <td>
                        <asp:Label ID="LblRecipMobileNum" runat="server"  ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">Recipient's Nickname:</td>
                    <td>
                        <asp:Label ID="LblRecipNickNameMobileNum" runat="server"  ></asp:Label>
                    </td>
                </tr>
                   <tr>
                    <td class="auto-style3">Recipient's Bank Account Number:</td>
                    <td>
                        <asp:Label ID="LblRecipBankAccnumMobileNum" runat="server"  ></asp:Label>
                    </td>
                </tr>
                   <tr>
                    <td class="auto-style3">Recipient's Bank Account Name:</td>
                    <td>
                        <asp:Label ID="LblRecipBankAccnameMobileNum" runat="server"  ></asp:Label>
                    </td>
                </tr>
               </table>
                </asp:Panel>
            <asp:Panel ID="VerifyNRIC1" runat="server" Visible="false">
                <table class="table">
                    <tr>
                    <td class="auto-style5">Recipient's NRIC:</td>
                    <td>
                        <asp:Label ID="LblRecipNRIC" runat="server"  ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">Recipient's Nickname:</td>
                    <td>
                        <asp:Label ID="LblRecipNickNameNRIC" runat="server"  ></asp:Label>
                    </td>
                </tr>
                    <tr>
                    <td class="auto-style5">Recipient's Bank Account Number:</td>
                    <td>
                        <asp:Label ID="LblRecipBankaccnumNRIC" runat="server"  ></asp:Label>
                    </td>
                </tr>
                    <tr>
                    <td class="auto-style5">Recipient's Bank Account Name:</td>
                    <td>
                        <asp:Label ID="LblRecipBankaccnameNRIC" runat="server"  ></asp:Label>
                    </td>
                </tr>
               </table>
            </asp:Panel>
            
            <hr />
            <h5>Transfer Details</h5>
            <table class="table">
                <tr>
                    <td class="auto-style1">Transfer Amount:</td>
                    <td>
                        <asp:Label ID="LblTransferAmt" runat="server"  ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">From Account:</td>
                    <td>
                        <asp:Label ID="LblAccount" runat="server"  ></asp:Label>
                    </td>
                </tr>
            </table>
            <div class="d-flex">
	            <div class="form-group">
                    <!-- Cancel Button -->
                    <asp:Button ID="redirectTHButtonPayYouCompletion" runat="server" Text="Transaction History" type="reset" class="btn btn-primary" OnClick="redirectTHButtonPayYouCompletion_Click"/> &nbsp;&nbsp;
                    <!-- Submit Button -->
                    <asp:Button ID="submitButtonPayYouCompletion" runat="server" Text="Finish" type="submit" class="btn btn-success" OnClick="submitButtonPayYouCompletion_Click"/>
                </div>
                <asp:Label ID="Lbl_errcomplete" runat="server" Text=""></asp:Label>
            </div>
    			
    </div>
</section>
</asp:Content>
