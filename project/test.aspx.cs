﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //List<Currency> data = GetCurriences();
            //if (data != null)
            //{
            //    GridViewCurrTest.DataSource = data;
            //    GridViewCurrTest.DataBind();
            //}
        }
        //public List<Currency> GetCurriences()
        //{
        //    HttpClient client = new HttpClient();
        //    // state the URI address that hosted the web API
        //    client.BaseAddress = new Uri("http://data.fixer.io/api/latest?access_key=e624a04f905847ef8dce1959ecb0aaf4&symbols=USD,AUD,CAD,PLN,MXN&format=1");
        //    List<Currency> data = new List<Currency>();
        //    Task<HttpResponseMessage> getTask = client.GetAsync("api/Currency");
        //    getTask.Wait();

        //    var result = getTask.Result;
        //    if (result.IsSuccessStatusCode)
        //    {
        //        var readTask = result.Content.ReadAsAsync<List<Currency>>();
        //        readTask.Wait();

        //        data = readTask.Result;
        //    }
        //    else
        //    {
        //        data = null;
        //    }
        //    return data;
        //}
        [WebService(Namespace = "http://tempuri.org/")]
        [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
        // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
        [System.Web.Script.Services.ScriptService]
        public class WebService : System.Web.Services.WebService
        {
            [WebMethod]
            public string CurrencyConversion(decimal amount, string fromCurrency, string toCurrency)
            {
                WebClient web = new WebClient();
                string url = string.Format("http://www.google.com/ig/calculator?hl=en&q={2}{0}%3D%3F{1}", fromCurrency.ToUpper(), toCurrency.ToUpper(), amount);
                string response = web.DownloadString(url);
                Regex regex = new Regex(@":(?<rhs>.+?),");
                string[] arrDigits = regex.Split(response);
                string rate = arrDigits[3];
                return rate;
            }
        }
    }
}