﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class budget_Overview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }

            if (!IsPostBack)
            {
                try
                {
                    //Retrieving Customer's Bank Accounts
                    List<int> read_Account_List = new List<int>();
                    AccountsDAO read_AccountDAO = new AccountsDAO();
                    read_Account_List = read_AccountDAO.read_Account(Session["custNRIC"].ToString());
                    foreach (var account in read_Account_List)
                    {
                        ddl_Bg_Read_Account.Items.Add(Convert.ToString(account));
                    }

                    //Checking & Creating if Current Month's Budget Exist
                    int bankaccnum = Convert.ToInt32(ddl_Bg_Read_Account.SelectedValue);
                    budgetDAO read_BudgetDAO = new budgetDAO();
                    read_BudgetDAO.validate_Budget(bankaccnum);

                    //Retrieve Specific Bank Account's Budget for Current Month
                    read_Account_Info();
                }
                catch
                {
                    //Exception
                }
            }
        }

        protected void ddl_Bg_Read_Account_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int bankaccnum = Convert.ToInt32(ddl_Bg_Read_Account.SelectedValue);
                budgetDAO read_BudgetDAO = new budgetDAO();
                read_BudgetDAO.validate_Budget(bankaccnum);
                read_Account_Info();
            }
            catch
            {
                //Exception
            }

        }

        protected void btn_Bg_Update_Data_Click(object sender, EventArgs e)
        {
            try
            {
                tb_Bg_Update_Consumer_Debt.Text = tb_Bg_Read_Consumer_Debt.Text;
                tb_Bg_Update_Food_Grocery.Text = tb_Bg_Read_Food_Grocery.Text;
                tb_Bg_Update_Healthcare.Text = tb_Bg_Read_Healthcare.Text;
                tb_Bg_Update_Housing.Text = tb_Bg_Read_Housing.Text;
                tb_Bg_Update_Personal_Care.Text = tb_Bg_Read_Personal_Care.Text;
                tb_Bg_Update_Utility.Text = tb_Bg_Read_Utility.Text;
                tb_Bg_Update_Entertainment.Text = tb_Bg_Read_Entertainment.Text;
                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "initiate_Update_Modal();", true);
            }
            catch
            {

            }
        }

        protected void btn_Bg_Update_Data_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                if (validate_Update())
                {
                    int bankaccnum = Convert.ToInt32(ddl_Bg_Read_Account.SelectedValue);
                    DateTime budget_Date = Convert.ToDateTime(tb_Bg_Read_Date.Text);
                    decimal salary = Convert.ToDecimal(tb_Bg_Read_Salary.Text);
                    decimal consumer_Debt = Convert.ToDecimal(tb_Bg_Update_Consumer_Debt.Text);
                    decimal food_Grocery = Convert.ToDecimal(tb_Bg_Update_Food_Grocery.Text);
                    decimal healthcare = Convert.ToDecimal(tb_Bg_Update_Healthcare.Text);
                    decimal housing = Convert.ToDecimal(tb_Bg_Update_Housing.Text);
                    Decimal personal_Care = Convert.ToDecimal(tb_Bg_Update_Personal_Care.Text);
                    decimal utility = Convert.ToDecimal(tb_Bg_Update_Utility.Text);
                    decimal entertainment = Convert.ToDecimal(tb_Bg_Update_Entertainment.Text);

                    //Calculating Emergency Fund & Saving
                    List<decimal> emergency_Fund_Saving = calculate_Emergency_Fund_Saving(salary, consumer_Debt, food_Grocery, healthcare, housing, personal_Care, utility, entertainment);

                    //Updating Budget Data
                    budgetDAO updateBudgetDAO = new budgetDAO();
                    decimal emergency_Fund = emergency_Fund_Saving[0];
                    decimal saving = emergency_Fund_Saving[1];
                    updateBudgetDAO.update_Budget(bankaccnum, budget_Date, consumer_Debt, food_Grocery, healthcare, housing, personal_Care, utility, entertainment, emergency_Fund, saving);
                    Response.Redirect(Request.RawUrl);
                }
            }
            catch
            {

            }
        }

        public string read_Account_Info()
        {
            lb_Bg_Title.Text = "Budget for " + DateTime.Today.ToString("MMMM yyyy");
            budget read_Budget = new budget();
            budgetDAO read_BudgetDAO = new budgetDAO();
            read_Budget = read_BudgetDAO.read_Budget_List(Convert.ToInt32(ddl_Bg_Read_Account.SelectedValue));
            try
            {
                //Retrieving Current Month's Budget
                tb_Bg_Read_Date.Text = read_Budget.budget_Date.ToString();
                tb_Bg_Read_Count.Text = "Your Budget has been updated " + read_Budget.update_Count.ToString() + " times!";
                tb_Bg_Read_Detail.Text = "Last Updated: " + read_Budget.update_Detail.ToString();
                tb_Bg_Read_Consumer_Debt.Text = read_Budget.consumer_Debt.ToString();
                tb_Bg_Read_Food_Grocery.Text = read_Budget.food_Grocery.ToString();
                tb_Bg_Read_Healthcare.Text = read_Budget.healthcare.ToString();
                tb_Bg_Read_Housing.Text = read_Budget.housing.ToString();
                tb_Bg_Read_Personal_Care.Text = read_Budget.personal_Care.ToString();
                tb_Bg_Read_Utility.Text = read_Budget.utility.ToString();
                tb_Bg_Read_Entertainment.Text = read_Budget.entertainment.ToString();
                tb_Bg_Read_Emergency_Fund.Text = read_Budget.emergency_Fund.ToString();
                tb_Bg_Read_Saving.Text = read_Budget.saving.ToString();

                //Retrieving Previous 3 Months & Current Month's Expenses
                Dictionary<string, Dictionary<string, decimal>> expenses_Dict = new Dictionary<string, Dictionary<string, decimal>>();
                TransactionHistoryDAO transactionDAO_Obj = new TransactionHistoryDAO();
                expenses_Dict = transactionDAO_Obj.read_Total_Per_Category(Convert.ToInt32(ddl_Bg_Read_Account.SelectedValue));
                List<List<decimal>> month_Overview = new List<List<decimal>>();
                List<decimal> consumer_Debt = new List<decimal>();
                List<decimal> food_Grocery = new List<decimal>();
                List<decimal> healthcare = new List<decimal>();
                List<decimal> housing = new List<decimal>();
                List<decimal> personal_Care = new List<decimal>();
                List<decimal> utility = new List<decimal>();
                List<decimal> entertainment = new List<decimal>();
                List<decimal> salary = new List<decimal>();
                foreach (KeyValuePair<string, Dictionary<string, decimal>> monthly_Expenses in expenses_Dict)
                {
                    consumer_Debt.Add(monthly_Expenses.Value["consumer_Debt"]);
                    food_Grocery.Add(monthly_Expenses.Value["food_Grocery"]);
                    healthcare.Add(monthly_Expenses.Value["healthcare"]);
                    housing.Add(monthly_Expenses.Value["housing"]);
                    personal_Care.Add(monthly_Expenses.Value["personal_Care"]);
                    utility.Add(monthly_Expenses.Value["utility"]);
                    entertainment.Add(monthly_Expenses.Value["entertainment"]);
                    salary.Add(monthly_Expenses.Value["salary"]);
                }

                //Retrieving Current Salary & Summary
                lb_Bg_Summary.Text = null;
                decimal current_Salary = salary[salary.Count - 1];
                if (current_Salary == 0)
                {
                    pn_Bg_Salary.Visible = false;
                    lb_Bg_Summary.Text += "Unfortunately, there is no Salary in this Account for us to compare. Please switch to an Account with a Salary for us to compare. <br>";
                }
                else
                {
                    pn_Bg_Salary.Visible = true;
                    tb_Bg_Read_Salary.Text = Convert.ToString(current_Salary);

                    //Comparing Current Month's Budget Categories
                    decimal recommended_Need_Percentage = 50;
                    decimal recommended_Want_Percentage = 30;
                    decimal recommended_Reserve_Percentage = 20;
                    decimal difference = 0;

                    decimal total_Need_Percentage = Math.Round(((read_Budget.consumer_Debt + read_Budget.food_Grocery + read_Budget.healthcare + read_Budget.housing + read_Budget.personal_Care + read_Budget.utility) / current_Salary * 100), 2);
                    decimal total_Want_Percentage = Math.Round((read_Budget.entertainment / current_Salary * 100), 2);
                    decimal total_Reserve_Percentage = 100 - total_Need_Percentage - total_Want_Percentage;

                    if (total_Need_Percentage > recommended_Need_Percentage)
                    {
                        difference = total_Need_Percentage - recommended_Need_Percentage;
                        lb_Bg_Summary.Text += "Your Current Percentage of Needs to Salary is " + total_Need_Percentage.ToString() + "%, which is <strong><font color='#D9534F'>" + difference.ToString() + "%</font></strong> higher than the Recommended Percentage of 50%! <br>";
                    }
                    if (total_Want_Percentage > recommended_Want_Percentage)
                    {
                        difference = total_Want_Percentage - recommended_Want_Percentage;
                        lb_Bg_Summary.Text += "Your Current Percentage of Wants to Salary is " + total_Want_Percentage.ToString() + "%, which is <strong><font color='#D9534F'>" + difference.ToString() + "%</font></strong> higher than the Recommended Percentage of 30%! <br>";
                    }
                    if (total_Reserve_Percentage < recommended_Reserve_Percentage)
                    {
                        difference = recommended_Reserve_Percentage - total_Reserve_Percentage;
                        lb_Bg_Summary.Text += "Your Current Percentage of Reserves to Salary is " + total_Reserve_Percentage.ToString() + "%, which is <strong><font color='#D9534F'>" + difference.ToString() + "%</font></strong> lower than the Recommended Percentage of 20%! <br>";
                    }
                }

                //Calculating the Average Spending per Category
                decimal average_Consumer_Debt = (consumer_Debt.GetRange(0, 3)).Average();
                decimal average_Food_Grocery = (food_Grocery.GetRange(0, 3)).Average();
                decimal average_Healthcare = (healthcare.GetRange(0, 3)).Average();
                decimal average_Housing = (housing.GetRange(0, 3)).Average();
                decimal average_Personal_Care = (personal_Care.GetRange(0, 3)).Average();
                decimal average_Utility = (utility.GetRange(0, 3)).Average();
                decimal average_Entertainment = (entertainment.GetRange(0, 3)).Average();
                lb_Bg_Summary.Text +=
                    "<br> Based on your expenses in the past 3 months, <br>"
                    + "<br>Average Consumer Debts is S$" + Math.Round(average_Consumer_Debt, 2).ToString()
                    + "<br>Average Food & Groceries is S$" + Math.Round(average_Food_Grocery, 2).ToString()
                    + "<br>Average Healthcare is S$" + Math.Round(average_Healthcare, 2).ToString()
                    + "<br>Average Housing is S$" + Math.Round(average_Housing, 2).ToString()
                    + "<br>Average Personal Care is S$" + Math.Round(average_Personal_Care, 2).ToString()
                    + "<br>Average Utility is S$" + Math.Round(average_Utility, 2).ToString();

                month_Overview.Add(consumer_Debt);
                month_Overview.Add(food_Grocery);
                month_Overview.Add(healthcare);
                month_Overview.Add(housing);
                month_Overview.Add(personal_Care);
                month_Overview.Add(utility);
                month_Overview.Add(entertainment);
                string value = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(month_Overview);
                return value;
            }
            catch
            {
                return null;
            }
        }

        protected List<decimal> calculate_Emergency_Fund_Saving(decimal salary, decimal consumer_Debt, decimal food_Grocery, decimal healthcare, decimal housing, decimal personal_Care, decimal utility, decimal entertainment)
        {
            List<decimal> remaining_Salary_List = new List<decimal>();
            try
            {
                //Emergency Fund: 20% & Saving: 80%
                decimal remaining_Salary = salary - (consumer_Debt + food_Grocery + healthcare + housing + personal_Care + utility + entertainment);
                decimal emergency_Fund = remaining_Salary * Convert.ToDecimal(0.20);
                decimal saving = remaining_Salary * Convert.ToDecimal(0.80);
                remaining_Salary_List.Add(emergency_Fund);
                remaining_Salary_List.Add(saving);
            }
            catch
            {
                //Exception
            }
            return remaining_Salary_List;
        }

        protected Boolean validate_Update()
        {
            decimal consumer_Debt;
            decimal food_Grocery;
            decimal healthcare;
            decimal housing;
            decimal personal_Care;
            decimal utility;
            decimal entertainment;
            try
            {
                if (!decimal.TryParse(tb_Bg_Update_Consumer_Debt.Text, out consumer_Debt))
                {
                    lb_Bg_Update_Error.Text += "Please Enter a Valid Amount for Consumer Debt! <br>";
                }
                if (!decimal.TryParse(tb_Bg_Update_Food_Grocery.Text, out food_Grocery))
                {
                    lb_Bg_Update_Error.Text += "Please Enter a Valid Amount for Food & Groceries! <br>";
                }
                if (!decimal.TryParse(tb_Bg_Update_Healthcare.Text, out healthcare))
                {
                    lb_Bg_Update_Error.Text += "Please Enter a Valid Amount for Healthcare! <br>";
                }
                if (!decimal.TryParse(tb_Bg_Update_Housing.Text, out housing))
                {
                    lb_Bg_Update_Error.Text += "Please Enter a Valid Amount for Housing! <br>";
                }
                if (!decimal.TryParse(tb_Bg_Update_Personal_Care.Text, out personal_Care))
                {
                    lb_Bg_Update_Error.Text += "Please Enter a Valid Amount Personal Care! <br>";
                }
                if (!decimal.TryParse(tb_Bg_Update_Utility.Text, out utility))
                {
                    lb_Bg_Update_Error.Text += "Please Enter a Valid Amount for Utilities! <br>";
                }
                if (!decimal.TryParse(tb_Bg_Update_Entertainment.Text, out entertainment))
                {
                    lb_Bg_Update_Error.Text += "Please Enter a Valid Amount for Entertainment! <br>";
                }

                if (String.IsNullOrEmpty(lb_Bg_Update_Error.Text))
                {
                    //lb_Fp_Error_Title.Text = "Data Successfully Inserted!";
                    return true;
                }
                else
                {
                    tb_Bg_Error.Text = "Updating of Budget Failed! Please Re-Evaluate!";
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

    }
}
