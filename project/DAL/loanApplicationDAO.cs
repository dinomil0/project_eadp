﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Text;

namespace project.DAL
{
    public class loanApplicationDAO
    {
        public void applyLoan(customer customerobj, double EIR, int tenor, int loanAmt, double repaymentAmt, string bankName, double salary)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

            StringBuilder sqlStr = new StringBuilder();
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("INSERT INTO Applications (custNRIC, custSalary, EIR, tenor, creditScore, applicationDate, loanAmt, repaymentAmount, bankName)");
            sqlStr.AppendLine("VALUES (@customerNRIC, @customerSalary, @EIR, @tenor, @customerCS, @applicationDate, @loanAmt, @repaymentAmt, @bankName)");

            SqlConnection myConn = new SqlConnection(DBConnect);
            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@customerNRIC", customerobj.userID);
            sqlCmd.Parameters.AddWithValue("@customerSalary", salary);
            sqlCmd.Parameters.AddWithValue("@EIR", EIR);
            sqlCmd.Parameters.AddWithValue("@tenor", tenor);
            sqlCmd.Parameters.AddWithValue("@customerCS", customerobj.customerCS);
            sqlCmd.Parameters.AddWithValue("@applicationDate", DateTime.Now);
            sqlCmd.Parameters.AddWithValue("@loanAmt", loanAmt);
            sqlCmd.Parameters.AddWithValue("@repaymentAmt", repaymentAmt);
            sqlCmd.Parameters.AddWithValue("@bankName", bankName);

            myConn.Open();
            sqlCmd.ExecuteNonQuery();
            myConn.Close();
        }

        public void updateLoanApplicationStatus(int loanID, string status)
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

            StringBuilder sqlStr = new StringBuilder();
            SqlCommand sqlCmd = new SqlCommand();

            //sqlStr.AppendLine("DELETE FROM Applications WHERE applicationID= @applicationID");
            sqlStr.AppendLine("UPDATE Applications SET status = @status WHERE applicationID = @applicationID AND status IS NULL");
            SqlConnection myConn = new SqlConnection(DBConnect);
            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@status", status);
            sqlCmd.Parameters.AddWithValue("@applicationID", loanID);

            myConn.Open();
            sqlCmd.ExecuteNonQuery();
            myConn.Close();
        }
        
        public List<loanApplication> retrieveLoans()
        {
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

            StringBuilder sqlStr = new StringBuilder();
            StringBuilder sqlCommand = new StringBuilder();

            sqlCommand.AppendLine("Select * from Applications where status IS NULL");

            SqlConnection myConn = new SqlConnection(DBConnect);
            SqlDataAdapter da = new SqlDataAdapter(sqlCommand.ToString(), myConn);

            DataSet ds = new DataSet();

            da.Fill(ds, "applications");
            var applicationList = new List<loanApplication>();
            int rec_cnt = ds.Tables["applications"].Rows.Count;
            if (rec_cnt > 0)
            {
                for (int i = 0; i < rec_cnt; i++)
                {
                    loanApplication applicationOBJ = new loanApplication();
                    DataRow row = ds.Tables["applications"].Rows[i];
                    applicationOBJ.applicationID = Convert.ToInt32(row["applicationID"]);
                    applicationOBJ.custNRIC = row["custNRIC"].ToString();
                    applicationOBJ.custSalary = Convert.ToInt32(row["custSalary"]);
                    applicationOBJ.creditScore = row["creditScore"].ToString();
                    applicationOBJ.applicationDate = Convert.ToDateTime(row["applicationDate"]);
                    applicationOBJ.EIR = Convert.ToDouble(row["EIR"]);
                    applicationOBJ.tenor = Convert.ToInt32(row["tenor"]);
                    applicationOBJ.loanAmt = Convert.ToInt32(row["loanAmt"]);
                    applicationOBJ.repaymentAmt = Convert.ToDouble(row["repaymentAmount"]);
                    applicationOBJ.bankName = row["bankName"].ToString();
                    applicationOBJ.status = row["status"].ToString();

                    applicationList.Add(applicationOBJ);
                }
            }
            else return null;
            return applicationList;
        }
    }


}