﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace project.DAL
{
    public class GIRODAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        public GIRO getGIROdata(string userID)
        {
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();

            StringBuilder sqlStr = new StringBuilder();
            sqlStr.AppendLine("SELECT * From GIRO where userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            SqlDataAdapter da = new SqlDataAdapter(sqlStr.ToString(), myConn);

            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

            da.Fill(ds, "TransactionHistory");

            GIRO obj = new GIRO();
            int rec_cnt = ds.Tables["TransactionHistory"].Rows.Count;
            if (rec_cnt > 0)
            {
                DataRow row = ds.Tables["TransactionHistory"].Rows[0];
                obj.giroID = Convert.ToInt32(row["giroID"]);
                obj.date = Convert.ToDateTime(row["date"]);
                obj.billingOrganisation = row["billingOrganisation"].ToString();
                obj.amount = Convert.ToDouble(row["amount"]);
                obj.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
                obj.userID = row["userID"].ToString();
                obj.status = Convert.ToInt32(row["status"]);
            }
            else
            {
                obj = null;
            }

            return obj;
        }
        public List<GIRO> getGIRObyuserIDList(string userID)
        {
            List<GIRO> GIROList = new List<GIRO>();
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();
            StringBuilder sqlStr = new StringBuilder();

            sqlStr.AppendLine("SELECT * From GIRO where userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            SqlDataAdapter da = new SqlDataAdapter(sqlStr.ToString(), myConn);

            da.SelectCommand.Parameters.AddWithValue("@parauserID", userID);
            da.Fill(ds, "GIROTable");
            int rec_cnt = ds.Tables["GIROTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["GIROTable"].Rows)
                {
                    GIRO obj = new GIRO();
                    obj.giroID = Convert.ToInt32(row["giroID"]);
                    obj.billingOrganisation = row["billingOrganisation"].ToString();
                    obj.date = Convert.ToDateTime(row["date"]);
                    obj.amount = Convert.ToDouble(row["amount"]);
                    obj.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
                    obj.userID = row["userID"].ToString();
                    GIROList.Add(obj);
                }
            }
            else
            {
                GIROList = null;
            }

            return GIROList;

        }
        public void InsertGIRO(DateTime date, string billingOrganisation, double amount, int bankaccnum, string userID)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("INSERT INTO GIRO (date, billingOrganisation, amount, bankaccnum, userID)");
            sqlStr.AppendLine("VALUES (@paradate,@parabillingOrganisation,@paraamount,@parabankaccnum,@parauserID)");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@paradate", date);
            sqlCmd.Parameters.AddWithValue("@parabillingOrganisation", billingOrganisation);
            sqlCmd.Parameters.AddWithValue("@paraamount", amount);
            sqlCmd.Parameters.AddWithValue("@parabankaccnum", bankaccnum);
            sqlCmd.Parameters.AddWithValue("@parauserID", userID);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();

        }
        public void DeleteTDbyGIROID(int giroID)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;    
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("DELETE FROM GIRO");
            sqlStr.AppendLine("WHERE giroID = @paragiroID");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@paragiroID", giroID);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();

        }
        public void UpdateTDfordate(int giroID, DateTime date)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("UPDATE GIRO SET date = @date");
            sqlStr.AppendLine("WHERE giroID = @giroID");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@date", date);
            sqlCmd.Parameters.AddWithValue("@giroID", giroID);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();
        }
    }
}