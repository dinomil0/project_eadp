﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class loanApplication
    {
        public loanApplication() { }

        public int applicationID { get; set; }
        public string custNRIC { get; set; }
        public int custSalary { get; set; }
        public int loanAmt { get; set; }
        public double EIR { get; set; }
        public double repaymentAmt { get; set; }
        public int tenor { get; set; }
        public string creditScore { get; set; }
        public string bankName { get; set; }
        public string status { get; set; }
        public DateTime applicationDate { get; set; }
    }
}