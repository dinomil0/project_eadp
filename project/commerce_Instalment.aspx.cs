﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class commerce_Instalment : System.Web.UI.Page
    {
        string userID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            userID = Session["custNRIC"].ToString();
            double total = Convert.ToDouble(Request.QueryString["price"]) * Convert.ToInt16(Request.QueryString["qty"]);
            Lbl_Payment.Text = "Your total is $" + Convert.ToString(total) + "/" + Convert.ToString(total * 4) + " VC";
            Class_AccountDAO accDAO = new Class_AccountDAO();
            List<Class_Account.Account> accList = accDAO.getAccount(userID);
            foreach (Class_Account.Account acc in accList)
            {
                if (ddl_bankNumber.Items.FindByValue(Convert.ToString(acc.bankaccnum)) == null)
                {
                    ddl_bankNumber.Items.Add(new ListItem(Convert.ToString(acc.bankaccname) + " " + Convert.ToString(acc.bankaccnum), Convert.ToString(acc.bankaccnum)));
                }
            }
        }
        protected void btn_instalpaycard_Click(object sender, EventArgs e)
        {
            if (ddl_CardPeriod.SelectedIndex == 0)
            {
                Lbl_err.Text = "Please select a period!";
            }
            else
            {
                string ProdId = Request.QueryString["ProdId"];
                int mths = Convert.ToInt16(ddl_CardPeriod.SelectedItem.Text);
                int qty = Convert.ToInt16(Request.QueryString["qty"]);
                double rate = Convert.ToDouble(ddl_CardPeriod.SelectedValue);
                Class_TransactionDAO transDAO = new Class_TransactionDAO();
                transDAO.Add_Instalment(ProdId, rate, userID, mths, qty, "Card");
                Response.Redirect("commerce_Transactions.aspx");
            }
        }

        protected void btn_instalpayvc_Click(object sender, EventArgs e)
        {
            if (ddl_VCPeriod.SelectedIndex == 0)
            {
                Lbl_err.Text = "Please select a period!";
            }
            else
            {
                string ProdId = Request.QueryString["ProdId"];
                int mths = Convert.ToInt16(ddl_VCPeriod.SelectedItem.Text);
                int qty = Convert.ToInt16(Request.QueryString["qty"]);
                double rate = Convert.ToDouble(ddl_VCPeriod.SelectedValue);
                Class_TransactionDAO transDAO = new Class_TransactionDAO();
                transDAO.Add_Instalment(ProdId, rate, userID, mths, qty, "VC");
                Response.Redirect("commerce_Transactions.aspx");
            }
        }

        protected void ddl_CardPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_CardPeriod.SelectedIndex == 0)
            {
                Lbl_CardInterest.Text = "Interest Rate";
                Lbl_CardMthlyRepay.Text = "Monthly Repayment";
                Lbl_CardTotalRepay.Text = "Total Repayment";
            }
            else
            {
                double total = Convert.ToDouble(Request.QueryString["price"]) * Convert.ToInt16(Request.QueryString["qty"]);
                Lbl_CardInterest.Text = Convert.ToString(Convert.ToDouble(ddl_CardPeriod.SelectedValue) * 100) + "%";
                double newtotal = total + total * Convert.ToDouble(ddl_CardPeriod.SelectedValue);
                Lbl_CardMthlyRepay.Text = "$" + string.Format("{0:#.00}", Convert.ToDecimal(Convert.ToString(Math.Round(newtotal / Convert.ToInt16(ddl_CardPeriod.SelectedItem.Text), 2))));
                Lbl_CardTotalRepay.Text = "$" + Convert.ToString(newtotal / Convert.ToInt16(ddl_CardPeriod.SelectedItem.Text) * Convert.ToInt16(ddl_CardPeriod.SelectedItem.Text));
                Lbl_Payment.Text = "Your total is " + Lbl_CardTotalRepay.Text;
            }
        }

        protected void ddl_VCPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_VCPeriod.SelectedIndex == 0)
            {
                Lbl_CardInterest.Text = "Interest Rate";
                Lbl_CardMthlyRepay.Text = "Monthly Repayment";
                Lbl_CardTotalRepay.Text = "Total Repayment";
            }
            else
            {
                double total = Convert.ToDouble(Request.QueryString["price"]) * Convert.ToInt16(Request.QueryString["qty"]) * 4;
                Lbl_VCInterest.Text = Convert.ToString(Convert.ToDouble(ddl_VCPeriod.SelectedValue) * 100) + "%";
                double newtotal = total + total * Convert.ToDouble(ddl_VCPeriod.SelectedValue);
                Lbl_VCMthlyRepay.Text = Convert.ToString(Math.Round(newtotal / Convert.ToInt16(ddl_VCPeriod.SelectedItem.Text), 0)) + " VC";
                Lbl_VCTotalRepay.Text = Convert.ToString(total * (1 + Convert.ToDouble(ddl_VCPeriod.SelectedValue))) + " VC";
                Lbl_Payment.Text = "Your total is " + Lbl_VCTotalRepay.Text;
            }
        }

        protected void commerce_PayCard_Click(object sender, CommandEventArgs e)
        {
            PanelCardInstalment.Visible = true;
            PanelVCInstalment.Visible = false;
            double total = Convert.ToDouble(Request.QueryString["price"]) * Convert.ToInt16(Request.QueryString["qty"]);
            Lbl_Payment.Text = "Your total is $" + Convert.ToString(total);
        }

        protected void commerce_PayVC_Click(object sender, CommandEventArgs e)
        {
            PanelCardInstalment.Visible = false;
            PanelVCInstalment.Visible = true;
            double total = Convert.ToDouble(Request.QueryString["price"]) * Convert.ToInt16(Request.QueryString["qty"]);
            Lbl_Payment.Text = "Your total is " + Convert.ToString(total * 4) + " VC";
        }
    }
}