﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project
{
    public class transactionDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

        public Dictionary<string, Dictionary<string, decimal>> get_Total_Per_Category(int bankaccnum)
        {
            Dictionary<string, List<int>> merchant_Code = new Dictionary<string, List<int>>
            {
                { "Consumer Debt", new List<int> { 4112, 4121, 4131, 8220 } },
                { "Food and Groceries", new List<int> {5411, 5441, 5462, 5812, 5814 } },
                { "Healthcare", new List<int> { 8021, 8044, 8062 } },
                { "Housing", new List<int> { 1711, 1731 } },
                { "Personal Care", new List<int> { 5137, 5661, 5691 } },
                { "Utilities", new List<int> { 4814, 4899, 4900 } },
                { "Entertainment", new List<int> { 3082, 3098, 7933, 7998 } },
            };

            Dictionary<string, Dictionary<string, decimal>> expense_Per_Month = new Dictionary<string, Dictionary<string, decimal>>
            {
                {DateTime.Today.AddMonths(-3).ToString("MMM"), new Dictionary<string, decimal>()},
                {DateTime.Today.AddMonths(-2).ToString("MMM"), new Dictionary<string, decimal>()},
                {DateTime.Today.AddMonths(-1).ToString("MMM"), new Dictionary<string, decimal>()},
                {DateTime.Today.ToString("MMM"), new Dictionary<string, decimal>()},
            };


            //Getting Total Expenses 

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            SqlDataAdapter da;

            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("SELECT * FROM TRANSACTIONHISTORY");
            sqlString.AppendLine("WHERE BANKACCNUM = @PARABANKACCNUM");
            sqlString.AppendLine("AND DATE >= DATEADD(MONTH, -4, GETDATE())");

            SqlConnection sqlConnection = new SqlConnection(DBConnect);

            da = new SqlDataAdapter(sqlString.ToString(), sqlConnection);
            da.SelectCommand.Parameters.AddWithValue("PARABANKACCNUM", bankaccnum);

            da.Fill(ds, "total_Expense_Table");

            int rec_cnt = ds.Tables["total_Expense_Table"].Rows.Count;
            if (rec_cnt == 0)
            {

            }
            else
            {
                foreach (DataRow row in ds.Tables["total_Expense_table"].Rows)
                {
                    string month = (Convert.ToDateTime(row["date"])).ToString("MMM");


                }
            }
            return expense_Per_Month;
        }
    }
}