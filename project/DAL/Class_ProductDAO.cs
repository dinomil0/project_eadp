﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class Class_ProductDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        public List<Class_Order> get_cart(string userID)
        {
            List<Class_Order> orderList = new List<Class_Order>();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Cart");
            sqlCommand.AppendLine("WHERE userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

            da.Fill(ds, "CartTable");

            foreach (DataRow row in ds.Tables["CartTable"].Rows)
            {
                Class_Order order = new Class_Order();
                order.ordId = Convert.ToString(row["ordId"]);
                order.ProdId = Convert.ToString(row["ProdId"]);
                order.ProdName = Convert.ToString(row["ProdName"]);
                order.price = Convert.ToString(row["price"]);
                order.BuyQty = Convert.ToInt16(row["BuyQty"]);
                order.total = Convert.ToString(Convert.ToDouble(order.price) * order.BuyQty);
                orderList.Add(order);
            }
            return orderList;
        }
        public List<Class_Product> get_products()
        {
            List<Class_Product> prodList = new List<Class_Product>();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Product");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);

            da.Fill(ds, "ProductTable");

            foreach (DataRow row in ds.Tables["ProductTable"].Rows)
            {
                Class_Product product = new Class_Product();
                product.ProdId = Convert.ToString(row["ProdId"]);
                product.ProdName = Convert.ToString(row["ProdName"]);
                product.ProdDesc = Convert.ToString(row["ProdDesc"]);
                product.ProdPrice = Convert.ToDouble(row["ProdPrice"]);
                product.ProdQty = Convert.ToInt16(row["ProdQty"]);
                product.SellerID = Convert.ToString(row["SellerID"]);
                product.ProdImage = (byte[])row["ProdImage"];
                prodList.Add(product);
            }

            return prodList;
        }
        public List<Class_Product> get_listing(string SellerID)
        {
            List<Class_Product> prodList = new List<Class_Product>();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Product");
            sqlCommand.AppendLine("WHERE SellerID = @paraSellerID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("paraSellerID", SellerID);

            da.Fill(ds, "ProductTable");

            foreach (DataRow row in ds.Tables["ProductTable"].Rows)
            {
                Class_Product product = new Class_Product();
                product.ProdId = Convert.ToString(row["ProdId"]);
                product.ProdName = Convert.ToString(row["ProdName"]);
                product.ProdDesc = Convert.ToString(row["ProdDesc"]);
                product.ProdPrice = Convert.ToDouble(row["ProdPrice"]);
                product.ProdQty = Convert.ToInt16(row["ProdQty"]);
                product.SellerID = Convert.ToString(row["SellerID"]);
                product.ProdImage = (byte[])row["ProdImage"];
                prodList.Add(product);
            }

            return prodList;
        }
        public bool testLess_Qty(String ProdId, int Qty)
        {
            Class_Product Product = new Class_Product();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM PRODUCT");
            sqlCommand.AppendLine("WHERE ProdId = @paraProdId");
            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("paraProdId", ProdId);
            da.Fill(ds, "Product");

            foreach (DataRow row in ds.Tables["Product"].Rows)
            {
                Product.ProdQty = Convert.ToInt16(row["ProdQty"]);
            }
            if (Product.ProdQty - Qty < 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void Less_Qty(string ProdId, int Qty)
        {
            Class_Product Product = new Class_Product();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM PRODUCT");
            sqlCommand.AppendLine("WHERE ProdId = @paraProdId");
            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("paraProdId", ProdId);
            da.Fill(ds, "Product");

            foreach (DataRow row in ds.Tables["Product"].Rows)
            {
                Product.ProdQty = Convert.ToInt16(row["ProdQty"]);
            }

            StringBuilder addCommand = new StringBuilder();
            SqlCommand excommand = new SqlCommand();
            addCommand.AppendLine("UPDATE PRODUCT");
            addCommand.AppendLine("SET ProdQty = ProdQty - @paraQty");
            addCommand.AppendLine("WHERE ProdId = @paraId");
            SqlConnection myConn2 = new SqlConnection(DBConnect);
            excommand = new SqlCommand(addCommand.ToString(), myConn2);
            excommand.Parameters.AddWithValue("paraId", ProdId);
            excommand.Parameters.AddWithValue("paraQty", Qty);
            myConn2.Open();
            excommand.ExecuteNonQuery();
            myConn2.Close();
        }
        public void Add_Product(Class_Product product)
        {
            StringBuilder sqlCommand = new StringBuilder();
            SqlCommand excommand = new SqlCommand();
            sqlCommand.AppendLine("INSERT INTO Product");
            sqlCommand.AppendLine("VALUES (@paraId, @paraName, @paraDesc, @paraPrice, @paraQty, @paraSellerID, @paraProdImage)");
            SqlConnection myConn = new SqlConnection(DBConnect);
            excommand = new SqlCommand(sqlCommand.ToString(), myConn);
            excommand.Parameters.AddWithValue("paraId", product.ProdId);
            excommand.Parameters.AddWithValue("paraName", product.ProdName);
            excommand.Parameters.AddWithValue("paraDesc", product.ProdDesc);
            excommand.Parameters.AddWithValue("paraPrice", product.ProdPrice);
            excommand.Parameters.AddWithValue("paraQty", product.ProdQty);
            excommand.Parameters.AddWithValue("paraSellerID", product.SellerID);
            excommand.Parameters.AddWithValue("paraProdImage", product.ProdImage);
            myConn.Open();
            excommand.ExecuteNonQuery();
            myConn.Close();
        }
        public void Less_Product(string ProdId)
        {
            StringBuilder sqlCommand = new StringBuilder();
            SqlCommand excommand = new SqlCommand();
            sqlCommand.AppendLine("DELETE FROM Product");
            sqlCommand.AppendLine("WHERE ProdId = @paraProdId");
            SqlConnection myConn = new SqlConnection(DBConnect);
            excommand = new SqlCommand(sqlCommand.ToString(), myConn);
            excommand.Parameters.AddWithValue("paraProdId", ProdId);
            myConn.Open();
            excommand.ExecuteNonQuery();
            myConn.Close();
        }
        public string getSellerID(string ProdId)
        {
            string SellerID;
            SellerID = "None";
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Product");
            sqlCommand.AppendLine("WHERE ProdId = @paraProdId");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("paraProdId", ProdId);
            da.Fill(ds, "ProductTable");

            foreach (DataRow row in ds.Tables["ProductTable"].Rows)
            {
                SellerID = Convert.ToString(row["SellerID"]);
            }

            return SellerID;
        }
        public Class_Product getProduct(string ProdId)
        {
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM PRODUCT");
            sqlCommand.AppendLine("WHERE ProdId = @paraProdId");
            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("paraProdId", ProdId);
            da.Fill(ds, "ProductTable");

            Class_Product product = new Class_Product();
            foreach (DataRow row in ds.Tables["ProductTable"].Rows)
            {
                product.ProdId = Convert.ToString(row["ProdId"]);
                product.ProdName = Convert.ToString(row["ProdName"]);
                product.ProdDesc = Convert.ToString(row["ProdDesc"]);
                product.ProdPrice = Convert.ToDouble(row["ProdPrice"]);
                product.ProdQty = Convert.ToInt16(row["ProdQty"]);
                product.SellerID = Convert.ToString(row["SellerID"]);
                product.ProdImage = (byte[])row["ProdImage"];
                return product;
            }

            return product;
        }
    }
}