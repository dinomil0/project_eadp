﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
namespace project.DAL
{
    public class budgetDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

        public void validate_Budget(int bankaccnum)
        {
            try
            {
                SqlDataAdapter da;
                DataSet ds = new DataSet();
                SqlCommand sqlCommand = new SqlCommand();
                StringBuilder sqlString = new StringBuilder();
                sqlString.AppendLine("SELECT * FROM BUDGET");
                sqlString.AppendLine("WHERE BANKACCNUM = @PARABANKACCNUM AND MONTH(BUDGET_DATE) = MONTH(GETDATE())");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlString.ToString(), sqlConnection);
                da.SelectCommand.Parameters.AddWithValue("@PARABANKACCNUM", bankaccnum);
                da.Fill(ds, "budgetTable");
                int rec_cnt = ds.Tables["budgetTable"].Rows.Count;
                if (rec_cnt == 0)
                {
                    SqlDataAdapter read_da;
                    DataSet read_ds = new DataSet();
                    SqlCommand read_sqlCommand = new SqlCommand();
                    StringBuilder read_sqlString = new StringBuilder();
                    read_sqlString.AppendLine("SELECT * FROM BUDGET");
                    read_sqlString.AppendLine("WHERE BANKACCNUM = @PARABANKACCNUM AND MONTH(BUDGET_DATE) >= (MONTH(GETDATE()) - 3)");
                    SqlConnection read_sqlConnection = new SqlConnection(DBConnect);
                    read_da = new SqlDataAdapter(read_sqlString.ToString(), read_sqlConnection);
                    read_da.SelectCommand.Parameters.AddWithValue("@PARABANKACCNUM", bankaccnum);
                    read_da.Fill(ds, "read_BudgetTable");
                    int read_rec_cnt = ds.Tables["read_BudgetTable"].Rows.Count;
                    StringBuilder insert_sqlString = new StringBuilder();
                    SqlCommand insert_sqlCommand = new SqlCommand();
                    if (read_rec_cnt == 0)
                    {
                        insert_sqlString.AppendLine("INSERT INTO BUDGET (BANKACCNUM) ");
                        insert_sqlString.AppendLine("VALUES (@PARABANKACCNUM)");
                        SqlConnection insert_sqlConnection = new SqlConnection(DBConnect);
                        insert_sqlCommand = new SqlCommand(insert_sqlString.ToString(), insert_sqlConnection);
                        insert_sqlCommand.Parameters.AddWithValue("@PARABANKACCNUM", bankaccnum);
                        insert_sqlConnection.Open();
                        insert_sqlCommand.ExecuteNonQuery();
                        insert_sqlConnection.Close();
                    }
                    else
                    {
                        int total_Amount_Of_Budget_Months = read_rec_cnt;
                        decimal total_Consumer_Debt = 0;
                        decimal total_Food_Grocery = 0;
                        decimal total_Healthcare = 0;
                        decimal total_Housing = 0;
                        decimal total_Personal_Care = 0;
                        decimal total_Utility = 0;
                        decimal total_Entertainment = 0;
                        decimal total_Emergency_Fund = 0;
                        decimal total_Saving = 0;
                        foreach (DataRow row in ds.Tables["read_BudgetTable"].Rows)
                        {
                            total_Consumer_Debt += Convert.ToInt32(row["consumer_Debt"]);
                            total_Food_Grocery += Convert.ToInt32(row["food_Grocery"]);
                            total_Healthcare += Convert.ToInt32(row["healthcare"]);
                            total_Housing += Convert.ToInt32(row["housing"]);
                            total_Personal_Care += Convert.ToInt32(row["personal_Care"]);
                            total_Utility += Convert.ToInt32(row["utility"]);
                            total_Entertainment += Convert.ToInt32(row["entertainment"]);
                            total_Emergency_Fund += Convert.ToInt32(row["emergency_Fund"]);
                            total_Saving += Convert.ToInt32(row["saving"]);
                        }
                        decimal average_Consumer_Debt = total_Consumer_Debt / read_rec_cnt;
                        decimal average_Food_Grocery = total_Food_Grocery / read_rec_cnt;
                        decimal average_Healthcare = total_Healthcare / read_rec_cnt;
                        decimal average_Housing = total_Housing / read_rec_cnt;
                        decimal average_Personal_Care = total_Personal_Care / read_rec_cnt;
                        decimal average_Utility = total_Utility / read_rec_cnt;
                        decimal average_Entertainment = total_Entertainment / read_rec_cnt;
                        decimal average_Emergency_Fund = total_Emergency_Fund / read_rec_cnt;
                        decimal average_Saving = total_Saving / read_rec_cnt;
                        insert_sqlString.AppendLine("INSERT INTO BUDGET (BANKACCNUM, CONSUMER_DEBT, FOOD_GROCERY, HEALTHCARE, HOUSING, PERSONAL_CARE, UTILITY, ENTERTAINMENT, EMERGENCY_FUND, SAVING) ");
                        insert_sqlString.AppendLine("VALUES (@PARABANKACCNUM, @PARACONSUMER_DEBT, @PARAFOOD_GROCERY, @PARAHEALTHCARE, @PARAHOUSING, @PARAPERSONAL_CARE, @PARAUTILITY, @PARAENTERTAINMENT, @PARAEMERGENCY_FUND, @PARASAVING)");
                        SqlConnection insert_sqlConnection = new SqlConnection(DBConnect);
                        insert_sqlCommand = new SqlCommand(insert_sqlString.ToString(), insert_sqlConnection);
                        insert_sqlCommand.Parameters.AddWithValue("@PARABANKACCNUM", bankaccnum);
                        insert_sqlCommand.Parameters.AddWithValue("@PARACONSUMER_DEBT", average_Consumer_Debt);
                        insert_sqlCommand.Parameters.AddWithValue("@PARAFOOD_GROCERY", average_Food_Grocery);
                        insert_sqlCommand.Parameters.AddWithValue("@PARAHEALTHCARE", average_Healthcare);
                        insert_sqlCommand.Parameters.AddWithValue("@PARAHOUSING", average_Housing);
                        insert_sqlCommand.Parameters.AddWithValue("@PARAPERSONAL_CARE", average_Personal_Care);
                        insert_sqlCommand.Parameters.AddWithValue("@PARAUTILITY", average_Utility);
                        insert_sqlCommand.Parameters.AddWithValue("@PARAENTERTAINMENT", average_Entertainment);
                        insert_sqlCommand.Parameters.AddWithValue("@PARAEMERGENCY_FUND", average_Emergency_Fund);
                        insert_sqlCommand.Parameters.AddWithValue("@PARASAVING", average_Saving);
                        insert_sqlConnection.Open();
                        insert_sqlCommand.ExecuteNonQuery();
                        insert_sqlConnection.Close();
                    }
                }
            }
            catch
            {
                //Exception
            }
        }

        public budget read_Budget_List(int bankaccnum)
        {
            budget read_Budget = new budget();
            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                SqlDataAdapter da;
                StringBuilder sqlString = new StringBuilder();
                sqlString.AppendLine("SELECT * FROM BUDGET");
                sqlString.AppendLine("WHERE BANKACCNUM = @PARABANKACCNUM");
                sqlString.AppendLine("AND MONTH(BUDGET_DATE) = MONTH(GETDATE())");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlString.ToString(), sqlConnection);
                da.SelectCommand.Parameters.AddWithValue("@PARABANKACCNUM", bankaccnum);
                da.Fill(ds, "budgetTable");
                int rec_cnt = ds.Tables["budgetTable"].Rows.Count;
                if (rec_cnt == 0)
                {
                    return null;
                }
                else
                {
                    foreach (DataRow row in ds.Tables["budgetTable"].Rows)
                    {

                        read_Budget.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
                        read_Budget.budget_Date = Convert.ToDateTime(row["budget_Date"]);
                        read_Budget.update_Detail = Convert.ToDateTime(row["update_Detail"]);
                        read_Budget.update_Count = Convert.ToInt32(row["update_Count"]);
                        read_Budget.consumer_Debt = Convert.ToDecimal(row["consumer_Debt"]);
                        read_Budget.food_Grocery = Convert.ToDecimal(row["food_Grocery"]);
                        read_Budget.healthcare = Convert.ToDecimal(row["healthcare"]);
                        read_Budget.housing = Convert.ToDecimal(row["housing"]);
                        read_Budget.personal_Care = Convert.ToDecimal(row["personal_Care"]);
                        read_Budget.utility = Convert.ToDecimal(row["utility"]);
                        read_Budget.entertainment = Convert.ToDecimal(row["entertainment"]);
                        read_Budget.emergency_Fund = Convert.ToDecimal(row["emergency_Fund"]);
                        read_Budget.saving = Convert.ToDecimal(row["saving"]);
                    }
                }
            }
            catch
            {
                //Exception
            }
            return read_Budget;
        }

        public void update_Budget(int bankaccnum, DateTime budget_Date, decimal consumer_Debt, decimal food_Grocery, decimal healthcare, decimal housing, decimal personal_Care, decimal utility, decimal entertainment, decimal emergency_Fund, decimal saving)
        {
            try
            {
                StringBuilder sqlString = new StringBuilder();
                SqlCommand sqlCommand = new SqlCommand();
                sqlString.AppendLine("UPDATE BUDGET");
                sqlString.AppendLine("SET BANKACCNUM = @PARABANKACCNUM, BUDGET_DATE = @PARABUDGET_DATE, UPDATE_COUNT = (UPDATE_COUNT + 1), UPDATE_DETAIL = GETDATE(), CONSUMER_DEBT = @PARACONSUMER_DEBT, FOOD_GROCERY = @PARAFOOD_GROCERY, HEALTHCARE = @PARAHEALTHCARE, HOUSING = @PARAHOUSING, PERSONAL_CARE = @PARAPERSONAL_CARE, UTILITY = @PARAUTILITY, ENTERTAINMENT = @PARAENTERTAINMENT, EMERGENCY_FUND = @PARAEMERGENCY_FUND, SAVING = @PARASAVING");
                sqlString.AppendLine("WHERE BANKACCNUM = @PARABANKACCNUM");
                sqlString.AppendLine("AND BUDGET_DATE = @PARABUDGET_DATE");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                sqlCommand = new SqlCommand(sqlString.ToString(), sqlConnection);
                sqlCommand.Parameters.AddWithValue("@PARABANKACCNUM", bankaccnum);
                sqlCommand.Parameters.AddWithValue("@PARABUDGET_DATE", budget_Date);
                sqlCommand.Parameters.AddWithValue("@PARACONSUMER_DEBT", consumer_Debt);
                sqlCommand.Parameters.AddWithValue("@PARAFOOD_GROCERY", food_Grocery);
                sqlCommand.Parameters.AddWithValue("@PARAHEALTHCARE", healthcare);
                sqlCommand.Parameters.AddWithValue("@PARAHOUSING", housing);
                sqlCommand.Parameters.AddWithValue("@PARAPERSONAL_CARE", personal_Care);
                sqlCommand.Parameters.AddWithValue("@PARAUTILITY", utility);
                sqlCommand.Parameters.AddWithValue("@PARAENTERTAINMENT", entertainment);
                sqlCommand.Parameters.AddWithValue("@PARAEMERGENCY_FUND", emergency_Fund);
                sqlCommand.Parameters.AddWithValue("@PARASAVING", saving);
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch
            {
                //Exception
            }
        }
    }
}