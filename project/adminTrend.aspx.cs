﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class adminTrend : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }
            trendDAO dao = new trendDAO();
            //while (true)
            //{
            //    dao.useless();
            //}
            //var i = 0;
            //do
            //{
            //    dao.useless();
            //    i++;
            //} while (i < 10);
            if (!IsPostBack)
            {
                populateDDL();
                Session["allGroup"] = trendGraph(DateTime.Now);
            }
        }
        public string trendGraph(DateTime date)
        {
            trendDAO trendDAO = new trendDAO();
            List<trend> groupList = new List<trend>();
            groupList = trendDAO.getTrends(Convert.ToDateTime(date));
            List<int> allcount = new List<int>();
            for (var i = 0; i < groupList.Count(); i++)
            {
                allcount.Add(groupList[i].count);
            }
            return new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(allcount);
        }


        public void populateDDL()
        {
            var datenow = DateTime.Now;
            var month = datenow.Month;
            var year = datenow.Year;
            for (var i = 6; i <= month; i++)
            {
                var monthyear = Convert.ToDateTime(i + "/" + year.ToString());
                var months = monthyear.ToString("MMM");
                var years = monthyear.Year.ToString();
                ddlMonth.Items.Add(months + "/" + years);
            }
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            var hi = Convert.ToDateTime(ddlMonth.SelectedValue.ToString());
            Session["allGroup"] = trendGraph(hi);
        }
    }
}