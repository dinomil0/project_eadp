﻿<%@ Page Title="" Language="C#" MasterPageFile="~/commerceMaster.Master" AutoEventWireup="true" CodeBehind="commerce_Orders.aspx.cs" Inherits="project.commerce_Orders" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="form1">
    <section>
        <div class="col-3">
            <asp:Label ID="Lbl_userID" runat="server" Text="User Name" class="h3"></asp:Label>
            <h3>Orders</h3>
        </div>
        <hr />
        <div class="d-flex justify-content-center">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" class="table table-bordered" OnRowDataBound="GridView1_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="date" HeaderText="Date" DataFormatString="{0:M/d/yyyy}" />
                    <asp:BoundField DataField="ordId" HeaderText="Order ID" />
                    <asp:BoundField DataField="ProdId" HeaderText="Product Tag" />
                    <asp:BoundField DataField="reference" HeaderText="Reference" />
                    <asp:BoundField DataField="price" HeaderText="Price" />
                    <asp:BoundField DataField="BuyQty" HeaderText="Quantity" />
                    <asp:BoundField DataField="total" HeaderText="Total" />
                    <asp:BoundField DataField="debit" HeaderText="Debit (Withdrawal)" />
                    <asp:BoundField DataField="status" HeaderText="Status" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton runat="server" OnCommand="Clear_Click" CommandArgument='<%# Container.DataItemIndex %>' class="btn btn-outline-success">Clear</asp:LinkButton >
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton runat="server" OnCommand="Inquiry_Click" CommandArgument='<%# Container.DataItemIndex %>' class="btn btn-outline-danger" >Inquiry</asp:LinkButton >
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Label ID="Lbl_err" runat="server" Text="Lbl_err" CssClass="text-danger" Visible="false"></asp:Label>
        </div>
    </section>
    <div class="modal fade" id="Inquiry" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <table style="width: 100%;">
                        <tr>
                            <td>Order: </td>
                            <td><asp:Label ID="Lbl_ordId" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Product Tag: </td>
                            <td><asp:Label ID="Lbl_ProdId" runat="server" Text=""></asp:Label></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-body">
                    <table style="width: 100%;">
                        <tr>
                            <td>Action: </td>
                            <td><asp:DropDownList ID="ddl_actions" runat="server">
                                <asp:ListItem>--Select--</asp:ListItem>
                                <asp:ListItem Value="Refund">Refund</asp:ListItem>
                                <asp:ListItem Value="Replace">Replacement</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>Complaint: </td>
                            <td><textarea id="TA_complaint" name="TA_complaint" cols="20" rows="2" runat="server"></textarea></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                    <asp:Button ID="btn_Submit" runat="server" Text="Submit" class="btn btn-outline-success" OnClick="Inquiry_Submit"/>
                    <asp:Label ID="Lbl_modalErr" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <script type='text/javascript'>
            function openModal(){$('[id*=Inquiry]').modal('show');} 
    </script>
</div>
</asp:Content>
