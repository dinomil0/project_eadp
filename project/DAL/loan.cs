﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class loan
    {
        public loan()
        {

        }
        public int loanID { get; set; }
        public string loanName { get; set; }
        public double loanInt { get; set; }
        public double loanFees { get; set; }
        public double loanEIR { get; set; }
        public int loanTenor { get; set; }
        public double loanRepayment { get; set; }
        public string loanRemark { get; set; }
        public int loanMin { get; set; }
        public double loanMinSalary { get; set; }
    }
}