﻿<%@ Page Title="" Language="C#" MasterPageFile="~/commerceMaster.Master" AutoEventWireup="true" CodeBehind="commerce_BuyPage.aspx.cs" Inherits="project.commerce_BuyPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
    .ProdImg {
        width:175px;
        height:175px;
        overflow:hidden;
    }
</style>
<div id="form1">
    <section class="justify-content-center mt-5 border border-success rounded container w-50">
        <div class="mt-2">
            <asp:Label ID="Lbl_productname" runat="server" Text="Product Name " class="mt-2"></asp:Label>
            <asp:Label ID="LBl_productID" runat="server" Text="Product ID" class="mt-2"></asp:Label>
        </div>
        <div class="d-flex justify-content-center">
            <div class="mr-3 mt-5 mb-5">
                <asp:Image ID="ProdImage" runat="server" ImageUrl='<%# Eval("ProdImage") %>' cssclass="img-thumbnail ProdImg"/>  
            </div>
            <div class="mt-5">
                <table>
                    <tr>
                        <td>Product Description: </td>
                        <td><asp:Label ID="Lbl_ProdDesc" runat="server" Text="Product Description"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>Price: </td>
                        <td><asp:Label ID="Lbl_ProdPrice" runat="server" Text="Price"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>Quantity: </td>
                        <td><asp:TextBox ID="TB_Qty" runat="server" CssClass="w-25"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btn_instal" runat="server" Text="Pay By Instalment" class="btn btn-outline-success" OnClick="btn_instal_Click"/>
                        </td>
                        <td>
                            <asp:Button ID="btn_cart" runat="server" Text="Add to Cart" class="btn btn-outline-success" OnClick="btn_cart_Click"/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <asp:Label ID="Lbl_Err" runat="server" Text="" class="text-danger d-block" Visible="false"></asp:Label>
    </section>
    <br />
</div>
</asp:Content>
