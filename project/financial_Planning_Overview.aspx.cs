﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class financial_Planning_Overview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }
            //Reading Financial Plan
            List<financial_Plan> read_Financial_Plan_List = new List<financial_Plan>();
            financial_PlanDAO read_Financial_PlanDAO = new financial_PlanDAO();
            read_Financial_Plan_List = read_Financial_PlanDAO.read_Financial_Plan(Session["custNRIC"].ToString());
            gv_Fp_Overview.DataSource = read_Financial_Plan_List;
            gv_Fp_Overview.DataBind();
        }

        protected void btn_Fp_Read_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                int rowIndex = Convert.ToInt32(((sender as LinkButton).NamingContainer as GridViewRow).RowIndex);
                GridViewRow row = gv_Fp_Overview.Rows[rowIndex];

                tb_Fp_Delete_Id.Text = (row.FindControl("lb_Fp_Read_Id") as Label).Text;
                tb_Fp_Delete_Name.Text = (row.FindControl("lb_Fp_Read_Name") as Label).Text;
                tb_Fp_Delete_Age.Text = (row.FindControl("lb_Fp_Read_Age") as Label).Text; ;
                tb_Fp_Delete_Amount.Text = (row.FindControl("lb_Fp_Read_Amount") as Label).Text;
                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "initiate_Delete_Modal();", true);
            }
            catch
            {
                //Exception
            }
        }

        protected void btn_Fp_Delete_Data_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                //Deleting Financial Data
                financial_PlanDAO delete_Financial_PlanDAO = new financial_PlanDAO();
                delete_Financial_PlanDAO.delete_Financial_Plan(Convert.ToInt32(tb_Fp_Delete_Id.Text));
                Response.Redirect(Request.RawUrl);
            }
            catch
            {
                //Exception
            }
        }
    }
}