﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class financial_Plan_Analysis
    {
        public int count { set; get; }
        public string name { set; get; }
        public int matured_Age { set; get; }
        public string duration { set; get; }
        public decimal monthly_Contribution { set; get; }
    }
}