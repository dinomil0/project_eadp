﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class Accounts
    {
        public Accounts()
        {

        }
        public int bankaccnum { get; set; }
        public DateTime DOE { get; set; }
        public int CVV { get; set; }
        public long cardNum { get; set; }
        public string userID { get; set; }
        public string bankname { get; set; }
        public string merchant { get; set; }
        public double balance { get; set; }
        public string bankaccname { get; set; }
        public double amountowed { get; set; }
    }
}
