﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="bankHome.aspx.cs" Inherits="project.bankHome" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
        .pictureinBankHome {
            width: 100%;
            height: 279px;
        }
        .chart-container {
            position: relative;
            margin: auto;
            height: 80vh;
            width: 80vw;
        }
        a {
            color: #0060B6;
            text-decoration: none;
        }

        a:hover 
        {
             color:#00A0C6; 
             text-decoration:none; 
             cursor:pointer;  
        }
         .auto-style1 {
            width: 292px;
        }
        .auto-style2 {
            width: 361px;
        }
    
        
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- Picture --%>
    <img src="img/skybannerhome.jpg" alt="Learn banking with us" class="pictureinBankHome"/>
<%-- Nav bar pill --%>
<br />
<br />
<section class="">
    <nav>
        <div class="nav nav-tabs d-flex text-center" id="nav-tab" role="tablist">
            <a class="col-6 nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-dashboard" aria-selected="false"><i class="fas fa-shopping-bag text-primary"></i> &nbsp; Spend</a>
            <a class="col-6 nav-item nav-link" id="nav-spend-tab" data-toggle="tab" href="#nav-spend" role="tab" aria-controls="nav-profile" aria-selected="false"><i class="fas fa-piggy-bank text-primary"></i> &nbsp; Save</a>
            <%--<a class="col-4 nav-item nav-link" id="nav-graphs-tab" data-toggle="tab" href="#nav-learnmore" role="tab" aria-controls="nav-contact" aria-selected="false">Learn more</a>--%>
        </div>
    </nav>
</section>

<%-- Home(Spend) --%>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
    <div class="container">
        <%-- Graphs & Analysis --%>
        <%--Deposit & Withdrawal--%>
        <div class="chart-container" style="position: relative; height:280px; width:600px ">
            <canvas id="myChart"></canvas>
        </div>
       <%-- <div class="chart-container" >
            <canvas id="myChart" style="position: relative; height:50px; width:200px"></canvas>
        </div>--%>
            <script>
            var d = new Date();
            var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                var thismonth = months[d.getMonth()];
                var thisyear = d.getFullYear();
            DepositWithdrawal = JSON.parse('<%=bargraph()%>');
            var ctx = document.getElementById('myChart').getContext('2d');
                var chart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ['Deposit', 'Withdrawal'],
                        datasets: [{
                            label: 'Amount',
                            backgroundColor: 'rgb(255, 99, 132)',
                            borderColor: 'rgb(255, 99, 132)',
                            data: [DepositWithdrawal[0].toFixed(2), DepositWithdrawal[1].toFixed(2)]
                        }]
                    },
                    options: {
                        title: {
                                display: true,
                                text: 'Deposit Vs Withdrawal For All Accounts In ' + thismonth,
                                fontSize: 25
                            },
                        maintainAspectRatio: true,
                        scales: {
                        yAxes: [{
                            stacked: true,
                            gridLines: {
                                display: true,
                                color: "rgba(255,99,132,0.2)"
                            }
                            }],
                        xAxes: [{
                            gridLines: {
                            display: false
                            }
                            }],
                        legend: {
                            display:true,
                            position:'right',
                        labels:{
                            fontColor: '#000'
                            }
                        },
                        tooltips:{
                            enabled:true
                        },
                        }}
                });
            </script>

            <br />
        <section id="accountsnmoney" class="border border-secondary bg-light shadow p-3 mb-5 rounded">
            <asp:Table ID="TableTotal" runat="server" class="table table-hover table-responsive-sm table-borderless">
                <asp:TableRow runat="server">
                        <asp:TableCell runat="server" class="text-center" style=" border-right:solid 1px">Money you have from all banks</asp:TableCell>
                        <asp:TableCell runat="server" class="text-center">Money u owe to all banks</asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server">
                        <asp:TableCell runat="server" class="text-center text-success" style="border-right:solid 1px">
                            <a href="TransferToSavings.aspx"><asp:Label ID="LblTotalMoney" runat="server" Text="Label"></asp:Label></a></asp:TableCell>
                        <asp:TableCell runat="server" class="text-center text-danger">
                            <asp:Label ID="LblTotalOwe" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </section>
        <%-- All the bank account --%>
        <section id="bankaccounts" class="border border-secondary bg-light shadow p-3 mb-5 rounded">
            <table ID="tablehomebankacc" class="table table-hover table-responsive-sm table-borderless">
                <tr>
                    <td runat="server"><h3>Bank Accounts</h3></td>
                </tr>
                <tr>
                    <asp:DataList runat="server" ID="bankAccDetails" RepeatColumns="2" RepeatDirection="Horizontal" CellPadding="20">
                        <ItemTemplate>
                                <td><h5><%# Eval("bankaccname") %></h5><h6><%# Eval("cardNum") %></h6></td>
                                <td><h5><%# Eval("balance") %></h5></td>
                        </ItemTemplate>
                    </asp:DataList>
                </tr>
            </table>
        </section>
    </div>
    </div>
<%-- Home(Save) --%>
    <div class="tab-pane fade" id="nav-spend" role="tabpanel" aria-labelledby="nav-profile-tab">
        <br />
            <div class="container">
                <%--<asp:LinkButton ID="AddSavings" runat="server" CssClass="btn btn-success" UseSubmitBehavior="false" data-toggle="modal" data-target="#Savings" OnClientClick="return false;" >--%>
                <asp:LinkButton ID="AddSavings" runat="server" CssClass="btn btn-success" OnClick="showModal" >
                <i class="fas fa-plus-circle"></i>&nbsp; Add Savings</asp:LinkButton>
                <%-- GridView --%>
                <asp:GridView ID="SavingsGridView" runat="server" AutoGenerateColumns="False" OnSelectedIndexChanged="SavingsGridView_SelectedIndexChanged" class="table table-responsive-sm table table-bordered mt-5" OnRowCommand="SavingsGridView_RowCommand" OnRowDataBound="SavingsGridView_RowDataBound">
                    <Columns>
                    <%--<asp:BoundField DataField="bankaccnum" HeaderText="Account Number" ReadOnly="True"/>--%>
                    <asp:BoundField DataField="savingsID" HeaderText="Saving ID" ReadOnly="True"/>
                    <asp:BoundField DataField="savingGoal" HeaderText="Saving Goal" ReadOnly="True"/>
                    <asp:BoundField DataField="amountsaved" HeaderText="Amount Saved" ReadOnly="True" />
                    <asp:BoundField DataField="description" HeaderText="Description" ReadOnly="True" />
                    <asp:BoundField DataField="endDate" HeaderText="End Date" ReadOnly="True" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:ButtonField ButtonType="Button" Text="Update" CommandName="update" >
                    <ControlStyle CssClass="btn btn-info" />
                    </asp:ButtonField>
                    <asp:ButtonField ButtonType="Button" CommandName="transfer" Text="Transfer">
                    <ControlStyle CssClass="btn btn-success" />
                    </asp:ButtonField>
                    <asp:ButtonField ButtonType="Button" Text="Delete" CommandName="delete" >
                    <ControlStyle CssClass="btn btn-danger" />
                    </asp:ButtonField>      
                </Columns>
                </asp:GridView>

              <div id="gaugecharts" class="d-flex"></div>      
                    <script>
                        Gauge = JSON.parse('<%=Gaugegraph()%>');
                        Chart.defaults.global.defaultFontFamily = 'Verdana';

                        var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
                        gradientStroke.addColorStop(0, '#80b6f4');
                        gradientStroke.addColorStop(1, '#f49080');

                        var i = 0;
                        var divsource = "";
                        for (values in Gauge) {
                            divsource += "<div id='canvas-holder' class='w-50'><canvas id='tchart-" + i + "'></canvas></div>";
                            i++;
                        }
                        document.getElementById("gaugecharts").innerHTML = divsource;

                        i = 0;
                        for (values in Gauge) {
                         var config = {
                            type: 'radialGauge',
                            data: {
                                labels: ['Amount Saved'],
                                datasets: [
                                    {
                                        data: [Gauge[values]],
                                        backgroundColor: [gradientStroke],
                                        borderWidth: 0,
                                        label: 'Percentage'
                                    }
                                ]
                            
                            },
                            options: {
                                responsive: true,
                                legend: {},
                                title: {
                                    display: true,
                                    text: 'Savings' + " " + values
                                },
                                centerPercentage: 80
                            }
                        }
                         var ctx1 = document.getElementById('tchart-'+i).getContext('2d');
                         new Chart(ctx1, config);
                            i++;
                        }
                </script>
            </div>
           <asp:Label ID="lbl_Savings" runat="server" Text=""></asp:Label>
         </div>
            <%-- Home(Learn More) --%>
            <%--<div class="tab-pane fade" id="nav-learnmore" role="tabpanel" aria-labelledby="nav-contact-tab">
                <div class="container">
                <p>12345</p>
                </div>
            </div>--%>
<%-- Modal to add Savings--%>
<div class="modal fade" id="AddSavingsmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Add a new Saving</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <asp:Label ID="LblNewSavingGoal" runat="server" Text="Saving Goal:"></asp:Label>
                <asp:TextBox ID="NewSavingGoalamt" runat="server" class="form-control" placeholder="Enter saving goal amount"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="LblNewSavingamt" runat="server" Text="Saved Amount:"></asp:Label>
                <asp:Label ID="NewSavingamtnone" runat="server" Text="0"></asp:Label>
            </div>
            <div class="form-group">
                <asp:Label ID="LblNewSavingdescription" runat="server" Text="Description:"></asp:Label>
                <asp:TextBox ID="NewSavingdescription" runat="server" class="form-control" placeholder="What are you saving for?"></asp:TextBox>
            </div>
            <div class="form-group">
            <asp:Label ID="LblNewSavingDate" runat="server" Text="End Date:"></asp:Label>
            <div class='input-group date' id='datetimepicker'>
                <asp:TextBox ID="NewSavingDate" runat="server" type="date" class="form-control"></asp:TextBox>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            <small id="" class="form-text text-muted">When can you save by?</small>
            </div>
            <asp:Label ID="LblbAccounts" runat="server" Text="Bank Accounts:"></asp:Label>
            <asp:DropDownList ID="ddlbankacclist" runat="server"  CssClass="btn btn-secondary dropdown-toggle"></asp:DropDownList>
           </div>
        <div class="modal-footer">
            <asp:Button ID="closebuttonmodalSavings" runat="server" Text="Back" type="reset" data-dismiss="modal" class="btn btn-info" OnClick="closebuttonmodalSavings_Click" /> &nbsp;&nbsp;
            <!-- Submit Button -->
            <asp:Button ID="submitbuttonmodalSavings" runat="server" Text="Submit" class="btn btn-success" OnClick="submitbuttonmodalSavings_Click"/>
        </div>
    </div>
</div>
</div>
</div>
<script type='text/javascript'>
    function openModal() {
        $('[id*=AddSavingsmodal]').modal('show');
    }
</script>
</div>

</asp:Content>