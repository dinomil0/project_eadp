﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class Class_InquiryDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        public bool check_VCInquiry(string userID)
        {
            Class_Inquiry inquiry = new Class_Inquiry();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Inquiry");
            sqlCommand.AppendLine("WHERE InquiryId = @paraInquiryId");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("paraInquiryId", "COVC" + userID);

            da.Fill(ds, "InquiryTable");

            foreach (DataRow row in ds.Tables["InquiryTable"].Rows)
            {
                inquiry.InquiryId = Convert.ToString(row["InquiryId"]);
            }
            if (inquiry.InquiryId != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Class_Inquiry get_VCInquiry(string userID)
        {
            Class_Inquiry inquiry = new Class_Inquiry();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Inquiry");
            sqlCommand.AppendLine("WHERE InquiryId = @paraInquiryId");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("paraInquiryId", "COVC" + userID);

            da.Fill(ds, "InquiryTable");

            foreach (DataRow row in ds.Tables["InquiryTable"].Rows)
            {
                inquiry.InquiryId = Convert.ToString(row["InquiryId"]);
                inquiry.ordId = Convert.ToString(row["ordId"]);
                inquiry.UserInput = Convert.ToString(row["UserInput"]);
                inquiry.InquiryDesc = Convert.ToString(row["InquiryDesc"]);
            }
            return inquiry;
        }
        public List<Class_Inquiry> get_buyinquiry(string userID)
        {
            List<Class_Inquiry> inquirylist = new List<Class_Inquiry>();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Inquiry");
            sqlCommand.AppendLine("WHERE userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

            da.Fill(ds, "InquiryTable");

            foreach (DataRow row in ds.Tables["InquiryTable"].Rows)
            {
                Class_Inquiry inquiry = new Class_Inquiry();
                inquiry.InquiryId = Convert.ToString(row["InquiryId"]);
                inquiry.ordId = Convert.ToString(row["ordId"]);
                inquiry.UserInput = Convert.ToString(row["UserInput"]);
                inquiry.SellerInput = Convert.ToString(row["SellerInput"]);
                inquiry.InquiryDesc = Convert.ToString(row["InquiryDesc"]);
                inquirylist.Add(inquiry);
            }
            return inquirylist;
        }
        public List<Class_Inquiry> get_sellinquiry(string userID)
        {
            List<Class_Inquiry> inquirylist = new List<Class_Inquiry>();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Inquiry");
            sqlCommand.AppendLine("WHERE sellerID = @parasellerID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parasellerID", userID);

            da.Fill(ds, "InquiryTable");

            foreach (DataRow row in ds.Tables["InquiryTable"].Rows)
            {
                Class_Inquiry inquiry = new Class_Inquiry();
                inquiry.InquiryId = Convert.ToString(row["InquiryId"]);
                inquiry.ordId = Convert.ToString(row["ordId"]);
                inquiry.UserInput = Convert.ToString(row["UserInput"]);
                inquiry.SellerInput = Convert.ToString(row["SellerInput"]);
                inquiry.InquiryDesc = Convert.ToString(row["InquiryDesc"]);
                inquirylist.Add(inquiry);
            }
            return inquirylist;
        }
        public void add_inquiry(Class_Inquiry inquiry)
        {
            StringBuilder sqlCommand = new StringBuilder();
            SqlCommand excommand = new SqlCommand();
            sqlCommand.AppendLine("INSERT INTO Inquiry");
            sqlCommand.AppendLine("VALUES (@paraInquiryId, @paraordId, @parauserID, @parasellerID, @parauinput, @parasinput, @paradesc)");
            SqlConnection myConn = new SqlConnection(DBConnect);
            excommand = new SqlCommand(sqlCommand.ToString(), myConn);
            excommand.Parameters.AddWithValue("paraInquiryId", inquiry.InquiryId);
            excommand.Parameters.AddWithValue("paraordId", inquiry.ordId);
            excommand.Parameters.AddWithValue("parauserID", inquiry.userID);
            excommand.Parameters.AddWithValue("parasellerID", inquiry.sellerID);
            excommand.Parameters.AddWithValue("parauinput", inquiry.UserInput);
            excommand.Parameters.AddWithValue("parasinput", inquiry.SellerInput);
            excommand.Parameters.AddWithValue("paradesc", inquiry.InquiryDesc);
            myConn.Open();
            excommand.ExecuteNonQuery();
            myConn.Close();
        }
        public void update_inquiry(string InquiryId, string user, string input)
        {
            Class_Inquiry inquiry = new Class_Inquiry();
            DataSet ds = new DataSet();

            if (user == "buyer")
            {
                StringBuilder sqlCommand = new StringBuilder();
                SqlCommand excommand = new SqlCommand();
                sqlCommand.AppendLine("UPDATE Inquiry");
                sqlCommand.AppendLine("SET UserInput = @parainput");
                sqlCommand.AppendLine("WHERE InquiryId = @paraInquiryId");
                SqlConnection myConn = new SqlConnection(DBConnect);
                excommand = new SqlCommand(sqlCommand.ToString(), myConn);
                excommand.Parameters.AddWithValue("paraInquiryId", InquiryId);
                excommand.Parameters.AddWithValue("parainput", input);
                myConn.Open();
                excommand.ExecuteNonQuery();
                myConn.Close();
            }
            else if (user == "seller")
            {
                StringBuilder sqlCommand1 = new StringBuilder();
                SqlCommand excommand1 = new SqlCommand();
                sqlCommand1.AppendLine("UPDATE Inquiry");
                sqlCommand1.AppendLine("SET SellerInput = @parainput");
                sqlCommand1.AppendLine("WHERE InquiryId = @paraInquiryId");
                SqlConnection myConn1 = new SqlConnection(DBConnect);
                excommand1 = new SqlCommand(sqlCommand1.ToString(), myConn1);
                excommand1.Parameters.AddWithValue("paraInquiryId", InquiryId);
                excommand1.Parameters.AddWithValue("parainput", input);
                myConn1.Open();
                excommand1.ExecuteNonQuery();
                myConn1.Close();
            }
        }
        public string check_inquiry(string InquiryId)
        {
            Class_InquiryDAO inquiryDAO = new Class_InquiryDAO();

            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Inquiry");
            sqlCommand.AppendLine("WHERE InquiryId = @paraInquiryId");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("paraInquiryId", InquiryId);

            da.Fill(ds, "InquiryTable");

            Class_Inquiry inquiry = new Class_Inquiry();
            foreach (DataRow row in ds.Tables["InquiryTable"].Rows)
            {
                inquiry.InquiryId = Convert.ToString(row["InquiryId"]);
                inquiry.ordId = Convert.ToString(row["ordId"]);
                inquiry.userID = Convert.ToString(row["userID"]);
                inquiry.sellerID = Convert.ToString(row["sellerID"]);
                inquiry.UserInput = Convert.ToString(row["UserInput"]);
                inquiry.SellerInput = Convert.ToString(row["SellerInput"]);
                inquiry.InquiryDesc = Convert.ToString(row["InquiryDesc"]);
            }
            if (inquiry.UserInput == inquiry.SellerInput)
            {
                if (inquiry.UserInput == "Refund")
                {
                    List<Class_Transaction> transList = new List<Class_Transaction>();
                    Class_Transaction transactionBuyer = new Class_Transaction();
                    Class_Transaction transactionSeller = new Class_Transaction();
                    Class_TransactionDAO transDAO = new Class_TransactionDAO();
                    transList = transDAO.get_transaction_by_ordId(inquiry.ordId);
                    foreach (Class_Transaction transaction in transList)
                    {
                        if (transaction.userID == inquiry.userID)
                        {
                            transactionBuyer.transID = "BREFUND" + transactionBuyer.getCount();
                            transactionBuyer.ordId = "RF" + transactionBuyer.transID;
                            transactionBuyer.date = DateTime.Now;
                            transactionBuyer.reference = transaction.reference;
                            transactionBuyer.price = Convert.ToString(transaction.price);
                            transactionBuyer.qty = transaction.qty;
                            transactionBuyer.total = transaction.total;
                            transactionBuyer.debit = "0.00";
                            transactionBuyer.credit = transaction.debit;
                            transactionBuyer.userID = inquiry.userID;
                            transactionBuyer.status = "Cleared";
                            string type;
                            if (transactionBuyer.credit.Length > 3 && Convert.ToString(transactionBuyer.credit[transactionBuyer.credit.Length - 2]) == "V")
                            {
                                type = "VC";
                            }
                            else
                            {
                                type = "Card";
                            }
                            Class_AccountDAO accDAO = new Class_AccountDAO();
                            List<Class_Account.Account> accList = accDAO.getAccount(transactionBuyer.userID);
                            long bankaccnum = accList[0].bankaccnum;
                            transDAO.Add_Transaction(transactionBuyer, type, bankaccnum);
                        }
                        else if (transaction.userID == inquiry.sellerID)
                        {
                            transactionSeller.transID = "SREFUND" + transactionSeller.getCount();
                            transactionSeller.ordId = "RF" + transactionSeller.transID;
                            transactionSeller.date = DateTime.Now;
                            transactionSeller.reference = transaction.reference;
                            transactionSeller.price = Convert.ToString(transaction.price);
                            transactionSeller.qty = transaction.qty;
                            transactionSeller.total = Convert.ToString(transaction.total);
                            transactionSeller.debit = transaction.credit;
                            transactionSeller.credit = "0.00";
                            transactionSeller.userID = inquiry.sellerID;
                            transactionSeller.status = "Cleared";
                            Class_AccountDAO accDAO = new Class_AccountDAO();
                            List<Class_Account.Account> accList = accDAO.getAccount(transactionBuyer.userID);
                            long bankaccnum = accList[0].bankaccnum;
                            transDAO.Add_Transaction(transactionSeller, "Card", bankaccnum);
                        }
                    }
                    return "Refund";
                }
                else if (inquiry.UserInput == "Replace")
                {
                    Class_OrderDAO orderDAO = new Class_OrderDAO();
                    orderDAO.Update_Status(inquiry.ordId, "Pending");
                    return "Replace";
                }
            }
            else
            {
                return "No Match";
            }
            return null;
        }
        public void remove_inquiry(string InquiryId)
        {
            StringBuilder sqlCommand = new StringBuilder();
            SqlCommand excommand = new SqlCommand();
            sqlCommand.AppendLine("DELETE from Inquiry");
            sqlCommand.AppendLine("WHERE InquiryId = @paraInquiryId");
            SqlConnection myConn = new SqlConnection(DBConnect);
            excommand = new SqlCommand(sqlCommand.ToString(), myConn);
            excommand.Parameters.AddWithValue("paraInquiryId", InquiryId);
            myConn.Open();
            excommand.ExecuteNonQuery();
            myConn.Close();
        }
    }
}