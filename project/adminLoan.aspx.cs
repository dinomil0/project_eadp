﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;
using TextmagicRest;
using TextmagicRest.Model;

namespace project
{
    public partial class adminLoan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }
            Session["custNRIC"] = "A";
            if (!IsPostBack)
            {
                //populateDDL();
                //Session["allGroup"] =  trendGraph(DateTime.Now);
                loanApplicationDAO loanAppDAO = new loanApplicationDAO();
                var applicationList = loanAppDAO.retrieveLoans();
                Session["applicationList"] = applicationList;
                gvAdminLoan.DataSource = applicationList;
                gvAdminLoan.DataBind();
                Session["NRIC"] = "";
                Session["loanAppID"] = "";
            }
            
        }

        //public bool buttonApproved = false;

        protected void gvAdminLoan_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //try
            //{
            //    if (e.CommandName == "acceptLoan")
            //    {
            //        if (buttonApproved == true)
            //        {
            //            TransactionHistory transactionHistory = new TransactionHistory();
            //            TransactionHistoryDAO TransactionHistoryDAO = new TransactionHistoryDAO();

            //            List<loanApplication> loanApps = new List<loanApplication>();
            //            loanApplicationDAO loanApplicationDAO = new loanApplicationDAO();

            //            int index = Convert.ToInt32(e.CommandArgument);
            //            GridViewRow row = gvAdminLoan.Rows[index];
            //            customer customerobj = new customer();
            //            customerDAO customerDAO = new customerDAO();

            //            List<Accounts> accountsobj = new List<Accounts>();
            //            AccountsDAO accountsDAO = new AccountsDAO();

            //            var NRIC = (row.FindControl("btnCheckCust") as LinkButton).Text;

            //            accountsobj = accountsDAO.getbackaccnumbyuserID(NRIC);
            //            customerobj = customerDAO.GetCustomerLoginCredential(NRIC);

            //            var number = customerobj.customerCC.ToString() + customerobj.phonenum.ToString();
            //            var amount = Convert.ToDouble(row.Cells[3].Text.TrimStart('$'));

            //            loanApps = loanApplicationDAO.retrieveLoans();
            //            var loanappOBJ = new loanApplication();
            //            foreach (var loanapplication in loanApps)
            //            {
            //                if (loanapplication.applicationID == Convert.ToInt32(row.Cells[0].Text))
            //                {
            //                    loanappOBJ = loanapplication;
            //                }
            //            }

            //            for (var i = 0; i < accountsobj.Count(); i++)
            //            {
            //                if (accountsobj[i].bankname == loanappOBJ.bankName)
            //                {
            //                    TransactionHistoryDAO.InsertTransactionsDeposit(DateTime.Now, "Loan", "Deposit", Convert.ToDouble(amount), "successful", NRIC, 0, 0, accountsobj[i].bankaccnum);
            //                    accountsDAO.updatebankaccbalance(accountsobj[i].bankaccnum, Convert.ToDouble(amount));
            //                    break;
            //                }
            //                else if (i == (accountsobj.Count() - 1))
            //                {
            //                    accountsDAO.createBankACC(NRIC, Convert.ToString(loanappOBJ.bankName), Convert.ToDouble(amount));
            //                }
            //                loanApplicationDAO.updateLoanApplicationStatus(Convert.ToInt32(row.Cells[0].Text), "Approved");
            //            }
            //            Response.Redirect(Request.RawUrl);
            //            sendMessage("Loan Approved", number);
            //        }
            //    }

            //    //else if (e.CommandName == "declineLoan")
            //    //{
            //    //    loanApplicationDAO loanApplicationDAO = new loanApplicationDAO();

            //    //    int index = Convert.ToInt32(e.CommandArgument);
            //    //    GridViewRow row = gvAdminLoan.Rows[index];
            //    //    customer customerobj = new customer();
            //    //    customerDAO customerDAO = new customerDAO();

            //    //    Accounts accountsobj = new Accounts();
            //    //    AccountsDAO accountsDAO = new AccountsDAO();

            //    //    var NRIC = (row.FindControl("btnCheckCust") as LinkButton).Text;
            //    //    accountsobj = accountsDAO.getbankaccs(NRIC);
            //    //    customerobj = customerDAO.GetCustomerLoginCredential(NRIC);


            //    //    Session["NRIC"] = NRIC;
            //    //    Session["loanAppID"] = row.Cells[0].Text;
            //    //    //var number = customerobj.customerCC.ToString() + customerobj.phonenum.ToString();
            //    //    //loanApplicationDAO.deleteLoan(Convert.ToInt32(row.Cells[0].Text));
            //    //    //get admin feedback
            //    //    //sendMessage("Loan Decline, ", number);
            //    //}
            //}
            //catch (Exception ex) { Console.WriteLine(ex); }
        }
        

        protected void gvAdminLoan_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var loanAmt = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "loanAmt").ToString());
                if (loanAmt > 50000)
                {
                    e.Row.Cells[7].Enabled = false;
                    e.Row.Cells[8].Enabled = false;
                }
            }
        }
        public void showApproveModal(object sender, EventArgs e)
        {
            int rowindex = Convert.ToInt32(((sender as Button).NamingContainer as GridViewRow).RowIndex);
            GridViewRow row = gvAdminLoan.Rows[rowindex];
            //var NRIC = (row.FindControl("btnCheckCust") as LinkButton).Text;
            Session["GVRow"] = row;
            //lblcustNRIC.Text = NRIC;

            ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openapproveModal()", true);
        }

        public void showModal(object sender, EventArgs e)
        {
            int rowindex = Convert.ToInt32(((sender as LinkButton).NamingContainer as GridViewRow).RowIndex);
            GridViewRow row = gvAdminLoan.Rows[rowindex];
             
            var NRIC = (row.FindControl("btnCheckCust") as LinkButton).Text;
            TransactionHistoryDAO TransactionHistoryDAO = new TransactionHistoryDAO();
            customer customerobj = new customer();
            customerDAO customerDAO = new customerDAO();
            customerobj = customerDAO.GetCustomerLoginCredential(NRIC);
            lblcustName.Text = customerobj.name.ToString();
            lblNRIC.Text = NRIC.ToString();
            var age = DateTime.Now.Year - Convert.ToDateTime(customerobj.customerDOB).Year; 
            lblAge.Text = age.ToString();
            lblSalary.Text = TransactionHistoryDAO.read_Salary(NRIC).ToString();
            //lblGender.Text = customerobj.customerGender.ToString();
            lblCS.Text = customerobj.customerCS.ToString();

            ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal()", true);
        }

        public void showRejectModal(object sender, EventArgs e)
        {
            int rowindex = Convert.ToInt32(((sender as Button).NamingContainer as GridViewRow).RowIndex);
            GridViewRow row = gvAdminLoan.Rows[rowindex];

            var NRIC = (row.FindControl("btnCheckCust") as LinkButton).Text;

            customer customerobj = new customer();
            customerDAO customerDAO = new customerDAO();
            customerobj = customerDAO.GetCustomerLoginCredential(NRIC);

            Session["NRIC"] = NRIC;
            Session["loanAppID"] = row.Cells[0].Text;

            ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openrejectModal()", true);
        }


        
        protected void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                TransactionHistory transactionHistory = new TransactionHistory();
                TransactionHistoryDAO TransactionHistoryDAO = new TransactionHistoryDAO();

                List<loanApplication> loanApps = new List<loanApplication>();
                loanApplicationDAO loanApplicationDAO = new loanApplicationDAO();

                customer customerobj = new customer();
                customerDAO customerDAO = new customerDAO();

                List<Accounts> accountsobj = new List<Accounts>();
                AccountsDAO accountsDAO = new AccountsDAO();

                var row = (GridViewRow)Session["GVRow"];

                var NRIC = (row.FindControl("btnCheckCust") as LinkButton).Text;

                accountsobj = accountsDAO.getbackaccnumbyuserID(NRIC);
                customerobj = customerDAO.GetCustomerLoginCredential(NRIC);

                var number = customerobj.customerCC.ToString() + customerobj.phonenum.ToString();
                var amount = Convert.ToDouble(row.Cells[3].Text.TrimStart('$'));

                loanApps = loanApplicationDAO.retrieveLoans();
                var loanappOBJ = new loanApplication();
                foreach (var loanapplication in loanApps)
                {
                    if (loanapplication.applicationID == Convert.ToInt32(row.Cells[0].Text))
                    {
                        loanappOBJ = loanapplication;
                    }
                }
                if (accountsobj == null)
                {
                    accountsDAO.createBankACC(NRIC, Convert.ToString(loanappOBJ.bankName), Convert.ToDouble(amount));
                }
                else
                {
                    for (var i = 0; i < accountsobj.Count(); i++)
                    {
                        if (accountsobj[i].bankname == loanappOBJ.bankName)
                        {
                            TransactionHistoryDAO.InsertTransactionsDeposit(DateTime.Now, "Loan", "Deposit", Convert.ToDouble(amount), "successful", NRIC, 0, 0, accountsobj[i].bankaccnum);
                            accountsDAO.updatebankaccbalance(accountsobj[i].bankaccnum, Convert.ToDouble(amount));
                            break;
                        }
                        else if (i == (accountsobj.Count() - 1))
                        {
                            accountsDAO.createBankACC(NRIC, Convert.ToString(loanappOBJ.bankName), Convert.ToDouble(amount));
                        }
                    }
                
                loanApplicationDAO.updateLoanApplicationStatus(Convert.ToInt32(row.Cells[0].Text), "Approved");
                }
                sendMessage("Loan Approved", number);
                Response.Redirect(Request.RawUrl);

            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        protected void btnReasonSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var reason = tbReason.Text;
                loanApplicationDAO loanApplicationDAO = new loanApplicationDAO();
                customer customerobj = new customer();
                customerDAO customerDAO = new customerDAO();
                var NRIC = Session["NRIC"].ToString();
                customerobj = customerDAO.GetCustomerLoginCredential(NRIC);
                var number = customerobj.customerCC.ToString() + customerobj.phonenum.ToString();

                //sendMessage("Loan Decline, " + reason, number);
                loanApplicationDAO.updateLoanApplicationStatus(Convert.ToInt32(Session["loanAppID"].ToString()), "Declined");
                Response.Redirect(Request.RawUrl);
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }
        public static void sendMessage(string msg, string number)
        {
            var client = new Client("limzhengjie", "l0KkWkR1KECotdNGZNGFzgrJums4Q6");
            var link = client.SendMessage(msg, number);
        }
    }
}