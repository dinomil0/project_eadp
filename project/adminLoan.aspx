﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="adminLoan.aspx.cs" Inherits="project.adminLoan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<section>
    <%----%>
        <section class="border border-secondary bg-light shadow p-3 mb-5 rounded container mt-3">
        <div>
            <h3>
                Loan Applications
            </h3>
        </div>
        <asp:GridView ID="gvAdminLoan" class="table table-responsive-sm table-bordered" runat="server" AutoGenerateColumns="False" OnRowCommand="gvAdminLoan_RowCommand" OnRowDataBound="gvAdminLoan_RowDataBound">
            <Columns>
                <asp:BoundField DataField="applicationID" HeaderText="Application ID" />
                <asp:TemplateField HeaderText="Customer's NRIC" ShowHeader="False"> 
                    <ItemTemplate>
                        <asp:LinkButton ID="btnCheckCust" runat="server" CausesValidation="false" CommandName="checkCust" Text='<%# Eval("custNRIC") %>' OnClick="showModal"></asp:LinkButton>
                    </ItemTemplate>
                    <ControlStyle CssClass="text-primary" />
                </asp:TemplateField>
                <asp:BoundField DataField="bankName" HeaderText="Bank Name" />
                <asp:BoundField HeaderText="Loan Amount" DataField="loanAmt" DataFormatString="{0:C}" />
                <asp:BoundField HeaderText="Effective Interest Rate (EIR)" DataField="EIR" DataFormatString="{0:P}"/>
                <asp:BoundField HeaderText="Tenor" DataField="tenor" />
                <asp:BoundField HeaderText="Repayment Amount" DataField="repaymentAmt" DataFormatString="${0:N}"/>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:Button ID="approveLoan" runat="server" CausesValidation="false" CommandName="acceptLoan" Text="Accept Loan" OnClick="showApproveModal"/>
                    </ItemTemplate>
                    <ControlStyle CssClass="btn btn-success" />
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:Button ID="declineLoan" runat="server" CausesValidation="false" CommandName="declineLoan" Text="Decline Loan" OnClick="showRejectModal" />
                    </ItemTemplate>
                    <ControlStyle CssClass="btn btn-danger" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        </section>
        <div class="modal fade" id="rejectForm" tabindex="-1" role="dialog" aria-labelledby="rejectForm" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="md_Fp_Label2">Decline Loan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="Label1" runat="server" Text="Label">Reason of declining loan</asp:Label>
                        <asp:TextBox ID="tbReason" runat="server"></asp:TextBox>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnReasonSubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnReasonSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="approveModal" tabindex="-1" role="dialog" aria-labelledby="rejectForm" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="md_Fp_Labela">Approve Loan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="Label6" runat="server" Text="Label">Confirm Approval?</asp:Label>
                        <%--<asp:Label ID="lblcustNRIC" runat="server"></asp:Label>--%>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnApprove" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnApprove_Click"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Customer Details</h4>
                        <button type="button" class="close" data-dismiss="modal">
                            &times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            <div class="form-group">
                                <asp:Label ID="lbl1" runat="server" Text="Customer Name:"></asp:Label>
                                <asp:Label ID="lblcustName" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="Customer NRIC:"></asp:Label>
                                <asp:Label ID="lblNRIC" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label5" runat="server" Text="Customer Age:"></asp:Label>
                                <asp:Label ID="lblAge" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text="Monthly Salary:"></asp:Label>
                                <asp:Label ID="lblSalary" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text="Credit Score:"></asp:Label>
                                <asp:Label ID="lblCS" runat="server" Text=""></asp:Label>
                            </div>

                        </div>
                    </div>
                    <%--<div class="modal-footer">
                        <asp:Button ID="btnSave" runat="server" Text="Pay" OnClick="" CssClass="btn btn-info" />
                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                    </div>--%>
                </div>
            </div>
        </div>
        <script type='text/javascript'>
                function openModal(){$('[id*=myModal]').modal('show');} 
                function openrejectModal(){$('[id*=rejectForm]').modal('show');} 
                function openapproveModal(){$('[id*=approveModal]').modal('show');} 
        </script>
    <%----%>
</section>
</asp:Content>
