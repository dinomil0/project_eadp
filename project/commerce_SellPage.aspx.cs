﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class commerce_SellPage : System.Web.UI.Page
    {
        string userID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            userID = Session["custNRIC"].ToString();
        }

        protected void btn_addtolist_Click(object sender, EventArgs e)
        {
            Class_Product product = new Class_Product();
            product.ProdId = Tb_ProductName.Text.Substring(0, 2).ToUpper() + product.get_count() + userID;
            product.ProdName = Tb_ProductName.Text;
            product.ProdDesc = Tb_ProductDesc.Text;
            product.ProdPrice = Convert.ToInt16(Tb_ProductPrice.Text);
            product.ProdQty = Convert.ToInt16(Tb_ProductQuantity.Text);
            product.SellerID = userID;
            //test
            HttpPostedFile postedFile = file_imgUpload.PostedFile;
            string fileName = Path.GetFileName(postedFile.FileName);
            string fileExtension = Path.GetExtension(fileName);
            int fileSize = postedFile.ContentLength;
            if (fileExtension.ToLower() == ".jpg" || fileExtension.ToLower() == ".bmp" || fileExtension.ToLower() == ".gif" || fileExtension.ToLower() == ".png")
            {
                Stream stream = postedFile.InputStream;
                BinaryReader biReader = new BinaryReader(stream);
                product.ProdImage = biReader.ReadBytes(Convert.ToInt32(stream.Length));
            }
            else
            {
                //Lbl_err
            }
            //test
            Class_ProductDAO productDAO = new Class_ProductDAO();
            productDAO.Add_Product(product);
            Response.Redirect("commerce_Listing.aspx");
            //Create product object
        }

        protected void btn_Upload_Click(object sender, EventArgs e)
        {
            HttpPostedFile postedFile = file_imgUpload.PostedFile;
            string fileName = Path.GetFileName(postedFile.FileName);
            string fileExtension = Path.GetExtension(fileName);
            int fileSize = postedFile.ContentLength;
            if (fileExtension.ToLower() == ".jpg" || fileExtension.ToLower() == ".bmp" || fileExtension.ToLower() == ".gif" || fileExtension.ToLower() == ".png")
            {
                Stream stream = postedFile.InputStream;
                BinaryReader biReader = new BinaryReader(stream);
                byte[] bytes = biReader.ReadBytes(Convert.ToInt32(stream.Length));
            }
            else
            {
                //Lbl_err
            }
        }
    }
}