﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="budget_Overview.aspx.cs" Inherits="project.budget_Overview" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div id="form_Bg_Overview" >
        <asp:TextBox ID="tb_Bg_Read_Date" runat="server" Visible="False"></asp:TextBox>
        <div class="container my-5 border border-warning rounded">
            <asp:LinkButton class="mt-3 btn btn-secondary btn-block" data-toggle="collapse" data-target="#bg_Read_Collapse" aria-expanded="false" aria-controls="bg_Read_Collapse" runat="server">View Budget Here</asp:LinkButton>
            <div id="bg_Read_Collapse" class="row collapse">
                <div class="col-12 mt-3 d-flex">
                    <div class="px-0 col-sm-12 col-md-11 d-flex">
                        <asp:Label ID="lb_Bg_Title" class="h4 pl-0" runat="server"></asp:Label>
                        <asp:LinkButton ID="btn_Bg_Information" class="badge" data-toggle="modal" data-target="#bg_Information" runat="server">
                            <i class="fa fa-2x fa-info-circle" style="color:#1fbed6;"></i>
                        </asp:LinkButton>
                    </div>
                    <div>
                        <asp:Button ID="btn_Bg_Update_Data" class="btnEdit btn btn-success float-right" runat="server" Text="Edit" OnClick="btn_Bg_Update_Data_Click"></asp:Button>
                    </div>
                </div>
                <div class="col-12">
                    <h5 class="pt-2">Bank Account:&nbsp<asp:DropDownList ID="ddl_Bg_Read_Account" class="btn btn-light" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_Bg_Read_Account_SelectedIndexChanged"></asp:DropDownList></h5>
                    <asp:Textbox ID="tb_Bg_Read_Count" class="w-50 border-0" ReadOnly="True" runat="server" ></asp:Textbox>
                    <asp:TextBox ID="tb_Bg_Read_Detail" class="w-50 border-0" ReadOnly="True" runat="server"></asp:TextBox>
                    <asp:Panel id="pn_Bg_Salary" runat="server">
                        <div class="form-group row mt-3">
                            <asp:Label ID="lb_Bg_Read_Salary" class="col-2 col-form-label" runat="server">Current Salary:</asp:Label>
                            <div class="input-group col-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Read_Salary" class="form-control" ReadOnly="true" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:TextBox ID="tb_Bg_Error" class="h5 text-danger mt-3 w-50 border-0" runat="server"></asp:TextBox>
                </div>
                <div class="col-xs-12 col-md-4 mt-4">
                    <h5 class="text-left text-md-center font-weight-bold">
                        Needs
                    </h5>
                    <hr />
                    <div class="container p-0">
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Read_Consumer_Debt" class="col-6 col-form-label" runat="server">Consumer Debts:</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Read_Consumer_Debt" class="flex-fill form-control" ReadOnly="true" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Read_Food_Grocery" class="col-6 col-form-label" runat="server">Food & Groceries</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Read_Food_Grocery" class="flex-fill form-control" ReadOnly="true" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Read_Healthcare" class="col-6 col-form-label" runat="server">Healthcare:</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Read_Healthcare" class="flex-fill form-control"  ReadOnly="true" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Read_Housing" class="col-6 col-form-label" runat="server">Housing:</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Read_Housing" class="flex-fill form-control"  ReadOnly="true" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Read_Personal_Care" class="col-6 col-form-label" runat="server">Personal Care:</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Read_Personal_Care" class="flex-fill form-control"  ReadOnly="true" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Read_Utility" class=" col-6 col-form-label" runat="server">Utilities:</asp:Label>       
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Read_Utility" class="flex-fill form-control"  ReadOnly="true" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>      
                </div>

                <div class="col-xs-12 col-md-4 mt-4">
                    <h5 class="text-left text-md-center font-weight-bold">
                        Wants
                    </h5>
                    <hr />
                    <div class="container">
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Read_Entertainment" class="col-6 col-form-label" runat="server">Entertainment:</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Read_Entertainment" class="flex-fill form-control"  ReadOnly="true" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 mt-4">
                    <h5 class="text-left text-md-center font-weight-bold">
                        Reserves
                    </h5>
                    <hr />
                    <div class="container">
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Read_Emergency_Fund" class="col-6 col-form-label" runat="server">Emergency Funds:</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Read_Emergency_Fund" class="flex-fill form-control"  ReadOnly="true" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Read_Saving" class="col-6 col-form-label" runat="server">Savings:</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                            <asp:TextBox ID="tb_Bg_Read_Saving" class="flex-fill form-control" ReadOnly="true"  runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <div class="container">
                <div class="chart-container">
                    <canvas id="cv_Bg_Graph" style="position: relative; height:300px; width:600px"></canvas>            
                </div>

                <asp:Label class="h5 d-flex justify-content-center font-weight-bold" Text="Summary" runat="server"></asp:Label>
                <hr class="w-25" />
                <div class="container ml-5 ml-5">
                    <asp:Label id="lb_Bg_Summary" runat="server"></asp:Label>
                </div>
            </div>
        </div>

        <!--Budget Information-->
        <div class="modal fade" id="bg_Information" tabindex="-1" role="dialog" aria-labelledby="md_Bg_Title" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="md_Bg_Title">
                            Budget Categories Information
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <strong>1. Consumer Debts:</strong><br />
                        <br />
                        Includes Credit Card Bills, Student Loans, Installment Agreements and Car Payments<br />
                        <br />
                        <strong>2. Food & Groceries</strong><br />
                        <br />
                        Expenses depending on Family Size, Eating Habits<br />
                        <br />
                        <strong>3. Healthcare</strong><br />
                        <br />
                        Expenses such as Premiums for Health, Disability, Vision and Dental Insurance.<br />
                        <br />
                        <strong>4. Housing</strong><br />
                        <br />
                        Rent Expenses, including Household Maintenance and Repairs, Renters or Homeowners Insurance and Property Taxes that you pay to your County or Government Jurisdiction.<br />
                        <br />
                        <strong>5. Personal Care</strong><br />
                        <br />
                        Inclusive of Salons, Hair Care Products, Personal Hygiene and Laundry Supplies, Shoes and Shoe Repair, Clothing and Dry Cleaning.<br />
                        <br />
                       <strong>6. Utilities</strong><br />
                        <br />
                        Includes Gas, Electricity, Water and Sewage, Land Line and Cell Phone Payments, Cable Television and Internet Access.<br />
                        <br />
                        <strong>7. Entertainment</strong><br />
                        <br />
                        Putting aside money for the vacation you dream of taking, or Treating your family to a "stay-cation" with a day at the local amusement park.<br />
                        <br />
                        <strong>8. Emergency Funds</strong><br />
                        <br />
                        When one is unable to take care of yourself and your family.<br />
                        <br />
                        <strong>9. Savings</strong><br />
                        <br />
                        Saving for your Present Needs and, especially, your Future Needs.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!--Updating Budget Data-->
        <div class="modal fade" id="md_Bg_Update_Data" tabindex="-1" role="dialog" aria-labelledby="md_Bg_Update_Label" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="md_Bg_Update_Label">Updating Budget Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lb_Bg_Update_Error" class="text-danger" runat="server"></asp:Label>
                        <div class="form-group row mt-3">
                            <asp:Label ID="lb_Bg_Update_Consumer_Debt" class="col-6 col-form-label" runat="server">Consumer Debts:</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Update_Consumer_Debt" class="flex-fill form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Update_Food_Grocery" class="col-6 col-form-label" runat="server">Food & Groceries:</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Update_Food_Grocery" class="flex-fill form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Update_Healthcare" class="col-6 col-form-label" runat="server">Healthcare:</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Update_Healthcare" class="flex-fill form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Update_Housing" class="col-6 col-form-label" runat="server">Housing:</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Update_Housing" class="flex-fill form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Update_Personal_Care" class="col-6 col-form-label" runat="server">Personal Care:</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Update_Personal_Care" class="flex-fill form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Update_Utility" class="col-6 col-form-label" runat="server">Utilities:</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Update_Utility" class="flex-fill form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Bg_Update_Entertainment" class="col-6 col-form-label" runat="server">Entertainment:</asp:Label>
                            <div class="input-group col-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <asp:TextBox ID="tb_Bg_Update_Entertainment" class="flex-fill form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btn_Bg_Update_Data_Submit" class="btn btn-success" Onclick="btn_Bg_Update_Data_Submit_Click" Text="Update Data"  runat="server" />
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            function initiate_Update_Modal() {
                $('[id*=md_Bg_Update_Data]').modal('show');
            } 

            budget_Previous_Data = JSON.parse('<%=read_Account_Info()%>');
            

            let myChart = document.getElementById('cv_Bg_Graph').getContext('2d');

            // Global Options
            Chart.defaults.global.defaultFontFamily = 'Lato';
            Chart.defaults.global.defaultFontSize = 18;
            Chart.defaults.global.defaultFontColor = '#777';

            const today_Date = new Date()
            const monthNames = ["Jan", "Feb", "March", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];


            let massPopChart = new Chart(myChart, {
                type: 'bar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
                data: {
                    labels: [monthNames[(today_Date.getMonth())-3], monthNames[(today_Date.getMonth())-2], monthNames[(today_Date.getMonth())-1], monthNames[today_Date.getMonth()]],
                    datasets: [{
                        label: 'Consumer Debts',
                        data: budget_Previous_Data[0],
                        //backgroundColor:'green',
                        backgroundColor: [
                            'rgba(211, 140, 237, 0.6)',
                            'rgba(211, 140, 237, 0.6)',
                            'rgba(211, 140, 237, 0.6)',
                            'rgba(211, 140, 237, 0.6)'
                        ],
                        borderWidth: 1,
                        borderColor: '#777',
                        hoverBorderWidth: 3,
                        hoverBorderColor: '#000'
                        
                    },
                    {
                        label: 'Food & Groceries',
                        data: budget_Previous_Data[1],
                        //backgroundColor:'green',
                        backgroundColor: [
                            'rgba(251, 225, 116, 0.6)',
                            'rgba(251, 225, 116, 0.6)',
                            'rgba(251, 225, 116, 0.6)',
                            'rgba(251, 225, 116, 0.6)'
                        ],
                        borderWidth: 1,
                        borderColor:'#777',
                        hoverBorderWidth: 3,
                        hoverBorderColor: '#000'
                    },
                    {
                        label: 'Healthcare',
                        data: budget_Previous_Data[2],
                        //backgroundColor:'green',
                        backgroundColor: [
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(54, 162, 235, 0.6)'
                        ],
                        borderWidth: 1,
                        borderColor: '#777',
                        hoverBorderWidth: 3,
                        hoverBorderColor: '#000'
                    },
                    {
                        label: 'Housing',
                        data: budget_Previous_Data[3],
                        //backgroundColor:'green',
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.6)',
                            'rgba(255, 99, 132, 0.6)',
                            'rgba(255, 99, 132, 0.6)',
                            'rgba(255, 99, 132, 0.6)'
                        ],
                        borderWidth: 1,
                        borderColor: '#777',
                        hoverBorderWidth: 3,
                        hoverBorderColor: '#000'
                    },
                    {
                        label: 'Personal Care',
                        data: budget_Previous_Data[4],
                        //backgroundColor:'green',
                        backgroundColor: [
                            'rgba(255, 92, 0, 0.6)',
                            'rgba(255, 92, 0, 0.6)',
                            'rgba(255, 92, 0, 0.6)',
                            'rgba(255, 92, 0, 0.6)'
                        ],
                        borderWidth: 1,
                        borderColor: '#777',
                        hoverBorderWidth: 3,
                        hoverBorderColor: '#000'
                    },
                    {
                        label: 'Utilities',
                        data: budget_Previous_Data[5],
                        //backgroundColor:'green',
                        backgroundColor: [
                            'rgba(92, 255, 110, 0.6)',
                            'rgba(92, 255, 110, 0.6)',
                            'rgba(92, 255, 110, 0.6)',
                            'rgba(92, 255, 110, 0.6)'
                        ],
                        borderWidth: 1,
                        borderColor: '#777',
                        hoverBorderWidth: 3,
                        hoverBorderColor: '#000'
                    },
                    {
                        label: 'Entertainment',
                        data: budget_Previous_Data[6],
                        //backgroundColor:'green',
                        backgroundColor: [
                            'rgba(92, 255, 255, 0.6)',
                            'rgba(92, 255, 255, 0.6)',
                            'rgba(92, 255, 255, 0.6)',
                            'rgba(92, 255, 255, 0.6)'
                        ],
                        borderWidth: 1,
                        borderColor: '#777',
                        hoverBorderWidth: 3,
                        hoverBorderColor: '#000'
                    }]

                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: 'Expenses Tracker',
                        fontSize: 25
                    },
                    legend: {
                        display: false,
                        position: 'right',
                        labels: {
                            fontColor: '#000'
                        }
                    },
                    layout: {
                        padding: {
                            left: 50,
                            right: 0,
                            bottom: 0,
                            top: 0
                        }
                    },
                    tooltips: {
                        enabled: true
                    },
       //             annotation: {
       //                 annotations: [{
       //                     type: 'line',
       //                     mode: 'horizontal',
       //                     scaleID: 'y-axis-0',
       //                     value: '183',
       //                     borderColor: 'tomato',
       //                     borderWidth: 1,
       //                     label: {
							//	enabled: false,
							//	content: 'Test label'
							//}
       //                 }]
       //             }
                },
            });
        </script>
    
</asp:Content>
