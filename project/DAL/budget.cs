﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class budget
    {
        public int bankaccnum { set; get; }
        public DateTime budget_Date { set; get; }
        public DateTime update_Detail { set; get; }
        public int update_Count { set; get; }

        public decimal consumer_Debt { set; get; }
        public decimal food_Grocery { set; get; }
        public decimal healthcare { set; get; }
        public decimal housing { set; get; }
        public decimal personal_Care { set; get; }
        public decimal utility { set; get; }

        public decimal entertainment { set; get; }

        public decimal emergency_Fund { set; get; }
        public decimal saving { set; get; }

    }
}