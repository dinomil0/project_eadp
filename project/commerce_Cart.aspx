﻿<%@ Page Title="" Language="C#" MasterPageFile="~/commerceMaster.Master" AutoEventWireup="true" CodeBehind="commerce_Cart.aspx.cs" Inherits="project.commerce_Cart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
    .ProdImg {
        width:175px;
        height:175px;
        overflow:hidden;
    }
</style>
<div id="form1">
    <section class="d-flex">
        <div class="m-3">
            <asp:Label ID="Lbl_userID" runat="server" Text="User Name" class="h3"></asp:Label>
            <h3>Cart</h3>
        </div>
        <div class="col-7">
            <asp:DataList runat="server" ID="Cart" RepeatColumns="3" CellPadding="20" >
                <ItemTemplate>
                    <div class="justify-content-center mt-5 border border-success rounded container">
                        <asp:Image ID="ProdImage" runat="server" ImageUrl='<%# Eval("ProdImage") %>' cssclass="img-thumbnail ProdImg mt-1"/>  
                        <table style="width: 100%;" class="m-2 mr-2">
                            <tr>
                                <td>Name: </td>
                                <td><asp:Label ID="Lbl_productname" runat="server" Text=""><%# Eval("ProdName") %></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Price: </td>
                                <td><asp:Label ID="Lbl_productprice" runat="server" Text="">$<%# Eval("Price") %></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Quantity: </td>
                                <td><asp:Label ID="Lbl_Buyqty" runat="server" Text=""><%# Eval("BuyQty") %></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Total: </td>
                                <td><asp:Label ID="Lbl_Total" runat="server" Text="">$<%# Eval("Total") %></asp:Label></td>
                            </tr>
                        </table>
                        <div class="d-flex justify-content-center">
                            <asp:LinkButton runat="server" class = "btn btn-outline-danger mb-1 justify-content-center" OnCommand="Remove_Click" CommandArgument = '<%# Eval("ordId") %>'>Remove</asp:LinkButton>
                            <asp:Label ID="Lbl_ProdIdInvis" runat="server" Visible="False"><%# Eval("ordId") %></asp:Label>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="border border-success justify-content-center mt-5">
            <div class="m-4">
                <h5 class="mt-4 font-weight-light">Order Summary</h5>
                <table style="width: 100%;">
                    <tr class="">
                        <td class="h5">Total: </td>
                        <td class="h5"><asp:Label ID="Lbl_Total" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr class="">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="">
                        <td><asp:Button ID="btn_checkout" runat="server" Text="Checkout" class="btn btn-outline-success" OnClick="btn_checkout_Click"/></td>
                        <td class ="d-flex"><asp:Button ID="btn_clear" runat="server" Text="Clear" class="btn btn-outline-success mx-auto my-auto" OnClick="btn_clear_Click"/></td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
    <br />
</div>
</asp:Content>
