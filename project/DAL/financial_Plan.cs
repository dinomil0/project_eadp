﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class financial_Plan
    {
        public int plan_Id { set; get; }
        public string name { set; get; }
        public int matured_Age { set; get; }
        public decimal monthly_Contribution { set; get; }
        public decimal amount { set; get; }
        public string userID { set; get; }
    }
}