﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class TransferPayYouInput : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userID = Session["custNRIC"].ToString();
            if (Session["custNRIC"] == null)
            {
                Response.Redirect("landing.aspx");
            }
            if (!IsPostBack)
            {
                // For Phone
                AccountsDAO accDAO = new AccountsDAO();
                List<Accounts> bankaccnumListphone = new List<Accounts>();
                bankaccnumListphone = accDAO.getbackaccnumbyuserID(userID);

                AccountsListDropdownPhone.Items.Clear();
                AccountsListDropdownPhone.Items.Insert(0, new ListItem("--Select--", "0"));
                AccountsListDropdownPhone.AppendDataBoundItems = true;
                AccountsListDropdownPhone.DataTextField = "bankaccname";
                AccountsListDropdownPhone.DataValueField = "bankaccnum";
                AccountsListDropdownPhone.DataSource = bankaccnumListphone;
                AccountsListDropdownPhone.DataBind();

                // For NRIC
                AccountsDAO accsDAO = new AccountsDAO();
                List<Accounts> bankaccnumListNRIC = new List<Accounts>();

                bankaccnumListNRIC = accDAO.getbackaccnumbyuserID(userID);
                AccountsListDropdownNRIC.Items.Clear();
                AccountsListDropdownNRIC.Items.Insert(0, new ListItem("--Select--", "0"));
                AccountsListDropdownNRIC.AppendDataBoundItems = true;
                AccountsListDropdownNRIC.DataTextField = "bankaccname";
                AccountsListDropdownNRIC.DataValueField = "bankaccnum";
                AccountsListDropdownNRIC.DataSource = bankaccnumListNRIC;
                AccountsListDropdownNRIC.DataBind();
            }

        }

        protected void searchButton_Click(object sender, EventArgs e)
        {

        }

        protected void cancelButtonPayYouInput_Click(object sender, EventArgs e)
        {
            Response.Redirect("bankHome.aspx");
        }

        protected void submitButtonPayYouInput_Click(object sender, EventArgs e)
        {
            Session["PayYouType"] = InputType.SelectedValue.ToString();
            // Session PhoneNum
            Session["PayYouPhonenum"] = txtPhoneNumber.Text;
            Session["PayYounamePhoneNum"] = PNNickname.Text;
            Session["PayYouRecipBankaccnumPhoneNum"] = PNBankaccnum.Text;
            Session["PayYouRecipBankaccnamePhoneNum"] = PNBankaccname.Text;
            Session["PayYouamountPhoneNum"] = txtTransferAmtPhone.Text;
            Session["PayYoubankaccPhoneNum"] = AccountsListDropdownPhone.SelectedValue;


            // Session NRIC
            Session["PayYouNRIC"] = txtNRIC.Text;
            Session["PayYounameNRIC"] = NRICNickname.Text;
            Session["PayYouRecipBankaccnumNRIC"] = NRICBankaccnum.Text;
            Session["PayYouRecipBankaccnameNRIC"] = NRICBankaccname.Text;
            Session["PayYouamountNRIC"] = txtTransferAmtNRIC.Text;
            Session["PayYoubankaccNRIC"] = AccountsListDropdownNRIC.SelectedValue.ToString();

            //Redirect To Verification
            Response.Redirect("TransferPayYouVerify.aspx");
        }

        protected void InputType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (InputType.SelectedIndex == 1)
            {
                panelPhone.Visible = true;
                panelNRIC.Visible = false;
            }
            else if (InputType.SelectedIndex == 2)
            {
                panelNRIC.Visible = true;
                panelPhone.Visible = false;
            }
            else
            {
                panelPhone.Visible = false;
                panelNRIC.Visible = false;
            }
        }

        protected void AccountsListDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void searchButtonPhoneNumber_Click(object sender, EventArgs e)
        {
            customer cusObj = new customer();
            customerDAO cusDao = new customerDAO();

            Accounts accObj = new Accounts();
            AccountsDAO accDAO = new AccountsDAO();
            List<int> bankaccnumList = new List<int>();
            List<string> bankaccnameList = new List<string>();

            string recipuserID = "";
            //Session["userID"] = "S1234567A";
            string userID = Session["custNRIC"].ToString();


            cusObj = cusDao.getNamebyphonenum(Convert.ToInt32(txtPhoneNumber.Text));

            if (cusObj.phonenum == Convert.ToInt32(txtPhoneNumber.Text))
            {
                PNNickname.Text = cusObj.name;
                recipuserID = cusObj.userID;
                bankaccnumList = accDAO.getbackaccnumbyuserIDint(recipuserID);
                bankaccnameList = accDAO.getbackaccnamebyuserIDstring(recipuserID);
                PNBankaccnum.Text = bankaccnumList[0].ToString();
                PNBankaccname.Text = bankaccnameList[0].ToString();
            }
            else
            {
                Lbl_err.Text = "Phone number is not registered";
            }
        }

        protected void searchButtonNRIC_Click(object sender, EventArgs e)
        {
            customer cusObj = new customer();
            customerDAO cusDao = new customerDAO();

            Accounts accObj = new Accounts();
            AccountsDAO accDAO = new AccountsDAO();
            List<int> bankaccnumList = new List<int>();
            List<string> bankaccnameList = new List<string>();

            //Session["userID"] = "S1234567A";
            string userID = Session["custNRIC"].ToString();
            string recipuserID = "";
            bankaccnumList = accDAO.getbackaccnumbyuserIDint(userID);

            cusObj = cusDao.getNamebynric(txtNRIC.Text);

            if (cusObj.userID == txtNRIC.Text)
            {
                NRICNickname.Text = cusObj.name;
                recipuserID = cusObj.userID;
                bankaccnumList = accDAO.getbackaccnumbyuserIDint(recipuserID);
                bankaccnameList = accDAO.getbackaccnamebyuserIDstring(recipuserID);
                NRICBankaccnum.Text = bankaccnumList[0].ToString();
                NRICBankaccname.Text = bankaccnameList[0].ToString();
            }
            else
            {
                Lbl_err.Text = "NRIC is not registered";
            }

        }

        protected void AccountsListDropdownPhone_SelectedIndexChanged(object sender, EventArgs e)
        {
            Accounts accObj = new Accounts();
            AccountsDAO accDAO = new AccountsDAO();

            accObj = accDAO.getbalanceBybankaccnum(Convert.ToInt32(AccountsListDropdownPhone.SelectedValue));

            if (accObj.bankaccnum == Convert.ToInt32(AccountsListDropdownPhone.SelectedValue))
            {
                LblSGDBalPhone.Text = "$" + accObj.balance.ToString("F");
            }
            else
            {
                Lbl_err.Text += "Account not found in user";
            }
        }
        protected void AccountsListDropdownNRIC_SelectedIndexChanged(object sender, EventArgs e)
        {
            Accounts accsObj = new Accounts();
            AccountsDAO accountDAO = new AccountsDAO();

            accsObj = accountDAO.getbalanceBybankaccnum(Convert.ToInt32(AccountsListDropdownNRIC.SelectedValue));

            if (accsObj.bankaccnum == Convert.ToInt32(AccountsListDropdownNRIC.SelectedValue))
            {
                LblSGDBalNRIC.Text = "$" + accsObj.balance.ToString("F");
            }
            else
            {
                Lbl_err.Text += "Account not found in user";
            }
        }
    }
}