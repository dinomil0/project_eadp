﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class financial_Planning_Analysis : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }

                //Retrieving Financial Data
                List<financial_Data> read_Financial_Data_List = new List<financial_Data>();
                financial_DataDAO read_Financial_DataDAO = new financial_DataDAO();
                read_Financial_Data_List = read_Financial_DataDAO.read_Financial_Data(Session["custNRIC"].ToString());
                gv_Fp_Information.DataSource = read_Financial_Data_List;
                gv_Fp_Information.DataBind();

                //Analysing Financial Data
                decimal total_Asset = 0;
                decimal total_Liability = 0;
                foreach (var data in read_Financial_Data_List)
                {
                    if (data.type == "Asset")
                    {
                        total_Asset += data.amount;
                    }
                    else if (data.type == "Liability")
                    {
                        total_Liability += data.amount;
                    }
                }
                if (total_Liability == 0)
                {
                    //lb_Fp_Information.Text = "Congratulations! You do not have any Liabilities! <br>";
                }
                else
                {
                    calculate_Liquidity(total_Asset, total_Liability);
                    //lb_Fp_Information.Text = "Coming soon to the theatres near you! <br>";
                }

                //Retrieving Plan that Customer entered
                List<object> render_plan = (List<object>)Session["financial_Plan"];
                string plan_Name = Convert.ToString(render_plan[0]);
                if (plan_Name == "Other")
                {
                    plan_Name = Convert.ToString(render_plan[1]);
                }
                int max_Age = Convert.ToInt32(render_plan[2]);
                string matured_Age = Convert.ToString(max_Age - 9) + " - " + max_Age;
                string duration = Convert.ToString(render_plan[3]);
                string amount = Convert.ToString(render_plan[4]);

                lb_Fp_Plan.Text = "Plan: " + plan_Name
                                + "<br> Age @ Plan Maturity: " + matured_Age
                                + "<br> Remaining Time: " + duration
                                + "<br> Minimum Amount: S$" + amount;

                //Retrieving Other Data
                lb_Fp_Remark.Text = null;
                TransactionHistoryDAO read_TransactionDAO = new TransactionHistoryDAO();
                financial_PlanDAO read_Financial_PlanDAO = new financial_PlanDAO();

                //Retrieving Salary
                decimal salary = read_TransactionDAO.read_Salary(Convert.ToString(Session["custNRIC"]));
                lb_Fp_Remark.Text += "Current Salary: S$" + Convert.ToString(salary) + "<br>";

                //Retrieving Average Expense
                decimal average_Expense = read_TransactionDAO.read_Average_Expense_Per_Month(Convert.ToString(Session["custNRIC"]));
                lb_Fp_Remark.Text += "Average Expense for the past 3 Months: S$" + Convert.ToString(average_Expense) + "<br>";

                //Retrieving Previous Financial Plans
                List<financial_Plan> read_Financial_Plan_List = new List<financial_Plan>();
                read_Financial_Plan_List = read_Financial_PlanDAO.read_Financial_Plan(Convert.ToString(Session["custNRIC"]));
                if (read_Financial_Plan_List != null)
                {
                    foreach (financial_Plan each_Financial_Plan in read_Financial_Plan_List)
                    {
                        lb_Fp_Remark.Text += Convert.ToString(each_Financial_Plan.name) + " Monthly Contribution: S$" + Convert.ToString(each_Financial_Plan.monthly_Contribution) + "<br>";
                    }
                }
                else
                {
                    //Exception
                }

                //Evaluating Options
                List<financial_Plan_Analysis> read_Financial_Plan_Before_Analysis_List = new List<financial_Plan_Analysis>();
                List<financial_Plan_Analysis> read_Financial_Plan_After_Analysis_List = new List<financial_Plan_Analysis>();
                List<financial_Plan_Analysis> read_Financial_Plan_Reject_List = new List<financial_Plan_Analysis>();

                List<object> read_Data = (List<object>)Session["financial_Plan"];
                int customer_Age = Convert.ToInt32(Session["custAge"]);
                string name = Convert.ToString(read_Data[0]);
                if (name == "Other")
                {
                    name = Convert.ToString(read_Data[1]);
                }
                int maximum_Age_Range = Convert.ToInt32(read_Data[2]);
                decimal minimum_Amount = Convert.ToDecimal(read_Data[4]);
                int age_Range_Difference = maximum_Age_Range - 9;
                if (age_Range_Difference < customer_Age)
                {
                    age_Range_Difference = customer_Age + 1;
                }

                for (int count = (age_Range_Difference); count <= maximum_Age_Range; count++)
                {
                    financial_Plan_Analysis read_Financial_Plan_Analysis = new financial_Plan_Analysis();
                    int age_Difference = count - customer_Age;
                    read_Financial_Plan_Analysis.name = name;
                    read_Financial_Plan_Analysis.matured_Age = count;
                    read_Financial_Plan_Analysis.duration = Convert.ToString(age_Difference) + " years";
                    read_Financial_Plan_Analysis.monthly_Contribution = Math.Round(((Convert.ToDecimal(minimum_Amount) / age_Difference) / 12), 2);
                    read_Financial_Plan_Before_Analysis_List.Add(read_Financial_Plan_Analysis);
                }

                decimal difference_Asset_Liability = total_Asset - total_Liability;
                foreach (financial_Plan_Analysis each_Option in read_Financial_Plan_Before_Analysis_List)
                {
                    //Total Net Salary upon Plan Maturity
                    decimal net_Salary = (salary - average_Expense) * (each_Option.matured_Age - customer_Age);

                    //Total Additional Income from Other Plans
                    decimal additional_Plan_Income = 0;
                    foreach (financial_Plan each_Financial_Plan in read_Financial_Plan_List)
                    {
                        if (each_Financial_Plan.matured_Age < each_Option.matured_Age)
                        {
                            additional_Plan_Income += (each_Financial_Plan.monthly_Contribution) * (each_Option.matured_Age - each_Financial_Plan.matured_Age);
                        }
                    }

                    //Total Additional Income from Loans
                    decimal additional_Loan_Income = 0;








                    //Total Average Savings Per Month
                    decimal average_Savings_Per_Month = Math.Round(((difference_Asset_Liability + net_Salary + additional_Plan_Income + additional_Loan_Income) / (each_Option.matured_Age - customer_Age) / 12), 2);

                    //Updating read_Financial_Plan_After_Analyis_List
                    if (average_Savings_Per_Month >= each_Option.monthly_Contribution)
                    {
                        read_Financial_Plan_After_Analysis_List.Add(each_Option);
                    }
                    else
                    {
                        read_Financial_Plan_Reject_List.Add(each_Option);
                    }
                }

                //Additional Comments
                lb_Fp_Remark.Text += "<br> Given the information provided, <br>" +
                                     "We have evaluated the following options in the table below. <br>" +
                                     "The options are being ranked according to when you would achieve your plan the <strong>Earliest</strong>! <br>" +
                                     "If you aspire to take Option " + Convert.ToString((read_Financial_Plan_After_Analysis_List.Count) + 1) + " onwards, make sure to get somebody to fund the excess amount!";

                //Merging read_Financial_Plan_Reject_List into read_Financial_Plan_After_Analysis_List
                read_Financial_Plan_Reject_List.Reverse();
                foreach (financial_Plan_Analysis each_Reject in read_Financial_Plan_Reject_List)
                {
                    read_Financial_Plan_After_Analysis_List.Add(each_Reject);
                }
                int rank = 1;
                foreach (financial_Plan_Analysis each_Finalised_Option in read_Financial_Plan_After_Analysis_List)
                {
                    each_Finalised_Option.count = rank;
                    rank += 1;
                }

                gv_Fp_Option.DataSource = read_Financial_Plan_After_Analysis_List;
                gv_Fp_Option.DataBind();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        protected void gv_Fp_Information_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //Disabling Buttons on Gridview
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string name = DataBinder.Eval(e.Row.DataItem, "name").ToString();
                    if (name == "Cash at Bank" || name == "Loan Instalment")
                    {
                        e.Row.Cells[4].Enabled = false;
                        e.Row.Cells[5].Enabled = false;
                    }
                }
            }
            catch
            {

            }
        }

        protected void btn_Fp_Insert_Data_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                if (validate_Insert())
                {
                    //Inserting Financial Data
                    financial_DataDAO insert_Financial_DataDAO = new financial_DataDAO();
                    insert_Financial_DataDAO.insert_Financial_Data(Session["custNRIC"].ToString(), tb_Fp_Insert_Name.Text, ddl_Fp_Insert_Type.SelectedValue, "Yes", Convert.ToDecimal(tb_Fp_Insert_Amount.Text));
                    Response.Redirect(Request.RawUrl);
                }
            }
            catch
            {
                //Exeception
            }
        }

        protected void btn_Fp_Read_Update_Click(object sender, EventArgs e)
        {
            try
            {
                //Initiating Update Modal
                int rowIndex = Convert.ToInt32(((sender as LinkButton).NamingContainer as GridViewRow).RowIndex);
                GridViewRow row = gv_Fp_Information.Rows[rowIndex];

                tb_Fp_Update_Name.Text = (row.FindControl("lb_Fp_Read_Name") as Label).Text;
                tb_Fp_Update_Type.Text = (row.FindControl("lb_Fp_Read_Type") as Label).Text; ;
                tb_Fp_Update_Amount.Text = (row.FindControl("lb_Fp_Read_Amount") as Label).Text;
                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "initiate_Update_Modal();", true);
            }
            catch
            {
                //Exception
            }
        }

        protected void btn_Fp_Update_Data_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                if (validate_Update())
                {
                    //Updating Financial Data
                    financial_DataDAO update_Financial_DataDAO = new financial_DataDAO();
                    update_Financial_DataDAO.update_Financial_Data(Session["custNRIC"].ToString(), tb_Fp_Update_Name.Text, Convert.ToDecimal(tb_Fp_Update_Amount.Text));
                    Response.Redirect(Request.RawUrl);
                }
            }
            catch
            {
                //Exception
            }
        }

        protected void btn_Fp_Read_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                //Initiating Delete Modal
                int rowIndex = Convert.ToInt32(((sender as LinkButton).NamingContainer as GridViewRow).RowIndex);
                GridViewRow row = gv_Fp_Information.Rows[rowIndex];

                tb_Fp_Delete_Name.Text = (row.FindControl("lb_Fp_Read_Name") as Label).Text;
                tb_Fp_Delete_Type.Text = (row.FindControl("lb_Fp_Read_Type") as Label).Text; ;
                tb_Fp_Delete_Amount.Text = (row.FindControl("lb_Fp_Read_Amount") as Label).Text;
                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "initiate_Delete_Modal();", true);
            }
            catch
            {
                //Exception
            }
        }

        protected void btn_Fp_Delete_Data_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                //Deleting Financial Data
                financial_DataDAO delete_Financial_DataDAO = new financial_DataDAO();
                delete_Financial_DataDAO.delete_Financial_Data(Session["custNRIC"].ToString(), tb_Fp_Delete_Name.Text);
                Response.Redirect(Request.RawUrl);
            }
            catch
            {
                //Exception
            }
        }

        protected void btn_Fp_Read_Insert_Plan_Click(object sender, EventArgs e)
        {
            try
            {
                //Initiating Create Financial Plan Modal
                int rowIndex = Convert.ToInt32(((sender as LinkButton).NamingContainer as GridViewRow).RowIndex);
                GridViewRow row = gv_Fp_Option.Rows[rowIndex];

                tb_Fp_Insert_Plan_Rank.Text = (row.FindControl("lb_Fp_Read_Plan_Count") as Label).Text;
                tb_Fp_Insert_Plan_Name.Text = (row.FindControl("lb_Fp_Read_Plan_Name") as Label).Text; ;
                tb_Fp_Insert_Plan_Age.Text = (row.FindControl("lb_Fp_Read_Plan_Age") as Label).Text;
                tb_Fp_Insert_Plan_Monthly_Contribution.Text = (row.FindControl("lb_Fp_Read_Plan_Monthly_Contribution") as Label).Text;
                tb_Fp_Insert_Plan_Amount.Text = Convert.ToString(((List<object>)Session["financial_Plan"])[4]);
                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "initiate_Insert_Plan_Modal();", true);
            }
            catch
            {
                //Exception
            }
        }

        protected void btn_Fp_Insert_Plan_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                //Inserting Financial Plan
                financial_PlanDAO insert_Financial_PlanDAO = new financial_PlanDAO();
                insert_Financial_PlanDAO.insert_Financial_Plan(tb_Fp_Insert_Plan_Name.Text, Convert.ToInt32(tb_Fp_Insert_Plan_Age.Text), Convert.ToDecimal(tb_Fp_Insert_Plan_Monthly_Contribution.Text), Convert.ToDecimal(tb_Fp_Insert_Plan_Amount.Text), Convert.ToString(Session["custNRIC"]));
                Session.Remove("financial_Plan");
                //Updating Financial Analysis
                financial_AnalysisDAO update_Financial_Analysis = new financial_AnalysisDAO();
                update_Financial_Analysis.update_Financial_Analysis(Convert.ToDecimal(Session["working_Capital"]), Convert.ToDecimal(Session["working_Capital_Ratio"]), Convert.ToString(Session["custNRIC"]));
                Session.Remove("working_Capital");
                Session.Remove("working_Capital_Ratio");
                Response.Redirect("financial_Planning_Overview.aspx");
            }
            catch
            {
                //Exception
            }
        }

        protected void calculate_Liquidity(decimal total_Asset, decimal total_Liability)
        {
            try
            {
                lb_Fp_Information.Text = null;
                //Retrieving Past Financial Ratios
                Dictionary<string, financial_Analysis> read_Financial_Analysis_Dict = new Dictionary<string, financial_Analysis>();
                financial_AnalysisDAO read_Financial_Analysis_DAO = new financial_AnalysisDAO();
                read_Financial_Analysis_Dict = read_Financial_Analysis_DAO.read_Financial_Analysis(Session["custNRIC"].ToString());
                string past_Working_Capital_Update_Detail = Convert.ToString(read_Financial_Analysis_Dict["Working Capital"].update_Detail);
                decimal past_Working_Capital_Amount = Convert.ToDecimal(read_Financial_Analysis_Dict["Working Capital"].amount);
                string past_Working_Capital_Ratio_Update_Detail = Convert.ToString(read_Financial_Analysis_Dict["Working Capital Ratio"].update_Detail);
                decimal past_Working_Capital_Ratio_Amount = Convert.ToDecimal(read_Financial_Analysis_Dict["Working Capital Ratio"].amount);

                //Calculating & Analysing Current Working Capital
                decimal current_Working_Capital_Amount = Math.Round((total_Asset - total_Liability), 2);
                Session["working_Capital"] = current_Working_Capital_Amount;
                decimal working_Capital_Difference;
                string working_Capital_Difference_Text;
                if (current_Working_Capital_Amount < past_Working_Capital_Amount)
                {
                    working_Capital_Difference = past_Working_Capital_Amount - current_Working_Capital_Amount;
                    working_Capital_Difference_Text = "<strong><font color='#D9534F'>LOWER</font></strong> than the last time on (" + past_Working_Capital_Update_Detail + ") by S$" + working_Capital_Difference.ToString();
                }
                else if (current_Working_Capital_Amount == past_Working_Capital_Amount)
                {
                    working_Capital_Difference_Text = "<strong><font color='#0275D8'>EQUAL TO</font></strong> the last time on (" + past_Working_Capital_Update_Detail + ")";
                }
                else
                {
                    working_Capital_Difference = current_Working_Capital_Amount - past_Working_Capital_Amount;
                    working_Capital_Difference_Text = "<strong><font color='#5CB85C'>HIGHER</font></strong> than the last time on (" + past_Working_Capital_Update_Detail + ") by S$" + working_Capital_Difference.ToString();
                }

                lb_Fp_Information.Text += "Your Current Working Capital is S$" + current_Working_Capital_Amount.ToString() + "<br> This is " + working_Capital_Difference_Text + " <br><br>";

                //Calculating & Analysing Working Capital Ratio
                decimal current_Working_Capital_Ratio_Amount = Math.Round((total_Asset / total_Liability), 2);
                Session["working_Capital_Ratio"] = current_Working_Capital_Ratio_Amount;
                decimal working_Capital_Ratio_Difference;
                string working_Capital_Ratio_Difference_Text;
                if (current_Working_Capital_Ratio_Amount < past_Working_Capital_Ratio_Amount)
                {
                    working_Capital_Ratio_Difference = past_Working_Capital_Ratio_Amount - current_Working_Capital_Ratio_Amount;
                    working_Capital_Ratio_Difference_Text = "<strong><font color='#D9534F'>LOWER</font></strong> than the last time on (" + past_Working_Capital_Ratio_Update_Detail + ")  by " + working_Capital_Ratio_Difference.ToString();
                    if (current_Working_Capital_Ratio_Amount < 1)
                    {
                        lb_Fp_Information_Summary.Text += "You have <strong><font color='#D9534F'>INSUFFICIENT</font></strong> Assets to pay for your Liabilities! <br> For every S$1 of Liabilities you incur, you only have S$" + current_Working_Capital_Ratio_Amount.ToString() + " of Assets.";
                    }
                    else
                    {
                        lb_Fp_Information_Summary.Text += "You have <strong><font color='#5CB85C'>SUFFICIENT</font></strong> Assets to pay for your Liabilities! <br> For every S$1 of Liabilities you incur, you have S$" + current_Working_Capital_Ratio_Amount.ToString() + " of Assets.";
                    }
                }
                else if (current_Working_Capital_Ratio_Amount == past_Working_Capital_Ratio_Amount)
                {
                    working_Capital_Ratio_Difference_Text = "<strong><font color='#0275D8'>EQUAL TO</font></strong> the last time on (" + past_Working_Capital_Ratio_Update_Detail + ")";
                    if (current_Working_Capital_Ratio_Amount < 1)
                    {
                        lb_Fp_Information_Summary.Text += "You have <strong><font color='#D9534F'>INSUFFICIENT</font></strong> Assets to pay for your Liabilities! <br> For every S$1 of Liabilities you incur, you only have S$" + current_Working_Capital_Ratio_Amount.ToString() + " of Assets.";
                    }
                    else
                    {
                        lb_Fp_Information_Summary.Text += "You have <strong><font color='#5CB85C'>SUFFICIENT</font></strong> Assets to pay for your Liabilities! <br> For every S$1 of Liabilities you incur, you have S$" + current_Working_Capital_Ratio_Amount.ToString() + " of Assets.";
                    }
                }
                else
                {
                    working_Capital_Ratio_Difference = current_Working_Capital_Ratio_Amount - past_Working_Capital_Ratio_Amount;
                    working_Capital_Ratio_Difference_Text = "<strong><font color='#5CB85C'>HIGHER</font></strong> than the last time on (" + past_Working_Capital_Ratio_Update_Detail + ") by " + working_Capital_Ratio_Difference.ToString();
                    if (current_Working_Capital_Ratio_Amount < 1)
                    {
                        lb_Fp_Information_Summary.Text += "You have <strong><font color='#D9534F'>INSUFFICIENT</font></strong> Assets to pay for your Liabilities! <br> For every S$1 of Liabilities you incur, you only have S$" + current_Working_Capital_Ratio_Amount.ToString() + " of Assets.";
                    }
                    else
                    {
                        lb_Fp_Information_Summary.Text += "You have <strong><font color='#5CB85C'>SUFFICIENT</font></strong> Assets to pay for your Liabilities! <br> For every S$1 of Liabilities you incur, you have S$" + current_Working_Capital_Ratio_Amount.ToString() + " of Assets.";
                    }
                }

                lb_Fp_Information.Text += "Your Current Working Capital Ratio is " + current_Working_Capital_Ratio_Amount.ToString() + " : 1 <br> This is " + working_Capital_Ratio_Difference_Text + " <br><br>";
            }
            catch
            {
                //Exception
            }     
        }

        protected Boolean validate_Insert()
        {
            decimal amount;
            try
            {
                lb_Fp_Error_Title.Text = null;
                lb_Fp_Insert_Error.Text = null;
                if (String.IsNullOrEmpty(tb_Fp_Insert_Name.Text))
                {
                    lb_Fp_Insert_Error.Text += "Please Enter the Name of your Data! <br>";
                }
                if (ddl_Fp_Insert_Type.SelectedValue == "--Select--")
                {
                    lb_Fp_Insert_Error.Text += "Please Select a Valid Data Type! <br>";
                }
                if (!decimal.TryParse(tb_Fp_Insert_Amount.Text, out amount))
                {
                    lb_Fp_Insert_Error.Text += "Please Enter a Valid Amount! <br>";
                }

                if (String.IsNullOrEmpty(lb_Fp_Insert_Error.Text))
                {
                    //lb_Fp_Error_Title.Text = "Data Successfully Inserted!";
                    return true;
                }
                else
                {
                    lb_Fp_Error_Title.Text = "Adding of Data Failed! Please Re-Evaluate!";
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        protected Boolean validate_Update()
        {
            decimal amount;
            try
            {
                lb_Fp_Error_Title.Text = null;
                lb_Fp_Update_Error.Text = null;
                if (!decimal.TryParse(tb_Fp_Update_Amount.Text, out amount))
                {
                    lb_Fp_Update_Error.Text += "Please Enter a Valid Amount! <br>";
                }

                if (String.IsNullOrEmpty(lb_Fp_Update_Error.Text))
                {
                    //lb_Fp_Error_Title.Text = "Data Successfully Updated!";
                    return true;
                }
                else
                {
                    lb_Fp_Error_Title.Text = "Updating of Data Failed! Please Re-Evaluate!";
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        protected void btn_Fp_Back_Click(object sender, EventArgs e)
        {
            Response.Redirect("financial_Planning_New.aspx");
        }
    }
}