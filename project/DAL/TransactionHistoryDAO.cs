﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class TransactionHistoryDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

        public decimal read_Average_Expense_Per_Month(string userID)
        {
            decimal average_Expense = 0;
            try
            {
                //Getting Total Expenses for past months (Maximum 3)
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                SqlDataAdapter da;
                StringBuilder sqlString = new StringBuilder();
                sqlString.AppendLine("SELECT ROUND((SUM(TH.AMOUNT) / COUNT(DISTINCT MONTH(DATE))) , 2) FROM TRANSACTIONHISTORY TH");
                sqlString.AppendLine("INNER JOIN ACCOUNTS A ON TH.BANKACCNUM = A.BANKACCNUM");
                sqlString.AppendLine("INNER JOIN CUSTOMER C ON A.USERID = C.USERID");
                sqlString.AppendLine("WHERE TH.MERCHANTCODE NOT LIKE 9999");
                sqlString.AppendLine("AND MONTH(TH.DATE) BETWEEN MONTH(GETDATE())-3 AND MONTH(GETDATE())-1");
                sqlString.AppendLine("AND C.USERID = @PARAUSERID");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlString.ToString(), sqlConnection);
                da.SelectCommand.Parameters.AddWithValue("@PARAUSERID", userID);
                da.Fill(ds, "average_Expense_Table");
                int rec_cnt = ds.Tables["average_Expense_Table"].Rows.Count;
                if (rec_cnt == 0)
                {
                    return 0;
                }
                else
                {
                    foreach (DataRow row in ds.Tables["average_Expense_table"].Rows)
                    {
                        average_Expense = Math.Round((Convert.ToDecimal(row[0])), 2);
                    }
                }
            }
            catch
            {
                //Exception
            }
            return average_Expense;
        }

        public decimal read_Salary(string userID)
        {
            decimal salary = 0;
            try
            {
                // Retrieving Salary
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                SqlDataAdapter da;
                StringBuilder sqlString = new StringBuilder();
                sqlString.AppendLine("SELECT TH.AMOUNT FROM TRANSACTIONHISTORY TH");
                sqlString.AppendLine("INNER JOIN ACCOUNTS A ON TH.BANKACCNUM = A.BANKACCNUM");
                sqlString.AppendLine("INNER JOIN CUSTOMER C ON C.USERID = A.USERID");
                sqlString.AppendLine("WHERE TH.MERCHANTCODE = 9999");
                sqlString.AppendLine("AND MONTH(TH.DATE) = MONTH(GETDATE())");
                sqlString.AppendLine("AND C.USERID = @PARAUSERID");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlString.ToString(), sqlConnection);
                da.SelectCommand.Parameters.AddWithValue("@PARAUSERID", userID);
                da.Fill(ds, "total_Expense_Table");
                int rec_cnt = ds.Tables["total_Expense_Table"].Rows.Count;
                if (rec_cnt == 0)
                {
                    //Exception
                }
                else
                {
                    foreach (DataRow row in ds.Tables["total_Expense_table"].Rows)
                    {
                        salary = Convert.ToDecimal(row[0]);
                    }
                }
            }
            catch
            {
                //Exception
            }
            return salary;
        }
        public Dictionary<string, Dictionary<string, decimal>> read_Total_Per_Category(int bankaccnum)
        {
            Dictionary<string, List<int>> merchant_Code = new Dictionary<string, List<int>>
            {
                { "consumer_Debt", new List<int> { 4112, 4121, 4131, 8220 } },
                { "food_Grocery", new List<int> {5411, 5441, 5462, 5812, 5814 } },
                { "healthcare", new List<int> { 8021, 8044, 8062 } },
                { "housing", new List<int> { 1711, 1731 } },
                { "personal_Care", new List<int> { 5137, 5661, 5691 } },
                { "utility", new List<int> { 4814, 4899, 4900 } },
                { "entertainment", new List<int> { 3082, 3098, 7933, 7998 } },
                { "salary", new List<int> { 9999 } }
            };

            Dictionary<string, Dictionary<string, decimal>> expense_Per_Month_Dict = new Dictionary<string, Dictionary<string, decimal>>
            {
                {DateTime.Today.AddMonths(-3).ToString("MMM"), new Dictionary<string, decimal>
                    {
                        {"consumer_Debt", 0 },
                        {"food_Grocery", 0 },
                        {"healthcare", 0 },
                        {"housing", 0 },
                        {"personal_Care", 0 },
                        {"utility", 0 },
                        {"entertainment", 0 },
                        {"salary", 0 }
                    }
                },
                {DateTime.Today.AddMonths(-2).ToString("MMM"), new Dictionary<string, decimal>
                    {
                        {"consumer_Debt", 0 },
                        {"food_Grocery", 0 },
                        {"healthcare", 0 },
                        {"housing", 0 },
                        {"personal_Care", 0 },
                        {"utility", 0 },
                        {"entertainment", 0 },
                        {"salary", 0 }
                    }
                },
                {DateTime.Today.AddMonths(-1).ToString("MMM"), new Dictionary<string, decimal>
                    {
                        {"consumer_Debt", 0 },
                        {"food_Grocery", 0 },
                        {"healthcare", 0 },
                        {"housing", 0 },
                        {"personal_Care", 0 },
                        {"utility", 0 },
                        {"entertainment", 0 },
                        {"salary", 0 }
                    }
                },
                {DateTime.Today.ToString("MMM"), new Dictionary<string, decimal>
                    {
                        {"consumer_Debt", 0 },
                        {"food_Grocery", 0 },
                        {"healthcare", 0 },
                        {"housing", 0 },
                        {"personal_Care", 0 },
                        {"utility", 0 },
                        {"entertainment", 0 },
                        {"salary", 0 }
                    }
                },
            };


            //Getting Total Expenses 

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            SqlDataAdapter da;

            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("SELECT * FROM TRANSACTIONHISTORY");
            sqlString.AppendLine("WHERE BANKACCNUM = @PARABANKACCNUM");
            sqlString.AppendLine("AND DATE > DATEADD(MONTH, -4, GETDATE())");

            SqlConnection sqlConnection = new SqlConnection(DBConnect);

            da = new SqlDataAdapter(sqlString.ToString(), sqlConnection);
            da.SelectCommand.Parameters.AddWithValue("PARABANKACCNUM", bankaccnum);

            da.Fill(ds, "total_Expense_Table");

            int rec_cnt = ds.Tables["total_Expense_Table"].Rows.Count;
            if (rec_cnt == 0)
            {

            }
            else
            {
                foreach (DataRow row in ds.Tables["total_Expense_table"].Rows)
                {
                    int merchant_Code_Success;
                    string month = (Convert.ToDateTime(row["date"])).ToString("MMM");
                    if (int.TryParse(row["merchantCode"].ToString(), out merchant_Code_Success))
                    {
                        int row_Code = Convert.ToInt32(row["merchantCode"]);
                        Dictionary<string, decimal> respective_Month = expense_Per_Month_Dict[month];
                        foreach (KeyValuePair<string, List<int>> record in merchant_Code)
                        {
                            if (record.Value.Contains(row_Code))
                            {
                                decimal new_Temporary_Figure = respective_Month[record.Key] + Convert.ToDecimal(row["amount"]);
                                respective_Month[record.Key] = new_Temporary_Figure;
                            }
                            else
                            {
                                //Exception
                            }
                        }
                        expense_Per_Month_Dict[month] = respective_Month;
                    }
                    else
                    {

                    }
                }
            }
            return expense_Per_Month_Dict;
        }
        public List<TransactionHistory> getTDbyuserIDList(string userID, int bankaccnum)
        //public List<TransactionHistory> getTDbyuserIDList(string userID)
        {
            List<TransactionHistory> THList = new List<TransactionHistory>();
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();
            StringBuilder sqlStr = new StringBuilder();

            sqlStr.AppendLine("SELECT * From TransactionHistory where userID = @parauserID and bankaccnum = @parabankaccnum");
            //sqlStr.AppendLine("SELECT * From TransactionHistory where userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            SqlDataAdapter da = new SqlDataAdapter(sqlStr.ToString(), myConn);

            da.SelectCommand.Parameters.AddWithValue("@parauserID", userID);
            da.SelectCommand.Parameters.AddWithValue("@parabankaccnum", bankaccnum);
            da.Fill(ds, "TransactionHistory");
            int rec_cnt = ds.Tables["TransactionHistory"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["TransactionHistory"].Rows)
                {
                    TransactionHistory obj = new TransactionHistory();
                    obj.transactionID = Convert.ToInt32(row["transactionID"]);
                    obj.userID = row["userID"].ToString();
                    obj.date = Convert.ToDateTime(row["date"]);
                    obj.description = row["description"].ToString();
                    obj.type = row["type"].ToString();
                    obj.amount = Convert.ToDouble(row["amount"]);
                    obj.status = row["status"].ToString();
                    obj.userID = row["userID"].ToString();
                    obj.address = row["address"].ToString();
                    //obj.frombankaccnum = Convert.ToInt32(row["frombankaccnum"]);
                    //obj.tobankaccnum = Convert.ToInt32(row["tobankaccnum"]);
                    obj.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
                    obj.sendto = row["sendto"].ToString();
                    obj.sendusing = row["sendusing"].ToString();

                    THList.Add(obj);
                }
            }
            else
            {
                THList = null;
            }

            return THList;

        }
        public List<double> getbargraphdata(string userID)
        {
            int todayDate = Convert.ToInt32(DateTime.Today.ToString("dd")) - 1;
            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection myConn = new SqlConnection(DBConnect);
            DataSet ds = new DataSet();

            List<double> DepositWithdrawalList = new List<double>();

            double deposit = 0;
            double withdrawal = 0;
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT type,amount FROM TransactionHistory");
            sqlCommand.AppendLine("where userID = @parauserID");
            sqlCommand.AppendLine("and date >= dateadd(DAY, -@paradate, getdate())");


            TransactionHistory obj = new TransactionHistory();
            SqlDataAdapter da;
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            da.SelectCommand.Parameters.AddWithValue("paradate", todayDate);
            // fill dataset
            da.Fill(ds, "BarGraphTable");
            int rec_cnt = ds.Tables["BarGraphTable"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["BarGraphTable"].Rows)
                {
                    obj.type = row["type"].ToString();
                    obj.amount = Convert.ToDouble(row["amount"]);
                    if (obj.type.ToString() == "Deposit")
                    {
                        deposit += obj.amount;
                    }
                    else if (obj.type.ToString() == "Withdrawal")
                    {
                        withdrawal += obj.amount;
                    }
                }
                DepositWithdrawalList.Add(deposit);
                DepositWithdrawalList.Add(withdrawal);
            }
            else
            {
                DepositWithdrawalList = null;
            }

            return DepositWithdrawalList;
        }
        public TransactionHistory getTDbyuserID(string userID)
        {
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();

            StringBuilder sqlStr = new StringBuilder();
            sqlStr.AppendLine("SELECT * From TransactionHistory where userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            SqlDataAdapter da = new SqlDataAdapter(sqlStr.ToString(), myConn);

            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

            da.Fill(ds, "TransactionHistory");

            TransactionHistory obj = new TransactionHistory();
            int rec_cnt = ds.Tables["TransactionHistory"].Rows.Count;
            if (rec_cnt > 0)
            {
                DataRow row = ds.Tables["TransactionHistory"].Rows[0];
                obj.transactionID = Convert.ToInt32(row["transactionID"]);
                obj.date = Convert.ToDateTime(row["date"]);
                obj.description = row["description"].ToString();
                obj.type = row["type"].ToString();
                obj.amount = Convert.ToDouble(row["amount"]);
                obj.status = row["status"].ToString();
                obj.userID = row["userID"].ToString();
                obj.address = row["address"].ToString();
                obj.frombankaccnum = Convert.ToInt32(row["frombankaccnum"]);
                obj.sendto = row["sendto"].ToString();
                obj.sendusing = row["sendusing"].ToString();
                obj.tobankaccnum = Convert.ToInt32(row["tobankaccnum"]);
                obj.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
            }
            else
            {
                obj = null;
            }

            return obj;
        }
        public TransactionHistory getTDbyUserTransactionID(int transactionID)
        {
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();

            StringBuilder sqlStr = new StringBuilder();
            sqlStr.AppendLine("SELECT * From TransactionHistory where transactionID = @paratransactionID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            SqlDataAdapter da = new SqlDataAdapter(sqlStr.ToString(), myConn);

            da.SelectCommand.Parameters.AddWithValue("paratransactionID", transactionID);

            da.Fill(ds, "TransactionHistory");

            TransactionHistory obj = new TransactionHistory();
            int rec_cnt = ds.Tables["TransactionHistory"].Rows.Count;
            if (rec_cnt > 0)
            {
                DataRow row = ds.Tables["TransactionHistory"].Rows[0];
                obj.transactionID = Convert.ToInt32(row["transactionID"]);
                obj.date = Convert.ToDateTime(row["date"]);
                obj.description = row["description"].ToString();
                obj.type = row["type"].ToString();
                obj.amount = Convert.ToDouble(row["amount"]);
                obj.status = row["status"].ToString();
                obj.userID = row["userID"].ToString();
                obj.address = row["address"].ToString();
                obj.frombankaccnum = Convert.ToInt32(row["frombankaccnum"]);
                obj.sendto = row["sendto"].ToString();
                obj.sendusing = row["sendusing"].ToString();
                obj.tobankaccnum = Convert.ToInt32(row["tobankaccnum"]);
                obj.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
            }
            else
            {
                obj = null;
            }

            return obj;
        }
        public TransactionHistory getTDbyUserTransactionIDmoreinfo(int transactionID)
        {
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();

            StringBuilder sqlStr = new StringBuilder();
            sqlStr.AppendLine("SELECT * From TransactionHistory where transactionID = @paratransactionID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            SqlDataAdapter da = new SqlDataAdapter(sqlStr.ToString(), myConn);

            da.SelectCommand.Parameters.AddWithValue("paratransactionID", transactionID);

            da.Fill(ds, "TransactionHistory");

            TransactionHistory obj = new TransactionHistory();
            int rec_cnt = ds.Tables["TransactionHistory"].Rows.Count;
            if (rec_cnt > 0)
            {
                DataRow row = ds.Tables["TransactionHistory"].Rows[0];
                obj.transactionID = Convert.ToInt32(row["transactionID"]);
                obj.date = Convert.ToDateTime(row["date"]);
                obj.description = row["description"].ToString();
                obj.type = row["type"].ToString();
                obj.amount = Convert.ToDouble(row["amount"]);
                obj.status = row["status"].ToString();
                obj.userID = row["userID"].ToString();
                obj.address = row["address"].ToString();
                obj.frombankaccnum = Convert.ToInt32(row["frombankaccnum"]);
                obj.sendto = row["sendto"].ToString();
                obj.sendusing = row["sendusing"].ToString();
                obj.tobankaccnum = Convert.ToInt32(row["tobankaccnum"]);
                obj.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
            }
            else
            {
                obj = null;
            }

            return obj;
        }
        public int InsertTransactionsWithdrawal(DateTime date, string description, string type, double amount, string status, string userID, string sendto, int frombankaccnum, string sendusing, int tobankaccnum, int bankaccnum)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("INSERT INTO TransactionHistory (date, description, type, amount, status, userID, sendto, frombankaccnum, sendusing, tobankaccnum, bankaccnum)");
            sqlStr.AppendLine("VALUES (@paradate,@paradescription,@paratype,@paraamount,@parastatus,@parauserID,@parasendto,@parafrombankaccnum,@parasendusing,@paratobankaccnum,@parabankaccnum)");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@paradate", date);
            sqlCmd.Parameters.AddWithValue("@paradescription", description);
            sqlCmd.Parameters.AddWithValue("@paratype", type);
            sqlCmd.Parameters.AddWithValue("@paraamount", amount);
            sqlCmd.Parameters.AddWithValue("@parastatus", status);
            sqlCmd.Parameters.AddWithValue("@parauserID", userID);
            sqlCmd.Parameters.AddWithValue("@parasendto", sendto);
            sqlCmd.Parameters.AddWithValue("@parafrombankaccnum", frombankaccnum);
            sqlCmd.Parameters.AddWithValue("@parasendusing", sendusing);
            sqlCmd.Parameters.AddWithValue("@paratobankaccnum", tobankaccnum);
            sqlCmd.Parameters.AddWithValue("@parabankaccnum", bankaccnum);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();

            return result;

        }
        public int InsertTransactionsNormal(DateTime date, string description, string type, double amount, string status, string userID, int frombankaccnum, int bankaccnum)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("INSERT INTO TransactionHistory (date, description, type, amount, status, userID, frombankaccnum, bankaccnum)");
            sqlStr.AppendLine("VALUES (@paradate,@paradescription,@paratype,@paraamount,@parastatus,@parauserID,@parafrombankaccnum,@parabankaccnum)");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@paradate", date);
            sqlCmd.Parameters.AddWithValue("@paradescription", description);
            sqlCmd.Parameters.AddWithValue("@paratype", type);
            sqlCmd.Parameters.AddWithValue("@paraamount", amount);
            sqlCmd.Parameters.AddWithValue("@parastatus", status);
            sqlCmd.Parameters.AddWithValue("@parauserID", userID);
            sqlCmd.Parameters.AddWithValue("@parafrombankaccnum", frombankaccnum);
            sqlCmd.Parameters.AddWithValue("@parabankaccnum", bankaccnum);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();

            return result;

        }
        public int InsertTransactionsDeposit(DateTime date, string description, string type, double amount, string status, string userID, int frombankaccnum, int tobankaccnum, int bankaccnum)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("INSERT INTO TransactionHistory (date, description, type, amount, status, userID, frombankaccnum, bankaccnum)");
            sqlStr.AppendLine("VALUES (@paradate,@paradescription,@paratype,@paraamount,@parastatus,@parauserID,@parafrombankaccnum, @parabankaccnum)");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@paradate", date);
            sqlCmd.Parameters.AddWithValue("@paradescription", description);
            sqlCmd.Parameters.AddWithValue("@paratype", type);
            sqlCmd.Parameters.AddWithValue("@paraamount", amount);
            sqlCmd.Parameters.AddWithValue("@parastatus", status);
            sqlCmd.Parameters.AddWithValue("@parauserID", userID);
            sqlCmd.Parameters.AddWithValue("@parafrombankaccnum", frombankaccnum);
            sqlCmd.Parameters.AddWithValue("@paratobankaccnum", tobankaccnum);
            sqlCmd.Parameters.AddWithValue("@parabankaccnum", bankaccnum);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();

            return result;

        }
        public void UpdateTDforstatus(int transactionID, string status)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("UPDATE TransactionHistory SET status = @status");
            sqlStr.AppendLine("WHERE transactionID = @transactionID");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@status", status);
            sqlCmd.Parameters.AddWithValue("@transactionID", transactionID);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();
        }
        public void DeleteTDbytransactionID(int transactionID)
        {

            StringBuilder sqlStr = new StringBuilder();
            int result = 0;
            SqlCommand sqlCmd = new SqlCommand();

            sqlStr.AppendLine("DELETE FROM TransactionHistory");
            sqlStr.AppendLine("WHERE transactionID = @paratransactionID");

            SqlConnection myConn = new SqlConnection(DBConnect);

            sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

            sqlCmd.Parameters.AddWithValue("@paratransactionID", transactionID);

            myConn.Open();
            result = sqlCmd.ExecuteNonQuery();

            myConn.Close();
        }
        public List<TransactionHistory> getTDbyuserIDListmonths(string userID, int bankaccnum,int months)
        {
            int thismonth = months;
            List<TransactionHistory> THList = new List<TransactionHistory>();
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();
            StringBuilder sqlStr = new StringBuilder();
            SqlDataAdapter da;

            if (thismonth == 00)
            {
                sqlStr.AppendLine("SELECT * From TransactionHistory where userID = @parauserID and bankaccnum = @parabankaccnum");
                SqlConnection myConn = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlStr.ToString(), myConn);

                da.SelectCommand.Parameters.AddWithValue("@parauserID", userID);
                da.SelectCommand.Parameters.AddWithValue("@parabankaccnum", bankaccnum);
            }
            else
            {
                sqlStr.AppendLine("SELECT * From TransactionHistory where userID = @parauserID and bankaccnum = @parabankaccnum");
                sqlStr.AppendLine("and Month(date) = @thismonth");
                SqlConnection myConn = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlStr.ToString(), myConn);

                da.SelectCommand.Parameters.AddWithValue("@parauserID", userID);
                da.SelectCommand.Parameters.AddWithValue("@parabankaccnum", bankaccnum);
                da.SelectCommand.Parameters.AddWithValue("@thismonth", months);
            }

            
            da.Fill(ds, "TransactionHistory");
            int rec_cnt = ds.Tables["TransactionHistory"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["TransactionHistory"].Rows)
                {
                    TransactionHistory obj = new TransactionHistory();
                    obj.transactionID = Convert.ToInt32(row["transactionID"]);
                    obj.userID = row["userID"].ToString();
                    obj.date = Convert.ToDateTime(row["date"]);
                    obj.description = row["description"].ToString();
                    obj.type = row["type"].ToString();
                    obj.amount = Convert.ToDouble(row["amount"]);
                    obj.status = row["status"].ToString();
                    obj.userID = row["userID"].ToString();
                    obj.address = row["address"].ToString();
                    obj.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
                    obj.sendto = row["sendto"].ToString();
                    obj.sendusing = row["sendusing"].ToString();

                    THList.Add(obj);
                }
            }
            else
            {
                THList = null;
            }

            return THList;

        }
        public List<TransactionHistory> getTDbyuserIDListyears(string userID, int bankaccnum, int months, int years)
        {
            int thismonth = months;
            int thisyear = years;
            List<TransactionHistory> THList = new List<TransactionHistory>();
            DataSet ds = new DataSet();
            DataTable tdData = new DataTable();
            StringBuilder sqlStr = new StringBuilder();
            SqlDataAdapter da;

            if (thismonth == 00)
            {
                sqlStr.AppendLine("SELECT * From TransactionHistory where userID = @parauserID and bankaccnum = @parabankaccnum");
                SqlConnection myConn = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlStr.ToString(), myConn);

                da.SelectCommand.Parameters.AddWithValue("@parauserID", userID);
                da.SelectCommand.Parameters.AddWithValue("@parabankaccnum", bankaccnum);
            }
            else
            {
                sqlStr.AppendLine("SELECT * From TransactionHistory where userID = @parauserID and bankaccnum = @parabankaccnum");
                sqlStr.AppendLine("and Month(date) = @thismonth and Year(date) = @thisyear");
                SqlConnection myConn = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlStr.ToString(), myConn);

                da.SelectCommand.Parameters.AddWithValue("@parauserID", userID);
                da.SelectCommand.Parameters.AddWithValue("@parabankaccnum", bankaccnum);
                da.SelectCommand.Parameters.AddWithValue("@thismonth", months);
                da.SelectCommand.Parameters.AddWithValue("@thisyear", years);
            }


            da.Fill(ds, "TransactionHistory");
            int rec_cnt = ds.Tables["TransactionHistory"].Rows.Count;
            if (rec_cnt > 0)
            {
                foreach (DataRow row in ds.Tables["TransactionHistory"].Rows)
                {
                    TransactionHistory obj = new TransactionHistory();
                    obj.transactionID = Convert.ToInt32(row["transactionID"]);
                    obj.userID = row["userID"].ToString();
                    obj.date = Convert.ToDateTime(row["date"]);
                    obj.description = row["description"].ToString();
                    obj.type = row["type"].ToString();
                    obj.amount = Convert.ToDouble(row["amount"]);
                    obj.status = row["status"].ToString();
                    obj.userID = row["userID"].ToString();
                    obj.address = row["address"].ToString();
                    obj.bankaccnum = Convert.ToInt32(row["bankaccnum"]);
                    obj.sendto = row["sendto"].ToString();
                    obj.sendusing = row["sendusing"].ToString();

                    THList.Add(obj);
                }
            }
            else
            {
                THList = null;
            }

            return THList;

        }

        //public int InsertTransactionsNormal(DateTime date, string description, string type, double amount, string status, string userID, int frombankaccnum, int bankaccnum)
        //{

        //    StringBuilder sqlStr = new StringBuilder();
        //    int result = 0;
        //    SqlCommand sqlCmd = new SqlCommand();

        //    sqlStr.AppendLine("INSERT INTO TransactionHistory (date, description, type, amount, status, userID, frombankaccnum, bankaccnum)");
        //    sqlStr.AppendLine("VALUES (@paradate,@paradescription,@paratype,@paraamount,@parastatus,@parauserID,@parafrombankaccnum,@parabankaccnum)");

        //    SqlConnection myConn = new SqlConnection(DBConnect);

        //    sqlCmd = new SqlCommand(sqlStr.ToString(), myConn);

        //    sqlCmd.Parameters.AddWithValue("@paradate", date);
        //    sqlCmd.Parameters.AddWithValue("@paradescription", description);
        //    sqlCmd.Parameters.AddWithValue("@paratype", type);
        //    sqlCmd.Parameters.AddWithValue("@paraamount", amount);
        //    sqlCmd.Parameters.AddWithValue("@parastatus", status);
        //    sqlCmd.Parameters.AddWithValue("@parauserID", userID);
        //    sqlCmd.Parameters.AddWithValue("@parafrombankaccnum", frombankaccnum);
        //    sqlCmd.Parameters.AddWithValue("@parabankaccnum", bankaccnum);

        //    myConn.Open();
        //    result = sqlCmd.ExecuteNonQuery();

        //    myConn.Close();

        //    return result;

        //}
    }
}

