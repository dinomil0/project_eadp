﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="financial_Planning_Overview.aspx.cs" Inherits="project.financial_Planning_Overview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .img_Fp_Read {
            max-width: 100%;
            height: 300px;
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="fp_Form">
        <img src="img/financial_Planning.jpg" class="img_Fp_Read" />
        <div class="container mt-3">
            <asp:Label ID="lb_Fp_Overview_Title" class="h5 font-weight-bold font-italic" runat="server" Text="Financial Planning Overview"></asp:Label>
            <asp:GridView ID="gv_Fp_Overview" class="table table-responsive-sm mt-2" runat="server" AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateField HeaderText="Plan ID">
                        <ItemTemplate>
                            <asp:Label ID="lb_Fp_Read_Id" runat="server" Text='<%# Eval("plan_Id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Plan Name">
                        <ItemTemplate>
                            <asp:Label ID="lb_Fp_Read_Name" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Age @ Plan Maturity">
                        <ItemTemplate>
                            <asp:Label ID="lb_Fp_Read_Age" runat="server" Text='<%# Eval("matured_Age") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Monthly Contribution">
                        <ItemTemplate>
                            <asp:Label ID="lb_Fp_Read_Monthly_Contribution" runat="server" Text='<%# Eval("monthly_Contribution") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Minimum Amount">
                        <ItemTemplate>
                            <asp:Label ID="lb_Fp_Read_Amount" runat="server" Text='<%# Eval("amount") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="btn_Fp_Read_Delete" OnClick="btn_Fp_Read_Delete_Click" runat="server">
                                <i class="fas fa-trash-alt"></i>
                            </asp:LinkButton>
                        </ItemTemplate>
                        <ControlStyle CssClass="btn btn-sm btn-secondary d-flex justify-content-center py-2" />
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle Cssclass="thead-dark" />
            </asp:GridView>
        </div>

        <!--Deleting Financial Plan-->
        <div class="modal fade" id="md_Fp_Delete_Plan" tabindex="-1" role="dialog" aria-labelledby="md_Fp_Update_Label" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="md_Fp_Delete_Label">Deleting Plan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lb_Fp_Delete_Error" class="text-danger" runat="server"></asp:Label>
                        <div class="form-group row mt-3">
                            <asp:Label ID="lb_Fp_Delete_Id" class="col-form-label col-sm-4" runat="server" Text="Plan ID:"></asp:Label>
                            <div class="col-sm-8">
                                <asp:Textbox ID="tb_Fp_Delete_Id" class="form-control" ReadOnly="true" runat="server"></asp:Textbox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Fp_Delete_Name" class="col-form-label col-sm-4" runat="server" Text="Plan Name:"></asp:Label>
                            <div class="col-sm-8">
                                <asp:Textbox ID="tb_Fp_Delete_Name" class="form-control" ReadOnly="true" runat="server"></asp:Textbox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Fp_Delete_Age" class="col-form-label col-sm-4" runat="server" Text="Age @ Plan Maturity:"></asp:Label>
                            <div class="col-sm-8">
                                <asp:Textbox ID="tb_Fp_Delete_Age" class="form-control" ReadOnly="true" runat="server"></asp:Textbox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lb_Fp_Delete_Amount" class="col-form-label col-sm-4" runat="server" Text="Minimum Amount:"></asp:Label>
                            <div class="col-sm-8">
                                <asp:Textbox ID="tb_Fp_Delete_Amount" class="form-control" ReadOnly="true" runat="server"></asp:Textbox>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btn_Fp_Delete_Data_Submit" class="btn btn-success" Onclick="btn_Fp_Delete_Data_Submit_Click" Text="Delete Data"  runat="server" />
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <script type='text/javascript'>
            function initiate_Delete_Modal() {
                $('[id*=md_Fp_Delete_Plan]').modal('show');
            }
        </script>
    
</asp:Content>
