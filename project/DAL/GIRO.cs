﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class GIRO
    {
        public GIRO()
        {

        }
        public int giroID { get; set; }
        public DateTime date { get; set; }
        public string billingOrganisation { get; set; }
        public double amount { get; set; }
        public int bankaccnum { get; set; }
        public string userID { get; set; }
        public int status { get; set; }
    }
}