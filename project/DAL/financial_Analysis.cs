﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class financial_Analysis
    {
        public string userID { set; get; }
        public string name { set; get; }
        public DateTime update_Detail { set; get; }
        public decimal amount { set; get; }
    }
}