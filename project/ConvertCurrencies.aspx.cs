﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }
            if (!IsPostBack)
            {
                AccountsDAO accDAO = new AccountsDAO();
                List<Accounts> bankaccnumList = new List<Accounts>();

                string userID = Session["custNRIC"].ToString();
                bankaccnumList = accDAO.getbackaccnumbyuserID(userID);

                Ddlchosenbankacc.Items.Clear();
                Ddlchosenbankacc.Items.Insert(0, new ListItem("--Select--", "0"));
                Ddlchosenbankacc.AppendDataBoundItems = true;
                Ddlchosenbankacc.DataTextField = "bankaccname";
                Ddlchosenbankacc.DataValueField = "bankaccnum";
                Ddlchosenbankacc.DataSource = bankaccnumList;
                Ddlchosenbankacc.DataBind();

                Lblconvertedcurrencies.Text = Session["CurrencyName"].ToString();

                Currency curr = new Currency();
                CurrencyDAO currDAO = new CurrencyDAO();

                curr = currDAO.getCurrencyWallet(userID);
                
                if (curr != null)
                {
                    if (Lblconvertedcurrencies.Text == "Malaysian Ringgit")
                    {
                        LblcurrencyBalance.Text = Session["Symbol"] + Math.Round(Convert.ToDouble(curr.CurrencyAmtMYR), 2).ToString("F");
                    }
                    else if (Lblconvertedcurrencies.Text == "United States Dollar")
                    {
                        LblcurrencyBalance.Text = Session["Symbol"] + Math.Round(Convert.ToDouble(curr.CurrencyAmtUSD), 2).ToString("F");
                    }
                    else if (Lblconvertedcurrencies.Text == "Euro")
                    {
                        LblcurrencyBalance.Text = Session["Symbol"] + Math.Round(Convert.ToDouble(curr.CurrencyAmtEUR), 2).ToString("F");
                    }
                    else if (Lblconvertedcurrencies.Text == "Pound Sterling")
                    {
                        LblcurrencyBalance.Text = Session["Symbol"] + Math.Round(Convert.ToDouble(curr.CurrencyAmtGBP), 2).ToString("F");
                    }
                    else
                    {
                        Lbl_error.Text += "Currency not found";
                    }
                }
                else
                {
                    Lbl_error.Text += "Customer class not found";
                }
                



            }
            
            
        }
        public List<Currency> GetCurriences()
        {
            HttpClient client = new HttpClient();
            // state the URI address that hosted the web API
            client.BaseAddress = new Uri("http://localhost:62746/");
            List<Currency> data = new List<Currency>();
            Task<HttpResponseMessage> getTask = client.GetAsync("api/Currency");
            getTask.Wait();

            var result = getTask.Result;
            if (result.IsSuccessStatusCode)
            {
                var readTask = result.Content.ReadAsAsync<List<Currency>>();
                readTask.Wait();

                data = readTask.Result;
            }
            else
            {
                data = null;
            }
            return data;
        }
       
        protected void Ddlchosenbankacc_SelectedIndexChanged(object sender, EventArgs e)
        {
            Accounts accObj = new Accounts();
            AccountsDAO accDAO = new AccountsDAO();
            var userID = Session["custNRIC"].ToString();
            if (Session["custNRIC"] == null)
            {
                Response.Redirect("landing.aspx");
            }


            accObj = accDAO.getbalanceBybankaccnum(Convert.ToInt32(Ddlchosenbankacc.SelectedValue));

            if (accObj.bankaccnum == Convert.ToInt32(Ddlchosenbankacc.SelectedValue))
            {
                LblTransferBalance.Text = "SGD" + accObj.balance.ToString();
            }
            else
            {
                Lbl_error.Text += "Account not found in user";
            }
        }

        protected void BtnConvertCurrencies_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.Now;
                double amount = Convert.ToDouble(TxtAmountinput.Text);
                string userID = Session["custNRIC"].ToString();
                TransactionHistoryDAO newdata = new TransactionHistoryDAO();
                int frombankaccnum = Convert.ToInt32(Ddlchosenbankacc.SelectedValue);
                int insCnt = newdata.InsertTransactionsNormal(date, "Convert Currencies", "Withdrawal", amount, "Successful", userID, frombankaccnum, frombankaccnum);
                if (insCnt == 1)
                {
                    
                    Accounts accsObj = new Accounts();
                    AccountsDAO accsDAO = new AccountsDAO();

                    accsObj = accsDAO.getbankaccbalance(frombankaccnum);
                    accsObj.balance = accsObj.balance - amount;

                    AccountsDAO updateDAOwithdrawal = new AccountsDAO();
                    updateDAOwithdrawal.updatebankaccbalance(frombankaccnum, accsObj.balance);

                    //add acc balance for foreign currency
                    Currency curr = new Currency();
                    CurrencyDAO currDAO = new CurrencyDAO();

                    curr = currDAO.getCurrencyWallet(userID);

                    if (curr != null)
                    {
                        if (Lblconvertedcurrencies.Text == "Malaysian Ringgit")
                        {
                            curr.CurrencyAmtMYR = curr.CurrencyAmtMYR + (Convert.ToDouble(TxtAmountinput.Text)* Convert.ToDouble(Session["ConversionRate"]));
                            currDAO.updatebalancebyuserIDmyr(userID, Lblconvertedcurrencies.Text, curr.CurrencyAmtMYR);
                            //LblcurrencyBalance.Text = Session["Symbol"] + Math.Round(Convert.ToDouble(curr.CurrencyAmtMYR), 2).ToString("F");
                        }
                        else if (Lblconvertedcurrencies.Text == "United States Dollar")
                        {
                            curr.CurrencyAmtUSD = curr.CurrencyAmtUSD + (Convert.ToDouble(TxtAmountinput.Text) * Convert.ToDouble(Session["ConversionRate"]));
                            currDAO.updatebalancebyuserIDusd(userID, Lblconvertedcurrencies.Text, curr.CurrencyAmtUSD);
                        }
                        else if (Lblconvertedcurrencies.Text == "Euro")
                        {
                            curr.CurrencyAmtEUR = curr.CurrencyAmtEUR + (Convert.ToDouble(TxtAmountinput.Text) * Convert.ToDouble(Session["ConversionRate"]));
                            currDAO.updatebalancebyuserIDeur(userID, Lblconvertedcurrencies.Text, curr.CurrencyAmtEUR);
                        }
                        else if (Lblconvertedcurrencies.Text == "Pound Sterling")
                        {
                            curr.CurrencyAmtGBP = curr.CurrencyAmtGBP + (Convert.ToDouble(TxtAmountinput.Text) * Convert.ToDouble(Session["ConversionRate"]));
                            currDAO.updatebalancebyuserIDgbp(userID, Lblconvertedcurrencies.Text, curr.CurrencyAmtGBP);
                        }
                        else
                        {
                            Lbl_error.Text += "Currency balance not updated";
                        }

                    }
                    else
                    {
                        Lbl_error.Text += "Customer class not found";
                    }
                    Response.Redirect("ConvertCurrencies.aspx");
                    
                }
                else
                {
                    Lbl_error.Text = "Transaction failed";
                }
                
            }
            catch (Exception f)
            {
                Lbl_error.Text = f.ToString();
            }
           
        }

        protected void TxtAmountinput_TextChanged(object sender, EventArgs e)
        {
            Lbltransfercurrencies.Text = "SGD" + TxtAmountinput.Text + " = " + Session["Symbol"] + Math.Round(Convert.ToDouble(TxtAmountinput.Text) * Convert.ToDouble(Session["ConversionRate"]), 2); 
        }
    }
}