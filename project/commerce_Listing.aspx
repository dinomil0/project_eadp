﻿<%@ Page Title="" Language="C#" MasterPageFile="~/commerceMaster.Master" AutoEventWireup="true" CodeBehind="commerce_Listing.aspx.cs" Inherits="project.commerce_Listing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
    .ProdImg {
        width:175px;
        height:175px;
        overflow:hidden;
    }
</style>
<div id="form1">
    <div class="col-3 m-4">
        <asp:Label ID="Lbl_userID" runat="server" Text="User Name" class="h3"></asp:Label>
        <h3>Listings: </h3>
    </div>
    <section class="d-flex justify-content-center">
        <asp:DataList runat="server" ID="ProdList" RepeatColumns="5" CellPadding="20">
            <ItemTemplate>
                <div class ="d-flex justify-content-center border border-success rounded container">
                    <div class="m-3">
                        <asp:Image ID="ProdImage" runat="server" ImageUrl='<%# Eval("ProdImage") %>' cssclass="img-thumbnail ProdImg"/>  
                        <table style="width: 100%;">
                            <tr>
                                <td>Name:</td>
                                <td><%# Eval("ProdName") %></td>
                            </tr>
                            <tr>
                                <td>Tag:</td>
                                <td><%# Eval("ProdId") %></td>
                            </tr>
                            <tr>
                                <td>Price:</td>
                                <td>$<%# Eval("ProdPrice") %></td>
                            </tr>
                            <tr>
                                <td>Qty Left:</td>
                                <td><%# Eval("ProdQty") %></td>
                            </tr>
                        </table>
                        <div class="d-flex justify-content-center">
                            <asp:LinkButton runat="server" class ="btn btn-outline-danger mt-1" OnCommand="btn_Cancel_Click" CommandArgument = '<%# Eval("ProdId") %>' >Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:DataList>
    </section>
    <br />
</div>
</asp:Content>
