﻿<%@ Page Title="" Language="C#" MasterPageFile="~/commerceMaster.Master" AutoEventWireup="true" CodeBehind="commerce_Enquiries.aspx.cs" Inherits="project.commerce_Enquiries" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="form1">
    <section class="d-flex justify-content-center">
        <div class="">
            <h4 class="m-4">Products Bought</h4>
            <asp:DataList runat="server" ID="BuyInquiryList" RepeatColumns="2" CellPadding="20">
                <ItemTemplate>
                    <div class ="d-flex justify-content-center mt-5 border border-success rounded container">
                        <div class="m-3">
                            <table style="width: 100%;">
                                <tr>
                                    <td>Inquiry Tag:</td>
                                    <td><%# Eval("InquiryId") %></td>
                                </tr>
                                <tr>
                                    <td>Order Tag:</td>
                                    <td><%# Eval("ordId") %></td>
                                </tr>
                                <tr>
                                    <td>Your demand:</td>
                                    <td><%# Eval("UserInput") %></td>
                                </tr>
                                <tr>
                                    <td>Seller's Offer:</td>
                                    <td><%# Eval("SellerInput") %></td>
                                </tr>
                                <tr>
                                    <td>Description:</td>
                                    <td><%# Eval("InquiryDesc") %></td>
                                </tr>
                                <tr>
                                    <td><asp:LinkButton runat="server" class ="btn btn-outline-danger m-1" OnCommand="btn_buyRefund_Click" CommandArgument = '<%# Eval("InquiryId") %>' >Refund</asp:LinkButton></td>
                                    <td class="d-flex justify-content-center"><asp:LinkButton runat="server" class ="btn btn-outline-danger m-1" OnCommand="btn_buyReplace_Click" CommandArgument = '<%# Eval("InquiryId") %>' >Replace</asp:LinkButton></td>
                                </tr>
                            </table>
                            <div class="d-flex justify-content-center">
                                <asp:LinkButton ID="btn_Accept_Click" runat="server" class="btn btn-outline-success" OnCommand="btn_Accept_Click" CommandArgument = '<%# Eval("InquiryId") %>'>Accept</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="">
            <h4 class="m-4">Products Sold</h4>
            <asp:DataList runat="server" ID="SellInquiryList" RepeatColumns="2" CellPadding="20">
                <ItemTemplate>
                    <div class ="d-flex justify-content-center mt-5 border border-danger rounded container">
                        <div class="m-3">
                            <table style="width: 100%;">
                                <tr>
                                    <td>Inquiry Tag:</td>
                                    <td><%# Eval("InquiryId") %></td>
                                </tr>
                                <tr>
                                    <td>Order Tag:</td>
                                    <td><%# Eval("ordId") %></td>
                                </tr>
                                <tr>
                                    <td>Buyer's Demand:</td>
                                    <td><%# Eval("UserInput") %></td>
                                </tr>
                                <tr>
                                    <td>Your Offer:</td>
                                    <td><%# Eval("SellerInput") %></td>
                                </tr>
                                <tr>
                                    <td>Description:</td>
                                    <td><%# Eval("InquiryDesc") %></td>
                                </tr>
                                <tr>
                                    <td><asp:LinkButton runat="server" class ="btn btn-outline-danger m-1" OnCommand="btn_sellRefund_Click" CommandArgument = '<%# Eval("InquiryId") %>' >Refund</asp:LinkButton></td>
                                    <td><asp:LinkButton runat="server" class ="btn btn-outline-danger m-1" OnCommand="btn_sellReplace_Click" CommandArgument = '<%# Eval("InquiryId") %>' >Replace</asp:LinkButton></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:DataList>
        </div>
    </section>
</div>
</asp:Content>
