﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class VCAccountDAO
    {
        public VCAccount GetVCAccount(string userID)
        {
            VCAccount VCAcc = new VCAccount();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM AccountVC");
            sqlCommand.AppendLine("WHERE userID = @parauserID");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

            da.Fill(ds, "AccountVCTable");

            foreach (DataRow row in ds.Tables["AccountVCTable"].Rows)
            {
                VCAcc.name = Convert.ToString(row["userID"]);
                VCAcc.vc = Convert.ToInt32(row["VCBal"]);
            }

            return VCAcc;
        }
        public void VCconversion(string userID, int amt, int vc)
        {
            SqlDataAdapter da;

            string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("UPDATE AccountVC");
            sqlCommand.AppendLine("SET VCBal = VCBal + @paravc");
            sqlCommand.AppendLine("WHERE userID = @parauserID;");
            //sqlCommand.AppendLine("UPDATE SavingAccount");
            //sqlCommand.AppendLine("SET bal = bal - @paraamt");
            //sqlCommand.AppendLine("WHERE userID = @parauserID;");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);
            da.SelectCommand.Parameters.AddWithValue("paraamt", amt);
            //da.SelectCommand.Parameters.AddWithValue("paravc", vc);
        }
    }
}