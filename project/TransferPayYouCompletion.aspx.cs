﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class TransferPayYouCompletion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userID = Session["custNRIC"].ToString();
            if (Session["custNRIC"] == null)
            {
                Response.Redirect("landing.aspx");
            }
            LblType.Text = Session["PayYouType"].ToString();

            if (LblType.Text == "Phone Number")
            {
                VerifyPhone1.Visible = true;
                LblRecipMobileNum.Text = Session["PayYouPhonenum"].ToString();
                LblRecipNickNameMobileNum.Text = Session["PayYounamePhoneNum"].ToString();
                LblRecipBankAccnumMobileNum.Text = Session["PayYouRecipBankaccnumPhoneNum"].ToString();
                LblRecipBankAccnameMobileNum.Text = Session["PayYouRecipBankaccnamePhoneNum"].ToString();
                LblTransferAmt.Text = "$" + Session["PayYouamountPhoneNum"].ToString();
                LblAccount.Text = Session["PayYoubankaccPhoneNum"].ToString();
            }
            else if (LblType.Text == "NRIC")
            {
                VerifyNRIC1.Visible = true;
                LblRecipNRIC.Text = Session["PayYouNRIC"].ToString();
                LblRecipNickNameNRIC.Text = Session["PayYounameNRIC"].ToString();
                LblRecipBankaccnumNRIC.Text = Session["PayYouRecipBankaccnumNRIC"].ToString();
                LblRecipBankaccnameNRIC.Text = Session["PayYouRecipBankaccnameNRIC"].ToString();
                LblTransferAmt.Text = "$" + Session["PayYouamountNRIC"].ToString();
                LblAccount.Text = Session["PayYoubankaccNRIC"].ToString();
            }
            else
            {
                Lbl_errcomplete.Text = "Error Found";
            }
        }

        protected void submitButtonPayYouCompletion_Click(object sender, EventArgs e)
        {
            Response.Redirect("bankHome.aspx");
        }
        protected void redirectTHButtonPayYouCompletion_Click(object sender, EventArgs e)
        {
            Response.Redirect("TransactionHistory.aspx");
        }
    }
}