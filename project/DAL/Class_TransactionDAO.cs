﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class Class_TransactionDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        public string checkdate(DateTime date)
        {
            string status = "";
            int result = DateTime.Compare(DateTime.Today, date);
            if (result > 0)
            {
                status = "Cleared";
                return status;
            }
            else if (result == 0)
            {
                status = "Cleared";
                return status;
            }
            else if (result < 0)
            {
                status = "Pending";
                return status;
            }
            return status;
        }
        public bool Add_Transaction(Class_Transaction transaction, string type, long bankaccnum)
        {
            if (type == "Card")
            {
                Class_AccountDAO accDAO = new Class_AccountDAO();
                string test = transaction.credit;
                string test1 = transaction.total;
                bool result = accDAO.useCard(transaction.userID, bankaccnum,Convert.ToDouble(transaction.debit), Convert.ToDouble(transaction.credit.Substring(1)));
                if (result == true)
                {
                    StringBuilder sqlCommand = new StringBuilder();
                    SqlCommand excommand = new SqlCommand();
                    sqlCommand.AppendLine("INSERT INTO Transactions");
                    sqlCommand.AppendLine("VALUES (@paratransID, @paraordId, @paradate, @parareference, @paraprice, @paraqty, @paratotal, @paradebit, @paracredit, @parauserID, @parastatus)");
                    SqlConnection myConn = new SqlConnection(DBConnect);
                    excommand = new SqlCommand(sqlCommand.ToString(), myConn);
                    excommand.Parameters.AddWithValue("paratransID", transaction.transID);
                    excommand.Parameters.AddWithValue("paraordId", transaction.ordId);
                    excommand.Parameters.AddWithValue("paradate", transaction.date);
                    excommand.Parameters.AddWithValue("parareference", transaction.reference);
                    excommand.Parameters.AddWithValue("paraprice", transaction.price);
                    excommand.Parameters.AddWithValue("paraqty", transaction.qty);
                    excommand.Parameters.AddWithValue("paratotal", transaction.total);
                    excommand.Parameters.AddWithValue("paradebit", transaction.debit);
                    excommand.Parameters.AddWithValue("paracredit", transaction.credit);
                    excommand.Parameters.AddWithValue("parauserID", transaction.userID);
                    excommand.Parameters.AddWithValue("parastatus", transaction.status);
                    myConn.Open();
                    excommand.ExecuteNonQuery();
                    myConn.Close();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (type == "VC")
            {
                Class_AccountDAO accDAO = new Class_AccountDAO();
                bool result = accDAO.useVC(transaction.userID, Convert.ToDouble(transaction.debit.Substring(0, transaction.debit.Length - 2)), Convert.ToDouble(transaction.credit.Substring(0, transaction.credit.Length - 2)));
                if (result == true)
                {
                    StringBuilder sqlCommand = new StringBuilder();
                    SqlCommand excommand = new SqlCommand();
                    sqlCommand.AppendLine("INSERT INTO Transactions (transID, ordId, date, reference, price, qty, total, debit, credit, userID, status)");
                    sqlCommand.AppendLine("VALUES (@paratransID, @paraordId, @paradate, @parareference, @paraprice, @paraqty, @paratotal, @paradebit, @paracredit, @parauserID, @parastatus)");
                    SqlConnection myConn = new SqlConnection(DBConnect);
                    excommand = new SqlCommand(sqlCommand.ToString(), myConn);
                    excommand.Parameters.AddWithValue("paratransID", transaction.transID);
                    excommand.Parameters.AddWithValue("paraordId", transaction.ordId);
                    excommand.Parameters.AddWithValue("paradate", transaction.date);
                    excommand.Parameters.AddWithValue("parareference", transaction.reference);
                    excommand.Parameters.AddWithValue("paraprice", transaction.price);
                    excommand.Parameters.AddWithValue("paraqty", transaction.qty);
                    excommand.Parameters.AddWithValue("paratotal", transaction.total);
                    excommand.Parameters.AddWithValue("paradebit", transaction.debit);
                    excommand.Parameters.AddWithValue("paracredit", transaction.credit);
                    excommand.Parameters.AddWithValue("parauserID", transaction.userID);
                    excommand.Parameters.AddWithValue("parastatus", transaction.status);
                    myConn.Open();
                    excommand.ExecuteNonQuery();
                    myConn.Close();
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
        public void Add_Instalment(string ProdId, double rate, string userID, int mths, int qty, string type)
        {
            Class_ProductDAO productDAO = new Class_ProductDAO();
            Class_Product product = productDAO.getProduct(ProdId);
            product.ProdPrice = Math.Round(product.ProdPrice * (1 + rate), 0);
            int i = 0;
            for (i = 0; i < mths; i++)
            {
                DateTime date = DateTime.Now.AddMonths(i);

                Class_Transaction sell = new Class_Transaction();
                sell.userID = productDAO.getSellerID(ProdId);
                sell.transID = product.ProdId + Convert.ToString(sell.getCount());
                sell.date = date;
                sell.reference = product.ProdName;
                sell.price = Convert.ToString(Math.Round(product.ProdPrice / mths * qty, 0));
                if (i == 0)
                {
                    sell.qty = qty;
                }
                else
                {
                    sell.qty = 0;
                }
                sell.total = sell.price;
                sell.credit = Convert.ToString(sell.price);
                sell.debit = "0";
                sell.status = checkdate(date);

                Class_Transaction buy = new Class_Transaction();
                buy.userID = userID;
                buy.transID = product.ProdId + Convert.ToString(buy.getCount());
                buy.date = date;
                buy.reference = product.ProdName;
                buy.price = sell.price;
                if (i == 0)
                {
                    buy.qty = qty;
                }
                else
                {
                    buy.qty = 0;
                }
                if (type == "VC")
                {
                    buy.total = Convert.ToString(Convert.ToDouble(buy.price) * 4) + "VC";
                }
                else if (type == "Card")
                {
                    buy.total = Convert.ToString(buy.price);
                }
                buy.debit = buy.total;
                buy.credit = "0.00";
                buy.status = checkdate(date);

                StringBuilder sqlCommand = new StringBuilder();
                SqlCommand excommand = new SqlCommand();
                sqlCommand.AppendLine("INSERT INTO Transactions");
                sqlCommand.AppendLine("VALUES (@paratransID, @paraordId, @paradate, @parareference, @paraprice, @paraqty, @paratotal, @paradebit, @paracredit, @parauserID,  @parastatus)");
                SqlConnection myConn = new SqlConnection(DBConnect);
                excommand = new SqlCommand(sqlCommand.ToString(), myConn);
                excommand.Parameters.AddWithValue("paratransID", buy.transID);
                excommand.Parameters.AddWithValue("paraordId", " ");
                excommand.Parameters.AddWithValue("paradate", buy.date);
                excommand.Parameters.AddWithValue("parareference", buy.reference);
                excommand.Parameters.AddWithValue("paraprice", buy.price);
                excommand.Parameters.AddWithValue("paraqty", buy.qty);
                excommand.Parameters.AddWithValue("paratotal", buy.total);
                excommand.Parameters.AddWithValue("paradebit", buy.debit);
                excommand.Parameters.AddWithValue("paracredit", buy.credit);
                excommand.Parameters.AddWithValue("parauserID", buy.userID);
                excommand.Parameters.AddWithValue("parastatus", buy.status);
                myConn.Open();
                excommand.ExecuteNonQuery();
                myConn.Close();

                StringBuilder sqlCommand1 = new StringBuilder();
                SqlCommand excommand1 = new SqlCommand();
                sqlCommand1.AppendLine("INSERT INTO Transactions");
                sqlCommand1.AppendLine("VALUES (@paratransID, @paraordId, @paradate, @parareference, @paraprice, @paraqty, @paratotal, @paradebit, @paracredit, @parauserID, @parastatus)");
                SqlConnection myConn1 = new SqlConnection(DBConnect);
                excommand1 = new SqlCommand(sqlCommand1.ToString(), myConn1);
                excommand1.Parameters.AddWithValue("paratransID", sell.transID);
                excommand1.Parameters.AddWithValue("paraordId", " ");
                excommand1.Parameters.AddWithValue("paradate", sell.date);
                excommand1.Parameters.AddWithValue("parareference", sell.reference);
                excommand1.Parameters.AddWithValue("paraprice", sell.price);
                excommand1.Parameters.AddWithValue("paraqty", sell.qty);
                excommand1.Parameters.AddWithValue("paratotal", sell.total);
                excommand1.Parameters.AddWithValue("paradebit", sell.debit);
                excommand1.Parameters.AddWithValue("paracredit", sell.credit);
                excommand1.Parameters.AddWithValue("parauserID", sell.userID);
                excommand1.Parameters.AddWithValue("parastatus", sell.status);
                myConn1.Open();
                excommand1.ExecuteNonQuery();
                myConn1.Close();
            }
            productDAO.Less_Qty(ProdId, qty);
        }
        public List<Class_Transaction> get_transactions(string userID)
        {
            List<Class_Transaction> transList = new List<Class_Transaction>();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Transactions");
            sqlCommand.AppendLine("WHERE userID = @parauserID");
            sqlCommand.AppendLine("ORDER BY date DESC");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("parauserID", userID);

            da.Fill(ds, "TransactionTable");

            foreach (DataRow row in ds.Tables["TransactionTable"].Rows)
            {
                Class_TransactionDAO transDAO = new Class_TransactionDAO();
                Class_Transaction transaction = new Class_Transaction();
                transaction.date = Convert.ToDateTime(row["date"]);
                transaction.transID = Convert.ToString(row["transID"]);
                transaction.reference = Convert.ToString(row["reference"]);
                transaction.price = "$" + string.Format("{0:#.00}", Convert.ToDecimal(row["price"]));
                transaction.qty = Convert.ToInt16(row["qty"]);
                string str = Convert.ToString(row["total"]);
                if (str != "0")
                {
                    if (str.Length > 3 && Convert.ToString(str[str.Length - 2]) == "V")
                    {
                        transaction.total = Convert.ToString(row["total"]);
                    }
                    else
                    {
                        transaction.total = "$" + string.Format("{0:#.00}", Convert.ToDecimal(row["total"]));
                    }
                }
                else
                {
                    transaction.total = "$" + string.Format("{0:#.00}", Convert.ToDecimal(str));
                }
                string str1 = Convert.ToString(row["debit"]);
                if (str1 != "0")
                {
                    if (str1.Length > 3 && Convert.ToString(str1[str1.Length - 2]) == "V")
                    {
                        transaction.debit = Convert.ToString(row["debit"]);
                    }
                    else
                    {
                        transaction.debit = "$" + string.Format("{0:#.00}", Convert.ToDecimal(row["debit"]));
                    }
                }
                else
                {
                    transaction.debit = "$" + string.Format("{0:#.00}", Convert.ToDecimal(str1));
                }
                string str2 = Convert.ToString(row["credit"]);
                if (str2 != "0")
                {
                    if (str2.Length > 3 && Convert.ToString(str2[str2.Length - 2]) == "V")
                    {
                        transaction.credit = Convert.ToString(row["credit"]);
                    }
                    else
                    {
                        transaction.credit = "$" + string.Format("{0:#.00}", Convert.ToDecimal(row["credit"]));
                    }
                }
                else
                {
                    transaction.credit = "$" + string.Format("{0:#.00}", Convert.ToDecimal(str2));
                }
                transaction.status = transDAO.checkdate(transaction.date);
                transList.Add(transaction);
            }
            return transList;
        }
        public List<Class_Transaction> get_transaction_by_ordId(string ordId)
        {
            List<Class_Transaction> transList = new List<Class_Transaction>();
            SqlDataAdapter da;
            DataSet ds = new DataSet();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.AppendLine("SELECT * FROM Transactions");
            sqlCommand.AppendLine("WHERE ordId = @paraordId");

            SqlConnection myConn = new SqlConnection(DBConnect);
            da = new SqlDataAdapter(sqlCommand.ToString(), myConn);
            da.SelectCommand.Parameters.AddWithValue("paraordId", ordId);

            da.Fill(ds, "TransactionTable");

            foreach (DataRow row in ds.Tables["TransactionTable"].Rows)
            {
                Class_TransactionDAO transDAO = new Class_TransactionDAO();
                Class_Transaction transaction = new Class_Transaction();
                transaction.date = Convert.ToDateTime(row["date"]);
                transaction.transID = Convert.ToString(row["transID"]);
                transaction.reference = Convert.ToString(row["reference"]);
                transaction.price = string.Format("{0:#.00}", Convert.ToDecimal(row["price"]));
                transaction.qty = Convert.ToInt16(row["qty"]);
                string str = Convert.ToString(row["total"]);
                if (str != "0")
                {
                    if (str.Length > 3 && Convert.ToString(str[str.Length - 2]) == "V")
                    {
                        transaction.total = Convert.ToString(row["total"]);
                    }
                    else
                    {
                        transaction.total = string.Format("{0:#.00}", Convert.ToDecimal(row["total"]));
                    }
                }
                else
                {
                    transaction.total = string.Format("{0:#.00}", Convert.ToDecimal(str));
                }
                string str1 = Convert.ToString(row["debit"]);
                if (str1 != "0")
                {
                    if (str1.Length > 3 && Convert.ToString(str1[str1.Length - 2]) == "V")
                    {
                        transaction.debit = Convert.ToString(row["debit"]);
                    }
                    else
                    {
                        transaction.debit = string.Format("{0:#.00}", Convert.ToDecimal(row["debit"]));
                    }
                }
                else
                {
                    transaction.debit = string.Format("{0:#.00}", Convert.ToDecimal(str1));
                }
                string str2 = Convert.ToString(row["credit"]);
                if (str2 != "0")
                {
                    if (str2.Length > 3 && Convert.ToString(str2[str2.Length - 2]) == "V")
                    {
                        transaction.credit = Convert.ToString(row["credit"]);
                    }
                    else
                    {
                        transaction.credit = string.Format("{0:#.00}", Convert.ToDecimal(row["credit"]));
                    }
                }
                else
                {
                    transaction.credit = string.Format("{0:#.00}", Convert.ToDecimal(str2));
                }
                transaction.userID = Convert.ToString(row["userID"]);
                transaction.status = transDAO.checkdate(transaction.date);
                transList.Add(transaction);
            }
            return transList;
        }
    }
}