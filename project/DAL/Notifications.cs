﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class Notifications
    {
        public Notifications()
        {

        }
        public int notificationsID { get; set; }
        public string details { get; set; }
        public string userID { get; set; }
    }
}