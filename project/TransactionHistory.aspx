﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="TransactionHistory.aspx.cs" Inherits="project.TransactionHistory" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .cannotSee
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">  
    <div class="container">
<%-- Filter --%>

<div class="d-flex mt-4">
    <div class="margin-left">
        <asp:Label ID="LblbAccounts" runat="server" Text="Bank Accounts:"></asp:Label>
        <asp:DropDownList ID="bankacclist" runat="server"  CssClass="btn btn-secondary dropdown-toggle" AutoPostBack="True" OnSelectedIndexChanged="bankacclist_SelectedIndexChanged">
                </asp:DropDownList>
    </div>

    <div class="d-block mx-auto my-auto">
        <asp:Label ID="Lblmonth" runat="server" Text="Month:"></asp:Label>
        <asp:DropDownList ID="Ddlmonths" CssClass="btn btn-secondary dropdown-toggle" runat="server" OnSelectedIndexChanged="Ddlmonths_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Value="00">--Select--</asp:ListItem>
            <asp:ListItem Value="01">Jan</asp:ListItem>
            <asp:ListItem Value="02">Feb</asp:ListItem>
            <asp:ListItem Value="03">Mar</asp:ListItem>
            <asp:ListItem Value="04">Apr</asp:ListItem>
            <asp:ListItem Value="05">May</asp:ListItem>
            <asp:ListItem Value="06">Jun</asp:ListItem>
            <asp:ListItem Value="07">Jul</asp:ListItem>
            <asp:ListItem Value="08">Aug</asp:ListItem>
            <asp:ListItem Value="09">Sep</asp:ListItem>
            <asp:ListItem Value="10">Oct</asp:ListItem>
            <asp:ListItem Value="11">Nov</asp:ListItem>
            <asp:ListItem Value="12">Dec</asp:ListItem>
        </asp:DropDownList>
    </div>

    <div class="margin-right">
        <asp:Label runat="server" Text="Year:"></asp:Label>
        <asp:DropDownList ID="ddlyear" CssClass="btn btn-secondary dropdown-toggle" runat="server" OnSelectedIndexChanged="ddlyear_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem>--Select--</asp:ListItem>
            <asp:ListItem>2018</asp:ListItem>
            <asp:ListItem>2019</asp:ListItem>
            <asp:ListItem>2020</asp:ListItem>
        </asp:DropDownList>
    </div>
</div>
        
<asp:Label ID="TH_err" runat="server" Text="" CssClass="text-danger"></asp:Label>
<br />
<asp:Label runat="server" Text="Bank Balance:"></asp:Label>
<asp:Label ID="bankaccbalance" runat="server" Text=""></asp:Label>
<asp:GridView ID="THGridView" class="table table-responsive-sm table table-bordered mt-5" runat="server" AutoGenerateColumns="False" OnSelectedIndexChanged="THGridView_SelectedIndexChanged" OnRowCommand="THGridView_RowCommand" OnRowDataBound="THGridView_RowDataBound">
    <Columns>
        <asp:BoundField DataField="transactionID" HeaderText="Reference No." ReadOnly="True"/>
        <asp:BoundField DataField="date" HeaderText="Date" ReadOnly="True" />
        <asp:BoundField DataField="description" HeaderText="Description" ReadOnly="True" />
        <asp:BoundField DataField="type" HeaderText="Type" ReadOnly="True" />
        <asp:BoundField DataField="amount" HeaderText="Amount" ReadOnly="True" DataFormatString="{0:n}" />
        <asp:BoundField DataField="status" HeaderText="Status" ReadOnly="True" />
        <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton runat="server" OnClick="showModal">More Information</asp:LinkButton>
            </ItemTemplate>
            <ControlStyle CssClass="btn btn-info" />
        </asp:TemplateField>
        <%--<asp:ButtonField ButtonType="Button" runat="server" CommandName="moreinfo" Text="More Information" >
        <ControlStyle CssClass="btn btn-info" />
        </asp:ButtonField>--%>
        <asp:TemplateField HeaderText="Timer" ValidateRequestMode="Disabled">
        <ItemTemplate>
            <asp:Label ID="lblSecsLeft" runat="server" Text="" CssClass="cannotSee"></asp:Label>
            <asp:Label ID="lblCountDown" runat="server" Text=""></asp:Label>
            <script>
                // <%# Container.DataItemIndex %>
                window.setInterval(function () {
                    // get the value in lblSecsLeft
                    var SecLeft = document.getElementById("ContentPlaceHolder1_THGridView_lblSecsLeft_<%# Container.DataItemIndex %>").innerHTML * 1;
                    if (SecLeft > 0)
                        SecLeft--;
                    var minutes = Math.floor(SecLeft / 60);
                    var seconds = SecLeft - minutes * 60;
                    document.getElementById("ContentPlaceHolder1_THGridView_lblCountDown_<%# Container.DataItemIndex %>").innerHTML = minutes + " m "+seconds+" s ";
                    document.getElementById("ContentPlaceHolder1_THGridView_lblSecsLeft_<%# Container.DataItemIndex %>").innerHTML = SecLeft;
                    // reduce by 1, set to lblCountDown, set to lblCountDown and lblSecsLeft
                    }, 1000);
            </script>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:ButtonField ButtonType="Button" runat="server" CommandName="pstatus" Text="Status" >
            <ControlStyle CssClass="btn btn-warning" />
        </asp:ButtonField>    
        <asp:ButtonField ButtonType="Button" CommandName="tagain" Text="Transfer Again">
        <ControlStyle CssClass="btn btn-secondary" />
        </asp:ButtonField>
    </Columns>
</asp:GridView>


<div class="modal fade" id="TransactionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Transaction History Details</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <asp:Label class="col-6 col-form-label" runat="server">Reference Number:</asp:Label>
                    <asp:Label ID="LblReferencenum" class="col-6 col-form-label" runat="server"></asp:Label>     
                </div>
                <div class="form-group row">
                    <asp:Label class="col-6 col-form-label" runat="server">Date:</asp:Label>
                    <asp:Label ID="Lbldate" class="col-6 col-form-label" runat="server"></asp:Label>     
                </div>
                <div class="form-group row">
                    <asp:Label class="col-6 col-form-label" runat="server">Description:</asp:Label>
                    <asp:Label ID="Lbldescription" class="col-6 col-form-label" runat="server"></asp:Label>     
                </div>
                <div class="form-group row" >
                    <asp:Label class="col-6 col-form-label" runat="server">Type:</asp:Label>
                    <asp:Label ID="LblType" class="col-6 col-form-label" runat="server"></asp:Label>     
                </div>
                <div class="form-group row">
                    <asp:Label class="col-6 col-form-label" runat="server">Amount:</asp:Label>
                    <asp:Label ID="LblAmount" class="col-6 col-form-label" runat="server"></asp:Label>     
                </div>
                <div class="form-group row">
                    <asp:Label class="col-6 col-form-label" runat="server">Status:</asp:Label>
                    <asp:Label ID="LblStatus" class="col-6 col-form-label" runat="server"></asp:Label>     
                </div>
                <asp:Panel ID="PanelTransfer" runat="server" Visible="false">
                    <div class="form-group row">
                    <asp:Label class="col-6 col-form-label" runat="server">Send to:</asp:Label>
                    <asp:Label ID="Lblsendto" class="col-6 col-form-label" runat="server"></asp:Label>     
                </div>
                <div class="form-group row">
                    <asp:Label class="col-6 col-form-label" runat="server">Send Using:</asp:Label>
                    <asp:Label ID="Lblsendusing" class="col-6 col-form-label" runat="server"></asp:Label>     
                </div>
                </asp:Panel>
                <asp:Panel ID="PanelMerchantCode" runat="server" Visible="false">
                    <div class="form-group row">
                        <asp:Label class="col-6 col-form-label" runat="server">Merchant Code:</asp:Label>
                        <asp:Label ID="Lblmerchantcode" class="col-6 col-form-label" runat="server"></asp:Label>     
                    </div>
                </asp:Panel>
                <div class="form-group row">
                    <asp:Label class="col-6 col-form-label" runat="server">From Bank Accout Number:</asp:Label>
                    <asp:Label ID="Lblfrombankaccnum" class="col-6 col-form-label" runat="server"></asp:Label>     
                </div>
                <div class="form-group row">
                    <asp:Label class="col-6 col-form-label" runat="server">NRIC:</asp:Label>
                    <asp:Label ID="LbluserID" class="col-6 col-form-label" runat="server"></asp:Label>     
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript'>
    function openModal() {
        $('[id*=TransactionModal]').modal('show');
    } 
</script>

</div>
</asp:Content>
