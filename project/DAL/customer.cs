﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class customer
    {
        public customer()
        {

        }
        public string userID { get; set; }
        public string name { get; set; }
        public int phonenum { get; set; }
        public string cc { get; set; }
        public string customerGender { get; set; }
        public DateTime customerDOB { get; set; }
        public string customerPassword { get; set; }
        public string customerAddr { get; set; }
        public string customerEmail { get; set; }
        public string customerCS { get; set; }
        public string customerCC { get; set; }

    }

}