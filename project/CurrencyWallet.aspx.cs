﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }
            string userID = Session["custNRIC"].ToString();
            List<Currency> data = GetCurriences();
            Currency curr = new Currency();
            CurrencyDAO currDAO = new CurrencyDAO();

            curr = currDAO.getCurrencyWallet(userID);
            LblCurrencyAmtMYR.Text = Math.Round(Convert.ToDouble(curr.CurrencyAmtMYR), 2).ToString("F");
            LblCurrencyAmtUSD.Text = Math.Round(Convert.ToDouble(curr.CurrencyAmtUSD), 2).ToString("F");
            LblCurrencyAmtEUR.Text = Math.Round(Convert.ToDouble(curr.CurrencyAmtEUR), 2).ToString("F");
            LblCurrencyAmtGBP.Text = Math.Round(Convert.ToDouble(curr.CurrencyAmtGBP), 2).ToString("F");

            if (data != null)
            {
                GridViewCurrencies.DataSource = data;
                GridViewCurrencies.DataBind();
            }
        }
        public List<Currency> GetCurriences()
        {
            HttpClient client = new HttpClient();
            // state the URI address that hosted the web API
            client.BaseAddress = new Uri("http://localhost:62746/");
            List<Currency> data = new List<Currency>();
            Task<HttpResponseMessage> getTask = client.GetAsync("api/Currency");
            getTask.Wait();

            var result = getTask.Result;
            if (result.IsSuccessStatusCode)
            {
                var readTask = result.Content.ReadAsAsync<List<Currency>>();
                readTask.Wait();

                data = readTask.Result;
            }
            else
            {
                data = null;
            }
            return data;
        }

        protected void GridViewCarLoan_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridViewCurrencies_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "convertcurrencies")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = GridViewCurrencies.Rows[index];
                Session["CurrencyName"] = row.Cells[0].Text;
                Session["ConversionRate"] = row.Cells[1].Text;
                Session["Country"] = row.Cells[2].Text;
                Session["Symbol"] = row.Cells[3].Text;
                Response.Redirect("ConvertCurrencies.aspx");
            }
            
        }
    }
}