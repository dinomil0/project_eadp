﻿<%@ Page Title="" Language="C#" MasterPageFile="~/commerceMaster.Master" AutoEventWireup="true" CodeBehind="commerce_Payment.aspx.cs" Inherits="project.commerce_Payment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="form1">
    <section class="border border-success d-flex justify-content-center flex-column w-50 m-auto">
        <div class="d-flex justify-content-center mt-2">
            <asp:Label ID="Lbl_Payment" runat="server" Text="Your Total is SGD$50 [2000VC]"></asp:Label>
        </div>
        <div class="d-flex justify-content-center">
            <p class="">Pay securely with a Credit/Debit Card or Virtual Currency</p>
        </div>
        <div class="d-flex">
            <div class="col-6">
                <asp:LinkButton ID="commerce_PayCard" runat="server" OnCommand="commerce_PayCard_Click" CssClass="float-right"><i class="far fa-credit-card fa-7x text-success"></i></asp:LinkButton>
            </div>
            <div class="col-6">
                <asp:LinkButton ID="commerce_PayVC" runat="server" OnCommand="commerce_PayVC_Click" CssClass="float-left"><i class="fas fa-coins fa-7x text-success"></i></asp:LinkButton>
            </div>
        </div>
        <br />
        <asp:Panel ID="PanelCard" runat="server" Visible="False">
            <div class="d-flex justify-content-center">
                <asp:DropDownList ID="ddl_bankNumber" runat="server" class="w-50" OnSelectedIndexChanged="ddl_card_Number_SelectedIndexChanged" AutoPostBack="True">
                    <asp:ListItem Selected="True">------------------------Select------------------------</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="d-flex justify-content-center mt-5">
                <div class="justify-content-center">
                    <table style="width: 100%;">
                        <tr>
                            <td>Card Number: </td>
                            <td><asp:TextBox ID="TB_card_Num" runat="server" placeholder="Card Number"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CVV: </td>
                            <td><asp:TextBox ID="TB_card_CVV" runat="server" class="" placeholder="CVV"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Date of Expiry: </td>
                            <td><asp:TextBox ID="TB_card_ExpDate" runat="server" class="" placeholder="MM/YY"></asp:TextBox></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="d-flex justify-content-center mb-5">
                <div class="mt-5 d-flex justify-content-center">
                    <asp:Button ID="btn_paycard" runat="server" Text="Pay" class="btn btn-outline-success mr-1" OnClick="btn_paycard_Click"/>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="PanelVC" runat="server" Visible="False">
            <div>
                <table style="width: 48%; margin: auto">
                    <tr>
                        <td>Current Balance: </td>
                        <td>
                            <asp:Label ID="Lbl_currentbal" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>Price: </td>
                        <td>
                            <asp:Label ID="Lbl_price" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>End Balance: </td>
                        <td>
                            <asp:Label ID="Lbl_endbal" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
                <div class="float-right mb-3 mr-5">
                    <asp:Button ID="btn_payvc" runat="server" Text="Pay" class="btn btn-outline-success" OnClick="btn_payvc_Click"/>
                </div>
            </div>
        </asp:Panel>
        <asp:Label ID="Lbl_err" runat="server" Text="" Visible="false" CssClass="text-danger"></asp:Label>
    </section>
</div>
</asp:Content>