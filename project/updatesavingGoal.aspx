﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="updatesavingGoal.aspx.cs" Inherits="project.updatesavingGoal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
    .auto-style1 {
        width: 292px;
    }
    .auto-style2 {
        width: 361px;
    }
</style>

    <section id="UpdateSavingGoal" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
    <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="bankHome.aspx">Bank Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Update Saving Goal</li>
          </ol>
    </nav>
    <h2 class="text-center display-4">Update Saving Goal</h2>
    <table class="table table-hover">
        <tr>
            <td class="auto-style1">Savings ID:</td>
            <td class="auto-style2">
                <asp:Label ID="LblsavingsID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">User ID:</td>
            <td class="auto-style2">
                <asp:Label ID="LbluserID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Savings Goal:</td>
            <td class="auto-style2">
                <asp:Label ID="LblsavingGoal" runat="server"></asp:Label>
            </td>
            
        </tr>
        <tr>
            <td class="auto-style1">New Savings Goal:</td>
            <td>
                <asp:TextBox ID="newsavingGoal" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Savings Saved:</td>
            <td class="auto-style2">
                <asp:Label ID="LblsavingSaved" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Start Date:</td>
            <td class="auto-style2">
                <asp:Label ID="LblstartDate" runat="server"></asp:Label>  
            </td>
        </tr>
        <tr>
            <td class="auto-style1">End Date:</td>
            <td class="auto-style2">
                <asp:Label ID="LblendDate" Text="" runat="server" DataFormatString="{0:dd/MM/yyyy}"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">New End Date:</td>
                <td class="auto-style2">
                    <div class='input-group date' id='datetimepicker2'>
                        <asp:TextBox ID="NewEndDate" runat="server" type="date" class="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </td>
        </tr>
        
        <tr>
            <td class="auto-style1">Description</td>
            <td class="auto-style2">
                <asp:Label ID="Lbldescription" Text="" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">
                <asp:Button ID="BtnUpdate" runat="server" Text="Update" OnClick="BtnUpdate_Click" class="btn btn-success"/>&nbsp;&nbsp;
                <asp:Button ID="BtnBack" runat="server" OnClick="BtnBack_Click" Text="Back" class="btn btn-secondary"/>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">
                <asp:Label ID="LblResult" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</section>

</asp:Content>
