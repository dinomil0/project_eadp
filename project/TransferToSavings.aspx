﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="TransferToSavings.aspx.cs" Inherits="project.TransferToSavings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<section id="TransferSavings" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
    <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="bankHome.aspx">Bank Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Transfer to Savings</li>
          </ol>
    </nav>
    <div class="row">
        <div class="col-4 mx-auto my-auto ">
            <div class="d-flex text-center">
              <div>
                  <h4> Transfer from: </h4>
              </div>
              <div>
                  <asp:DropDownList ID="DdlTransferfrom" CssClass="btn btn-secondary dropdown-toggle" runat="server" OnSelectedIndexChanged="DdlTransferfrom_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
              </div>
          </div>
            <asp:Label runat="server" Text="Balance:"></asp:Label>
            <asp:Label ID="LblTransferBalance" runat="server" Text=""></asp:Label>
        </div>
          <div class="col-4 text-center mx-auto my-auto ">
            <div class="input-group mb-3 ml-4">
                <div class="input-group-prepend">
                    <span class="input-group-text">$</span>
                    <asp:TextBox ID="TxtAmountinput" type="number" runat="server" step=".01" ></asp:TextBox>
                </div>
            </div>
              <asp:Button ID="BtnTransferSavings" runat="server" Text="Transfer" class="btn btn-success" OnClick="BtnTransferSavings_Click"/>
          </div>
          <div class="col-4">
              <table class="table table-borderless table-hover">
                    <tr class="row text-center">
                        <td><h3>Savings Goal</h3></td>
                    </tr>
                    <tr>
                        <td class="">Savings ID:</td>
                        <td class="">
                            <asp:Label ID="LblTransferSavingsID" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="">Savings Goal:</td>
                        <td class="">
                            <asp:Label ID="LblTransferSavingsGoal" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="">Amount Saved:</td>
                        <td class="">
                            <asp:Label ID="LblTransferAmountSaved" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                   <asp:Label ID="Lbl_err" runat="server" Text=""></asp:Label>
              </table>
          </div>
    </div>
    <asp:Label ID="Lbl_error" runat="server" Text=""></asp:Label>
</section>

</asp:Content>
