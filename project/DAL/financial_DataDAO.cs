﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace project.DAL
{
    public class financial_DataDAO
    {
        string DBConnect = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;

        //Inserting Financial Data
        public void insert_Financial_Data(string userID, string data_name, string data_type, string manual_Input, decimal amount)
        {
            try
            {
                StringBuilder sqlString = new StringBuilder();
                SqlCommand sqlCommand = new SqlCommand();
                sqlString.AppendLine("INSERT INTO FINANCIAL_DATA");
                sqlString.AppendLine("VALUES (@PARAUSERID, @PARADATA_NAME, @PARADATA_TYPE, @PARAMANUAL_INPUT, @PARAAMOUNT)");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                sqlCommand = new SqlCommand(sqlString.ToString(), sqlConnection);
                sqlCommand.Parameters.AddWithValue("@PARAUSERID", userID);
                sqlCommand.Parameters.AddWithValue("@PARADATA_NAME", data_name);
                sqlCommand.Parameters.AddWithValue("@PARADATA_TYPE", data_type);
                sqlCommand.Parameters.AddWithValue("@PARAMANUAL_INPUT", manual_Input);
                sqlCommand.Parameters.AddWithValue("@PARAAMOUNT", amount);
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch
            {
                //Exception
            }
        }

        //Reading Financial Data
        public List<financial_Data> read_Financial_Data(string userID)
        {
            List<financial_Data> financial_Data_List = new List<financial_Data>();
            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                SqlDataAdapter da;
                StringBuilder sqlString = new StringBuilder();
                sqlString.AppendLine("SELECT * FROM FINANCIAL_DATA");
                sqlString.AppendLine("WHERE USERID = @PARAUSERID");
                sqlString.AppendLine("ORDER BY MANUAL_INPUT, TYPE");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                da = new SqlDataAdapter(sqlString.ToString(), sqlConnection);
                da.SelectCommand.Parameters.AddWithValue("@PARAUSERID", userID);
                da.Fill(ds, "financial_DataTable");
                int rec_cnt = ds.Tables["financial_DataTable"].Rows.Count;
                if (rec_cnt == 0)
                {
                    financial_Data_List = null;
                }
                else
                {
                    foreach (DataRow row in ds.Tables["financial_DataTable"].Rows)
                    {
                        financial_Data each_Financial_Data = new financial_Data();
                        each_Financial_Data.name = Convert.ToString(row["name"]);
                        each_Financial_Data.type = Convert.ToString(row["type"]);
                        each_Financial_Data.manual_Input = Convert.ToString(row["manual_Input"]);
                        each_Financial_Data.amount = Convert.ToDecimal(row["amount"]);
                        financial_Data_List.Add(each_Financial_Data);
                    }
                }
            }
            catch
            {
                //Exception
            }
            return financial_Data_List;
        }

        //Updating Financial Data
        public void update_Financial_Data(string userID, string name, decimal amount)
        {
            try
            {
                StringBuilder sqlString = new StringBuilder();
                SqlCommand sqlCommand = new SqlCommand();
                sqlString.AppendLine("UPDATE FINANCIAL_DATA");
                sqlString.AppendLine("SET AMOUNT = @PARAAMOUNT");
                sqlString.AppendLine("WHERE USERID = @PARAUSERID AND NAME = @PARANAME");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                sqlCommand = new SqlCommand(sqlString.ToString(), sqlConnection);
                sqlCommand.Parameters.AddWithValue("@PARAAMOUNT", amount);
                sqlCommand.Parameters.AddWithValue("@PARAUSERID", userID);
                sqlCommand.Parameters.AddWithValue("@PARANAME", name);
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch
            {
                //Exception
            }

        }

        //Deleting Financial Data
        public void delete_Financial_Data(string userID, string name)
        {
            try
            {
                StringBuilder sqlString = new StringBuilder();
                SqlCommand sqlCommand = new SqlCommand();
                sqlString.AppendLine("DELETE FROM FINANCIAL_DATA");
                sqlString.AppendLine("WHERE USERID = @PARAUSERID AND NAME = @PARANAME");
                SqlConnection sqlConnection = new SqlConnection(DBConnect);
                sqlCommand = new SqlCommand(sqlString.ToString(), sqlConnection);
                sqlCommand.Parameters.AddWithValue("@PARAUSERID", userID);
                sqlCommand.Parameters.AddWithValue("@PARANAME", name);
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch
            {
                //Exception
            }
        }
    }
}