﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class Class_Account
    {
        public string userID { get; set; }

        public class Account : Class_Account
        {
            public double bal { get; set; }
            public double intrate { get; set; }
            public long bankaccnum { get; set; }
            public string bankaccname { get; set; }
            public long cardaccnum { get; set; }
            public int cvv { get; set; }
            public DateTime DOE { get; set; }
        }
        public class VCAccount : Class_Account
        {
            public long VCbal { get; set; }
        }
    }
}