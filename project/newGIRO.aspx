﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bankMaster.Master" AutoEventWireup="true" CodeBehind="newGIRO.aspx.cs" Inherits="project.WebForm9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<section id="newSavingGoal" class="border border-secondary bg-light shadow p-3 mb-5 rounded container">
    <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="GIRO.aspx">GIRO services</a></li>
            <li class="breadcrumb-item active" aria-current="page">New GIRO</li>
          </ol>
    </nav>
    <h2 class="text-center display-4">New GIRO</h2>
    <div class="container">
    <div class="form-group">
        <asp:Label runat="server" Text="From:"></asp:Label>
        <asp:DropDownList ID="Ddlbankaccs" CssClass="btn btn-secondary dropdown-toggle" AutoPostBack="True" runat="server"></asp:DropDownList>
    </div>
    <div class="form-group">
        <asp:Label runat="server" Text="To:"></asp:Label>
        <asp:DropDownList ID="DdlBillingOrganisation" CssClass="btn btn-secondary dropdown-toggle" runat="server">
            <asp:ListItem Value="0">--Select--</asp:ListItem>
            <asp:ListItem>M1 Limited</asp:ListItem>
            <asp:ListItem>MyRepublic Pte Ltd	</asp:ListItem>
            <asp:ListItem>Singapore Telecommunications Ltd</asp:ListItem>
            <asp:ListItem>StarHub Ltd	</asp:ListItem>
            <asp:ListItem>SP Services</asp:ListItem>
            <asp:ListItem>DBS Loan</asp:ListItem>
            <asp:ListItem>OCBC Loan</asp:ListItem>
            <asp:ListItem>UOB Loan</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="form-group">
        <asp:Label runat="server" Text="Amount:"></asp:Label>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text">$</span>
          </div>
            <asp:TextBox ID="TbGIROAmt" runat="server" class="form-control" placeholder="What is the GIRO amount?"></asp:TextBox>
        </div>
    </div>
    <asp:Label runat="server" Text="GIRO Date:"></asp:Label>
    <div class='input-group date' id='datetimepicker'>
        <asp:TextBox ID="NewGIRODate" runat="server" type="date" class="form-control"></asp:TextBox>
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
    <div class="modal-footer">
        <asp:Button ID="closebuttonmodalSavings" runat="server" Text="Back" type="reset" data-dismiss="modal" class="btn btn-info" OnClick="closebuttonGIRO_Click"/> &nbsp;&nbsp;
        <!-- Submit Button -->
        <asp:Button ID="submitbuttonmodalSavings" runat="server" Text="Submit" type="submit" data-dismiss="modal" class="btn btn-success" OnClick="submitbuttonGIRO_Click"/>
    </div>
    <asp:Label ID="lbl_Savings" runat="server" Text=""></asp:Label>
</div>
</section>

</asp:Content>
