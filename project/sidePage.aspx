﻿<%@ Page Title="" Language="C#" MasterPageFile="~/landingMaster.Master" AutoEventWireup="true" CodeBehind="sidePage.aspx.cs" Inherits="project.sidePage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="d-flex">
        <div class="">
            <a href="commerceLanding.aspx">
            <h3 class="text-center">E-commerce</h3>
            <img src="img/commerceimg.jpg"  class="img-fluid commerce" alt="Responsive image" />
            </a>
        </div>
        <div class="">
           <a href="OTPBank.aspx">
           <h3 class="text-center">Banks</h3>
           <img src="img/bankimg.jpg" class="img-fluid commerce" alt="Responsive image">
        </div>
    </section>
</asp:Content>
