﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.DAL
{
    public class Currency
    {
        public string CurrencyName { get; set; }
        public double ConversionRate { get; set; }
        public string Country { get; set; }
        public string Symbol { get; set; }
        public double CurrencyAmtMYR { get; set; }
        public double CurrencyAmtUSD { get; set; }
        public double CurrencyAmtEUR { get; set; }
        public double CurrencyAmtGBP { get; set; }
    }
}