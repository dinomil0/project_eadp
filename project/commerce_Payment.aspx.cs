﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class commerce_Payment : System.Web.UI.Page
    {
        string userID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            userID = Session["custNRIC"].ToString();
            Class_Account.VCAccount vcacc = new Class_Account.VCAccount();
            Class_AccountDAO accDAO = new Class_AccountDAO();
            vcacc = accDAO.getVCAccount(userID);
            //Get area code from session
            //Get currency details from area code
            //List[0] = Symbol, List[1] = Rate
            //Lbl_Payment.Text = "Your total is " + Symbol + Request.QueryString["Total"] + "/" + Convert.ToString(Math.Round(Convert.ToDouble(Request.QueryString["Total"])/Rate * 4, 0)) + " VC";
            Lbl_Payment.Text = "Your total is $" + Request.QueryString["Total"] + "/" + Convert.ToString(Convert.ToDouble(Request.QueryString["Total"]) * 4) + " VC";
            Lbl_currentbal.Text = Convert.ToString(vcacc.VCbal) + " VC";
            //Lbl_price.Text = Convert.ToString(Math.Round(Convert.ToDouble(Request.QueryString["Total"])/Rate * 4, 0)) + " VC";
            Lbl_price.Text = Convert.ToString(Convert.ToDouble(Request.QueryString["Total"]) * 4) + " VC";
            Lbl_endbal.Text = Convert.ToString(vcacc.VCbal - Convert.ToDouble(Request.QueryString["Total"]) * 4) + " VC";
            List<Class_Account.Account> accList = accDAO.getAccount(userID);
            foreach (Class_Account.Account acc in accList)
            {
                if (ddl_bankNumber.Items.FindByValue(Convert.ToString(acc.bankaccnum)) == null)
                {
                    ddl_bankNumber.Items.Add(new ListItem(Convert.ToString(acc.bankaccname) + " " + Convert.ToString(acc.bankaccnum), Convert.ToString(acc.bankaccnum)));
                }
            }
        }
        protected void commerce_PayCard_Click(object sender, CommandEventArgs e)
        {
            PanelCard.Visible = true;
            btn_paycard.Visible = true;
            PanelVC.Visible = false;
        }
        protected void commerce_PayVC_Click(object sender, CommandEventArgs e)
        {
            PanelVC.Visible = true;
            btn_payvc.Visible = true;
            PanelCard.Visible = false;
        }

        protected void btn_paycard_Click(object sender, EventArgs e)
        {
            Class_ListDAO listDAO = new Class_ListDAO();
            if (!listDAO.test_checkout(userID).Any())
            {
                listDAO.Checkout(userID, "Card", Convert.ToInt32(ddl_bankNumber.SelectedValue));
                Response.Redirect("commerce_Transactions.aspx");
            }
            else
            {
                Lbl_err.Text = "Some Products can't be checked out: ";
                foreach (string ProdId in listDAO.test_checkout(userID))
                {
                    Lbl_err.Text += ProdId + ", ";
                }
                Lbl_err.Visible = true;
            }
        }
        protected void btn_payvc_Click(object sender, EventArgs e)
        {
            Class_ListDAO listDAO = new Class_ListDAO();
            if (!listDAO.test_checkout(userID).Any())
            {
                listDAO.Checkout(userID, "VC", 123312);
                Response.Redirect("commerce_Transactions.aspx");
            }
            else
            {
                Lbl_err.Text = "Some Products can't be checked out: ";
                foreach (string ProdId in listDAO.test_checkout(userID))
                {
                    Lbl_err.Text += ProdId + ", ";
                }
                Lbl_err.Visible = true;
            }
        }

        protected void ddl_card_Number_SelectedIndexChanged(object sender, EventArgs e)
        {
            string accNum = ddl_bankNumber.SelectedValue;
            Class_Account.Account acc = new Class_Account.Account();
            Class_AccountDAO accDAO = new Class_AccountDAO();
            acc = accDAO.get_byAccNum(Convert.ToInt32(accNum));
            TB_card_Num.Text = Convert.ToString(acc.cardaccnum);
            TB_card_CVV.Text = Convert.ToString(acc.cvv);
            TB_card_ExpDate.Text = acc.DOE.ToString("MM/dd");
            TB_card_Num.ReadOnly = true;
            TB_card_CVV.ReadOnly = true;
            TB_card_ExpDate.ReadOnly = true;
        }
    }
}