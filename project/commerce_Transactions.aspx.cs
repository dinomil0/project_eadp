﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project.DAL;

namespace project
{
    public partial class commerce_Transactions : System.Web.UI.Page
    {
        string userID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            userID = Session["custNRIC"].ToString();
            Lbl_userID.Text = "User: " + userID;
            Class_TransactionDAO transDAO = new Class_TransactionDAO();
            List<Class_Transaction> transList = new List<Class_Transaction>();
            transList = transDAO.get_transactions(userID);
            GridView1.DataSource = transList;
            GridView1.DataBind();
        }
    }
}