﻿using project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project
{
    public partial class newsavingGoal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Session["custNRIC"].ToString())) { Response.Redirect("landing.aspx"); }
            string userID = Session["custNRIC"].ToString();

            NewSavingDate.Attributes.Add("min", (DateTime.Now.ToString("yyyy-MM-dd")));
        }

        protected void submitbuttonmodalSavings_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (validate_Input())
            //    {
            //        double savingGoal = Convert.ToDouble(NewSavingGoalamt.Text.ToString());
            //        double amountSaved = Convert.ToDouble(NewSavingamtnone.Text.ToString());
            //        String description = NewSavingdescription.Text.ToString();
            //        //DateTime endDate = Convert.ToDateTime(NewSavingGoalamt.Text);
            //        DateTime endDate = Convert.ToDateTime(NewSavingDate.Text.ToString());
            //        DateTime startDate = DateTime.Now.Date;
            //        string userID = Session["custNRIC"].ToString();
            //        try
            //        {
            //            SavingsDAO newdata = new SavingsDAO();
            //            //int insCnt = newdata.InsertTD(savingGoal, amountSaved, description, endDate, startDate, userID,0,);
            //            if (insCnt == 1)
            //            {
            //                lbl_Savings.Text = "Savings Created";
            //            }
            //            else
            //            {
            //                lbl_Savings.Text = "Savings failed";
            //            }
            //            submitbuttonmodalSavings.Enabled = false;
            //        }
            //        catch (Exception f)
            //        {
            //            lbl_Savings.Text = f.ToString();
            //        }
            //    }
            //    else
            //    {
            //        lbl_Savings.Visible = true;
            //    }
            //}
            //catch
            //{


            //}           
            
        }

        protected void closebuttonmodalSavings_Click(object sender, EventArgs e)
        {
            Response.Redirect("bankHome.aspx");
        }
        protected Boolean validate_Input()
        {
            double amount;
            DateTime date;
            try
            {
                lbl_Savings.Text = null;
                if (String.IsNullOrEmpty(NewSavingdescription.Text))
                {
                    lbl_Savings.Text += "Please enter the description" + "<br/>";
                }
                if (!DateTime.TryParse(NewSavingDate.Text, out date))
                {
                    lbl_Savings.Text += "Please enter the correct date" + "<br/>";
                }
                if (!double.TryParse(NewSavingGoalamt.Text, out amount))
                {
                    lbl_Savings.Text += "Please enter a valid amount!" + "<br/>";
                }
                if (String.IsNullOrEmpty(lbl_Savings.Text))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}