﻿<%@ Page Title="" Language="C#" MasterPageFile="~/commerceMaster.Master" AutoEventWireup="true" CodeBehind="commerceLanding.aspx.cs" Inherits="project.commerceLanding" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .ProdImg {
            width:175px;
            height:175px;
            overflow:hidden;
        }
    </style>
    
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active" style="text-align:center;">
              <img src="img/Sale.jpg" style="width: 70vw; height: 50vh;" alt="Responsive image" />
            </div>
            <div class="carousel-item" style="text-align:center;"> 
              <img src="img/Shopping.jpg" style="width: 70vw; height: 50vh" alt="Responsive image" />
            </div>
            <div class="carousel-item" style="text-align:center;">
              <img src="img/SALES.jpg" style="width: 70vw; height: 50vh" alt="Responsive image" />
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        <hr />
        <section class ="d-flex justify-content-center">
            <div class="">
                <asp:DataList runat="server" ID="ProdList" RepeatColumns="5" RepeatDirection="Horizontal" CellPadding="20" >
                    <ItemTemplate>
                        <div class ="justify-content-center mt-5 border border-success rounded container">
                            <div class="m-3">
                                <asp:Image ID="ProdImage" runat="server" ImageUrl='<%# Eval("ProdImage") %>' cssclass="img-thumbnail ProdImg"/>  
                                <table style="width: 100%;">
                                    <tr>
                                        <td>Name:</td>
                                        <td>
                                            <asp:Label ID="Lbl_ProdName" runat="server" Text=""><%# Eval("ProdName") %></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tag:</td>
                                        <td>
                                            <asp:Label ID="Lbl_ProdId" runat="server" Text=""><%# Eval("ProdId") %></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Price:</td>
                                        <td>
                                            <asp:Label ID="Lbl_ProdPrice" runat="server" Text="">$<%# Eval("ProdPrice") %></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                <div class="d-flex justify-content-center">
                                    <asp:LinkButton runat="server" class = "btn btn-outline-success mt-1 justify-content-center" OnCommand="Buy_Click" CommandArgument = '<%# Eval("ProdName") + ";" + Eval("ProdId") + ";" + Eval("ProdPrice") %>'>Buy</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
            </div>
        </section>
    
</asp:Content>
