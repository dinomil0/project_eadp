﻿<%@ Page Title="" Language="C#" MasterPageFile="~/commerceMaster.Master" AutoEventWireup="true" CodeBehind="commerce_Wishlist.aspx.cs" Inherits="project.commerce_Wishlist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<form id="form1" runat="server">
    <section class="d-flex">
        <div class="col-3">
            <asp:Label ID="Lbl_userID" runat="server" Text="User Name" class="h3"></asp:Label>
            <h3>Wishlist</h3>
        </div>
        <div class="col-6 border border-success mt-5">
            <div class="d-flex justify-content-center">
                <div class="mr-3 mt-2 mb-2">
                    <asp:Image ID="Image2" runat="server" Height="166px" Width="166px"/>
                </div>
                <div class="mr-5 mt-1">
                    <asp:Label ID="Lbl_productname" runat="server" Text="Name"></asp:Label><br />
                    <asp:Label ID="Lbl_productdescription" runat="server" Text="Desc"></asp:Label><br />
                    <asp:Label ID="Lbl_productprice" runat="server" Text="Price"></asp:Label>
                </div>
                <div class="ml-5 mt-5">
                    <asp:Button ID="btn_addtowishlist" runat="server" Text="Add to Cart" class="btn btn-success mt-5 mr-1"/>
                    <asp:Button ID="btn_remove" runat="server" Text="Remove" class="btn btn-danger mt-5"/>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <div class="mr-3 mt-2 mb-2">
                    <asp:Image ID="Image1" runat="server" Height="166px" Width="166px"/>
                </div>
                <div class="mr-5 mt-1">
                    <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label><br />
                    <asp:Label ID="Label2" runat="server" Text="Desc"></asp:Label><br />
                    <asp:Label ID="Label3" runat="server" Text="Price"></asp:Label>
                </div>
                <div class="ml-5 mt-5">
                    <asp:Button ID="Button1" runat="server" Text="Add to Cart" class="btn btn-success mt-5 mr-1"/>
                    <asp:Button ID="Button2" runat="server" Text="Remove" class="btn btn-danger mt-5"/>
                </div>
            </div>
        </div>
        <div class="ml-5 mt-5 col-2">
            <h5>Order Summary</h5>
            <table style="width: 100%;">
                <tr>
                    <td>Name</td>
                    <td>Price</td>
                </tr>
                <tr class="border-top border-dark">
                    <td>Total</td>
                    <td>Total Price</td>
                </tr>
            </table>
            <div class="d-flex justify-content-center mt-2">
                <asp:Button ID="btn_addtowishlist1" runat="server" Text="Add all to Cart" class="btn btn-success"/>
            </div>
        </div>
    </section>
    <br />

</asp:Content>
